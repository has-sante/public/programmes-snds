# Programmes SNDS

**Documentation**: [https://has-sante.pages.has-sante.fr/public/programmes-snds](https://has-sante.pages.has-sante.fr/public/programmes-snds)

**Code source**: [https://gitlab.has-sante.fr/has-sante/public/programmes-snds](https://gitlab.has-sante.fr/has-sante/public/programmes-snds)

## Description

Ce depôt de code contient les programmes développés au sein de la HAS pour requêter le SNDS.
