# Programmes SNDS

Le dépôt [Programmes SNDS](https://gitlab.has-sante.fr/has-sante/public/programmes-snds) contient les programmes développés au sein de la HAS à partir des données du SNDS.


## Liste des projets réalisés sur les données SNDS

📝 **Note :** En cliquant sur les projets ci-dessous, vous accédez directement à la documentation technique du projet et aux programmes ayant permis sa réalisation.

### 2024
- Les programmes et spécifications de l’étude sur l’exploitation des données du SNDS pour contextualiser l’utilisation des médicaments en accès précoces se 
trouvent à un [emplacement dédié](https://has-sante.pages.has-sante.fr/public/acces_precoces).

### 2023
- [](epilepsie/index.md)
- [](irm-mi/index.md)
- [](acromioplastie/index.md)

### 2022
- Les programmes pour le calcul des indicateurs BPCO se trouvent dans un [dépôt dédié](https://gitlab.has-sante.fr/has-sante/public/snds-indicateurs-parcours-bpco).
- Les programmes et spécifications des indicateurs de résultats en chirurgie orthopédique à partir du PMSI se trouvent dans un [dépôt dédié](https://gitlab.has-sante.fr/has-sante/public/indicateurs-resultats-chirurgie-orthopedique).


```{toctree}
:hidden:

Epilepsie <epilepsie/index.md>
IRM MI <irm-mi/index.md>
Acromioplastie <acromioplastie/index.md>
License <license>
```
