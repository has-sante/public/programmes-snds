/* ************************************************************************************** */
/*                      ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                          */
/* ************************************************************************************** */

/***************************************************************************************/
/* Nom du programme : Extraction_ACE

/* Objectif : extraire les donnees du PMSI ACE 
concernant les etablissements publics pour la population d'etude  */

/* Tables de sortie : epi_ext.T_MCO_FBSTC, epi_ext.T_MCO_FCSTC, epi_ext.T_MCO_FHSTC, 
epi_ext.T_MCO_FLSTC, epi_ext.T_MCO_FMSTC, epi_ext.T_MCO_FPSTC
/***************************************************************************************/


%macro extrac_pmsi_ext_pub(type /*MCO HAD SSR PSY*/,table /*Table interrogee, FM,FP .. */,nom_table,pop,deb,fin); 
%do aa=&deb %to &fin;
%let an=%substr(&aa.,3,2);
%let dted=input(ent_dat1,ddmmyy8.) ;

PROC SQL  ;
%connectora;
   		CREATE TABLE  &nom_table._&an. (compress=yes) AS 
   		SELECT   *, &dted. as ent_dat format=date9.
		from connection to oracle
		(
			select  distinct
			c.nir_ano_17,c.eta_num,c.ent_dat as ent_dat1,c.sej_num,c.seq_num,
			&table..*

			from T_MCO&an.CSTC c 
   			inner join	 T_&type&an.&table &table on  c.seq_NUM = &table..seq_NUM AND c.ETA_NUM = &table..ETA_NUM 
			&pop. 

			where nir_ret='0' and nai_ret='0' and ias_ret='0' and sex_ret='0'  and ENT_DAT_RET='0' 	 
				
		) ;				
		QUIT;
%end;
%mend  extrac_pmsi_ext_pub;



/*%extrac_pmsi_ext_pub(mco,fastc,&lib_ext..T_MCO_FASTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);*/
%extrac_pmsi_ext_pub(mco,fbstc,&lib_ext..T_MCO_FBSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);
%extrac_pmsi_ext_pub(mco,fcstc,&lib_ext..T_MCO_FCSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);
%extrac_pmsi_ext_pub(mco,fhstc,&lib_ext..T_MCO_FHSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);
%extrac_pmsi_ext_pub(mco,flstc,&lib_ext..T_MCO_FLSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);
%extrac_pmsi_ext_pub(mco,fmstc,&lib_ext..T_MCO_FMSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);
%extrac_pmsi_ext_pub(mco,fpstc,&lib_ext..T_MCO_FPSTC,INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,2017,2019);


proc sql;


select count(*) from oravue.T_MCO18FHSTC;
select count(*) from oravue.T_MCO19FHSTC;

quit;

proc sql;

update &lib_ext..T_MCO_FMSTC_17 set ccam_cod=strip(ccam_cod);
update &lib_ext..T_MCO_FMSTC_18 set ccam_cod=strip(ccam_cod);
update &lib_ext..T_MCO_FMSTC_19 set ccam_cod=strip(ccam_cod);

quit;

/*************************************************/
/* Tables finales */
/*************************************************/


/*%tb_mco_finale(table=fastc,deb=2017,fin=2019);*/
%tb_mco_finale(table=fbstc,deb=2017,fin=2019);
%tb_mco_finale(table=fcstc,deb=2017,fin=2019);
%tb_mco_finale(table=fhstc,deb=2017,fin=2019);
%tb_mco_finale(table=flstc,deb=2017,fin=2019);
%tb_mco_finale(table=fmstc,deb=2017,fin=2019);
%tb_mco_finale(table=fpstc,deb=2017,fin=2019);


/***************************************************************************************/
/* Objectif : automatiser le processus d'extraction des donnees du PMSI ACE 
concernant les etablissements prives pour la population d'etude  */

/* Tables de sortie : epi_ext.T_MCO_FB, epi_ext.T_MCO_FC, epi_ext.T_MCO_FH, 
epi_ext.T_MCO_FL, epi_ext.T_MCO_FM, epi_ext.T_MCO_FP
/***************************************************************************************/


/***************************************************************************************/
/** Action : Cree le fichier des honoraires ETB PRIVES   **/
/** Pour une population donnee        **/

options nomprint;
%let cond=/*	1=2 and 	equivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FB,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FB,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2018,/* Annee de debut 2006 */	 
fin			=	2018,/* Annee de fin 2014*/ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fb.ACT_COD
					§ fb.ACT_COE
					§ fb.ACT_NBR
,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fb.ACT_COD
					§ fb.ACT_COE
					§ fb.ACT_NBR
					having count(*)>0
,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ ACT_COD
					§ ACT_COE
					§ ACT_NBR

);

/******************************************************************/
/** Action : Cree le fichier des honoraires ETB PRIVES   **/
/** Pour une population donnee        **/

options nomprint;
%let cond=/*	1=2 and 	équivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FC,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FC,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut  */	 
fin			=	2019,/* Annee de fin */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fc.ACT_COD
					§ fc.ACT_COE
					§ fc.ACT_NBR
,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fc.ACT_COD
					§ fc.ACT_COE
					§ fc.ACT_NBR
					having count(*)>0
,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ ACT_COD
					§ ACT_COE
					§ ACT_NBR

);


/******************************************************************/
/** Action : Cree le fichier des medicaments en SUS GHS  **/
/** Pour une population donnee        **/


options nomprint;
%let cond=/*	1=2 and 	equivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FH,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FH,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut */	 
fin			=	2019,/* Annee de fin   */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fh.qua_COD,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fh.qua_COD
					having count(*)>0,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &code. as ucd_cod
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtdlv. as dte_dlv format=date9.
					§ &dtef. as sor_dat format=date9.
					§ qua_COD*&taux./100 as nb_dlv );



/******************************************************************/
/** Action : Cree le fichier des actes bio ETB PRIVES   **/
/** Pour une population donnee        **/

options nomprint;
%let cond=/*	1=2 and 	equivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FL,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FL,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut  */	 
fin			=	2019,/* Annee de fin */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fl.nabm_COD
					§ fl.act_nbr
					§ fl.del_dat_ent
,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fl.nabm_COD
					§ fl.act_nbr
					§ fl.del_dat_ent
					having count(*)>0
,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ nabm_COD
					§ act_nbr
					§ del_dat_ent
);




/******************************************************************/
/** Action : Cree le fichier des actes ccam ETB PRIVES   **/
/** Pour une population donnee        **/

options nomprint;
%let cond=/*	1=2 and 	equivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FM,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FM,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut  */	 
fin			=	2019,/* Annee de fin */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fm.CCAM_COD
					§ fm.ACV_ACT
,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fm.CCAM_COD
					§ fm.ACV_ACT
					having count(*)>0
,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ CCAM_COD
					§ ACV_ACT
);


proc sql;

update &lib_ext..T_MCO_FM_17 set ccam_cod=strip(ccam_cod);
update &lib_ext..T_MCO_FM_18 set ccam_cod=strip(ccam_cod);
update &lib_ext..T_MCO_FM_19 set ccam_cod=strip(ccam_cod);

quit;


/******************************************************************/
/** Action : Cree le fichier des DMI ETB PRIVES   **/
/** Pour une population donnee        **/

options nomprint;
%let cond=/*	1=2 and 	equivalent de inobs=0 en oracle*/;
%extrac_pmsi (
table		=	FP,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_FP,		
option		=  /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut */	 
fin			=	2019,/* Annee de fin */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fp.lpp_COD
					§ fp.lpp_qua
					§ fp.tip_prs_ide
,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ fp.lpp_COD
					§ fp.lpp_qua
					§ fp.tip_prs_ide
					having count(*)>0
,
varliste2=		 	nir_ano_17 
					§ rsa_num
					§ eta_num
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ lpp_COD
					§ lpp_qua
					§ tip_prs_ide
);




/*************************************************/
/* Tables finales */
/*************************************************/
%tb_mco_finale(table=fb,deb=2017,fin=2019);
%tb_mco_finale(table=fc,deb=2017,fin=2019);
%tb_mco_finale(table=fh,deb=2017,fin=2019);
%tb_mco_finale(table=fl,deb=2017,fin=2019);
%tb_mco_finale(table=fm,deb=2017,fin=2019);
%tb_mco_finale(table=fp,deb=2017,fin=2019);