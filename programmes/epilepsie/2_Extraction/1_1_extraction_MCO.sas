/* ************************************************************************************** */
/*                      ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                          */
/* ************************************************************************************** */


/* ************************************************************************************** */
/* Nom du programme : Extraction_MCO 

/* Objectif : 
*extraire les donnees du PMSI MCO concernant la population d'etude 
*generer les tables finales concernant les donnees du PMSI pour la population d'etude


/* Tables de sortie : epi_ext.T_MCO_A, epi_ext.T_MCO_B, epi_ext.T_MCO_C, 
epi_ext.T_MCO_D
/* ************************************************************************************** */


/******************************************************************/
/** Table A                                                      **/
/******************************************************************/


%let cond= /* 1=2 and equivalent de inobs=0 en oracle*/;

%extrac_pmsi (
table		=	a,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_A,		
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=	/* innner join */,/* 	 Jointure fichier de cod*/
deb			=	2017, /* Annee de debut */	 
fin			=	2019, /* Annee de fin */ 
condition	=	and b.rss_num=a.rss_num /*and  */, 	
varliste=		 	c.nir_ano_17  /* Liste des variables a garder */
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ a.ent_dat_del
					§ a.cdc_act
					/*§ cod.act_lib_crt*/
					§ a.PHA_ACT
					§ a.ACV_ACT
					§ a.DOC_ACT
					§ sum(a.nbr_exe_act) as nb_actes,
varliste1=			c.nir_ano_17  /* Liste des variables du group by */
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ a.ent_dat_del
					§ a.cdc_act
				/*	§ cod.act_lib_crt*/
					§ a.PHA_ACT
					§ a.ACV_ACT
					§ a.DOC_ACT
					having sum(nbr_exe_act)>0,
varliste2=		 	nir_ano_17 /* Liste des variables a garder */
					§ ETA_NUM
					§ RSA_NUM
					§ RSS_NUM
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtact. as dte_acte format=date9. 
					§ &dtef. as sor_dat format=date9.
					§ cdc_act
					/*§ act_lib_crt*/
					§ PHA_ACT
					§ ACV_ACT
					§ DOC_ACT
					§ nb_actes );
 






/******************************************************************/
/** Table B                                                      **/
/******************************************************************/

%let cond= /* 1=2 and equivalent de inobs=0 en oracle*/;

%extrac_pmsi (
table		=	b,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_B,		
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=	/*inner join  */,/* 	 Jointure fichier de cod*/
deb			=	2017,/* Annee de debut */	 
fin			=	2019,/* Annee de fin */ 
condition	=	/*and  */, 	/* on selectionne toutes les lignes */
varliste=		 	c.nir_ano_17
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ b.ENT_MOD
					§ b.ENT_PRV
					§ b.SOR_MOD
					§ b.SOR_DES
					§ c.sej_num
					§ b.dgn_pal
					§ b.dgn_rel
					§ b.grg_ghm
					§ b.age_ann
					§ b.cod_sex
					§ b.uhcd_top
					§ b.AGE_GES
					§ b.DEL_REG_ENT,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ b.ENT_MOD
					§ b.ENT_PRV
					§ b.SOR_MOD
					§ b.SOR_DES
					§ c.sej_num
					§ b.dgn_pal
					§ b.dgn_rel
					§ b.grg_ghm
					§ b.age_ann
					§ b.cod_sex
					§ b.uhcd_top
					§ b.AGE_GES
					b.DEL_REG_ENT,
varliste2=		 	nir_ano_17 
					§ ETA_NUM
					§ RSA_NUM
					§ RSS_NUM
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ ENT_MOD
					§ ENT_PRV
					§ SOR_MOD
					§ SOR_DES
					§ dgn_pal
					§ dgn_rel
					§ grg_ghm
					§ age_ann
					§ cod_sex
					§ uhcd_top
					§ AGE_GES
					§ DEL_REG_ENT
 );
 

/*******************/
/** Table C       **/
/*******************/


%let cond= /* 1=2 and equivalent de inobs=0 en oracle*/;

%extrac_pmsi_c (
table		=	c,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_C,		
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=	/* */,/* 	 Jointure fichier de cod*/
deb			=	2017,/* Annee de debut */	 
fin			=	2019,/* Annee de fin */ 
condition	=	/*and ...*/, 	/* on selectionne toutes les lignes */
varliste=		 	c.nir_ano_17
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ c.sej_num,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ c.sej_num,
varliste2=		 	nir_ano_17 
					§ ETA_NUM
					§ RSA_NUM
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
 );
 

/*****************************************************************/
/** Table des diagnostiques associes                             **/
/******************************************************************/

%let cond= /* 1=2 and equivalent de inobs=0 en oracle*/;

%extrac_pmsi (
table		=	d,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_D,		
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=	/* inner join  */,/* 	 Jointure fichier de cod*/
deb			=	2017,/* Annee de debut */	 
fin			=	2019,/* Annee de fin */ 
condition	=	and b.rss_num=d.rss_num /* and */, 	/* on selectionne toutes les lignes */
varliste=		 	c.nir_ano_17
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ c.sej_num
					§ d.ass_dgn,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ c.sej_num
					§ d.ass_dgn,
varliste2=		 	nir_ano_17 
					§ ETA_NUM
					§ RSA_NUM
					§ RSS_NUM
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ ass_dgn
 );
 

/******************************************************************/
/* Table des medicaments */
/******************************************************************/

%let cond=/*1=2 and  equivalent de inobs=0 en oracle*/;

%extrac_pmsi (
table		=	MEDATU,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_MEDATU,		
option		=   /*(compress=yes) ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=   ,
deb			=	2017,/* Annee de debut  */	 
fin			=	2019,/* Annee de fin */ 
condition	=	, 	
varliste=		 	c.nir_ano_17  
					§ ucd_ucd_cod
					/*§ ucd_cod*/
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ dat_delai
					§ adm_nbr
					§ sej_nbr
					§ sum(adm_nbr/sej_nbr) as nb_dlv,
varliste1=			c.nir_ano_17 
					§ c.sej_num
					§ c.ent_dat
					§ c.sor_dat
					§ ucd_ucd_cod
					/*§ ucd_cod*/
					§ dat_delai
					§ adm_nbr
					§ sej_nbr
					having sum(adm_nbr/sej_nbr)>0,
varliste2=		 	nir_ano_17 
					§ ETA_NUM
					§ RSA_NUM
					§ strip(ucd_ucd_cod) as ucd_ucd_cod
					/*§ strip(ucd_cod) as ucd_cod*/
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dted. + dat_delai as dte_dlv format=date9. 
					§ &dtef. as sor_dat format=date9.
					§ nb_dlv );




/*****************************************************************/
/* tables des UM */
/*****************************************************************/

%let cond= /* 1=2 and equivalent de inobs=0 en oracle*/;

%extrac_pmsi (
table		=	um,	/*Table interrogee*/
Nom_table	=	&lib_ext..T_MCO_UM,		
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=c.nir_ano_17,
cod			=	/*inner join  */,/* 	 Jointure fichier de cod*/
deb			=	2017, /* Annee de debut */	 
fin			=	2019, /* Annee de fin */ 
condition	=	and b.rss_num=um.rss_num , 	/* on selectionne toutes les lignes */
varliste=		 	c.nir_ano_17
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat 
					§ c.sej_num
					§ um.dgn_pal
					§ um.dgn_rel,
varliste1=			c.nir_ano_17 
					§ b.rss_num
					§ c.ent_dat
					§ c.sor_dat
					§ c.sej_num
					§ um.dgn_pal
					§ um.dgn_rel,
varliste2=		 	nir_ano_17 
					§ ETA_NUM
					§ RSA_NUM
					§ RSS_NUM
					§ &numsej. as sej_numc 
					§ sej_num 
					§ &dted. as ent_dat format=date9.
					§ &dtef. as sor_dat format=date9.
					§ dgn_pal
					§ dgn_rel
 );
 



/*****************************************************************/
/* tables finales */
/*****************************************************************/

%tb_mco_finale(table = C, 		deb = 2017, fin = 2019); 
%tb_mco_finale(table = B, 		deb = 2017, fin = 2019);
%tb_mco_finale(table = UM, 		deb = 2017, fin = 2019);
%tb_mco_finale(table = D, 		deb = 2017, fin = 2019);
%tb_mco_finale(table= A	,		deb = 2017, fin = 2019);

/*aucun code d'interet dans medatu */
*%tb_mco_finale(table= MEDATU,	deb = 2017, fin = 2019);

*%tb_mco_finale(table= MED,		deb = 2017, fin = 2019);
*%tb_mco_finale(table= MEDTHROMBO,	deb = 2017, fin = 2019);
*%tb_mco_finale(table= DMIP,		deb = 2017, fin = 2019);