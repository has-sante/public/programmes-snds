/* ************************************************************************************** */
/*                      ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                          */
/* ************************************************************************************** */

/*******************************************************************************************/
/* Nom du programme : Extraction_DCIR

/* Objectif : extraire les donnees du DCIR concernant la population d'etude  ***/

/*******************************************************************************************/
/*******************************************************************************************/


/*******************************************************************************************/
/*  REFERENTIEL DES BENEFICIAIRES  - ALD -  */
/*******************************************************************************************/
/* Objectif : extraire les donnees de la table des beneficiaires et du referentiel medicalise
concernant la population d'etude  ***/


/* tables de sortie : &lib_ext..ir_ben_r,  &lib_ext..ir_ben_r_arc,  &lib_ext..ir_imb_r */
/*******************************************************************************************/


PROC SQL ;
%connectora;

create table &lib_ext..IR_BEN_R  as
	SELECT * FROM connection to oracle ( 
	 	select	distinct ben.*	
			from  ir_ben_r ben  
				inner JOIN   &tb_id_extract. POP on pop.BEN_NIR_PSA=ben.BEN_NIR_PSA and pop.BEN_RNG_GEM=ben.ben_rng_gem
	);			
create index ben_id on &lib_ext..IR_BEN_R(BEN_NIR_PSA,BEN_RNG_GEM); 
create index BEN_NIR_ANO on &lib_ext..IR_BEN_R(BEN_NIR_ANO); 

create table &lib_ext..IR_BEN_R_ARC  as 
	SELECT * FROM connection to oracle ( 
	 	select	distinct ben.*	
			from  ir_ben_r_arc ben  
				inner JOIN   &tb_id_extract. POP on pop.BEN_NIR_PSA=ben.BEN_NIR_PSA and pop.BEN_RNG_GEM=ben.ben_rng_gem
	);		
create index ben_id on &lib_ext..IR_BEN_R_ARC(BEN_NIR_PSA,BEN_RNG_GEM); 
create index BEN_NIR_ANO on &lib_ext..IR_BEN_R_ARC(BEN_NIR_ANO); 


CREATE TABLE &lib_ext..IR_IMB_R AS 
	SELECT * FROM connection to oracle ( 
		select distinct imb.*
			from ir_imb_r imb
				inner JOIN &tb_id_extract. POP on pop.BEN_NIR_PSA=imb.BEN_NIR_PSA and pop.BEN_RNG_GEM=imb.ben_rng_gem  		
	);
create index ben_id on &lib_ext..IR_IMB_R(BEN_NIR_PSA,BEN_RNG_GEM); 
create index MED_MTF_COD on &lib_ext..IR_IMB_R(MED_MTF_COD); 
create index IMB_ALD_DTD on &lib_ext..IR_IMB_R(IMB_ALD_DTD); 


disconnect from oracle;
QUIT;



/********************************************************/
/************** ACTES DE BIOLOGIE ***********************/
/********************************************************/

/*******************************************************************************************/
/* Objectif : extraire les donnees de la table affinee d'actes medicaux ER_BIO_F 
concernant la population d'etude  ***/

/* tables de sortie : &lib_ext..bioprs_2017, &lib_ext..bioprs_2018, &lib_ext.bioprs_2019 */
/* Repertoire : &lib_ext. = epi_ext defini dans lib_rep.sas */
/*******************************************************************************************/


/***extraction des donnees prs sur la population d'etude  ***/


/*** Execution de macro precedente pour chaque mois de flux***/

%MACRO extract_dcir( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;


	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;




%sel_extrac
(table		=	BIO	,
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=PRS.BEN_NIR_PSA AND POP.BEN_RNG_GEM=PRS.BEN_RNG_GEM,
cod			=   inner join  cod_bio COD on COD.nabm=BIO.bio_prs_ide,
an          =   &an, /* annee en cours */
flx			=	&FLX_DIS_DTD , /* mois en cours a extraire */
condition	=	and &PERIODE_SOI /*and ...	*/,
ETB			= 	,/* Le fait de laisser ETB a vide cree un left join: PRS inner BIO left ETE*/
varliste	= 	PRS.BEN_NIR_PSA 
				§ PRS.BEN_RNG_GEM 
				§ PRS.EXE_SOI_DTD 
				§ BIO.bio_act_qsn 
				§ BIO.BIO_prs_ide 
				§ BIO.BIO_ORD_num
			§ PRS.BEN_CDI_NIR
			§ PRS.BEN_CMU_ORG
			§ PRS.BEN_CMU_TOP
			§ PRS.BEN_DCD_AME
			§ PRS.BEN_EHP_TOP
			§ PRS.BEN_NAI_ANN
			§ PRS.BEN_RES_DPT
			§ PRS.BEN_SEX_COD
			§ PRS.ORG_AFF_BEN
			§ PRS.EXE_SOI_DTF
			§ PRS.PRE_PRE_DTD
			§ PRS.ORB_BSE_NUM
			§ PRS.ORL_BSE_NUM
			§ PRS.RGM_COD
			§ PRS.RGM_GRG_COD
			§ PRS.BSE_PRS_NAT
			§ PRS.CPL_MAJ_TOP
			§ PRS.CPL_PRS_NAT
			§ PRS.DPN_QLF
			§ PRS.EXO_MTF
		/*	§ PRS.ORG_CLE_NEW*/
			§ PRS.PRE_REN_COD
			§ PRS.PRS_NAT_REF
			§ PRS.RGO_ASU_NAT
			§ PRS.RGO_REM_TAU
			§ PRS.ETB_PRE_FIN
			§ PRS.PFS_EXE_NUM
			§ PRS.PFS_PRE_NUM
			§ PRS.PRS_MTT_NUM
			§ PRS.PSE_ACT_NAT
			§ PRS.PSE_CNV_COD
			§ PRS.PSE_SPE_COD
			§ PRS.PSE_STJ_COD
			§ PRS.PSP_ACT_NAT
			§ PRS.PSP_CNV_COD
			§ PRS.PSP_PPS_NUM
			§ PRS.PSP_SPE_COD
			§ PRS.PSP_STJ_COD
			§ PRS.BSE_REM_BSE
			§ PRS.BSE_REM_MNT
			§ PRS.CPL_REM_BSE
			§ PRS.CPL_REM_MNT
			§ PRS.PRS_ACT_CFT
			§ PRS.PRS_ACT_COG
			§ PRS.PRS_ACT_NBR
			§ PRS.PRS_ACT_QTE
			§ PRS.PRS_PAI_MNT
			§ PRS.PSP_SVI_PPS
&jointure,
varliste1	= 	BEN_NIR_PSA 
				§  BEN_RNG_GEM 
				§ datepart(exe_soi_dtd) as exe_soi_dtd format =date9. 
				§ bio_act_qsn 
				§ BIO_prs_ide 
				§ BIO_ORD_num
			§ BEN_CDI_NIR	
			§ BEN_CMU_ORG	
			§ BEN_CMU_TOP	
			§ BEN_DCD_AME	
			§ BEN_EHP_TOP	
			§ BEN_NAI_ANN	
			§ BEN_RES_DPT	
			§ BEN_SEX_COD	
			§ ORG_AFF_BEN	
			§ EXE_SOI_DTF	
			§ PRE_PRE_DTD	
			§ ORB_BSE_NUM	
			§ ORL_BSE_NUM	
			§ RGM_COD		
			§ RGM_GRG_COD	
			§ BSE_PRS_NAT	
			§ CPL_MAJ_TOP	
			§ CPL_PRS_NAT	
			§ DPN_QLF		
			§ EXO_MTF		
		/*	§ ORG_CLE_NEW	*/
			§ PRE_REN_COD	
			§ PRS_NAT_REF	
			§ RGO_ASU_NAT	
			§ RGO_REM_TAU	
			§ ETB_PRE_FIN	
			§ PFS_EXE_NUM	
			§ PFS_PRE_NUM	
			§ PRS_MTT_NUM	
			§ PSE_ACT_NAT	
			§ PSE_CNV_COD	
			§ PSE_SPE_COD	
			§ PSE_STJ_COD	
			§ PSP_ACT_NAT	
			§ PSP_CNV_COD	
			§ PSP_PPS_NUM	
			§ PSP_SPE_COD	
			§ PSP_STJ_COD	
			§ BSE_REM_BSE	
			§ BSE_REM_MNT	
			§ CPL_REM_BSE
			§ CPL_REM_MNT	
			§ PRS_ACT_CFT	
			§ PRS_ACT_COG	
			§ PRS_ACT_NBR	
			§ PRS_PAI_MNT	
			§ PSP_SVI_PPS	
			§ prs_act_qte
&jointure1,
varliste2	= 	GROUP BY BEN_NIR_PSA 
				§  BEN_RNG_GEM 
				§ calculated exe_soi_dtd  
				§ bio_act_qsn 
				§ BIO_prs_ide 
				§ BIO_ORD_num
			§ BEN_CDI_NIR
			§ BEN_CMU_ORG
			§ BEN_CMU_TOP
			§ BEN_DCD_AME
			§ BEN_EHP_TOP
			§ BEN_NAI_ANN
			§ BEN_RES_DPT
			§ BEN_SEX_COD
			§ ORG_AFF_BEN
			§ EXE_SOI_DTF
			§ PRE_PRE_DTD
			§ ORB_BSE_NUM
			§ ORL_BSE_NUM
			§ RGM_COD
			§ RGM_GRG_COD
			§ BSE_PRS_NAT
			§ CPL_MAJ_TOP
			§ CPL_PRS_NAT
			§ DPN_QLF
			§ EXO_MTF
		/*	§ ORG_CLE_NEW */
			§ PRE_REN_COD
			§ PRS_NAT_REF
			§ RGO_ASU_NAT
			§ RGO_REM_TAU
			§ ETB_PRE_FIN
			§ PFS_EXE_NUM
			§ PFS_PRE_NUM
			§ PRS_MTT_NUM
			§ PSE_ACT_NAT
			§ PSE_CNV_COD
			§ PSE_SPE_COD
			§ PSE_STJ_COD
			§ PSP_ACT_NAT
			§ PSP_CNV_COD
			§ PSP_PPS_NUM
			§ PSP_SPE_COD
			§ PSP_STJ_COD
			§ BSE_REM_BSE
			§ BSE_REM_MNT
			§ CPL_REM_BSE
			§ CPL_REM_MNT
			§ PRS_ACT_CFT
			§ PRS_ACT_COG
			§ PRS_ACT_NBR
			§ PRS_PAI_MNT
			§ PSP_SVI_PPS
			§ prs_act_qte
&jointure1 
HAVING count(*)>0);



%COMPILATION() ;			
%END ;

PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	*
FROM	compil 
QUIT ;


%MEND extract_dcir;


/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;

%extract_dcir(	 Date_DEB = 201701,Date_FIN = 201712,table_sortie = &lib_ext..BIOPRS_2017 ) ;
%extract_dcir(	 Date_DEB = 201801,Date_FIN = 201812,table_sortie = &lib_ext..BIOPRS_2018 ) ;
%extract_dcir(	 Date_DEB = 201901,Date_FIN = 201912,table_sortie = &lib_ext..BIOPRS_2019 ) ;




/****************************************************************/
/************** ACTES MEDICAUX DE LA CCAM ***********************/
/****************************************************************/

/*******************************************************************************************/
/* Objectif : extraire les donnees de la table affinee d'actes medicaux ER_CAM_F 
concernant la population d'etude  ***/

/* tables de sortie : &lib_ext..camprs_2017, &lib_ext..camprs_2018, &lib_ext..camprs_2019 */
/* Repertoire : &lib_ext. = epi_ext defini dans lib_rep.sas */
/*******************************************************************************************/


/***extraction des donnees prs sur la population d'etude  ***/


/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO extract_dcir( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;


	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;



%sel_extrac
(table		=	CAM	,
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=PRS.BEN_NIR_PSA AND POP.BEN_RNG_GEM=PRS.BEN_RNG_GEM,
cod			=   inner join cod_ccam COD on COD.ccam=CAM.cam_prs_ide,
an          = &an, /* annee en cours */
flx			=	&FLX_DIS_DTD , /* mois en cours a extraire */
condition	=	and &PERIODE_SOI /*and ...	*/,
ETB			= 	,/* Le fait de laisser ETB a vide cree un left join: PRS inner CAM left ETE*/
varliste	= 	PRS.BEN_NIR_PSA 
				§ PRS.BEN_RNG_GEM 
				§ PRS.EXE_SOI_DTD 
				§ CAM.cam_prs_ide
				§ CAM.CAM_ACT_COD
				§ CAM.CAM_TRT_PHA
				§ CAM.CAM_DOC_EXT
				/*§ COD.cam_lib */
				§ ETE.DDP_COD 		 
				§ ETE.ETB_EXE_FIN 	 
				§ ETE.ETE_CAT_COD	 
				§ ETE.ETE_GHS_NUM	 
				§ ETE.ETE_IND_TAA	 
				§ ETE.ETE_TYP_COD	 
				§ ETE.MFT_COD		 
				§ ETE.PRS_PPU_SEC	 
			§ PRS.BEN_CDI_NIR
			§ PRS.BEN_CMU_ORG
			§ PRS.BEN_CMU_TOP
			§ PRS.BEN_DCD_AME
			§ PRS.BEN_EHP_TOP
			§ PRS.BEN_NAI_ANN
			§ PRS.BEN_RES_DPT
			§ PRS.BEN_SEX_COD
			§ PRS.ORG_AFF_BEN
			§ PRS.EXE_SOI_DTF
			§ PRS.PRE_PRE_DTD
			§ PRS.ORB_BSE_NUM
			§ PRS.ORL_BSE_NUM
			§ PRS.RGM_COD
			§ PRS.RGM_GRG_COD
			§ PRS.BSE_PRS_NAT
			§ PRS.CPL_MAJ_TOP
			§ PRS.CPL_PRS_NAT
			§ PRS.DPN_QLF
			§ PRS.EXO_MTF
		/*	§ PRS.ORG_CLE_NEW*/
			§ PRS.PRE_REN_COD
			§ PRS.PRS_NAT_REF
			§ PRS.RGO_ASU_NAT
			§ PRS.RGO_REM_TAU
			§ PRS.ETB_PRE_FIN
			§ PRS.PFS_EXE_NUM
			§ PRS.PFS_PRE_NUM
			§ PRS.PRS_MTT_NUM
			§ PRS.PSE_ACT_NAT
			§ PRS.PSE_CNV_COD
			§ PRS.PSE_SPE_COD
			§ PRS.PSE_STJ_COD
			§ PRS.PSP_ACT_NAT
			§ PRS.PSP_CNV_COD
			§ PRS.PSP_PPS_NUM
			§ PRS.PSP_SPE_COD
			§ PRS.PSP_STJ_COD
			§ PRS.BSE_REM_BSE
			§ PRS.BSE_REM_MNT
			§ PRS.CPL_REM_BSE
			§ PRS.CPL_REM_MNT
			§ PRS.PRS_ACT_CFT
			§ PRS.PRS_ACT_COG
			§ PRS.PRS_ACT_NBR
			§ PRS.PRS_ACT_QTE
			§ PRS.PRS_PAI_MNT
			§ PRS.PSP_SVI_PPS
&jointure,
varliste1	= 	BEN_NIR_PSA 
				§  BEN_RNG_GEM 
				§ datepart(exe_soi_dtd) as exe_soi_dtd format =date9. 
				§ cam_prs_ide 
				§ CAM_ACT_COD
				§ CAM_TRT_PHA
				§ CAM_DOC_EXT
				/*§ cam_lib */
				§ DDP_COD 	
				§ ETB_EXE_FIN 
				§ ETE_CAT_COD
				§ ETE_GHS_NUM
				§ ETE_IND_TAA
				§ ETE_TYP_COD
				§ MFT_COD	
				§ PRS_PPU_SEC
			§ BEN_CDI_NIR	
			§ BEN_CMU_ORG	
			§ BEN_CMU_TOP	
			§ BEN_DCD_AME	
			§ BEN_EHP_TOP	
			§ BEN_NAI_ANN	
			§ BEN_RES_DPT	
			§ BEN_SEX_COD	
			§ ORG_AFF_BEN	
			§ EXE_SOI_DTF	
			§ PRE_PRE_DTD	
			§ ORB_BSE_NUM	
			§ ORL_BSE_NUM	
			§ RGM_COD		
			§ RGM_GRG_COD	
			§ BSE_PRS_NAT	
			§ CPL_MAJ_TOP	
			§ CPL_PRS_NAT	
			§ DPN_QLF		
			§ EXO_MTF		
		/*	§ ORG_CLE_NEW	*/
			§ PRE_REN_COD	
			§ PRS_NAT_REF	
			§ RGO_ASU_NAT	
			§ RGO_REM_TAU	
			§ ETB_PRE_FIN	
			§ PFS_EXE_NUM	
			§ PFS_PRE_NUM	
			§ PRS_MTT_NUM	
			§ PSE_ACT_NAT	
			§ PSE_CNV_COD	
			§ PSE_SPE_COD	
			§ PSE_STJ_COD	
			§ PSP_ACT_NAT	
			§ PSP_CNV_COD	
			§ PSP_PPS_NUM	
			§ PSP_SPE_COD	
			§ PSP_STJ_COD	
			§ BSE_REM_BSE	
			§ BSE_REM_MNT	
			§ CPL_REM_BSE
			§ CPL_REM_MNT	
			§ PRS_ACT_CFT	
			§ PRS_ACT_COG	
			§ PRS_ACT_NBR	
			§ PRS_PAI_MNT	
			§ PSP_SVI_PPS	
			§ prs_act_qte
&jointure1,
varliste2	= 	GROUP BY BEN_NIR_PSA 
				§  BEN_RNG_GEM 
				§ calculated exe_soi_dtd  
				§ cam_prs_ide 
				§ CAM_ACT_COD
				§ CAM_TRT_PHA
				§ CAM_DOC_EXT
				/*§ cam_lib */
				§ DDP_COD 		 
				§ ETB_EXE_FIN 	 
				§ ETE_CAT_COD	 
				§ ETE_GHS_NUM	 
				§ ETE_IND_TAA	 
				§ ETE_TYP_COD	 
				§ MFT_COD		 
				§ PRS_PPU_SEC
			§ BEN_CDI_NIR
			§ BEN_CMU_ORG
			§ BEN_CMU_TOP
			§ BEN_DCD_AME
			§ BEN_EHP_TOP
			§ BEN_NAI_ANN
			§ BEN_RES_DPT
			§ BEN_SEX_COD
			§ ORG_AFF_BEN
			§ EXE_SOI_DTF
			§ PRE_PRE_DTD
			§ ORB_BSE_NUM
			§ ORL_BSE_NUM
			§ RGM_COD
			§ RGM_GRG_COD
			§ BSE_PRS_NAT
			§ CPL_MAJ_TOP
			§ CPL_PRS_NAT
			§ DPN_QLF
			§ EXO_MTF
		/*	§ ORG_CLE_NEW */
			§ PRE_REN_COD
			§ PRS_NAT_REF
			§ RGO_ASU_NAT
			§ RGO_REM_TAU
			§ ETB_PRE_FIN
			§ PFS_EXE_NUM
			§ PFS_PRE_NUM
			§ PRS_MTT_NUM
			§ PSE_ACT_NAT
			§ PSE_CNV_COD
			§ PSE_SPE_COD
			§ PSE_STJ_COD
			§ PSP_ACT_NAT
			§ PSP_CNV_COD
			§ PSP_PPS_NUM
			§ PSP_SPE_COD
			§ PSP_STJ_COD
			§ BSE_REM_BSE
			§ BSE_REM_MNT
			§ CPL_REM_BSE
			§ CPL_REM_MNT
			§ PRS_ACT_CFT
			§ PRS_ACT_COG
			§ PRS_ACT_NBR
			§ PRS_PAI_MNT
			§ PSP_SVI_PPS
			§ prs_act_qte
&jointure1 
HAVING count(*)>0);


%COMPILATION() ;			
%END ;

PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	*
FROM	compil 
QUIT ;


%MEND extract_dcir;


/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/

OPTIONS MPRINT ;
/* */

%extract_dcir(	 Date_DEB = 201701,Date_FIN = 201712,table_sortie = &lib_ext..CAMPRS_2017 ) ;
%extract_dcir(	 Date_DEB = 201801,Date_FIN = 201812,table_sortie = &lib_ext..CAMPRS_2018 ) ;
%extract_dcir(	 Date_DEB = 201901,Date_FIN = 201912,table_sortie = &lib_ext..CAMPRS_2019 ) ;




/****************************************************************/
/************** MEDICAMENTS DE VILLE ***********************/
/****************************************************************/



/******************************************************************************************/
/* Objectif : extraire les donnees de la table affinee de pharmacie ER_PHA_F 
concernant la population d'etude  ***/

/* tables de sortie : &lib_ext..phaprs_2017, &lib_ext..phaprs_2018, &lib_ext..phaprs_2019 */
/* Repertoire : &lib_ext. = epi_ext defini dans lib_rep.sas */
/*******************************************************************************************/


/***extraction des donnees prs sur la population d'etude  ***/


/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO extract_dcir( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;


	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;



%sel_extrac
(table		=	pha	,
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=PRS.BEN_NIR_PSA AND POP.BEN_RNG_GEM=PRS.BEN_RNG_GEM,
cod			=   INNER JOIN  COD_MED COD on COD.cod7=pha.pha_prs_ide or cod.cod13=pha.pha_prs_c13 ,
an          = &an, /* annee en cours */
flx			=	&FLX_DIS_DTD , /* mois en cours a extraire */
condition	=	and &PERIODE_SOI /* and ... */,
ETB			= 	,
varliste	= 	PRS.BEN_NIR_PSA 
				§ PRS.BEN_RNG_GEM 
				§ PRS.EXE_SOI_DTD 
				§ PHA.PHA_PRS_IDE
				/*§ COD.PHA_NOM_COURT
				§ COD.PHA_DOS_PRA_DSES
				§ COD.PHA_ATC_l07 
				§ COD.PHA_ATC_c07 
				§ PHA.PHA_CPA_PCP*/
				§ PHA.PHA_DEC_QSU
				§ PHA.PHA_DEC_TOP
				§ PHA.PHA_MOD_PRN
				§ PHA.PHA_SUB_MTF
				§ PHA.PHA_PRS_C13 
				§ PHA.PHA_SEQ_RNV
				§ PHA.PHA_ORD_NUM
				§ PHA.PHA_IDE_CPL
				§ PHA.PHA_act_qsn 
			§ PRS.BEN_CDI_NIR
			§ PRS.BEN_CMU_ORG
			§ PRS.BEN_CMU_TOP
			§ PRS.BEN_DCD_AME
			§ PRS.BEN_EHP_TOP
			§ PRS.BEN_NAI_ANN
			§ PRS.BEN_RES_DPT
			§ PRS.BEN_SEX_COD
			§ PRS.ETB_PRE_FIN
			§ PRS.PFS_EXE_NUM
			§ PRS.PFS_PRE_NUM
			§ PRS.PSE_SPE_COD
			§ PRS.PSE_STJ_COD
			§ PRS.PSP_PPS_NUM
			§ PRS.PSP_SPE_COD
			§ PRS.PSP_STJ_COD

&jointure,
varliste1	= 	BEN_NIR_PSA 
				§ BEN_RNG_GEM 
				§ datepart(exe_soi_dtd) as exe_soi_dtd format =date9. 
				§ PHA_PRS_IDE
				§ PHA_DEC_QSU
				§ PHA_DEC_TOP
				§ PHA_MOD_PRN
				§ PHA_SUB_MTF
				§ PHA_PRS_C13 
				§ PHA_SEQ_RNV
				§ PHA_ORD_NUM
				§ PHA_IDE_CPL
				§ sum(PHA_act_qsn) as PHA_act_qsn
			§ BEN_CDI_NIR	
			§ BEN_CMU_ORG	
			§ BEN_CMU_TOP	
			§ BEN_DCD_AME	
			§ BEN_EHP_TOP	
			§ BEN_NAI_ANN	
			§ BEN_RES_DPT	
			§ BEN_SEX_COD	
			§ ETB_PRE_FIN	
			§ PFS_EXE_NUM	
			§ PFS_PRE_NUM	
			§ PSE_SPE_COD	
			§ PSE_STJ_COD	
			§ PSP_PPS_NUM	
			§ PSP_SPE_COD	
			§ PSP_STJ_COD	
	&jointure1, 
varliste2	= 	GROUP BY 
				  BEN_NIR_PSA 
				§ BEN_RNG_GEM 
				§ calculated exe_soi_dtd   
				§ PHA_PRS_IDE
				§ PHA_DEC_QSU
				§ PHA_DEC_TOP
				§ PHA_MOD_PRN
				§ PHA_SUB_MTF
				§ PHA_PRS_C13 
				§ PHA_SEQ_RNV
				§ PHA_ORD_NUM
				§ PHA_IDE_CPL
			§ BEN_CDI_NIR
			§ BEN_CMU_ORG
			§ BEN_CMU_TOP
			§ BEN_DCD_AME
			§ BEN_EHP_TOP
			§ BEN_NAI_ANN
			§ BEN_RES_DPT
			§ BEN_SEX_COD
			§ ETB_PRE_FIN
			§ PFS_EXE_NUM
			§ PFS_PRE_NUM
			§ PSE_SPE_COD
			§ PSE_STJ_COD
			§ PSP_PPS_NUM
			§ PSP_SPE_COD
			§ PSP_STJ_COD
&jointure1 
HAVING count(*) > 0 );


%COMPILATION() ;			
%END ;

PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	*
FROM	compil 
QUIT ;


%MEND extract_dcir;


/****************************************************************************/
/*lancement de la macro de selection des prestations                        */
/****************************************************************************/
OPTIONS MPRINT ;

/* selon la taille de la population cible faire une table par annee*/
/* et faire la fusion dans un 2eme temps, si c'est possible (... tps de traitement ...) */
%extract_dcir(	 Date_DEB = 201701,Date_FIN = 201712,table_sortie = &lib_ext..PHAPRS_2017 ) ;
%extract_dcir(	 Date_DEB = 201801,Date_FIN = 201812,table_sortie = &lib_ext..PHAPRS_2018 ) ;
%extract_dcir(	 Date_DEB = 201901,Date_FIN = 201912,table_sortie = &lib_ext..PHAPRS_2019 ) ;




/*******************************************************************************************/
/* Prestations 
/*******************************************************************************************/


/* Objectif : extraire les donnees de la table des prestation ER_PRS_F 
concernant la population d'etude  ***/

/* tables de sortie : &lib_ext..prs_2017, &lib_ext..prs_2018, &lib_ext..prs_2019 */
/* Repertoire : &lib_ext. = epi_ext defini dans lib_rep.sas */
/*******************************************************************************************/


/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO extract_dcir( Date_DEB= ,Date_FIN= ,table_sortie=,mois_plus= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

%if %length(&mois_plus)= 0 %then %do;
	%let bonus= 0;
%end;
%else %do;
	 %let bonus= &mois_plus; 
%end;

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + &bonus ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;


	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;


%sel_extrac
(table	=	PRS,
option		= (compress=yes)/*  ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=PRS.BEN_NIR_PSA AND POP.BEN_RNG_GEM=PRS.BEN_RNG_GEM,
cod			=   inner join cod_prs COD on cod.prs_nat=PRS.prs_nat_ref ,
an          = &an, /* annee en cours */
flx			=	&FLX_DIS_DTD , /* mois en cours a extraire */
ETB			=   ,/* oui ou rien Jointure avec le fichier des etablissement */
condition	=   and &PERIODE_SOI,/* and ... */	
varliste	= PRS.BEN_NIR_PSA
			§ PRS.BEN_RNG_GEM
			§ PRS.BEN_CDI_NIR
			§ PRS.BEN_CMU_ORG
			§ PRS.BEN_CMU_TOP
			§ PRS.BEN_DCD_AME
			§ PRS.BEN_EHP_TOP
			§ PRS.BEN_NAI_ANN
			§ PRS.BEN_RES_DPT
			§ PRS.BEN_SEX_COD
			§ PRS.ORG_AFF_BEN
			§ PRS.EXE_SOI_DTD
			§ PRS.EXE_SOI_DTF
			§ PRS.PRE_PRE_DTD
			§ PRS.ORB_BSE_NUM
			§ PRS.ORL_BSE_NUM
			§ PRS.RGM_COD
			§ PRS.RGM_GRG_COD
			§ PRS.BSE_PRS_NAT
			§ PRS.CPL_MAJ_TOP
			§ PRS.CPL_PRS_NAT
			§ PRS.DPN_QLF
			§ PRS.EXO_MTF
			/*§ PRS.ORG_CLE_NEW*/ /* Indisponibilite temporaire sur le profil 108*/
			§ PRS.PRE_REN_COD
			§ PRS.PRS_NAT_REF
			§ PRS.RGO_ASU_NAT
			§ PRS.RGO_REM_TAU
			§ PRS.ETB_PRE_FIN
			§ PRS.PFS_EXE_NUM
			§ PRS.PFS_PRE_NUM
			§ PRS.PRS_MTT_NUM
			§ PRS.PSE_ACT_NAT
			§ PRS.PSE_CNV_COD
			§ PRS.PSE_SPE_COD
			§ PRS.PSE_STJ_COD
			§ PRS.PSP_ACT_NAT
			§ PRS.PSP_CNV_COD
			§ PRS.PSP_PPS_NUM
			§ PRS.PSP_SPE_COD
			§ PRS.PSP_STJ_COD
			§ PRS.BSE_REM_BSE
			§ PRS.BSE_REM_MNT
			§ PRS.CPL_REM_BSE
			§ PRS.CPL_REM_MNT
			§ PRS.PRS_ACT_CFT
			§ PRS.PRS_ACT_COG
			§ PRS.PRS_ACT_NBR
			§ PRS.PRS_ACT_QTE
			§ PRS.PRS_PAI_MNT
			§ PRS.PSP_SVI_PPS
			§ PRS.PRS_GRS_DTD
			§ ETE.DDP_COD 		 
			§ ETE.ETB_EXE_FIN 	 
			§ ETE.ETE_CAT_COD	 
			§ ETE.ETE_GHS_NUM	 
			§ ETE.ETE_IND_TAA	 
			§ ETE.ETE_TYP_COD	 
			§ ETE.MFT_COD		 
			§ ETE.PRS_PPU_SEC	 

&JOINTURE,/* Liste des variables a extraire */
varliste1	= BEN_NIR_PSA	
			§ BEN_RNG_GEM	
			§ BEN_CDI_NIR	
			§ BEN_CMU_ORG	
			§ BEN_CMU_TOP	
			§ BEN_DCD_AME	
			§ BEN_EHP_TOP	
			§ BEN_NAI_ANN	
			§ BEN_RES_DPT	
			§ BEN_SEX_COD	
			§ ORG_AFF_BEN	
			§ EXE_SOI_DTD	
			§ EXE_SOI_DTF	
			§ PRE_PRE_DTD	
			§ ORB_BSE_NUM	
			§ ORL_BSE_NUM	
			§ RGM_COD		
			§ RGM_GRG_COD	
			§ BSE_PRS_NAT	
			§ CPL_MAJ_TOP	
			§ CPL_PRS_NAT	
			§ DPN_QLF		
			§ EXO_MTF		
			/*§ ORG_CLE_NEW	*/
			§ PRE_REN_COD	
			§ PRS_NAT_REF	
			§ RGO_ASU_NAT	
			§ RGO_REM_TAU	
			§ ETB_PRE_FIN	
			§ PFS_EXE_NUM	
			§ PFS_PRE_NUM	
			§ PRS_MTT_NUM	
			§ PSE_ACT_NAT	
			§ PSE_CNV_COD	
			§ PSE_SPE_COD	
			§ PSE_STJ_COD	
			§ PSP_ACT_NAT	
			§ PSP_CNV_COD	
			§ PSP_PPS_NUM	
			§ PSP_SPE_COD	
			§ PSP_STJ_COD	
			§ BSE_REM_BSE	
			§ BSE_REM_MNT	
			§ CPL_REM_BSE
			§ CPL_REM_MNT	
			§ PRS_ACT_CFT	
			§ PRS_ACT_COG	
			§ PRS_ACT_NBR	
			§ PRS_PAI_MNT	
			§ PSP_SVI_PPS	
			§ prs_act_qte
			§ PRS_GRS_DTD
			§ ETB_EXE_FIN 
			§ ETE_CAT_COD
			§ ETE_GHS_NUM
			§ ETE_IND_TAA
			§ ETE_TYP_COD
			§ MFT_COD	
			§ PRS_PPU_SEC

&jointure1 ,/* liste de variables a garder= sous ensemble precedent +/- les variables a sommer*/
varliste2	= 	group by 
 BEN_NIR_PSA
§ BEN_RNG_GEM
§ BEN_CDI_NIR
§ BEN_CMU_ORG
§ BEN_CMU_TOP
§ BEN_DCD_AME
§ BEN_EHP_TOP
§ BEN_NAI_ANN
§ BEN_RES_DPT
§ BEN_SEX_COD
§ ORG_AFF_BEN
§ EXE_SOI_DTD
§ EXE_SOI_DTF
§ PRE_PRE_DTD
§ ORB_BSE_NUM
§ ORL_BSE_NUM
§ RGM_COD
§ RGM_GRG_COD
§ BSE_PRS_NAT
§ CPL_MAJ_TOP
§ CPL_PRS_NAT
§ DPN_QLF
§ EXO_MTF
/*§ ORG_CLE_NEW*/
§ PRE_REN_COD
§ PRS_NAT_REF
§ RGO_ASU_NAT
§ RGO_REM_TAU
§ ETB_PRE_FIN
§ PFS_EXE_NUM
§ PFS_PRE_NUM
§ PRS_MTT_NUM
§ PSE_ACT_NAT
§ PSE_CNV_COD
§ PSE_SPE_COD
§ PSE_STJ_COD
§ PSP_ACT_NAT
§ PSP_CNV_COD
§ PSP_PPS_NUM
§ PSP_SPE_COD
§ PSP_STJ_COD
§ BSE_REM_BSE
§ BSE_REM_MNT
§ CPL_REM_BSE
§ CPL_REM_MNT
§ PRS_ACT_CFT
§ PRS_ACT_COG
§ PRS_ACT_NBR
§ PRS_PAI_MNT
§ PSP_SVI_PPS
§ prs_act_qte
§ PRS_GRS_DTD
§ ETB_EXE_FIN 	 
§ ETE_CAT_COD	 
§ ETE_GHS_NUM	 
§ ETE_IND_TAA	 
§ ETE_TYP_COD	 
§ MFT_COD		 
§ PRS_PPU_SEC

&jointure1
HAVING count(*)> 0 );


%COMPILATION() ;			
%END ;

PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	*
FROM	compil 
QUIT ;


%MEND extract_dcir;


/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;

/* selon la taille de la population cible faire une table par annee*/
/* et faire la fusion dans un 2eme temps, si c'est possible (... tps de traitement ...) */
/* mois plus recherche prestations sur les flux au-dela de la date de soin limite */
%extract_dcir(	 Date_DEB = 201701,Date_FIN = 201712,table_sortie = &lib_ext..PRS_2017 ) ;
%extract_dcir(	 Date_DEB = 201801,Date_FIN = 201812,table_sortie = &lib_ext..PRS_2018 ) ;
%extract_dcir(	 Date_DEB = 201901,Date_FIN = 201912,table_sortie = &lib_ext..PRS_2019,mois_plus=7 ) ;


/*****************************************************************************/
/*******************          MEDICAMENTS  UCD         ***********************/
/*****************************************************************************/

/*******************************************************************************************/
/* Objectif : extraire les donnees de la table affinee de retrocession ER_UCD_F 
concernant la population d'etude  ***/

/* tables de sortie : &lib_ext..ucdprs_2017, &lib_ext..ucdaprs_2018, &lib_ext..ucdprs_2019 */
/* Repertoire : &lib_ext. = epi_ext defini dans lib_rep.sas */
/*******************************************************************************************/

/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO extract_dcir( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;


	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;


%sel_extrac
(table		=	ucd	,
option		=  (compress=yes) /*ou rien */,
pop			=   INNER JOIN &tb_id_extract. POP on POP.BEN_NIR_PSA=PRS.BEN_NIR_PSA AND POP.BEN_RNG_GEM=PRS.BEN_RNG_GEM,
cod			=   INNER JOIN  cod_med COD on COD.cod7_c=ucd.UCD_UCD_COD or COD.cod13_c=ucd.UCD_UCD_COD or COD.cod0=ucd.UCD_UCD_COD,
an          = &an, /* annee en cours d'extraction */
flx			=	&FLX_DIS_DTD , /* mois en cours a extraire */
condition	=	and &PERIODE_SOI /* and ... */,
ETB			= 	,
varliste	= 	PRS.BEN_NIR_PSA 
				§ PRS.BEN_RNG_GEM 
				§ PRS.EXE_SOI_DTD 
				/*§ COD.PHA_ATC_C07 
				§ COD.PHA_ATC_L07 
				§ cod.dose_unitaire
				§ cod.pha_nom_med*/
				§ UCD.UCD_dlv_nbr 
				§ UCD.ucd_frc_coe 
				§ UCD.UCD_TOP_UCD
				§ UCD.UCD_UCD_COD 
			§ PRS.BEN_CDI_NIR
			§ PRS.BEN_CMU_ORG
			§ PRS.BEN_CMU_TOP
			§ PRS.BEN_DCD_AME
			§ PRS.BEN_EHP_TOP
			§ PRS.BEN_NAI_ANN
			§ PRS.BEN_RES_DPT
			§ PRS.BEN_SEX_COD
			§ PRS.ORG_AFF_BEN
			§ PRS.EXE_SOI_DTF
			§ PRS.PRE_PRE_DTD
			§ PRS.ORB_BSE_NUM
			§ PRS.ORL_BSE_NUM
			§ PRS.RGM_COD
			§ PRS.RGM_GRG_COD
			§ PRS.BSE_PRS_NAT
			§ PRS.CPL_MAJ_TOP
			§ PRS.CPL_PRS_NAT
			§ PRS.DPN_QLF
			§ PRS.EXO_MTF
			/*§ PRS.ORG_CLE_NEW*/
			§ PRS.PRE_REN_COD
			§ PRS.PRS_NAT_REF
			§ PRS.RGO_ASU_NAT
			§ PRS.RGO_REM_TAU
			§ PRS.ETB_PRE_FIN
			§ PRS.PFS_EXE_NUM
			§ PRS.PFS_PRE_NUM
			§ PRS.PRS_MTT_NUM
			§ PRS.PSE_ACT_NAT
			§ PRS.PSE_CNV_COD
			§ PRS.PSE_SPE_COD
			§ PRS.PSE_STJ_COD
			§ PRS.PSP_ACT_NAT
			§ PRS.PSP_CNV_COD
			§ PRS.PSP_PPS_NUM
			§ PRS.PSP_SPE_COD
			§ PRS.PSP_STJ_COD
			§ PRS.BSE_REM_BSE
			§ PRS.BSE_REM_MNT
			§ PRS.CPL_REM_BSE
			§ PRS.CPL_REM_MNT
			§ PRS.PRS_ACT_CFT
			§ PRS.PRS_ACT_COG
			§ PRS.PRS_ACT_NBR
			§ PRS.PRS_ACT_QTE
			§ PRS.PRS_PAI_MNT
			§ PRS.PSP_SVI_PPS
&jointure,
varliste1	= 	BEN_NIR_PSA   
				§ BEN_RNG_GEM 
				§ datepart(exe_soi_dtd) as exe_soi_dtd format =date9.
				/*§ PHA_ATC_C07 
				§ PHA_ATC_L07 
				§ dose_unitaire
				§ pha_nom_med*/
				§ UCD_dlv_nbr 
				§ ucd_frc_coe 
				§ UCD_TOP_UCD
				§ UCD_UCD_COD
				§ sum(UCD_dlv_nbr*ucd_frc_coe) as nb_cps
			§ BEN_CDI_NIR	
			§ BEN_CMU_ORG	
			§ BEN_CMU_TOP	
			§ BEN_DCD_AME	
			§ BEN_EHP_TOP	
			§ BEN_NAI_ANN	
			§ BEN_RES_DPT	
			§ BEN_SEX_COD	
			§ ORG_AFF_BEN	
			§ EXE_SOI_DTF	
			§ PRE_PRE_DTD	
			§ ORB_BSE_NUM	
			§ ORL_BSE_NUM	
			§ RGM_COD		
			§ RGM_GRG_COD	
			§ BSE_PRS_NAT	
			§ CPL_MAJ_TOP	
			§ CPL_PRS_NAT	
			§ DPN_QLF		
			§ EXO_MTF		
			/*§ ORG_CLE_NEW	*/
			§ PRE_REN_COD	
			§ PRS_NAT_REF	
			§ RGO_ASU_NAT	
			§ RGO_REM_TAU	
			§ ETB_PRE_FIN	
			§ PFS_EXE_NUM	
			§ PFS_PRE_NUM	
			§ PRS_MTT_NUM	
			§ PSE_ACT_NAT	
			§ PSE_CNV_COD	
			§ PSE_SPE_COD	
			§ PSE_STJ_COD	
			§ PSP_ACT_NAT	
			§ PSP_CNV_COD	
			§ PSP_PPS_NUM	
			§ PSP_SPE_COD	
			§ PSP_STJ_COD	
			§ BSE_REM_BSE	
			§ BSE_REM_MNT	
			§ CPL_REM_BSE
			§ CPL_REM_MNT	
			§ PRS_ACT_CFT	
			§ PRS_ACT_COG	
			§ PRS_ACT_NBR	
			§ PRS_PAI_MNT	
			§ PSP_SVI_PPS	
			§ prs_act_qte
&jointure1,
varliste2	= 	GROUP BY BEN_NIR_PSA   
				§ BEN_RNG_GEM 
				§ calculated exe_soi_dtd
				/*§ PHA_ATC_C07 
				§ PHA_ATC_L07 
				§ dose_unitaire
				§ pha_nom_med*/
				§ UCD_dlv_nbr 
				§ ucd_frc_coe 
				§ UCD_TOP_UCD 
				§ UCD_UCD_COD
			§ BEN_CDI_NIR
			§ BEN_CMU_ORG
			§ BEN_CMU_TOP
			§ BEN_DCD_AME
			§ BEN_EHP_TOP
			§ BEN_NAI_ANN
			§ BEN_RES_DPT
			§ BEN_SEX_COD
			§ ORG_AFF_BEN
			§ EXE_SOI_DTF
			§ PRE_PRE_DTD
			§ ORB_BSE_NUM
			§ ORL_BSE_NUM
			§ RGM_COD
			§ RGM_GRG_COD
			§ BSE_PRS_NAT
			§ CPL_MAJ_TOP
			§ CPL_PRS_NAT
			§ DPN_QLF
			§ EXO_MTF
			/*§ ORG_CLE_NEW*/
			§ PRE_REN_COD
			§ PRS_NAT_REF
			§ RGO_ASU_NAT
			§ RGO_REM_TAU
			§ ETB_PRE_FIN
			§ PFS_EXE_NUM
			§ PFS_PRE_NUM
			§ PRS_MTT_NUM
			§ PSE_ACT_NAT
			§ PSE_CNV_COD
			§ PSE_SPE_COD
			§ PSE_STJ_COD
			§ PSP_ACT_NAT
			§ PSP_CNV_COD
			§ PSP_PPS_NUM
			§ PSP_SPE_COD
			§ PSP_STJ_COD
			§ BSE_REM_BSE
			§ BSE_REM_MNT
			§ CPL_REM_BSE
			§ CPL_REM_MNT
			§ PRS_ACT_CFT
			§ PRS_ACT_COG
			§ PRS_ACT_NBR
			§ PRS_PAI_MNT
			§ PSP_SVI_PPS
			§ prs_act_qte
&jointure1
HAVING count(*)> 0 );



%COMPILATION() ;			
%END ;

PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	*
FROM	compil 
QUIT ;


%MEND extract_dcir;


/****************************************************************************/
/*lancement de la macro de selection des prestations                        */
/****************************************************************************/
OPTIONS MPRINT ;

/* selon la taille de la population cible faire une table par annee*/
/* et faire la fusion dans un 2eme temps, si c'est possible (... tps de traitement ...) */
%extract_dcir(	 Date_DEB = 201701,Date_FIN = 201712,table_sortie = &lib_ext..UCDPRS_2017 ) ;
%extract_dcir(	 Date_DEB = 201801,Date_FIN = 201812,table_sortie = &lib_ext..UCDPRS_2018 ) ;
%extract_dcir(	 Date_DEB = 201901,Date_FIN = 201912,table_sortie = &lib_ext..UCDPRS_2019 ) ;





/****************************************************************************/
/* Tables finales */
/****************************************************************************/
/* Objectif : fusionner les tables annuelles  */
/* en une seule table et creer les index 		   */

/* tables de sortie : 
epi_ext.er_prs_f, 
epi_ext.er_pha_f,
epi_ext.er_cam_f,
epi_ext.er_bio_f, 
epi_ext.ir_imb_r 
*/

/* ************************************************************************ */

/* VERIFIER VOLUMETRIE ANNUELLE DES TABLES AVANT FUSION */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* AJOUTER BEN_IDT_ANO aux tables finales */





/* ER_PRS_F */


proc sql;
CREATE TABLE PRS_2019 AS 		
   	SELECT distinct i.ben_idt_ano, f.* , c.prs_gpe
	from &lib_ext..PRS_2019 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_prs c on c.prs_nat=f.prs_nat_ref
;

CREATE TABLE PRS_2018 AS 		
   	SELECT distinct i.ben_idt_ano, f.* , c.prs_gpe
	from &lib_ext..PRS_2018 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_prs c on c.prs_nat=f.prs_nat_ref
;

CREATE TABLE PRS_2017 AS 		
   	SELECT distinct i.ben_idt_ano, f.* , c.prs_gpe
	from &lib_ext..PRS_2017 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_prs c on c.prs_nat=f.prs_nat_ref
;


quit;

proc sql;

CREATE TABLE PRS AS 		
   	SELECT * from PRS_2019 
	union
   	SELECT * from PRS_2018 
	union
   	SELECT * from PRS_2017 

;


quit;


proc sql;

CREATE TABLE &lib_ext..ER_PRS_F AS 	
	select * 
	from PRS
;	

create index ben_idt_ano on &lib_ext..ER_PRS_F(ben_idt_ano); 
create index EXE_SOI_DTD on &lib_ext..ER_PRS_F(EXE_SOI_DTD);


quit;






/* ER_PHA_F */
PROC SQL;

CREATE TABLE PHAPRS_2019 AS 		
   	SELECT distinct i.ben_idt_ano,f.* , c.ind_med, dosage,nb_dose_bte,class_ae, class_co
		,MED_SPE_EPI, c.MED_EPI, c.MED_SCHIZO, c.MED_TB, c.MED_AD, c.MED_DLN, c.MED_MIG, c.MED_ANX, c.MED_OH, c.MED_AF, c.MED_CO
	from &lib_ext..PHAPRS_2019 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_med c on c.cod7=f.pha_prs_ide
;


CREATE TABLE PHAPRS_2018 AS 		
   	SELECT distinct i.ben_idt_ano,f.* , c.ind_med, dosage,nb_dose_bte,class_ae, class_co
		,MED_SPE_EPI, c.MED_EPI, c.MED_SCHIZO, c.MED_TB, c.MED_AD, c.MED_DLN, c.MED_MIG, c.MED_ANX, c.MED_OH, c.MED_AF, c.MED_CO
	from &lib_ext..PHAPRS_2018 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_med c on c.cod7=f.pha_prs_ide
;


CREATE TABLE PHAPRS_2017 AS 		
   	SELECT distinct i.ben_idt_ano,f.* , c.ind_med, dosage,nb_dose_bte,class_ae, class_co
		,MED_SPE_EPI, c.MED_EPI, c.MED_SCHIZO, c.MED_TB, c.MED_AD, c.MED_DLN, c.MED_MIG, c.MED_ANX, c.MED_OH, c.MED_AF, c.MED_CO
	from &lib_ext..PHAPRS_2017 f
		inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		left join cod_med c on c.cod7=f.pha_prs_ide
;
quit;





proc sql;

CREATE TABLE PHAPRS AS 		
   	SELECT * from PHAPRS_2019 
	union
   	SELECT * from PHAPRS_2018 
	union
   	SELECT * from PHAPRS_2017 

;

quit;


proc sql;

CREATE TABLE &lib_ext..ER_PHA_F AS 	
	select * 
	from PHAPRS
;	

create index ben_idt_ano on &lib_ext..ER_PHA_F(ben_idt_ano); 
create index EXE_SOI_DTD on &lib_ext..ER_PHA_F(EXE_SOI_DTD);
create index PHA_PRS_C13 on &lib_ext..ER_PHA_F(PHA_PRS_C13); 
create index PHA_PRS_IDE on &lib_ext..ER_PHA_F(PHA_PRS_IDE);  


quit;


/* 06/2022 */
/* ajout code lp du tt */

proc sql;

alter table &lib_ext..ER_PHA_F add lp num(1);

update &lib_ext..ER_PHA_F f set lp =
	(select distinct lp from cod_med c where c.cod7=f.pha_prs_ide or c.cod13=f.pha_prs_c13)
;
quit;





/* ER_UCD_F */
PROC SQL;

CREATE TABLE ER_UCD_F AS 		
   	SELECT * from &lib_ext..UCDPRS_2019
union 
	SELECT * from &lib_ext..UCDPRS_2018
union
	SELECT * from &lib_ext..UCDPRS_2017;
;				


CREATE TABLE &lib_ext..ER_UCD_F AS 	
	select i.ben_idt_ano, f.*, c.ind_med, dosage,nb_dose_bte,class_ae, class_co
		,MED_SPE_EPI, c.MED_EPI, c.MED_SCHIZO, c.MED_TB, c.MED_AD, c.MED_DLN, c.MED_MIG, c.MED_ANX, c.MED_OH, c.MED_AF, c.MED_CO
	from ER_UCD_F f
	inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
	left join cod_med c on c.cod0= f.ucd_ucd_cod
;	

create index ben_idt_ano on &lib_ext..ER_UCD_F(ben_idt_ano); 
create index EXE_SOI_DTD on &lib_ext..ER_UCD_F(EXE_SOI_DTD);
create index ucd_ucd_cod on &lib_ext..ER_UCD_F(ucd_ucd_cod);  

QUIT;


proc sql;
CREATE TABLE ER_UCD_F AS 	
	select distinct f.*, dosage,nb_dose_bte,class_ae, class_co
		,MED_SPE_EPI, c.MED_EPI, c.MED_SCHIZO, c.MED_TB, c.MED_AD, c.MED_DLN, c.MED_MIG, c.MED_ANX, c.MED_OH, c.MED_AF, c.MED_CO
	from &lib_ext..ER_UCD_F f
	left join cod_med c on c.cod0= f.ucd_ucd_cod
;	


CREATE TABLE &lib_ext..ER_UCD_F AS 	
select * from ER_UCD_F
;
create index ben_idt_ano on &lib_ext..ER_UCD_F(ben_idt_ano); 
create index EXE_SOI_DTD on &lib_ext..ER_UCD_F(EXE_SOI_DTD);
create index ucd_ucd_cod on &lib_ext..ER_UCD_F(ucd_ucd_cod);  


quit;



/* 06/2022 */
/* ajout code lp du tt */

proc sql;

alter table &lib_ext..ER_UCD_F add lp num(1);

update &lib_ext..ER_UCD_F f set lp =
	(select distinct lp from cod_med c where c.cod7_c=f.UCD_UCD_COD or c.cod13_c=f.UCD_UCD_COD or c.cod0=f.UCD_UCD_COD)
;
quit;



/* ER_CAM_F (CAMPRS)*/
PROC SQL;

CREATE TABLE ER_CAM_F AS 		
   	SELECT * from &lib_ext..CAMPRS_2019;
insert into ER_CAM_F
	SELECT * from &lib_ext..CAMPRS_2018;
insert into ER_CAM_F
	SELECT * from &lib_ext..CAMPRS_2017;
;				

CREATE TABLE &lib_ext..ER_CAM_F AS 	
	select distinct i.ben_idt_ano, f.*, c.ccam_gpe
	from ER_CAM_F f
	inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
	left join cod_ccam c on c.ccam= f.cam_prs_ide
;	

create index ben_idt_ano on &lib_ext..ER_CAM_F(ben_idt_ano); 
create index cam_prs_ide on &lib_ext..ER_CAM_F(cam_prs_ide); 
create index EXE_SOI_DTD on &lib_ext..ER_CAM_F(EXE_SOI_DTD);

QUIT;


/* ER_BIO_F (BIOPRS)*/
PROC SQL;

CREATE TABLE ER_BIO_F AS 		
   	SELECT * from &lib_ext..BIOPRS_2019;
insert into ER_BIO_F
	SELECT * from &lib_ext..BIOPRS_2018;
insert into ER_BIO_F
	SELECT * from &lib_ext..BIOPRS_2017;

;				

CREATE TABLE &lib_ext..ER_BIO_F AS 	
	select i.ben_idt_ano, f.*
	from ER_BIO_F f
	inner join epi_cib.identite i on i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
;

create index ben_idt_ano on &lib_ext..ER_BIO_F(ben_idt_ano); 
create index BIO_prs_ide on &lib_ext..ER_BIO_F(BIO_prs_ide); 
create index EXE_SOI_DTD on &lib_ext..ER_BIO_F(EXE_SOI_DTD);

QUIT;

/* IR_IMB_R */

proc sql;

CREATE TABLE IR_IMB_R as
select distinct i.ben_idt_ano,r.* ,c.cim_gpe
from &lib_ext..IR_IMB_R r
	inner join epi_cib.identite i on i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem
	left join cod_cim c on c.cim10=r.med_mtf_cod
;


drop table  &lib_ext..IR_IMB_R;
create table  &lib_ext..IR_IMB_R as 
	select * from IR_IMB_R;
create index ben_id on &lib_ext..IR_IMB_R(BEN_NIR_PSA,BEN_RNG_GEM); 
create index MED_MTF_COD on &lib_ext..IR_IMB_R(MED_MTF_COD); 
create index IMB_ALD_DTD on &lib_ext..IR_IMB_R(IMB_ALD_DTD); 

quit;




* verif ;
proc sql;

select count(*) from &lib_ext..PRS_2019;
select count(*) from &lib_ext..PRS_2018;
select count(*) from &lib_ext..PRS_2017;
select count(*) from &lib_ext..ER_PRS_F;

select count(*) from &lib_ext..CAMPRS_2019;
select count(*) from &lib_ext..CAMPRS_2018;
select count(*) from &lib_ext..CAMPRS_2017;
select count(*) from &lib_ext..ER_CAM_F;

select count(*) from &lib_ext..UCDPRS_2019;
select count(*) from &lib_ext..UCDPRS_2018;
select count(*) from &lib_ext..UCDPRS_2017;
select count(*) from &lib_ext..ER_UCD_F;

select count(*) from &lib_ext..BIOPRS_2019;
select count(*) from &lib_ext..BIOPRS_2018;
select count(*) from &lib_ext..BIOPRS_2017;
select count(*) from &lib_ext..ER_BIO_F;

select count(*) from &lib_ext..PHAPRS_2019;
select count(*) from &lib_ext..PHAPRS_2018;
select count(*) from &lib_ext..PHAPRS_2017;
select count(*) from &lib_ext..ER_PHA_F;

quit;