/********************************************************************/
/*           ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE               */
/********************************************************************/


/*****************************************************************/
/* Nom du programme : HIST_DIAG_ACT

/* Objectif : creer les tables des actes au cours d'un
 sejour hospitalier et en ACE 

/* tables de sortie : &lib_dmb..hist_diag, &lib_dmb..hist_act 
et &lib_dmb..hist_act2

/*****************************************************************/


/********************************************************
Creation table Hospit MCO
*********************************************************/

%_eg_conditional_dropds(&lib_dmb..hist_rss);
%_eg_conditional_dropds(&lib_dmb..hist_rum);
%_eg_conditional_dropds(&lib_dmb..hist_diag);
%_eg_conditional_dropds(&lib_dmb..hist_act);

 proc sql;

 /* RSS */
create table &lib_dmb..hist_rss as
	select distinct i.ben_idt_ano,b.nir_ano_17,b.eta_num,b.rsa_num,b.rss_num,b.ent_dat format ddmmyy10.,b.sor_dat format ddmmyy10.
		, b.sor_dat-b.ent_dat as nb_nuit,grg_ghm, dgn_pal,dp.cim_gpe as dp_gpe,	dgn_rel,dr.cim_gpe as dr_gpe
		,age_ges, del_reg_ent
	from &lib_ext..t_mco_b b 
		inner join &lib_dmb..identite i on i.ben_nir_psa=b.nir_ano_17
		left join cod_cim dp on b.dgn_pal=dp.cim10
		left join cod_cim dr on b.dgn_rel=dr.cim10
	where dgn_pal not in ('RSSABS')
	order by i.ben_idt_ano,eta_num,rsa_num,rss_num,ent_dat
;

create index ben_idt_ano on &lib_dmb..hist_rss(ben_idt_ano);
create index id_rss on &lib_dmb..hist_rss(eta_num,rsa_num,rss_num);
quit;

proc sql;
/* RUM */
create table &lib_dmb..hist_rum as
	select distinct i.ben_idt_ano,u.nir_ano_17,u.eta_num,u.rsa_num,u.rss_num,u.ent_dat format ddmmyy10.,u.sor_dat format ddmmyy10.,um_ord_num
		, ifn(par_dur_sej=.,0,par_dur_sej) as par_dur_sej
		, u.dgn_pal,dp.cim_gpe as dp_gpe,	u.dgn_rel,dr.cim_gpe as dr_gpe
	from &lib_ext..t_mco_um u 
		inner join &lib_dmb..identite i on i.ben_nir_psa=u.nir_ano_17
		inner join &lib_dmb..hist_rss r on r.eta_num=u.eta_num and r.rsa_num=u.rsa_num and r.rss_num=u.rss_num
		left join cod_cim dp on u.dgn_pal=dp.cim10
		left join cod_cim dr on u.dgn_rel=dr.cim10
	order by i.ben_idt_ano,u.eta_num,u.rsa_num,u.rss_num,u.ent_dat,um_ord_num
;

create index ben_idt_ano on &lib_dmb..hist_rum(ben_idt_ano);
create index id_rss on &lib_dmb..hist_rum(eta_num,rsa_num,rss_num);
quit;


proc sql;
/* DIAG d'interet */

create table &lib_dmb..hist_diag as
	select distinct ben_idt_ano,eta_num,rsa_num,rss_num,ent_dat format ddmmyy10.,sor_dat format ddmmyy10.,
			DGN_PAL as diag, 'DPS' as typ_diag, dp_gpe as cim_gpe
	from  &lib_dmb..hist_rss h
	where dp_gpe is not null
;		
insert into &lib_dmb..hist_diag 
	select distinct ben_idt_ano,eta_num,rsa_num,rss_num,ent_dat format ddmmyy10.,sor_dat format ddmmyy10.,
			DGN_REL , 'DRS', dr_gpe 
	from  &lib_dmb..hist_rss h
	where dr_gpe is not null
;
insert into &lib_dmb..hist_diag 
	select distinct ben_idt_ano,eta_num,rsa_num,rss_num, ent_dat format ddmmyy10.,sor_dat format ddmmyy10.,
		DGN_PAL , 'DPU', dp_gpe 
	from  &lib_dmb..hist_rum h
	where dp_gpe is not null
;		
insert into &lib_dmb..hist_diag 
	select distinct ben_idt_ano,eta_num,rsa_num,rss_num,ent_dat format ddmmyy10.,sor_dat format ddmmyy10.,
		DGN_REL , 'DRU', dr_gpe 
	from  &lib_dmb..hist_rum h
	where dr_gpe is not null
;
insert into &lib_dmb..hist_diag 
	select distinct i.ben_idt_ano,d.eta_num,d.rsa_num,d.rss_num, d.ent_dat format ddmmyy10.,d.sor_dat format ddmmyy10.,
		ass_dgn , 'DAS', cim_gpe 
	from &lib_ext..T_MCO_D d 
		inner join &lib_dmb..identite i on i.ben_nir_psa=d.nir_ano_17 
		inner join &lib_dmb..hist_rss r on r.eta_num=d.eta_num and r.rsa_num=d.rsa_num and r.rss_num=d.rss_num
		inner join cod_cim das on d.ass_dgn=das.cim10
;

create index ben_idt_ano on &lib_dmb..hist_diag(ben_idt_ano);
create index cim_gpe on &lib_dmb..hist_diag(cim_gpe);


quit;

/* top epi sur le rss */

proc sql;

create table tmp as 
	select distinct ben_idt_ano,eta_num,rsa_num,rss_num
	from &lib_dmb..hist_diag
	where cim_gpe='EPI'
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index id_rss on tmp(eta_num,rsa_num,rss_num);

alter table &lib_dmb..hist_rss add epi num(1);
update &lib_dmb..hist_rss r set epi = 1
	where exists (select * from tmp t where r.ben_idt_ano=t.ben_idt_ano and r.eta_num=t.eta_num and r.rsa_num=t.rsa_num and r.rss_num=t.rss_num)
;
update &lib_dmb..hist_rss r set epi = 0 where epi is null;

quit;


proc sql;
/* actes ccam des sejours mco*/
create table &lib_dmb..hist_act as
	select distinct i.ben_idt_ano,a.eta_num,a.rsa_num,a.rss_num, cdc_act,dte_acte,nb_actes, ccam_gpe ,'MCO' as source
	from &lib_ext..T_MCO_A a 
		inner join &lib_dmb..identite i on i.ben_nir_psa=a.nir_ano_17 
		inner join &lib_dmb..hist_rss r on r.eta_num=a.eta_num and r.rsa_num=a.rsa_num and r.rss_num=a.rss_num
		inner join cod_ccam ca on a.cdc_act=ca.ccam
;

create index ben_idt_ano on &lib_dmb..hist_act(ben_idt_ano);
create index ccam_gpe on &lib_dmb..hist_act(ccam_gpe);

quit;


/* actes de ville et actes externes mco */

proc sql;

/* deja dans dcir 
create table &lib_dmb..hist_act2 as
	select distinct i.ben_idt_ano,eta_num,rsa_num,CCAM_COD,ent_dat,1 as nb_actes, ccam_gpe ,'FM' as source
	from &lib_ext..T_MCO_FM a 
		inner join &lib_dmb..identite i on i.ben_nir_psa=a.nir_ano_17 
		inner join cod_ccam ca on a.CCAM_COD=ca.ccam 
;
*/

/* acte en liberal et etb prive et hors hospit */
create table &lib_dmb..hist_act2 as
	select distinct ben_idt_ano,etb_exe_fin as eta_num,'' as rsa_num,cam_prs_ide,exe_soi_dtd,sum(prs_act_qte) as act_qte, ccam_gpe ,'DCIR' as source
	from &lib_ext..ER_CAM_F a 
	where (ETE_IND_TAA not in (1) or ETE_IND_TAA is null) and (ete_ghs_num is null or ete_ghs_num=0)
	group  by ben_idt_ano,cam_prs_ide,exe_soi_dtd,ccam_gpe
	having sum(prs_act_qte)>0
;

alter table &lib_dmb..hist_act2 modify eta_num varchar(9) format=$9.;

insert into &lib_dmb..hist_act2
	select distinct i.ben_idt_ano,eta_num,'',CCAM_COD,ent_dat,1, ccam_gpe ,'FMS'
	from &lib_ext..T_MCO_FMSTC a 
		inner join &lib_dmb..identite i on i.ben_nir_psa=a.nir_ano_17
		inner join cod_ccam ca on a.CCAM_COD=ca.ccam
;

create index ben_idt_ano on &lib_dmb..hist_act2(ben_idt_ano);
create index ccam_gpe on &lib_dmb..hist_act2(ccam_gpe);

quit;