/* ************************************************************************************** */
/*                      ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                          */
/* ************************************************************************************** */


/* ************************************************************************************** */
/* Nom du programme : Macros_PMSI														  */

/* Objectif : 
/* automatiser le processus d'extraction des donnees du PMSI MCO                          */
/* concernant la population d'etude  													  */
/* ************************************************************************************** */


/**************************************************************************************** */
/* Il y a 3 macros dans ce projet            											  */
/* la premiere pour extraire les tables annuelles : extract_pmsi_c 						  */
/* la seconde pour extraire les tables mensuelles : extract_pmsi 						  */
/* la derniere pour obtenir des tables finales :tb_mco_finale 							  */


/**************************************************************************************** */


/*****************************************************************/
/* premiere macro : donnees annuelles */
/*****************************************************************/


%macro extrac_pmsi_c(
table			= 	,	/*Table interrogee, a,  dmip */
Nom_Table		=	,   /* Nom de la table en sortie l'annee sera automatiquement notee*/
option			= 	,	/* (compress=yes) ou rien */
pop				=	, 	/* Jointure fichier de pop*/
cod				=	,   /* Jointure fichier de cod*/
deb				=	,	/* Annee de debut */	 
fin				=	,	/* Annee de fin */ 
condition		=	, 	/* and.. Second condition du where rajouter and au debut */
varliste		=	, 	/* Liste des variables a extraire */
varliste1		=	, 	/* group by Liste des variables  a extraire (- les variables sommees)    */
varliste2		=	 	/* Liste des variables a garder (sous-ensamble de varliste) */
); 

/* ****************************************************************************************** */
/* *******************      GESTION VARIABLES et CONDITIONS            ********************** */
/* ****************************************************************************************** */

/* liste des finess geographiques APHP, APHM et HCL a supprimer pour eviter les doublons */
%let finess_out = and eta_num not in ('130780521', '130783236', '130783293', '130784234', '130804297',
                  '600100101', '750041543', '750100018', '750100042', '750100075',
                  '750100083', '750100091', '750100109', '750100125', '750100166',
                  '750100208', '750100216', '750100232', '750100273', '750100299',
                  '750801441', '750803447', '750803454', '910100015', '910100023',
                  '920100013', '920100021', '920100039', '920100047', '920100054',
                  '920100062', '930100011', '930100037', '930100045', '940100027',
                  '940100035', '940100043', '940100050', '940100068', '950100016',
                  '690783154', '690784137', '690784152', '690784178', '690787478',
                  '830100558') ;
%let nir_ano = and NIR_ANO_17 not in ('xxxxxxxxxxxxxxxxx' , 'XXXXXXXXXXXXXXXXD' , 'BXXXXXXXXXXXXXXXX') ;


%do aa=&deb. %to &fin.;
%let an=%substr(&aa.,3,2);
%if &an.>10 %then %do;
	%let elimine=
	nir_ret='0' and nai_ret='0' and sex_ret='0' and sej_ret='0' and FHO_RET ='0' and PMS_RET='0' 
    and DAT_RET='0' &nir_ano. &finess_out. ;
%end; 
%else %do;
	%let elimine=
	nir_ret='0' and nai_ret='0' and sex_ret='0' and sej_ret='0' and FHO_RET ='0' and PMS_RET='0' 
    and DAT_RET='0' &nir_ano. &finess_out. ;
%end;
		

/* II.gestions des formats de dates pour les sejours selon les annees */
%if &an.<=08 %then %do;
	%let dted=input('15'||'/'||sor_moi||'/'||sor_ann,ddmmyy10.)-coalesce(sej_nbj,0);
	%let dtef=input('15'||'/'||sor_moi||'/'||sor_ann,ddmmyy10.);
%end;
%else %do;
		%let dted=input(ent_dat,ddmmyy8.) ;
		%let dtef=input(sor_dat,ddmmyy8.) ;
%end;
/* III.gestions des num_sej */
%if &an.>08 %then %let numsej=input(sej_num,5.);%else %let numsej=input(sej_num,5.)+18263;

PROC SQL  ;
%connectora;
   		CREATE TABLE  &nom_table._&an. &option. AS 
   		SELECT  sor_ann, sor_moi,
			/* gestion des variables a prendre au niveau C */
			%if &an.>=13 and &an.<=18 and &table.=c %then %do; nir_ano_mam,hos_nn_mam,hos_plo, %end;
			%if &an.>= 19 and &table.=c %then %do; id_mam_enf,hos_nn_mam,hos_plo, %end;
			%if &an.>=15 and &table.=c %then %do; rng_ben,rng_nai, %end;

			%sysfunc(translate(&varliste2,%str(,),§))
		from connection to oracle
		(
			select  c.eta_num,c.RSA_NUM,
			/* gestion des variables a prendre au niveau C */
			%if &an.>=13 and &an.<=18 and &table.=c %then %do;c.nir_ano_mam,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>= 19 and &table.=c %then %do;c.id_mam_enf,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>=15 and &table.=c %then %do;c.rng_ben,c.rng_nai,%end;
 			%if &an.< 19 and &table.=c %then %do; c.sor_ann, c.sor_moi, %end; %else %do; substr(c.sor_dat,5,4)  as sor_ann, substr(c.sor_dat,3,2) as sor_moi, %end;

			%sysfunc(translate(&varliste,%str(,),§))

			from T_MCO&an.C c  
			 &pop. &cod.
			where  &elimine. &condition.  	 
			group by c.eta_num,c.RSA_NUM,
			%if &an.>=13 and &an.<=18 and &table.=c %then %do;c.nir_ano_mam,c.hos_nn_mam,c.hos_plo,%end;
			%if &an. >= 19 and &table.=c %then %do;c.id_mam_enf,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>=15 and &table.=c %then %do;c.rng_ben,c.rng_nai,%end;
 			%if &an.< 19 and &table.=c %then %do; c.sor_ann, c.sor_moi, %end; %else %do; substr(c.sor_dat,5,4) ,substr(c.sor_dat,3,2) , %end;

			%sysfunc(translate(&varliste1,%str(,),§))
		) ;	
disconnect from oracle;	
QUIT;
%end;
%mend  extrac_pmsi_c;

/*****************************************************************/




/*****************************************************************/
/* deuxieme macro : donnees mensuelles */
/*****************************************************************/


%macro extrac_pmsi(
table			= 	,	/*Table interrogee, a,  dmip */
Nom_Table		=	,   /* Nom de la table en sortie l'annee sera automatiquement notee*/
option			= 	,	/* (compress=yes) ou rien */
pop				=	, 	/* Jointure fichier de pop*/
cod				=	,   /* Jointure fichier de cod*/
deb				=	,	/* Annee de debut 2006*/	 
fin				=	,	/* Annee de fin 2014*/ 
condition		=	, 	/* and.. Second condition du where rajouter and au debut */
varliste		=	, 	/* Liste des variables a extraire */
varliste1		=	, 	/* group by Liste des variables a extraire (- les variables sommees)    */
varliste2		=	 	/* Liste des variables a garder (sous-ensamble de varliste) */
); 

/************************************************************************************************/
/********************      GESTION NIR DATES ET VARIABLES POUR SEJOURS    ***********************/
/************************************************************************************************/

/* liste des finess geographiques APHP, APHM et HCL a supprimer pour eviter les doublons */
%let finess_out = and c.eta_num not in ('130780521', '130783236', '130783293', '130784234', '130804297',
                  '600100101', '750041543', '750100018', '750100042', '750100075',
                  '750100083', '750100091', '750100109', '750100125', '750100166',
                  '750100208', '750100216', '750100232', '750100273', '750100299',
                  '750801441', '750803447', '750803454', '910100015', '910100023',
                  '920100013', '920100021', '920100039', '920100047', '920100054',
                  '920100062', '930100011', '930100037', '930100045', '940100027',
                  '940100035', '940100043', '940100050', '940100068', '950100016',
                  '690783154', '690784137', '690784152', '690784178', '690787478',
                  '830100558') ;
%let nir_ano = and c.NIR_ANO_17 not in ('xxxxxxxxxxxxxxxxx' , 'XXXXXXXXXXXXXXXXD' , 'BXXXXXXXXXXXXXXXX') ;




 %_eg_conditional_dropds(&nom_table._&an.);

%do aa=&deb. %to &fin.;
%let an=%substr(&aa.,3,2);
%if &an.=05 %then %do;
	%let elimine=
	nir_ret='0' and nai_ret='0' and sex_ret='0' and sej_ret='0' and FHO_RET ='0' and PMS_RET='0' 
	&nir_ano. &finess_out. ;
%end;
%else %do;
	%if &an.>10 %then %do;
	%let elimine=
	nir_ret='0' and nai_ret='0' and sex_ret='0' and sej_ret='0' and FHO_RET ='0' and PMS_RET='0' 
    and DAT_RET='0' 
	&nir_ano. &finess_out. ;

	%end; 
	%else %do;
	%let elimine=
	nir_ret='0' and nai_ret='0' and sex_ret='0' and sej_ret='0' and FHO_RET ='0' and PMS_RET='0' 
    and DAT_RET='0' 
	&nir_ano. &finess_out. ;
	%end;
%end;		

/*II.gestions des formats de dates pour les sejours selon les annees*/
%if &an.<=08 %then %do;
	%let dted=input('15'||'/'||sor_moi||'/'||sor_ann,ddmmyy10.)-coalesce(sej_nbj,0);
	%let dtef=input('15'||'/'||sor_moi||'/'||sor_ann,ddmmyy10.);
%end;
%else %do;
		%let dted=input(ent_dat,ddmmyy8.) ;
		%let dtef=input(sor_dat,ddmmyy8.) ;
%end;
/*III.gestions des num_sej */
%if &an.>08 %then %let numsej=input(sej_num,5.);%else %let numsej=input(sej_num,5.)+18263;

/** IV.a Gestion du format du delai entre l'acte et la date d'entree*/
%if &an.>=11 %then 
	%let dtact=&dted.+coalesce(ent_dat_del,0);
%else  
	%let dtact=&dted.+coalesce(input(ent_dat_del,3.),0);

/** IV.b LPP Gestion du format du delai entre la pose et la date d'entree*/
%if   &table. = DMIP %then 
	%let dtp=&dted.+coalesce(delai,0) ;
%else %do;
	%if  &table. = FP and &an.>12 %then %let dtp=&dted.+coalesce(del_dat_ent,0) ;%else %let dtp=.;
%end;
/** IV.c gestions des formats de codes UCD prive*/
%if &an.<08 %then %do;
	%let code=cod_ucp_cip;%let taux=input(cod_tau,3.);%end;
%else %do;
	%let code=cod_ucd;%let taux=coe_tau;
%end;
/** IV.b CCAM Gestion du format du delai entre la delivrance et la date d'entree : GHS prive*/
	%if  &table. = FH and &an.>=13 %then %let dtdlv=&dted.+coalesce(del_dat_ent,0) ;%else %let dtdlv=.;


PROC SQL  ;
%connectora;
   		CREATE TABLE  &nom_table._&an. &option. AS 
   		SELECT  sor_ann, sor_moi,
			/* gestion des variables a prendre au niveau C */
			%if &an.>=13 and &an.<=18 and &table.=c %then %do; nir_ano_mam,hos_nn_mam,hos_plo, %end;
			%if &an.>= 19 and &table.=c %then %do; id_mam_enf,hos_nn_mam,hos_plo, %end;
			%if &an.>=15 and &table.=c %then %do; rng_ben,rng_nai, %end;

 			/* gestion des variables a prendre au niveau B */
			%if &an.>=11 and &table.=b %then %do;DEL_REG_ENT,%end;

			/* gestion des variables a prendre au niveau UM */
			%if &an.<10 and &table.=um %then %do;um_typ,%end;
			%else %if &an.>=10 and &table.=um %then %do;par_dur_sej,aut_typ1_um,um_ord_num,igs2_cod,%end;
			%if &an.>=14 and &table.=um %then %do;age_ges,%end;

			%sysfunc(translate(&varliste2,%str(,),§))
		from connection to oracle
		(
			select  c.eta_num,c.RSA_NUM,
			/* Gestion des variables a prendre en compte pour le calcul de la date d'entree 
			ou de sortie selon les annees*/
			%if &an.<=08 %then %do; b.sej_nbj,%end;
			/*%else %do; c.ent_dat,c.sor_dat,%end;*/
			/* gestion des variables a prendre en cas de dispositifs implantables */
			%if   &table. = DMIP %then %do;delai , %end;
			%if   &table. = FP and &an.>12 %then %do;del_dat_ent, %end;
			/* gestion des variables a prendre en compte pour les med en sus ghs*/
			%if &an.<08 and &table.=FH %then %do;cod_ucp_cip,cod_tau,%end;
			%else %if &an.>=08 and &table.=FH %then %do;cod_ucd,coe_tau,%end;
	        %if  &table. = FH and &an.>=13 %then %do;del_dat_ent,%end;
			/* gestion des variables a prendre au niveau UM */
			%if &an.<10 and &table.=um %then %do;um_typ,%end;
			%else %if &an.>=10 and &table.=um %then %do;par_dur_sej,aut_typ1_um,um_ord_num,igs2_cod,%end;
			%if &an.>=14 and &table.=um %then %do;um.age_ges,%end;
			/* gestion des variables a prendre au niveau C */
			%if &an.>=13 and &an.<=18 and &table.=c %then %do;c.nir_ano_mam,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>= 19 and &table.=c %then %do;c.id_mam_enf,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>=15 and &table.=c %then %do;c.rng_ben,c.rng_nai,%end;
 			%if &an.< 19 %then %do; c.sor_ann, c.sor_moi, %end; %else %do; substr(c.sor_dat,5,4)  as sor_ann, substr(c.sor_dat,3,2) as sor_moi, %end;
 			/* gestion des variables a prendre au niveau B */
			%if &an.>=11 and &table.=b %then %do;b.DEL_REG_ENT,%end;

			%sysfunc(translate(&varliste,%str(,),§))

			from T_MCO&an.C c 
				inner join T_MCO&an.b b on  c.rsa_NUM = b.rsa_NUM AND c.ETA_NUM = b.ETA_NUM 
   				inner join	%if &an.<=07 and &table= FP %then ar_T_MCO&an.&table &table ;
					%else  T_MCO&an.&table &table;
					on  c.rsa_NUM = &table..rsa_NUM AND c.ETA_NUM = &table..ETA_NUM 
			 	&pop. &cod.

			where &cond. &elimine. &condition.  	 

			group by c.eta_num,c.RSA_NUM,
				%if &an.<=08 %then %do; b.sej_nbj,%end;
				/*%else %do; c.ent_dat,c.sor_dat,%end; */

			    %if   &table. = DMIP %then %do;delai , %end;
			    %if   &table. = FP and &an.>12 %then %do;del_dat_ent, %end;

				%if &an.<08 and &table.=FH %then %do;cod_ucp_cip,cod_tau,%end;
			    %else %if &an.>=08 and &table.=FH %then %do;cod_ucd,coe_tau,%end;
	            %if  &table. = FH and &an.>=13 %then %do;del_dat_ent,%end;

				%if &an.<10 and &table.=um %then %do;um_typ,%end;
				%else %if &an.>=10 and &table.=um %then %do;par_dur_sej,aut_typ1_um,um_ord_num,igs2_cod,%end;
				%if &an.>=14 and &table.=um %then %do;um.age_ges,%end;

			%if &an.>=13 and &an.<=18 and &table.=c %then %do;c.nir_ano_mam,c.hos_nn_mam,c.hos_plo,%end;
			%if &an. >= 19 and &table.=c %then %do;c.id_mam_enf,c.hos_nn_mam,c.hos_plo,%end;
			%if &an.>=15 and &table.=c %then %do;c.rng_ben,c.rng_nai,%end;
 			%if &an.< 19 %then %do; c.sor_ann, c.sor_moi, %end; %else %do; substr(c.sor_dat,5,4) ,substr(c.sor_dat,3,2) , %end;

				%if &an.>=11 and &table.=b %then %do;b.DEL_REG_ENT,%end;

			%sysfunc(translate(&varliste1,%str(,),§))
		) ;	
disconnect from oracle;	
QUIT;
%end;
%mend  extrac_pmsi;





/*****************************************************************/
/* troisieme macro : donnees finales */
/*****************************************************************/

%macro tb_mco_finale(
table			= 	,	/*Table interrogee, a,  dmip */
deb				=	,	/* Annee de debut 2006*/	 
fin				=		/* Annee de fin 2014*/ 
); 

 %_eg_conditional_dropds(&lib_ext..T_MCO_&table.);

%local list_tables; 
%do aa=&fin. %to &deb. %by -1;
	%let an=%substr(&aa.,3,2);
	%let list_tables= &list_tables &lib_ext..T_MCO_&table._&an.;
	%put &an;
	%put &list_tables;
%end;	

data &lib_ext..T_MCO_&table;
	set &list_tables;
run;


PROC SQL;
	create index NIR_ANO_17 on &lib_ext..T_MCO_&table(NIR_ANO_17); 
	create index ent_dat on &lib_ext..T_MCO_&table(ent_dat);
QUIT;


%if &table.=C %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
	QUIT;
%end;

%if &table.=B or &table.=UM %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num,rss_num);
	QUIT;
%end;

%if &table.=D %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num,rss_num);
		create index ass_dgn on &lib_ext..T_MCO_&table(ass_dgn);
	QUIT;
%end;

%if &table.=A %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num,rss_num);
		create index cdc_act on &lib_ext..T_MCO_&table(cdc_act);
	QUIT;
%end;

%if &table.=MED or &table.=MEDATU or &table.=MEDTHROMBO %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index dte_dlv on &lib_ext..T_MCO_&table(dte_dlv);
		create index ucd_ucd_cod on &lib_ext..T_MCO_&table(ucd_ucd_cod);
	QUIT;
%end;

%if &table.=DMIP %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index lpp_cod on &lib_ext..T_MCO_&table(lpp_cod);
		create index tip_prs_ide on &lib_ext..T_MCO_&table(tip_prs_ide);
		create index dte_pose on &lib_ext..T_MCO_&table(dte_pose);
	QUIT;
%end;



/* ACTIVITE EXTERNE PUBLIQUE */
/* ************************* */

%if &table.=FASTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
	QUIT;
%end;

%if &table.=FBSTC or &table.=FCSTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
		create index act_cod on &lib_ext..T_MCO_&table(act_cod);
	QUIT;
%end;


%if &table.=FHSTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
		create index ucd_cod on &lib_ext..T_MCO_&table(ucd_cod);
		create index ucd_ucd_cod on &lib_ext..T_MCO_&table(ucd_ucd_cod);
	QUIT;
%end;

%if &table.=FLSTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
		create index nabm_cod on &lib_ext..T_MCO_&table(nabm_cod);
	QUIT;
%end;

%if &table.=FMSTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
		create index ccam_cod on &lib_ext..T_MCO_&table(ccam_cod);
	QUIT;
%end;

%if &table.=FPSTC %then %do;
	PROC SQL;
		create index id_seq on &lib_ext..T_MCO_&table(eta_num,seq_num);
		create index lpp_cod on &lib_ext..T_MCO_&table(lpp_cod);
	QUIT;
%end;

/* ACTIVITE EXTERNE PRIVEE */
/* ************************* */

%if &table.=FB or &table.=FC %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index act_cod on &lib_ext..T_MCO_&table(act_cod);
	QUIT;
%end;

%if &table.=FH %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index ucd_cod on &lib_ext..T_MCO_&table(ucd_cod);

	QUIT;
%end;

%if &table.=FL %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index nabm_cod on &lib_ext..T_MCO_&table(nabm_cod);

	QUIT;
%end;

%if &table.=FM %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index ccam_cod on &lib_ext..T_MCO_&table(ccam_cod);

	QUIT;
%end;

%if &table.=FP %then %do;
	PROC SQL;
		create index id_rss on &lib_ext..T_MCO_&table(eta_num,rsa_num);
		create index lpp_cod on &lib_ext..T_MCO_&table(lpp_cod);

	QUIT;
%end;


%mend  tb_mco_finale;


