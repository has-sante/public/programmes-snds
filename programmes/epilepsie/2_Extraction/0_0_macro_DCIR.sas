/* ************************************************************************************** */
/*                      ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                          */
/* ************************************************************************************** */

/* ******************************************************************************************** */
/* Nom du programme : macro DCIR */

/* Objectif : automatiser le processus d'extraction des donnees du DCIR 
concernant la population d etude */

/* Sortie : macro sel_extract et compilation

/* ******************************************************************************************** */

%macro  sel_extrac
(table		=	,/* Prefixe nom de la table affinee DCIR*/
EXT			=	,/* Extension avec ETB=OUI :  S= selection ville SH=selection ETB */
option		= 	,/* (compress=yes) ou rien */
pop			= 	,/* Jointure avec la population selectionnee*/
cod			=   ,/* Jointure avec le fichier de codes pour libelle*/
an          =   ,
flx			=	,/* mois a extraire */	 
ETB			=   ,/* =Oui ou rien Jointure avec le fichier des etablissement prives pour donner cette indication*/
condition	=	,/* and ... Conditions du where: */	
varliste	= 	,/* Liste des variables a extraire */
varliste1	= 	,/* liste de variables a garder= precedent y compris les variables a sommer*/
varliste2	= 	,/* liste des variables du group by = precedent moins exe_soi_dtd*/);


%if &an.<=&an_histo %then %Let extension=_&an.; 
	%else %Let extension=;

%let aff_dd=INNER JOIN er_&table._f&extension. &table ON (&table..FLX_DIS_DTD=PRS.FLX_DIS_DTD
												AND   &table..FLX_TRT_DTD=PRS.FLX_TRT_DTD
												AND   &table..FLX_EMT_NUM=PRS.FLX_EMT_NUM
												AND   &table..FLX_EMT_ORD=PRS.FLX_EMT_ORD
												AND   &table..FLX_EMT_TYP=PRS.FLX_EMT_TYP
												AND   &table..DCT_ORD_NUM=PRS.DCT_ORD_NUM
												AND   &table..ORG_CLE_NUM=PRS.ORG_CLE_NUM
												AND   &table..PRS_ORD_NUM=PRS.PRS_ORD_NUM
												AND   &table..REM_TYP_AFF=PRS.REM_TYP_AFF ) ;

%if   &table= CAM or &table=TIP or &table= cam or &table=tip or &table= Cam or &table=Tip  %then %do;
	%if ETB=oui %then 
		%let ete_dd=INNER JOIN er_ETE_f&extension. ETE ON (	  ETE.FLX_DIS_DTD=PRS.FLX_DIS_DTD
												AND   ETE.FLX_TRT_DTD=PRS.FLX_TRT_DTD
												AND   ETE.FLX_EMT_NUM=PRS.FLX_EMT_NUM
												AND   ETE.FLX_EMT_ORD=PRS.FLX_EMT_ORD
												AND   ETE.FLX_EMT_TYP=PRS.FLX_EMT_TYP
												AND   ETE.DCT_ORD_NUM=PRS.DCT_ORD_NUM
												AND   ETE.ORG_CLE_NUM=PRS.ORG_CLE_NUM
												AND   ETE.PRS_ORD_NUM=PRS.PRS_ORD_NUM
												AND   ETE.REM_TYP_AFF=PRS.REM_TYP_AFF ) ;
	%else %let ete_dd=LEFT JOIN er_ETE_f&extension. ETE ON (ETE.FLX_DIS_DTD=PRS.FLX_DIS_DTD
												AND   ETE.FLX_TRT_DTD=PRS.FLX_TRT_DTD
												AND   ETE.FLX_EMT_NUM=PRS.FLX_EMT_NUM
												AND   ETE.FLX_EMT_ORD=PRS.FLX_EMT_ORD
												AND   ETE.FLX_EMT_TYP=PRS.FLX_EMT_TYP
												AND   ETE.DCT_ORD_NUM=PRS.DCT_ORD_NUM
												AND   ETE.ORG_CLE_NUM=PRS.ORG_CLE_NUM
												AND   ETE.PRS_ORD_NUM=PRS.PRS_ORD_NUM
												AND   ETE.REM_TYP_AFF=PRS.REM_TYP_AFF ) ;
	%end;
%else %let ete_dd=;

%if &table=PRS %then %do;
	%let aff_dd=;
	%let ete_dd=LEFT JOIN er_ETE_f&extension. ETE ON (ETE.FLX_DIS_DTD=PRS.FLX_DIS_DTD
												AND   ETE.FLX_TRT_DTD=PRS.FLX_TRT_DTD
												AND   ETE.FLX_EMT_NUM=PRS.FLX_EMT_NUM
												AND   ETE.FLX_EMT_ORD=PRS.FLX_EMT_ORD
												AND   ETE.FLX_EMT_TYP=PRS.FLX_EMT_TYP
												AND   ETE.DCT_ORD_NUM=PRS.DCT_ORD_NUM
												AND   ETE.ORG_CLE_NUM=PRS.ORG_CLE_NUM
												AND   ETE.PRS_ORD_NUM=PRS.PRS_ORD_NUM
												AND   ETE.REM_TYP_AFF=PRS.REM_TYP_AFF ) ;;
%end;

%if   &table= CAM  %then %do;
	PROC SQL  ;
	%connectora;
	   	CREATE TABLE  table_mois &option. AS 
	   		SELECT DISTINCT %sysfunc(translate(&varliste1,%str(,),§))
	    	FROM connection to oracle ( select %sysfunc(translate(&varliste,%str(,),§)) 
					from er_prs_f&extension. PRS &aff_dd. &pop. &ete_dd. 			      
	     			WHERE 
						PRS.FLX_DIS_DTD = TO_DATE(%str(%'&flx.%'),'DDMONYYYY') 
					 	and dpn_qlf <>71 and PRS_DPN_QLP <> 71	
		 				&condition
	 			)
	    	%sysfunc(translate(&varliste2,%str(,),§))/*GROUP BY and  Having compris dans varliste2*/
	 	;


		update table_mois set cam_prs_ide=trim(cam_prs_ide);

		create index cam_prs_ide on table_mois(cam_prs_ide);

		CREATE TABLE tmp  as 
		SELECT  distinct cam.*
		FROM    table_mois cam	   
				&cod.
		;
		
		drop table table_mois;
		create table table_mois as select * from tmp;

	disconnect from oracle;
	QUIT;
%end;
%else %do;

	PROC SQL  ;
		%connectora;
	   	CREATE TABLE  table_mois &option. AS 
	   		SELECT DISTINCT %sysfunc(translate(&varliste1,%str(,),§))
	    	FROM connection to oracle ( select %sysfunc(translate(&varliste,%str(,),§)) 
					from er_prs_f&extension. PRS &aff_dd. &pop. &cod. &ete_dd. 			      
	     			WHERE 
						PRS.FLX_DIS_DTD = TO_DATE(%str(%'&flx.%'),'DDMONYYYY') 
					 	and dpn_qlf <>71 and PRS_DPN_QLP <> 71	
		 				&condition
	 			)
	    	%sysfunc(translate(&varliste2,%str(,),§))/*GROUP BY and  Having compris dans varliste2*/
	 	;
		disconnect from oracle;
	QUIT;

%end;



%mend sel_extrac; 


/***  Macro Compilation des tables mensuelles ****/

%MACRO COMPILATION() ;
%IF %SYSFUNC(exist( compil )) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change table_mois=compil;
quit;
%END ;
%ELSE %DO;
proc append base=compil data=table_mois FORCE;run;
proc delete data=table_mois;run;
%END;
%MEND COMPILATION ;


