/*****************************************************************************/
/*              ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                     */
/*****************************************************************************/


/* *************************************************************** */
/* Nom du programme : data_ref


* Objectif : 
* creation des libname necessaires au deroulement des programmes ;
* repertoires dans lesquels sont stockes les tables sas resultat 
	du Data Management ;
/* *************************************************************** */


options compress=yes;

%let Fichiers=%sysget(HOME)/sasdata;

/* repertoires d'etude et libname */
libname epi_cib "&Fichiers/REPEPICO/ciblage";

libname epi_ext "&Fichiers/REPEPICO/extraction";

libname epi_dmb "&Fichiers/REPEPICO/dmb";

libname epi_ref "&Fichiers/REPEPICO/data_ref";


/* nom de la librairie de selection */
%let lib_sel=epi_cib ;
/* nom de la librairie d'extraction */
%let lib_ext=epi_ext ;
/* nom de la librairie des tables intermediaires du dmb */
%let lib_dmb=epi_dmb ;




/* Pour l extraction des donnees concernant la pop cible*/

/* Nom de la table des identites a extraire*/
*%let tb_id_extract=POP;
%let tb_id_extract=pop;
/* periode d'extraction des donnees DCIR*/
%let PERIODE_SOI=PRS.EXE_SOI_DTD between to_date('01JAN2017','DDMONYYYY') and to_date('31DEC2019','DDMONYYYY');
/* Premiere annee d'archivage des donnees DCIR */ 
%let an_histo=2012;