# Pratiques de prise en charge des adultes et des enfants avec épilepsie

## Description
La HAS a mené un travail sur le parcours de soins des adultes et des enfants avec épilepsie, comprenant dans un premier temps l’élaboration de fiches points clés portant sur le parcours de soins de l’épilepsie, et la définition ensuite des indicateurs qualité du parcours. Pour venir en support de ces travaux, la mission data a mené 
une étude sur le parcours de soins des enfants et des adultes en utilisant le Système National des Données de Santé (SNDS).

L'objectif de cette étude était de décrire le recours aux différents spécialistes et actes médicaux (correspondant à différents niveaux de soins : primaires, secondaires et tertiaires) en amont et en aval d’une initiation d’antiépileptique. 

L'étude a été menée en collaboration avec le SBP pour alimenter leurs travaux de recommandations et a permis d'appuyer le SEVOQSS dans la définition des indicateurs qualité du parcours.

Le projet a démarré en décembre 2021 et s'est terminée en juillet 2022. Les résultats ont été diffusés en juin 2023.

Ci-dessous des graphiques tirés du poster qui a été présenté au congrès EMOIS 2023 :

![Alt text](image.PNG)

## Liens utiles

### Vers les travaux de la HAS sur les parcours des patients avec épilepsie

- [Présentation de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-11/has_102_etude_pratiques_epilepsie_snds_cd_20230323.pdf)
- [Le rapport complet de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-06/has-102_rapport_epilepsie_snds_cd_2023-03-23_vd.pdf)
- [La recommandation de bonnes pratiques](https://www.has-sante.fr/upload/docs/application/pdf/2020-10/reco308_recommandations_epilepsies_preparation_mel.pdf)
- [Les guides du parcours de santé de l’adulte et de l’enfant avec épilepsie](https://www.has-sante.fr/jcms/p_3444925/fr/guides-du-parcours-de-sante-de-l-adulte-et-de-l-enfant-avec-epilepsie), dont lien vers l'ensemble des documents
- [la définition des indicateurs qualité du parcours](https://www.has-sante.fr/jcms/p_3477948/fr/indicateurs-de-qualite-du-parcours-definition-des-indicateurs-de-qualite-du-parcours-des-enfants-et-des-adultes-ayant-une-epilepsie-novembre-2023)


### Vers les programmes

L'ensemble des programmes de traitement se trouve [ici](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie)
avec des sous-dossiers par étape de traitement ou d'analyse de données :

- [Initialisation](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/0_Initialisation)
- [Ciblage](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/1_Ciblage)
- [Extraction_de_donnees](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/2_Extraction)
- [Selection_population](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/3_Selection)
- [Traitement_de_donnees](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/4_Traitement_de_donnees)
- [Analyse_de_donnees](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/epilepsie/5_Analyse)

>📝 **Notes :**
> - les noms des programmes sont préfixés par un numéro s'ils doivent être exécutés dans un ordre précis
> - les programmes contenus dans Initialisation doivent être automatiquement lancés pour permettre l'exécution des autres programmes
> - les programmes contenus dans Extraction_de_données sont génériques à l'extraction de données concernant une population cible


## Contexte technique

### Données et logiciels utilisées

Les données du DCIR et les données du PMSI (champ MCO) comprises dans le SNDS ont été utilisés pour mener cette étude. 
L'accès aux données du SNDS s'est fait via le portail de l'assurance maladie, au titre de l'accès permanent dont dispose la HAS.

L'extraction de données et les analyses ont été réalisées avec le logiciel SAS.

## Contacts

Pour toute question sur la mise en place de cette étude, et les programmes associés, n'hésitez pas à contacter la mission data: data@has-sante.fr

## Détails techniques

### Flux de données

Le flux de données ci-dessous accompagne les programmes mis à disposition sur le dépôt. 

> 📝 **Notes :**
> - Chaque encadré du fux de données correspond à une étape dans le traitement et/ou l'analyse des données :
> 1. [Initialisation](./index.html#initialisation) = mise à jour des référentiels et des noms de librairies ;
> 2. [Ciblage](./index.html#ciblage) = récupération des identités de patients concernés par l'un des critères d'inclusion ;
> 3. [Extraction de données](./index.html#extraction-de-donnees) = extraction de toutes les données du DCIR et du PMSI qui concernent la population ciblée ;
> 4. [Sélection de population](./index.html#selection-population) = application des critères d'exclusion à partir des données extraites pour mettre à jour la table des patients ;
> 5. [Traitement de données](./index.html#traitement-de-donnees) = construction des variables et des tables nécessaires à l'analyse ;
> 6. [Analyse des données](./index.html#analyse-de-donnees) = programmes d'analyse des données pour restitution des résultats sous la forme de tableaux ou de graphiques dans un fichier texte.

```mermaid
flowchart TB

first[("Donneés du SNDS")]-->Ciblage
first --> Initialisation

subgraph Ciblage

Prog1[[00_ALD_epi_autres.SAS]] --> one("Critères de Ciblage")
Prog2[[01_hosp_epi_autres.SAS]] --> one
Prog3[[02_med_ae.SAS]] --> one
Prog4[[03_med_autres.SAS]] --> one

one--> Prog5[[1_ident_cible.SAS]]
Prog5--> Prog6[[2_patient_cible.SAS]]
Prog6 --> Tab1{{"Table des patients 
patient.sas7bdat"}}  

end

Tab1 --> two("Population cible")
two-->Extraction_de_donnees

subgraph Initialisation
Prog00[[data_ref.SAS]]--> data{{Référentiels}}
Prog01[[lib_rep.SAS]] --> datab{{Librairies et Répertoires}}
end

style Initialisation  fill:#e5f9f4,stroke:#353c3a
style first fill:#f5cffa, stroke:#ff753d,stroke-width:2px
style Ciblage fill:#d8fcb2,stroke:#0f3015
style Tab1 fill:#ffe9a6,stroke:#f66
style one fill:#f5cffa, stroke:#ff753d,stroke-width:2px
style two fill:#f5cffa, stroke:#ff753d,stroke-width:2px
style data fill:#ffe9a6,stroke:#f66
style datab fill:#ffe9a6,stroke:#f66

subgraph Extraction_de_donnees

    subgraph DCIR
    Prog7[[00_macro_DCIR.SAS]] --> Prog8[[01_extraction_DCIR.SAS]]
    Prog8 --> three("TABLES DCIR")
    end

    subgraph PMSI 
    Prog9[[10_macro_PMSI.SAS]] --> Prog10[[11_extraction_MCO.SAS]]
    Prog10 --> four{{"TABLES PMSI"}}
    Prog9 --> Prog11[[12_extraction_ACE.SAS]]
    Prog11 --> four
    end

 three --> Prog12[[2_hist_diag_act.SAS]]
 four --> Prog12
 Prog12 --> Tab3{{"Tables des séjours hospitaliers
 hist_diag.sas7bdat"}}
 Prog12 --> Tab4{{"Tables des actes hospitaliers 
 hist_act.sas7bdat"}}
 Prog12 --> Tab5{{"Table des actes ambulatoires
 hist_act2.sas7bdat"}}
    
end

three --> five("Données extraites")
four --> five
Tab3 --> five
Tab4 --> five
Tab5 --> five

style Extraction_de_donnees  fill:#f9fcc0,stroke:#353c3a
style Tab3 fill:#ffe9a6,stroke:#f66
style Tab4 fill:#ffe9a6,stroke:#f66
style Tab5 fill:#ffe9a6,stroke:#f66
style three fill:#ffe9a6,stroke:#f66
style four fill:#ffe9a6,stroke:#f66
style five fill:#f5cffa, stroke:#ff753d,stroke-width:2px

five --> Selection_population

subgraph Selection_population
    Prog13[[1_ident_pop_maj.SAS]] --> |Mise à jour de la table identités| Prog14 
    Prog14[[2_def_critere_pop.SAS]] --> Tab2b{{"Mise à jour de la table patient
    patient.sas7bdat"}}
end

Tab2b --> six("Population d'étude")
six --> Traitement_de_donnees
five --> Traitement_de_donnees

style Tab2b fill:#ffe9a6,stroke:#f66
style six fill:#f5cffa, stroke:#ff753d,stroke-width:2px
style Selection_population fill:#d8fcb2,stroke:#0f3015

subgraph Traitement_de_donnees

    subgraph Medicaments
    Prog15[[00_macro_sequence.SAS]] --> Prog16[[0_sequence_ttt.SAS]]
    Prog16[[0_sequence_ttt.SAS]] --> Tab13{{"Table des séquences de ttt AE
    sequence.sas7bdat"}}
    end
      
    subgraph Soins
    Prog17[[1_consult.SAS]] --> |Table des consultations| Prog18[[2_soins_pop.SAS]] --> Tab15{{"Table des soins selon la période de suivi
    periode_soins.sas7bdat"}}
    end
   
    subgraph Grossesse
    Prog19[[3_ident_gross.SAS]] --> |Table des grossesses| Prog20[[4_med_ccam_bio_femmes.SAS]]
    Prog20 --> Prog21[[5_soins_femmes.SAS]] 
    Prog21 --> Tab16{{"Table des soins selon la période de grossesse
    periode_gross.sas7bdat"}}
    end

end

Tab13--> seven("Données à analyser")
Tab15 --> seven
Tab16 --> seven
seven--> Prog22[[00_tables_analyses.SAS]]

style Tab13 fill:#ffe9a6,stroke:#f66
style Tab15 fill:#ffe9a6,stroke:#f66
style Tab16 fill:#ffe9a6,stroke:#f66
style seven fill:#f5cffa, stroke:#ff753d,stroke-width:2px
style Traitement_de_donnees fill:#fee19f,stroke:#ef4b09 
style Medicaments fill:#f9d3a0,stroke:#f07948
style Soins fill:#f9d3a0,stroke:#f07948
style Grossesse fill:#f9d3a0,stroke:#f07948

subgraph Analyse_de_donnees
    Prog22--> Prog23[[stat_femmes.SAS]]
    Prog22--> Prog24[[stat_nouveaux_patients.SAS]]
    Prog22--> Prog25[[stat_patients_connus.SAS]]
    seven --> Prog26[[venn_pop.SAS]]
end

Prog23 --> eight("Résultats")
Prog24 --> eight
Prog25 --> eight
Prog26 --> eight

style Analyse_de_donnees fill:#e1f6fd,stroke:#021427
style eight fill:#f5cffa, stroke:#ff753d,stroke-width:2px
```

### Description des tables principales

#### Table patient
Table contenant les éléments descriptifs concernant le patient

| Variable          | Libellé                                                                                                                                                           |
| ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BEN_IDT_ANO       | Identifiant du patient                                                                                                                                            |
| yob               | Année de naissance                                                                                                                                                |
| mob               | Mois de naissance                                                                                                                                                 |
| sex               | Sexe                                                                                                                                                              |
| commune           | Commune de résidence                                                                                                                                              |
| dcd_dte           | date de décès                                                                                                                                                     |
| fup_dte           | date de fin de suivi (date de décès ou 31/12/2019 )                                                                                                               |
| ald_epi_18        | présence d'une ald pour épilepsie grave en 2018                                                                                                                   |
| new_ald_epi       | absence d'ald pour épilepsie grave avant 2018                                                                                                                     |
| date_index_ald    | date de début de l'ald pour épilepsie en 2018                                                                                                                     |
| med_epispe_18     | présence d'une délivrance d'antiépileptique spécifique en 2018                                                                                                    |
| med_epinonspe_18  | présence d'une délivrance d'antiépileptique non spécifique en 2018                                                                                                |
| med_oh            | présence d'au moins 3 délivrances de médicaments de la dépendance alcoolique entre 2017 et 2019                                                                   |
| med_dln           | présence d'au moins 3 délivrances de médicaments de la douleur chronique entre 2017 et 2019                                                                       |
| med_mig           | présence d'au moins 3 délivrances d'anti-migraineux entre 2017 et 2020                                                                                            |
| med_tb            | présence d'au moins 3 délivrances d'anxiolytiques entre 2017 et 2021                                                                                              |
| med_anx           | présence d'au moins 3 délivrances de médicaments thymorégulateurs entre 2017 et 2022                                                                              |
| med_clon_18       | présence d'au moins une délivrance de clonazépam en 2018                                                                                                          |
| med_pgb_18        | présence d'au moins une délivrance de prégabaline ou gabapentine en 2018                                                                                          |
| med_tbpsy_18      | présence d'au moins une délivrance de lamotrigine, carbamazépine, acide valproïque en 2018                                                                        |
| med_clonmono      | présence d'au moins une délivrance de clonazépam en monothérapie en 2018                                                                                          |
| med_clonsimult_18 | présence d'au moins une délivrance de clonazépam en 2018 associé à une délivrance de prégabaline ou gabapentine                                                   |
| first_ae_18_dte   | première date de délivrance d'antiépileptique en 2018                                                                                                             |
| first_ae_dte      | première date de délivrance d'antiépileptique                                                                                                                     |
| med_epi_18        | présence d'au moins une délivrance d'antiépileptique en 2018                                                                                                      |
| new_med_epi       | absence de délivrance d'antiépileptique avant 2018                                                                                                                |
| hosp_epi_18       | présence d'une hospitalisation pour épilepsie en 2018                                                                                                             |
| hosp_oh           | présence d'une hospitalisation pour troubles liés à l'alcool en 2018                                                                                              |
| hosp_dln          | présence d'une hospitalisation pour douleur chronique en 2018                                                                                                     |
| hosp_mig          | présence d'une hospitalisation pour migraine en 2018                                                                                                              |
| hosp_tb           | présence d'une hospitalisation pour trouble bipolaire en 2018                                                                                                     |
| hosp_anx          | présence d'une hospitalisation pour anxiété en 2018                                                                                                               |
| date_index_hosp   | date de début de l'hospitalisation pour épilepsie en 2018                                                                                                         |
| first_hosp_epi    | première date d'hospitalisation pour épilepsie sur la période d'étude                                                                                             |
| new_hosp          | absence d'hospitalisation pour épilepsie avant 2018                                                                                                               |
| epi               | présence d'un critère d'identification de épilepsie                                                                                                               |
| oh                | présence d'un critère d'identification de dépendance alcoolique                                                                                                   |
| dln               | présence d'un critère d'identification de douleur neuropathique                                                                                                   |
| mig               | présence d'un critère d'identification de migraine                                                                                                                |
| tb                | présence d'un critère d'identification de trouble bipolaire                                                                                                       |
| anx               | présence d'un critère d'identification d'anxiété                                                                                                                  |
| other_ind         | présence d'un autre contexte pathologique que l'épilepsie                                                                                                         |
| nb_epi_18         | nombre de délivrances d'antiépileptiques en 2018                                                                                                                  |
| pop1              | patient inclus dans la population 1                                                                                                                               |
| pop2              | patient inclus dans la population 2                                                                                                                               |
| date_index        | date d'inclusion du patient sur l'année 2018 (première date de délivrance d'antiépileptique, ou première hospitalisation, ou début de l'ald, ou 1er janvier 2018) |
| incident          | patient nouvelle identifité comme épileptique en 2018                                                                                                             |
| age               | age par rapport à la date index                                                                                                                                   |
| psp_first_ae      | prescripteur à la première délivrance d'antiépileptique en 2018                                                                                                   |
| stj_frist_ae      | mode d'exercice du prescripteur à la première délivrance d'antiépileptique en 2018                                                                                |
| first_indmed      | molécule délivrée à la première délivrance d'antiépileptique en 2018                                                                                              |
| index_ae          | inclusion du patient en 2018 par une délivrance d'antiépileptique                                                                                                 |
| first_lp          | présence d'une molécule à libération prolongée comme première délivrance d'antiépileptique                                                                        |


#### Table periode_soins
Table contenant le recours aux soins pour chaque patient sur chaque mois de suivi par rapport à la date d'inclusion

| Variable    | Libelle                                                                                                                                     |
| -------------------- | --------------------------------------------------------------------------------------------------------------- |
| BEN_IDT_ANO          | Identifiant du patient                                                                                          |
| yob                  | année de naissance                                                                                              |
| date_index           | date de début du suivi                                                                                          |
| incident             | patient nouvellement connu comme épileptique                                                                    |
| pop1                 | patient inclus dans la population 1                                                                             |
| pop2                 | patient inclus dans la population 2                                                                             |
| periode              | période de suivi                                                                                                |
| debut_periode        | date de début de la période de suivi                                                                            |
| fin_periode          | date de fin de la période de suivi                                                                              |
| nb_consult_neuro     | nombre de consultation neurologues                                                                              |
| nb_consult_mg        | nombre de consultation médecin généraliste                                                                      |
| nb_consult_ped       | nombre de consultation pédiatres                                                                                |
| nb_consult_tot       | nombre de consultation total                                                                                    |
| nb_hdj_epi           | nombre d'hospitalisation de jour pour épilepsie                                                                 |
| nb_hosp_epi          | nombre d'hospitalisation pour épilepsie                                                                         |
| nb_hosp              | nombre d'hospitalisation                                                                                        |
| nb_acte_eeg          | nombre d'actes d'éléctoencéphalogramme                                                                          |
| nb_acte_irm          | nombre d'actes d'imagerie par résonnance magnétique                                                             |
| nb_med_ae            | nombre de délivrances de médicaments antiépileptiques                                                           |
| nb_ind_med           | nombre de médicaments antiépileptiques délivrés                                                                 |
| nb_consult_tele      | nombre de téléconsultations                                                                                     |
| ind_med              | noms des médicaments antiépileptiques délivrés                                                                  |
| duree_poly           | durée de polythérapie (si plusieurs médicaments antiépileptiques délivrés)                                      |
| prim                 | patient concerné par un recours aux soins primaires                                                             |
| sec                  | patient concerné par un recours aux soins secondaires                                                           |
| ter                  | patient concerné par un recours aux soins tertiaires                                                            |
| date_prim            | date de premier recours aux soins primaires sur la période                                                      |
| date_sec             | date de premier recours aux soins secondaires sur la période                                                    |
| date_ter             | date de premier recours aux soins tertiaires sur la période                                                     |
| prim_mg              | patient concerné par un recours au médecin généraliste                                                          |
| prim_urg             | patient concerné par un recours aux urgences                                                                    |
| sec_eeg              | patient concerné par un recours à un eeg                                                                        |
| sec_neuro            | patient concerné par un recours à un neurologue                                                                 |
| sec_irm              | patient concerné par un recours à une irm                                                                       |
| sec_eeg_delai        | délai de recours à l'eeg par rapport à la date de début de la période d'étude                                   |
| sec_neuro_delai      | délai de recours à un neurologue par rapport à la date de début de la période d'étude                           |
| sec_irm_delai        | délai de recours à une irm par rapport à la date de début de la période d'étude                                 |
| ter_delai            | délai de recours à un soin tertiaire par rapport à la date de début de la période d'étude                       |
| minper               | periode correspondant à la première délivrance d'antiépileptique (période 6 = période d'inclusion dans l'étude) |
| first_etat_suivi     | première modalité de prise en charge pharmacologique : en monothérapie, en bithérapie ou en polythérapie        |
| index_ae             | identification du patient via une délivrance d'antiépileptique en 2018                                          |
| class_ae             | classe d'antiépileptique selon leur pouvoir inducteur/inhibiteur enzymatique                                    |
| class_co             | classe de contraceptif hormonal                                                                                 |
| diu_imp              | présence d'un acte de pose d'implant ou de dispositif intra-utérin                                              |
| nb_consult_mg_ter    | nombre de consultations de médecins omnipraticiens en établissement dit tertiaire                               |
| nb_consult_ped_ter   | nombre de consultations de pédiatres en établissement dit tertiaire                                             |
| nb_consult_neuro_ter | nombre de consultations de neurologues en établissement dit tertiaire                                           |
| nb_consult_tot_ter   | nombre de consultations totales en établissement dit tertiaire                                                  |
| nb_consult_urg_ter   | nombre de consultations aux urgences en établissement dit tertiaire                                             |
| nb_consult_tele_ter  | nombre de téléconsultations en établissement dit tertiaire                                                      |
| nb_consult_mg_chu    | nombre de consultations de médecins omnipraticiens en centre hospitalier universitaire                          |
| nb_consult_ped_chu   | nombre de consultations de pédiatres en centre hospitalier universitaire                                        |
| nb_consult_neuro_chu | nombre de consultations de neurologues en centre hospitalier universitaire                                      |
| nb_consult_tot_chu   | nombre de consultations totales en centre hospitalier universitaire                                             |
| nb_consult_urg_chu   | nombre de consultations aux urgences en centre hospitalier universitaire                                        |
| nb_consult_tele_chu  | nombre de téléconsultations en centre hospitalier universitaire                                                 |
| nb_consult_mg_ch     | nombre de consultations de médecins omnipraticiens en Centre hospitalier                                        |
| nb_consult_ped_ch    | nombre de consultations de pédiatres en centre hospitalier                                                      |
| nb_consult_neuro_ch  | nombre de consultations de neurologues en centre hospitalier                                                    |
| nb_consult_tot_ch    | nombre de consultations totales en centre hospitalier                                                           |
| nb_consult_urg_ch    | nombre de consultations aux urgences en centre hospitalier                                                      |
| nb_consult_tele_ch   | nombre de téléconsultations en centre hospitalier                                                               |
| nb_consult_mg_vil    | nombre de consultations de médecins omnipraticiens en ville                                                     |
| nb_consult_ped_vil   | nombre de consultations de pédiatres en ville                                                                   |
| nb_consult_neuro_vil | nombre de consultations de neurologues en ville                                                                 |
| nb_consult_tot_vil   | nombre de consultations totales en ville                                                                        |
| nb_consult_urg_vil   | nombre de consultations aux urgences en ville                                                                   |
| nb_consult_tele_vil  | nombre de téléconsultations en ville                                                                            |

#### Table periode_gross
Table contenant le recours aux soins pour chaque femme enceinte sur chaque mois de suivi par rapport à la date de début de grossesse

| Variable         | Libellé                                                                                   |
| ---------------- | ----------------------------------------------------------------------------------------- |
| BEN_IDT_ANO      | Identifiant du patient                                                                    |
| num_g            | numéro de la grossesse (plusieurs grossesses possibles sur la période d'étude)            |
| dte_debut_gross  | date de début de la grossesse                                                             |
| issue_dte        | date d'issue de la grossesse                                                              |
| issue            | issue de la grossesse                                                                     |
| periode          | période de suivi de la grossesse                                                          |
| debut_periode    | date de début de la période                                                               |
| fin_periode      | date de fin de la période                                                                 |
| nb_consult_neuro | nombre de consultation neurologue                                                         |
| nb_consult_mg    | nombre de consultation médecin généraliste                                                |
| nb_consult_ped   | nombre de consultation pédiatrie                                                          |
| nb_consult_tot   | nombre de consultation totale                                                             |
| nb_hdj_epi       | nombre d'hospitalisation de jour épilepsie                                                |
| nb_hosp_epi      | nombre d'hospitalisation pour épilepsie                                                   |
| nb_hosp          | nombre d'hospitalisation                                                                  |
| nb_acte_eeg      | nombre d'éléctroencéphalogramme                                                           |
| nb_acte_irm      | nombre d'IRM                                                                              |
| nb_med_ae        | nombre de délivrances d'antiépileptiques                                                  |
| nb_ind_med       | nombre de médicaments antiépileptiques délivrés                                           |
| nb_ind_fol       | nombre de délivrance d'acide folique                                                      |
| ind_fol          | patiente concernée par une délivrance d'acide folique                                     |
| prim             | patiente concerné par un recours à un soin primaire                                       |
| sec              | patiente concernée par un recours à un soin secondaire                                    |
| ter              | patiente concernée par un recours à un soin tertiaire                                     |
| date_prim        | date de premier recours aux soins primaires sur la période                                |
| date_sec         | date de premier recours aux soins secondaires sur la période                              |
| date_ter         | date de premier recours aux soins tertiaires sur la période                               |
| prim_mg          | patiente concernée par un recours au médecin généraliste                                  |
| prim_urg         | patiente concernée par un recours aux urgences                                            |
| sec_eeg          | patiente concernée par un recours à un eeg                                                |
| sec_neuro        | patiente concernée par un recours à un neurologue                                         |
| sec_irm          | patiente concernée par un recours à une irm                                               |
| sec_eeg_delai    | délai de recours à l'eeg par rapport à la date de début de la période d'étude             |
| sec_neuro_delai  | délai de recours à un neurologue par rapport à la date de début de la période d'étude     |
| sec_irm_delai    | délai de recours à une irm par rapport à la date de début de la période d'étude           |
| ter_delai        | délai de recours à un soin tertiaire par rapport à la date de début de la période d'étude |
| dosage_ae        | dosage d'antiépileptique                                                                  |
| ind_med          | médicaments antiépileptiques délivrés                                                     |
| date_eeg         | date de recours à l'électroencéphalogramme sur la période de suivi                        |
| date_neuro       | date de recours à un neurologue sur la période de suivi                                   |
| neuro_fa         | patiente concernée par une délivrance d'acide folique suite à une consultation neurologue |
| neuro_dosage     | patiente concernée par un dosage d'antiépileptique suite à une consultation neurologue    |


#### Table sequence_fin
Table contenant les séquences de traitement antiépileptique pour chaque patient

| Variable    | Libellé                                                                      |
| ----------- | ---------------------------------------------------------------------------- |
| BEN_IDT_ANO | Identifiant du patient                                                       |
| debut_seq   | date de début de la séquence des délivrances de médicaments antiépileptiques |
| fin_seq     | date de fin de la séquence des délivrances de médicaments antiépileptiques   |
| rgp         | nom du médicament antiépileptique délivré                                    |
| nb_rgp      | nombre de médicament antiépileptique délivré                                 |
| poly        | présence d'une polythérapie (plusieurs médicaments antiépileptiques)         |
| num_seq     | numéro de la séquence de traitement dans l'ordre chronologique               |
| class_ae    | classe d'antiépileptique                                                     |
| class_co    | classe de contraceptif oral (si concerné)         

