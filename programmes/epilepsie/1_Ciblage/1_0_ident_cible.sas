/* ************************************************************************************** */
/*                 ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                               */
/* ************************************************************************************** */

/* ********************************************************************************** */
/* Nom du programme : ident_cible *

/* Objectif : A partir des identites des patients avec une trace potentielle d'epilepsie
rechercher toutes les identites (couple ben_nir_psa ben_rng_gem) rattachees aux ben_idt_ano 
et creer la table de l'ensemble des ben_nir_psa des patients cibles sous oracle
pour permettre l'extraction des donnees du parcours de ces patients;

/* Referentiel /Source : patient avec au moins 1 trace d'epilepsie  
		ALD, Hospitalisation, Medicaments
&lib_sel..ald_2017_19, &lib_sel..hosp_epi_2017_19, &lib_sel..med_epi_2017_19
;

/* Methode : 
* recherche dans ir_ben_r(_arc) l'identifiant patient de chaque ben_nir_psa cible 
	passage sous oracle pour optimiser les temps de traitement ;
* recherche de l'ensemble des identites rattachees a chaque patient 
	ir_ben_r(_arc) de la population ciblee rapatriees sous sas pour la suite du DM;

/* Repertoire : lib_sel
/* Table en sortie : tables de l'ensemble des identites potentielles de l'etude
		&lib_sel..identite ;
/* ********************************************************************************** */




/* Identites des patients exposes : */
/* ald med ou hosp */

/* ben_nir_psa potentiel de l'etude*/
%_eg_conditional_dropds(tmp);
proc sql;
create table tmp as 
	select distinct ben_nir_psa from epi_cib.ald_2017_19
	union
	select distinct ben_nir_psa from epi_cib.med_epi_2017_19
	union
	select distinct nir_ano_17 from epi_cib.hosp_epi_2017_19
;
quit;


/* transfert de la table sous oracle */
%_eg_conditional_dropds(orauser.tmp);

proc sql;

create table orauser.tmp as
	select * from tmp;

quit;

proc sql;
%connectora;
execute(create index ben_nir_psa on tmp(ben_nir_psa)) by oracle;
disconnect from oracle;
quit;


proc sql;
%connectora;

/*** recherche toutes les identites rattachees aux ben_idt_ano exposes  ***/
execute(create table tmp_ano as select distinct ben_idt_ano from IR_BEN_R b inner join tmp t on t.ben_nir_psa=b.ben_nir_psa) by oracle;
execute(insert into tmp_ano select distinct ben_idt_ano from IR_BEN_R_ARC b inner join tmp t on t.ben_nir_psa=b.ben_nir_psa) by oracle;
execute(create index ben_idt_ano on tmp_ano(ben_idt_ano)) by oracle;

disconnect from oracle;
quit;


/* extraction ir_ben de la population ciblee */

proc sql;
%connectora;
create table epi_cib.ir_ben_r as 
	select * from connection to oracle(
		select distinct b.* from IR_BEN_R b inner join tmp_ano t on t.ben_idt_ano=b.ben_idt_ano
	)
;

create table epi_cib.ir_ben_r_arc as 
	select * from connection to oracle(
		select distinct b.* from IR_BEN_R_ARC b inner join tmp_ano t on t.ben_idt_ano=b.ben_idt_ano
	)
;
disconnect from oracle;
quit;

* creation des indexes;
proc sql;

create index id on epi_cib.ir_ben_r(ben_nir_psa, ben_rng_gem);
create index ben_idt_ano on epi_cib.ir_ben_r(ben_idt_ano);

create index id on epi_cib.ir_ben_r_arc(ben_nir_psa, ben_rng_gem);
create index ben_idt_ano on epi_cib.ir_ben_r_arc(ben_idt_ano);

quit;



* tables de l'ensemble des identites potentielles de l'etude;

proc sql;
/*
create table identite as 
	select distinct BEN_NIR_PSA,BEN_RNG_GEM,BEN_NIR_ANO, BEN_IDT_ANO, ben_idt_top, BEN_NAI_ANN,BEN_SEX_COD,BEN_DCD_AME
	from epi_cib.ir_ben_r;
create index ben_idt_ano on .identite(ben_idt_ano);
*/

* recherche des identites des patient qui ont un ben_nir_ano ;
create table tmp_ano as 
	select distinct BEN_IDT_ANO
	from epi_cib.ir_ben_r
	where ben_idt_top=1
	union
	select distinct BEN_IDT_ANO
	from epi_cib.ir_ben_r_arc
	where ben_idt_top=1
;
create index ben_idt_ano on tmp_ano(ben_idt_ano);



* commencer par ir_ben_r le plus a jour ;
create table identite as
	select distinct BEN_NIR_PSA,BEN_RNG_GEM,r.BEN_IDT_ANO, ben_idt_top
	from epi_cib.ir_ben_r r
	inner join tmp_ano t on r.BEN_IDT_ANO=t.BEN_IDT_ANO
	where r.ben_idt_top=1
;
create index ben_idt_ano on identite(ben_idt_ano);
create index id on identite(ben_nir_psa, ben_rng_gem);


* ajouter les identite archivees avec un vrai ben_nir_ano et non encore trouvees;
create table tmp_ano2 as
	select distinct BEN_NIR_PSA,BEN_RNG_GEM,r.BEN_IDT_ANO, ben_idt_top
	from epi_cib.ir_ben_r_arc r
	inner join tmp_ano t on r.BEN_IDT_ANO=t.BEN_IDT_ANO
	where r.ben_idt_top=1
		and not exists (select * from identite i where r.ben_nir_psa=i.ben_nir_psa and r.ben_rng_gem=i.ben_rng_gem)
;
insert into identite
	select * from tmp_ano2
;

* ajouter les identitess sans vrai ben_nir_ano et non encore trouvees;

create table tmp_id1 as 
	select distinct BEN_NIR_PSA,BEN_RNG_GEM,r.BEN_IDT_ANO, ben_idt_top
	from epi_cib.ir_ben_r r
	where ben_idt_top=0 and 
		not exists (select * from identite i where r.ben_nir_psa=i.ben_nir_psa and r.ben_rng_gem=i.ben_rng_gem)
;
insert into identite
	select * from tmp_id1
;

* ajouter les identite archivees sans vrai ben_nir_ano et non encore trouvees;

create table tmp_id2 as 
	select distinct BEN_NIR_PSA,BEN_RNG_GEM,r.BEN_IDT_ANO, ben_idt_top
	from epi_cib.ir_ben_r_arc r
	where ben_idt_top=0 and 
		not exists (select * from identite i where r.ben_nir_psa=i.ben_nir_psa and r.ben_rng_gem=i.ben_rng_gem)
;
insert into identite
	select * from tmp_id2
;
quit;


proc sql;

drop table epi_cib.identite;

create table epi_cib.identite as select * from identite;
create index ben_idt_ano on epi_cib.identite(ben_idt_ano);
create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);

quit;