/*****************************************************************************************/
/*                               ETUDE PRATIQUE EPILEPSIE                                */
/*****************************************************************************************/

/****************************************************************************************/
/* Nom du programme : med_autres /            

/* Objectif : identifier les beneficiaires ayant eu une delivrance 
de medicaments ayant pour indication 
douleur neuropathique, traitement de la dependance alcoolique, trouble bipolaire, migraine 
en pharmacie de ville */

/* Referentiel : Fichier excel cod_med */
/* Referentiel à charger dans ORAUSER ==> liste_medicaments */
/* Referentiel mis à jour le 22 décembre 2021*/

/* Repertoire : lib_sel
/* Table de sortie : &lib_sel..table_medicaments_aut */
/****************************************************************************************/



/****************************************************************************************/
/* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France */


/*********************************************************************************/
/************           SELECTION DES REMBOURSEMENTS DE MEDICAMENTS        *******/
/************                    DANS DCIR ARCHIVE ET DCIR   - PHA         *******/
/*********************************************************************************/

/*********************************************************************************/
/* DONNEES en entree 
				oravue.er_prs_f_&an ou oravue.er_prs_f
				oravue.er_pha_f_&an ou oravue.er_pha_f
				oravue.IR_PHA_R
				&table_population en entree (facultatif)*/


/* DONNEES en sortie
				&table_sortie a definir

/* PLAN du PROGRAMME ;
	  	- Definition par l'utilisateur des parametres suivants:
			* Choix de la librairie de donnees en sortie
			* options obs=0 ou MAX (respectivement pour tester le programme ou lancer la selection)
			* choix des codes de medicaments
			* liste des variables a selectionner
			* table de population a croiser (facultatif)
	  	- Macro outils
	  	- Selection des medicaments 
		- Copie de la table de population (facultatif) dans ORAUSER
	  	- Macro de selection des medicaments pour un mois de flux : CREA_TABLE
	  	- Macro de compilation des mois de flux : COMPILATION
	  	- Macro de suppression des regularisations : REGULARISATIONS
	  	- Macro TAB_MEDICAMENTS qui execute les macros CREA_TABLE et COMPILATION  pour chaque mois de flux et REGULARISATION sur la table finale
		- L'utilisateur lance la Macro TAB_MEDICAMENTS en choisissant
		    * le mois et annee de debut d'execution des soins (format aaaamm)
			* le mois et annee de fin d'execution des soins (format aaaamm)
			* le nom de la table de sortie
	 

/*********************************************************************************/



/*********************************************************************************/
/*****  Parametres a changer : repertoire,  liste des variables, des codes  *****/
/*********************************************************************************/

  
OPTIONS OBS  = MAX; /* 0 pour tester la requete ou OPTIONS OBS = MAX pour l executer sur les donnees */

/*Criteres de selection des medicaments  a extraire du referentiel pharmacie (cf communique du 24 janvier 2012)*/
/*%LET codes = ;
/* 		EXEMPLE 1 : Selection par CIP 13
			pha_cip_c13 in (3400933226558,3400934744198,3400926939939)
		EXEMPLE 2 : Selection par CIP 7 
			pha_prs_ide  in ( 3876282, 3517778, 3383272 ) 
		EXEMPLE 3 : Selection par code ATC  
			pha_atc_c03 like 'A10' and not(pha_atc_c07='A10BX06') 
		EXEMPLE 4 : Selection par principe actif
			pha_nom_pa like %METFORMINE%
		exemple 5 : Selection par CIP 7 ou ATC 7
			pha_prs_ide  in ( 3876282, 3517778, 3383272 ) or  pha_atc_C07  in ("J01AA02", "J01FA01" ,"J01MA01")	
		

/*liste des variables a conserver (Ne pas changer les prefixes p., pha.,  ir.)*/

/* variables de la table des prestations*/
%LET liste_var_prs =
		p.ben_nir_psa,	
		p.ben_rng_gem,
		p.exe_soi_dtd
		/*et autres variables selon le besoin*/;

/* variables de la table d actes affinee  = ici pharmacie ER_PHA_F*/
%LET liste_var_pha =
       	pha.pha_prs_c13 ,  
		pha.pha_prs_ide 
		/*et autres variables selon le besoin*/;

/* variables du referentiel  = ici referenciel pharmacie IR_PHA_R*/
%LET liste_var_ir =
		ir.pha_atc_c03,  
		ir.pha_atc_c07 
		/*et autres variables selon le besoin*/;

/* variables de la table medicament importee  = ici cod_med */
%LET liste_var_med = 
		med.ind_med,
		/*med.dci,
		med.specialite,*/
		med.atc7,
		med.dosage,
		med.dose_par_bte_mg,
		med.nb_dose_bte,
		med.med_anx,
		med.med_oh,
		med.med_tb,
		med.med_dln,
		med.med_mig
		/*et autres variables selon le besoin*/;

/*1ere annee de la table ER_PRS_F*/
%LET an_DCIR = 2017 ; 

/*Facultatif: Pour extraire les remboursements de medicaments d'une population deja constituee, et non de la population francaise entiere
Attention: 1 ligne = 1 individu 
Variables obligatoires : BEN_NIR_PSA, BEN_RNG_GEM
Si vous n'avez pas de table de population au depart: %let table_pop=  , sinon =nom de la table*/
%LET table_pop = orauser.identite;


/**********************************/
/********    MACRO OUTILS    ******/
/**********************************/

/* 9  cles techniques  */
%MACRO CLE_TEC ( p, d ) ;
&p..DCT_ORD_NUM = &d..DCT_ORD_NUM and
&p..FLX_DIS_DTD = &d..FLX_DIS_DTD and
&p..FLX_EMT_NUM = &d..FLX_EMT_NUM and
&p..FLX_EMT_ORD = &d..FLX_EMT_ORD and
&p..FLX_EMT_TYP = &d..FLX_EMT_TYP and
&p..FLX_TRT_DTD = &d..FLX_TRT_DTD and
&p..ORG_CLE_NUM = &d..ORG_CLE_NUM and
&p..prs_ORD_NUM = &d..prs_ORD_NUM and 
&p..REM_TYP_AFF = &d..REM_TYP_AFF
%MEND ;


/* Lorsque l on a besoin de fusionner une table personnelle avec une table oracle, il faut creer une table personnelle dans ORAUSER, 
   avant cela il faut s assurer qu une table avec le meme nom n existe pas deja ;  le cas echeant il faut supprimer cette table existante */
%MACRO KILL_ORAUSER ( table ) ;
%IF %SYSFUNC( exist( ORAUSER.&table )) %THEN %DO ;
	PROC SQL;
	DROP TABLE ORAUSER.&table;
	QUIT; 
%END ;
%MEND ;





/***************************************************************/
/****  CHOIX DES MEDICAMENTS DANS LE REFERENTIEL PHARMACIE  ****/
/***************************************************************/

%KILL_ORAUSER( liste_medicaments ) ;

/* A partir de la table ir_pha */
/*
PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	ORAVUE.IR_PHA_R
	WHERE 	&codes 
	;
QUIT ;*/

/* Ou selon une table existante*/


PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	libepico.CODE where med_oh=1 or med_anx=1 or med_tb=1 or med_dln=1 or med_mig=1
	;
QUIT ;



/***************************************************************/
/******      OPTION : CHOIX d'UNE TABLE de POPULATION     ******/
/***************************************************************/

%MACRO table_pop() ;
%IF &table_pop ne  %THEN %DO ;
	%PUT Table de population = &table_pop. ;
	%KILL_ORAUSER(pop) ;
	DATA orauser.pop ;
	SET &table_pop ;
	RUN ;
	%MACRO croisement_tab_pop() ;
		inner join orauser.pop pop
		on pop.ben_nir_psa=p.ben_nir_psa and (pop.ben_rng_gem=p.ben_rng_gem or pop.ben_rng_gem=.)
	%MEND ;
%END ;
%ELSE %DO ;
	%PUT no_table ;
	%MACRO croisement_tab_pop() ;
	%MEND ;
%END ;
%MEND ;
%table_pop ;





/***************************************************************/
/************        SELECTION DES PRESTATIONS       ***********/
/***************************************************************/ 
/*** Macro de creation d'une table de prestations mensuelle ***/

%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) Debut %SYSFUNC( datetime(),datetime. ) ;

/* Suppression de la table de prestations mensuelle si existante */
%IF %SYSFUNC ( exist( table_mois )) %THEN %DO ;
	PROC DELETE DATA = table_mois ;
	RUN ;
%END ;

PROC SQL; 
CREATE TABLE table_mois  as 
SELECT  	&liste_var_prs,  
		&liste_var_pha,
		&liste_var_med,
		pha.pha_act_qsn
FROM    &table_prs as p	    %croisement_tab_pop /*ne fait rien si pas de table de population a croiser*/
		INNER JOIN &table_pha as pha
			ON %CLE_TEC (p,pha)
		INNER JOIN orauser.liste_medicaments as med
			ON 	&CONDITION_JOINTURE_MEDICAMENTS
		LEFT JOIN &table_ete as etab
		ON %CLE_TEC(p, etab)
WHERE   p.flx_dis_dtd ="&FLX_DIS_DTD:0:0:0"dt
AND (p.exe_soi_dtd between "&DATE_EXE_DEB:0:0:0"dt and "&DATE_EXE_FIN:0:0:0"dt ) /* selection de la periode d execution du soin*/
AND (p.DPN_QLF <> 71 AND p.PRS_DPN_QLP <> 71) /*exclusion des soins externes transmis pour info*/
		AND (p.CPL_MAJ_TOP<2)                   /*exclusion des majorations*/
AND (etab.ETE_IND_TAA <> 1 OR etab.ETE_IND_TAA is missing) ; /* exclusion des ACE des sejours en hopitaux publics en T2A */

QUIT ;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles ****/

%MACRO COMPILATION() ;
%IF %SYSFUNC(exist( compil )) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change table_mois=compil;
quit;
%END ;
%ELSE %DO;
proc append base=compil data=table_mois FORCE;run;
proc delete data=table_mois;run;
%END;
%MEND COMPILATION ;

/*** Macro de suppression des regularisations ***/

%MACRO REGULARISATIONS( table_sortie );

/*liste des variables (hors variables quantite) sans les suffixes*/
%let liste_var_regul=%qsysfunc(tranwrd(%quote(&liste_var_prs),p.,)),%qsysfunc(tranwrd(%quote(&liste_var_pha),pha., )),%qsysfunc(tranwrd(%quote(&liste_var_med),med., ));
 
PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	distinct	
			&liste_var_regul,
			sum(pha_act_qsn) as pha_qt
FROM	compil 
GROUP BY	&liste_var_regul /* liste du GROUP_BY = liste du SELECT a l exception des variables calculees pour eviter la generation de doublons */
HAVING CALCULATED	pha_qt>0 ;
QUIT ;
%MEND REGULARISATIONS;

/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO TAB_MEDICAMENTS( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;

/*jointure par code CIP13 ou code CIP7 selon l'annee*/
	%IF  &an<2014 %THEN %LET CONDITION_JOINTURE_MEDICAMENTS=(pha.pha_prs_ide=med.cip7);
	%ELSE %LET CONDITION_JOINTURE_MEDICAMENTS=(pha.pha_prs_c13=med.cip13);
		
				 
	/*DCIR archive*/
	%IF &an<&an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f_&an ;
		%LET table_pha = oravue.er_pha_f_&an ;
		%LET table_ete = oravue.er_ete_f_&an ;
	%END;
	/*DCIR courant*/
	%IF  &AN >= &an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f  ;
		%LET table_pha = oravue.er_pha_f  ;
		%LET table_ete = oravue.er_ete_f ;
	%END;

	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT &table_prs ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;
%PUT CONDITION_JOINTURE_MEDICAMENTS=&CONDITION_JOINTURE_MEDICAMENTS;
	 %CREA_TABLE(&FLX_DIS_DTD) ;
	 %COMPILATION() ;			
%END ;
     %REGULARISATIONS( &table_sortie ) ;
%MEND TAB_MEDICAMENTS;

/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;
 %TAB_MEDICAMENTS(	 Date_DEB = 201701,Date_FIN = 201912,table_sortie = libepico.table_medicaments_aut ) ;