/* ************************************************************************************** */
/*                 ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                               */
/* ************************************************************************************** */


/* ********************************************************************************** */
/* Nom du programme : patient_cible

/* Objectif : generer la table patient a partir des identites

/!\ /!\ 
note : des top de selection de population (selon algorithme et variables d'exclusion 
definis au protocole) sont crees pour chaque identite ;
ils seront maj dans la partie selection de la population 
/!\  /!\ 

/* Repertoire : lib_sel
/* Table en sortie : les indicatrices sont enregistrees dans la table &lib_sel..identite ; 
la table finale est &lib_sel..patient*/

/* ********************************************************************************** */




* top inclusion/exclusion ald ;
* *********************************** ;


/* macro recherche ald
parametres
var_ald : nom de la variable "top ald"
condition : criteres de selection de l'ald dans la table des ald
*/
%macro search_ald(var_ald=, condition=);

%_eg_conditional_dropds(tmp);

proc sql;

	create table tmp_ald as 
	select distinct i.ben_nir_psa, i.ben_rng_gem , 1 as top_ald
	from epi_cib.identite i
	inner join epi_cib.ald_2017_19 a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	where &condition
	;
	create index id on tmp_ald(ben_nir_psa, ben_rng_gem);

	create table identite as 
	select i.*, case when top_ald is null then 0 else 1 end as &var_ald
	from epi_cib.identite i
	left join tmp_ald t on t.ben_nir_psa=i.ben_nir_psa and t.ben_rng_gem=i.ben_rng_gem
	;
quit;


proc sql;
	drop table epi_cib.identite;
	create table epi_cib.identite as 
		select * from identite;
	create index ben_nir_psa on epi_cib.identite(ben_nir_psa);

create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);

quit;


%mend;

* generation des top ald dans la table identite;
%search_ald(var_ald=ald_epi_18, condition= a.top_ald in ('EPI') and a.year=2018);
%search_ald(var_ald=ald_oh, condition= a.top_ald in ('OH') and a.year between 2017 and 2019);
%search_ald(var_ald=ald_dln, condition= a.top_ald in ('DLN') and a.year between 2017 and 2019);
%search_ald(var_ald=ald_mig, condition= a.top_ald in ('MIG') and a.year between 2017 and 2019);
%search_ald(var_ald=ald_tb, condition= a.top_ald in ('TB') and a.year between 2017 and 2019);
%search_ald(var_ald=ald_anx, condition= a.top_ald in ('ANX') and a.year between 2017 and 2019);


* top inclusion/exclusion hospit ;
* ****************************** ;

* preparation table diag pour aut ;

/* macro recherche hosp
parametres
var_hosp : nom de la variable "top hospit"
condition : criteres de selection sejour dans la table des diag
*/
%macro search_hosp(var_hosp=, condition=);

proc sql ;


create table tmp_hosp as 
	select distinct i.ben_nir_psa, 1 as top_hosp
	from epi_cib.identite i
	inner join epi_cib.hist_diag a on a.nir_ano_17=i.ben_nir_psa 
	where &condition
;
create index ben_nir_psa on tmp_hosp(ben_nir_psa);

create table identite as 
	select i.*, case when top_hosp is null then 0 else 1 end as &var_hosp
	from epi_cib.identite i
	left join tmp_hosp t on t.ben_nir_psa=i.ben_nir_psa 
;


quit;

proc sql;

drop table epi_cib.identite;
create table epi_cib.identite as 
	select * from identite;

create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);
create index ben_nir_psa on epi_cib.identite(ben_nir_psa);

quit;
%mend;


%search_hosp(var_hosp=hosp_epi_18, condition= cim_gpe in ('EPI') and year(sor_dat)=2018);
%search_hosp(var_hosp=hosp_oh, condition=  cim_gpe in ('OH') and cim_type in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_dln, condition=  cim_gpe in ('DLN') and cim_type in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_mig, condition=  cim_gpe in ('MIG') and cim_type in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_tb, condition=  cim_gpe in ('TB') and cim_type in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_anx, condition=  cim_gpe in ('ANX') and cim_type in ('DPS','DPU','DRS','DRU'));


* top inclusion/exclusion med autre ;
* ********************************* ;

/* macro recherche med epi non spe
parametres
var_med : nom de la variable "top med"
condition : criteres de selection des delivrances dans la table des medicaments
*/
%macro search_med(tb_med=, var_med=, condition=);

proc sql;

create table tmp_med as 
	select i.ben_nir_psa, i.ben_rng_gem, count(distinct datepart(exe_soi_dtd)) as nb, 1 as top_med
	from epi_cib.identite i
	inner join &tb_med a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	where &condition
	group by i.ben_nir_psa, i.ben_rng_gem
;

create index id on tmp_med(ben_nir_psa, ben_rng_gem);

create table identite as 
	select i.*
		,case when top_med is null then 0 else 1 end as &var_med
		,case when top_med is null then 0 else nb end as nb_&var_med
	from epi_cib.identite i
	left join tmp_med t on t.ben_nir_psa=i.ben_nir_psa and t.ben_rng_gem=i.ben_rng_gem
;


quit;

proc sql;

drop table epi_cib.identite;
create table epi_cib.identite as 
	select * from identite;

create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);

quit;

%mend;

* /!\ la table med_epi_2017_19 a ete supprimee apres usage (manque d'espace projet);

* med spe epi;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_epispe_18, condition= year(datepart(exe_soi_dtd))=2018 and med_spe_epi=1);
* med epi non spe;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_epinonspe_18, condition= year(datepart(exe_soi_dtd))=2018 and med_spe_epi=0);

%search_med(tb_med= epi_cib.table_medicaments_aut, var_med=med_oh, condition= med_oh=1);
%search_med(tb_med= epi_cib.table_medicaments_aut, var_med=med_dln, condition= med_dln=1);
%search_med(tb_med= epi_cib.table_medicaments_aut, var_med=med_mig, condition= med_mig=1);
%search_med(tb_med= epi_cib.table_medicaments_aut, var_med=med_tb, condition= med_tb=1);
%search_med(tb_med= epi_cib.table_medicaments_aut, var_med=med_anx, condition= med_anx=1);


* first med epi en 2018;

proc sql;

create table tmp_med as 
	select i.ben_nir_psa, i.ben_rng_gem, min(datepart(exe_soi_dtd)) format=ddmmyy10. as first_ae_18_dte
	from epi_cib.identite i
	inner join epi_cib.med_epi_2017_19 a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	where year(datepart(exe_soi_dtd))=2018
	group by i.ben_nir_psa, i.ben_rng_gem
;

create index id on tmp_med(ben_nir_psa, ben_rng_gem);

drop table identite;
create table identite as 
	select distinct i.*, t.first_ae_18_dte format=ddmmyy10.
	from epi_cib.identite i
	left join tmp_med t on t.ben_nir_psa=i.ben_nir_psa and t.ben_rng_gem=i.ben_rng_gem
;


quit;

proc sql;

drop table epi_cib.identite;
create table epi_cib.identite as 
	select * from identite;

create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);

quit;




*** autres criteres exclusion med non spe **** ;
* ******************************************** ;
proc sql;
alter table epi_cib.identite drop med_clon_18, med_pgb_18, med_pgbpsy_18,med_gbp_18,med_tbpsy_18;
quit;
* CLON;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_clon_18, condition= year(datepart(exe_soi_dtd))=2018 and ind_med in ('CLON'));

* PGB  ;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_pgb_18, condition= year(datepart(exe_soi_dtd))=2018 and ind_med in ('PGB'));
* PGB +- psy ;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_pgbpsy_18, condition= year(datepart(exe_soi_dtd))=2018 and ind_med in ('PGB') and psp_spe_cod in (33,75));

* GBP ;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_gbp_18, condition= year(datepart(exe_soi_dtd))=2018 and ind_med in ('GBP'));

* CBZ ou LTG ;
%search_med(tb_med= epi_cib.med_epi_2017_19, var_med=med_tbpsy_18, condition= year(datepart(exe_soi_dtd))=2018 and ind_med in ('CBZ','LTG') and psp_spe_cod in (33,75));


* CLON + (PGB ou GBP) le meme jour ;
proc sql;

create table tmp_med1 as 
	select distinct i.ben_idt_ano,  datepart(exe_soi_dtd) as exe_soi_dtd
	from epi_cib.identite i
	inner join epi_cib.med_epi_2017_19 a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	where year(datepart(exe_soi_dtd))=2018 and ind_med in ('CLON')
	group by i.ben_idt_ano
;
create index ben_idt_ano on tmp_med1(ben_idt_ano);
create index exe_soi_dtd on tmp_med1(exe_soi_dtd);

create table tmp_med as 
	select i.ben_idt_ano
	from epi_cib.identite i
	inner join epi_cib.med_epi_2017_19 a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	inner join tmp_med1 t on i.ben_idt_ano=t.ben_idt_ano
	where datepart(a.exe_soi_dtd)=t.exe_soi_dtd and ind_med in ('PGB','GBP')
	group by i.ben_idt_ano
;
create index ben_idt_ano on tmp_med(ben_idt_ano);

alter table epi_cib.identite add med_clonsimult_18 num(1);

update epi_cib.identite i set med_clonsimult_18=1
	where exists (select * from tmp_med m where m.ben_idt_ano=i.ben_idt_ano);

update epi_cib.identite set med_clonsimult_18=0
	where med_clonsimult_18 is null;

quit;

*** autres criteres exclusion ea - EEG **** ;
* ***************************************** ;

proc sql;
alter table epi_cib.identite drop eeg_12m_18;

create table tmp_eeg as 
	select distinct i.ben_nir_psa, i.ben_rng_gem, 1 as top_eeg
	from epi_cib.identite i
	inner join epi_cib.er_ccam a on a.ben_nir_psa=i.ben_nir_psa and a.ben_rng_gem=i.ben_rng_gem
	where first_ae_18_dte is not null and datepart(a.exe_soi_dtd) between first_ae_18_dte-360 and first_ae_18_dte+360
	group by i.ben_idt_ano
;

create index id on tmp_eeg(ben_nir_psa, ben_rng_gem);

create table identite as 
	select i.*
		,case when t.top_eeg is null then 0 else 1 end as eeg_12m_18
	from epi_cib.identite i
	left join tmp_eeg t on t.ben_nir_psa=i.ben_nir_psa and t.ben_rng_gem=i.ben_rng_gem
;


quit;

proc sql;

drop table epi_cib.identite;
create table epi_cib.identite as 
	select * from identite;

create index id on epi_cib.identite(ben_nir_psa, ben_rng_gem);

quit;

/* Note : ce critere ne sera finalement pas retenu pour selectionner les populations d etude dans la suite du travail */


* ************************** ;
* *** creation table patient ;
* ************************** ;

proc sql;
drop table epi_cib.patient;

create table epi_cib.patient as 
	select distinct ben_idt_ano 
		, max(ald_epi_18) as ald_epi_18, max(ald_oh) as ald_oh, max(ald_dln) as ald_dln, max(ald_mig) as ald_mig, max(ald_tb) as ald_tb, max(ald_anx) as ald_anx
		, max(hosp_epi_18) as hosp_epi_18, max(hosp_oh) as hosp_oh, max(hosp_dln) as hosp_dln, max(hosp_mig) as hosp_mig, max(hosp_tb) as hosp_tb, max(hosp_anx) as hosp_anx
		, max(med_epispe_18) as med_epispe_18, max(med_epinonspe_18) as med_epinonspe_18
		, sum(nb_med_epispe_18) as nb_med_epispe_18, sum(nb_med_epinonspe_18) as nb_med_epinonspe_18
		, max(med_oh) as med_oh, max(med_dln) as med_dln, max(med_mig) as med_mig, max(med_tb) as med_tb, max(med_anx) as med_anx
		, max(med_clon_18) as med_clon_18, max(med_pgb_18) as med_pgb_18, max(med_pgbpsy_18) as med_pgbpsy_18
		, max(med_gbp_18) as med_gbp_18, max(med_tbpsy_18) as med_tbpsy_18, max(med_clonsimult_18) as med_clonsimult_18
		, min(first_ae_18_dte) format=ddmmyy10. as first_ae_18_dte, max(eeg_12m_18) as top_eeg
	from epi_cib.identite
	group by ben_idt_ano
;
quit;

proc sql;
alter table epi_cib.patient add 
	epi num(1), oh num(1), dln num(1), mig num(1), tb num(1), anx num(1), other_ind num(1)
	, dln2 num(1), tb2 num(1), anx2 num(1), med_epi_18 num(1), nb_epi_18 smallint
	, pop1 num(1), pop2 num(1), pop3 num(1), pop2b num(1)
;

update epi_cib.patient set 
	epi=max(ald_epi_18,hosp_epi_18)
	,med_epi_18=max(med_epispe_18,med_epinonspe_18)
	,nb_epi_18=sum(nb_med_epispe_18,nb_med_epinonspe_18)
	,oh=max(ald_oh,hosp_oh,med_oh)
	,dln=max(ald_dln,hosp_dln,med_dln)
	,mig=max(ald_mig,hosp_mig,med_mig)
	,tb=max(ald_tb,hosp_tb,med_tb)
	,anx=max(ald_anx,hosp_anx,med_anx)
	,dln2=case when med_clonsimult_18 then 1 else 0 end
	,anx2=case when med_clon_18=0 and med_pgbpsy_18=1 then 1 else 0 end
	,tb2=case when med_clon_18=0 and med_pgbpsy_18=0 and  med_tbpsy_18=1 then 1 else 0 end

;

update epi_cib.patient set other_ind=max(oh, dln, mig,tb,anx,dln2, anx2,tb2); 

update epi_cib.patient set 
	pop1=case when  epi=1 or (med_epi_18=1 and other_ind=0)  then 1 else 0 end;

update epi_cib.patient set 
	pop2=case when epi=1 or (nb_epi_18>=2 and top_eeg=1) then 1 else 0 end;

update epi_cib.patient set 
	pop2b=case when epi=1 or  nb_epi_18>=2 then 1 else 0 end;


update epi_cib.patient set 
	pop3=case when  epi=1 or med_epispe_18=1 then 1 else 0 end;


quit;

/* /!\ les populations finalement retenues pour l'analyse sont les pop1 et pop3 appelees dans le rapport 
population 1 (pour l'estimation haute) et population 2 (pour l'estimation basse) */



/******************************/
* qualite identite;
/*****************************/

proc sql;

/*
create table identite as 
	select i.*, p.pop1
	from epi_cib.identite i 
		left join epi_cib.patient p  on p.ben_idt_ano=i.ben_idt_ano;
;
*/

drop table epi_cib.identite_cible;
create table epi_cib.identite_cible as 
	select * from identite;

create index id on epi_cib.identite_cible(ben_nir_psa, ben_rng_gem);
create index ben_idt_ano on epi_cib.identite_cible(ben_idt_ano);

quit;


proc sql;

create table tmp as 
	select ben_nir_psa, count(distinct ben_idt_ano) as n
	from epi_cib.identite
	group by ben_nir_psa
	having count(distinct ben_idt_ano)>1
;

quit;




/*******************************************************************/
/* Transfert de la table dans orauser*/
/********************************************************************/




%KILL_ORAUSER(pop) ;

proc sql;
create table orauser.pop as
	select distinct i.* 
	from epi_cib.patient p 
		inner join epi_cib.identite i on i.ben_idt_ano=p.ben_idt_ano
	where p.pop1=1 or p.pop3=1;
run ;



%KILL_ORAUSER(pop_test) ;

proc sql;
create table orauser.pop_test as
	select distinct i.* 
	from epi_cib.patient p 
		inner join epi_cib.identite i on i.ben_idt_ano=p.ben_idt_ano
	where p.ald_epi_18=1 and p.hosp_epi_18=1;
run ;