/*****************************************************************************/
/*              ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                     */
/*****************************************************************************/

/********************************************************************/
/* Nom du programme : ALD_epi_autres */

/* Objectif : identifier les beneficiaires en ALD pour epilepsie 
et pour toutes les autres affections servant de criteres d'exclusion : 
migraine, douleur neropathique, dependance alcoolique, trouble bipolaire, anxiete */

/* Referentiel : codes CIM_10 */

/* Table de sortie : &lib_sel..ald_2017_2018 */
/********************************************************************/



/* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France */

/********************************************************************/
/* LES ARGUMENTS A MODIFIER SONT LES DEUX MACRO VARIABLES : LIST_CODES et AAAA */
/*liste de codes CIM a selectionner */
%let list_codes = G40 G41 F10 F31 F30 F40 F41 R52 G43; /* recherche sur 3 caracteres */


/************************************************************************************/
/************ PROGRAMME DE SELECTION DES ALD EXONERANTES                   **********/
/************************************************************************************/
/*Selection des ALD exonerantes par codes CIM dans le referentiel*/
%macro selection_ALD(AAAA=);

/*date de debut de la periode consideree */
%let debut_ALD=01JAN&AAAA.;

/*date de fin de la periode consideree*/
%let fin_ALD=31DEC&AAAA.;


/*date de point : date maximale d insertion des exonerations. 
Permet d avoir un etat des lieux des exonerations a une date precise et de toujours retrouver le meme resultat a differentes dates d execution du programme.
On estime que la quasi totalite des exonerations sont restituees 6 mois apres la fin de la periode d interet.
Le delai de 6 mois supprime 'a tort' un tres faible nombre de periodes d'ALD*/
%let anpt = %eval(&AAAA + 1);
%let date_point=01JUL&anpt.;
%let nb_codes=%sysfunc(countw(&list_codes));


	/* Selection des ALD exonerantes en SQL code par code (+ rapide) - 1 table par code*/
	%do i=1 %to &nb_codes;
		%let code=%scan((&list_codes),&i);
		%put "&code.%";

		proc sql;
			create table ALD&i as 
			select * , /* partie ajoutee a la macro originelle pour pouvoir filtrer sur la pathologie */ case when &i in (1,2) then 'EPI' when &i=3 then 'OH' when &i in (4,5) then 'TB' when &i in(6,7) then 'ANX' when &i=8 then 'DLN' when &i=9 then 'MIG' else 'NA' end as TOP_ALD
			from oravue.ir_imb_r 
			where med_mtf_cod like "&code.%" 
				and imb_etm_nat in (41,43,45) 
				and ins_dte<"&date_point.:0:0:0"dt;
		quit;

	%end;

	/*regroupement de toutes les tables*/
	data ALD_&AAAA;
		set %do i=1 %to &nb_codes;
				ALD&I
			%end; ;
	run;

	%do i = 1 %to &nb_codes;
		proc datasets library=work;
			delete ALD&i.;
		run;
	%end;


/*Tri pour ne garder que les lignes correspondant a la date d'insertion la plus recente pour chaque code et motif*/
/*Remarque :lorsque l on ne s interesse qu a une seule pathologie, il n est pas necessaire de trier et de supprimer les doublons sur la variable med_mtf_cod */
proc sort data=ALD_&AAAA;
	by ben_nir_psa ben_rng_gem med_mtf_cod imb_etm_nat descending ins_dte descending upd_dte imb_ald_dtd imb_ald_dtf;
run;

proc sort data=ALD_&AAAA nodupkey;
	by ben_nir_psa ben_rng_gem med_mtf_cod imb_etm_nat;
run;

/*Restriction des ALD sur la periode d'interet*/
data ALD_&AAAA._exo_12mois;
	set ALD_&AAAA.;
	YEAR=&AAAA.;
	where imb_ald_dtd<="&fin_ALD.:0:0:0"dt and (imb_ald_dtf>="&debut_ALD.:0:0:0"dt or imb_ald_dtf=. or imb_ald_dtf='01JAN1600:0:0:0'dt);
run;

%mend;


/* Appel de la macro sur la periode d'etude */
%selection_ALD(AAAA=2017);

%selection_ALD(AAAA=2018);

%selection_ALD(AAAA=2019);


/* Union des trois tables annuelles */
/* Note : doublons possible de patients si ALD sur plus d'une annee */

proc sql;
   CREATE TABLE libepico.ald_2017_19 AS 
   select * from ALD_2017_exo_12mois
   UNION  
   select * from ALD_2018_exo_12mois
   UNION 
   select * from ALD_2019_exo_12mois;
quit;