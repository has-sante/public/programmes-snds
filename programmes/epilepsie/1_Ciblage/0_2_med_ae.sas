/********************************************************************/
/*           ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE               */
/********************************************************************/

/********************************************************************/
/* Nom du programme : med_AE */

/* Objectif : identifier les beneficiaires ayant eu une delivrance 
de medicaments antiepileptiques en pharmacie de ville 
de medicaments antiepileptiques en pharmacie hospitaliere (retrocession) 
de medicaments antiepileptiques au cours d'un sejour hospitalier 

/* Referentiel : Fichier excel cod_med */
/* Referentiel a charger dans ORAUSER ==> orauser.liste_medicaments */
/* Referentiel datant du 22 décembre 2021 */

/* Repertoire : lib_sel
/* Tables de sortie intermediaires : 
&lib_sel..table_medicaments_pha, 
&lib_sel..table_medicaments_ucd, 
&lib_sel..med_atu_2017_19 ==> vide = 0 beneficiaire
Table finale : &lib_sel..med_epi_2017_19  */

/********************************************************************/

/*********************************************************************************/
/************           SELECTION DES REMBOURSEMENTS DE MEDICAMENTS        *******/
/************                    DANS DCIR ARCHIVE ET DCIR   - PHA         *******/
/*********************************************************************************/


/* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France */


/*********************************************************************************/
/* DONNEES en entree 
				oravue.er_prs_f_&an ou oravue.er_prs_f
				oravue.er_pha_f_&an ou oravue.er_pha_f
				oravue.IR_PHA_R
				&table_population en entree (facultatif)*/


/* DONNEES en sortie
				&table_sortie a definir

/* PLAN du PROGRAMME ;
	  	- Definition par l'utilisateur des parametres suivants:
			* Choix de la librairie de donnees en sortie
			* options obs=0 ou MAX (respectivement pour tester le programme ou lancer la selection)
			* choix des codes de medicaments
			* liste des variables a selectionner
			* table de population a croiser (facultatif)
	  	- Macro outils
	  	- Selection des medicaments 
		- Copie de la table de population (facultatif) dans ORAUSER
	  	- Macro de selection des medicaments pour un mois de flux : CREA_TABLE
	  	- Macro de compilation des mois de flux : COMPILATION
	  	- Macro de suppression des regularisations : REGULARISATIONS
	  	- Macro TAB_MEDICAMENTS qui execute les macros CREA_TABLE et COMPILATION  pour chaque mois de flux et REGULARISATION sur la table finale
		- L'utilisateur lance la Macro TAB_MEDICAMENTS en choisissant
		    * le mois et annee de debut d'execution des soins (format aaaamm)
			* le mois et annee de fin d'execution des soins (format aaaamm)
			* le nom de la table de sortie
	 

/*********************************************************************************/


/*********************************************************************************/
/*****  Parametres a changer : repertoire,  liste des variables, des codes  *****/
/*********************************************************************************/


  
OPTIONS OBS  =MAX; /* 0 pour tester la requete ou OPTIONS OBS = MAX pour l executer sur les
 donnees */

/*Criteres de selection des medicaments a extraire du referentiel pharmacie (cf communique du 24 janvier 2012)*/
/*%LET codes = ;
/* 		EXEMPLE 1 : Selection par CIP 13
			pha_cip_c13 in (3400933226558,3400934744198,3400926939939)
		EXEMPLE 2 : Selection par CIP 7 
			pha_prs_ide  in ( 3876282, 3517778, 3383272 ) 
		EXEMPLE 3 : Selection par code ATC  
			pha_atc_c03 like 'A10' and not(pha_atc_c07='A10BX06') 
		EXEMPLE 4 : Selection par principe actif
			pha_nom_pa like %METFORMINE%
		exemple 5 : Selection par CIP 7 ou ATC 7
			pha_prs_ide  in ( 3876282, 3517778, 3383272 ) or  pha_atc_C07  in ("J01AA02", "J01FA01" ,"J01MA01")	
		

/*liste des variables a conserver (Ne pas changer les prefixes p., pha.,  ir.)*/

/* variables de la table des prestations*/
%LET liste_var_prs =
		p.ben_nir_psa,	
		p.ben_rng_gem,
		p.exe_soi_dtd,		
		p.ben_res_dpt, 
		p.org_aff_ben,
		p.pse_spe_cod,
		p.psp_spe_cod
		/*et autres variables selon le besoin*/;

/* variables de la table d actes affinee  = ici pharmacie ER_PHA_F*/
%LET liste_var_pha =
       	pha.pha_prs_c13 ,  
		pha.pha_prs_ide 
		/*et autres variables selon le besoin*/;

/* variables du referentiel  = ici referenciel pharmacie IR_PHA_R*/
%LET liste_var_ir =
		ir.pha_atc_c03,  
		ir.pha_atc_c07 
		/*et autres variables selon le besoin*/;

/* variables de la table medicament importee  = ici cod_med */
%LET liste_var_med = 
		med.ind_med,
		med.dci,
		med.specialite,
		med.atc7,
		med.dosage,
		med.dose_par_bte_mg,
		med.nb_dose_bte,
		med.med_spe_epi,
		med.class_ae
		/*et autres variables selon le besoin*/;

/*1ere annee de la table ER_PRS_F*/
%LET an_DCIR = 2017 ; 

/*Facultatif: Pour extraire les remboursements de medicaments d'une population deja constituee, et non de la population francaise entiere
Attention: 1 ligne = 1 individu 
Variables obligatoires : BEN_NIR_PSA, BEN_RNG_GEM
Si vous n'avez pas de table de population au depart: %let table_pop=  , sinon =nom de la table*/
%LET table_pop = ;


/**********************************/
/********    MACRO OUTILS    ******/
/**********************************/

/* 9  cles techniques  */
%MACRO CLE_TEC ( p, d ) ;
&p..DCT_ORD_NUM = &d..DCT_ORD_NUM and
&p..FLX_DIS_DTD = &d..FLX_DIS_DTD and
&p..FLX_EMT_NUM = &d..FLX_EMT_NUM and
&p..FLX_EMT_ORD = &d..FLX_EMT_ORD and
&p..FLX_EMT_TYP = &d..FLX_EMT_TYP and
&p..FLX_TRT_DTD = &d..FLX_TRT_DTD and
&p..ORG_CLE_NUM = &d..ORG_CLE_NUM and
&p..prs_ORD_NUM = &d..prs_ORD_NUM and 
&p..REM_TYP_AFF = &d..REM_TYP_AFF
%MEND ;


/* Lorsque l on a besoin de fusionner une table personnelle avec une table oracle, il faut creer une table personnelle dans ORAUSER, 
   avant cela il faut s assurer qu une table avec le meme nom n existe pas deja ;  le cas echeant il faut supprimer cette table existante */
%MACRO KILL_ORAUSER ( table ) ;
%IF %SYSFUNC( exist( ORAUSER.&table )) %THEN %DO ;
	PROC SQL;
	DROP TABLE ORAUSER.&table;
	QUIT; 
%END ;
%MEND ;





/***************************************************************/
/****       CHOIX DES MEDICAMENTS DANS UN REFERENTIEL       ****/
/***************************************************************/

%KILL_ORAUSER( liste_medicaments ) ;

/* A partir de la table ir_pha */
/*
PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	ORAVUE.IR_PHA_R
	WHERE 	&codes 
	;
QUIT ;*/

/* Ou selon une table existante*/


PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	libepico.COD_MED where med_epi=1
	;
QUIT ;



/***************************************************************/
/******      OPTION : CHOIX d'UNE TABLE de POPULATION     ******/
/***************************************************************/
/* NON CONCERNE */

%MACRO table_pop() ;
%IF &table_pop ne  %THEN %DO ;
	%PUT Table de population = &table_pop. ;
	%KILL_ORAUSER(pop) ;
	DATA orauser.pop ;
	SET &table_pop ;
	RUN ;
	%MACRO croisement_tab_pop() ;
		inner join orauser.pop pop
		on pop.ben_nir_psa=p.ben_nir_psa and pop.ben_rng_gem=p.ben_rng_gem
	%MEND ;
%END ;
%ELSE %DO ;
	%PUT no_table ;
	%MACRO croisement_tab_pop() ;
	%MEND ;
%END ;
%MEND ;
%table_pop ;


/***************************************************************/
/************        SELECTION DES PRESTATIONS       ***********/
/***************************************************************/ 
/*** Macro de creation d'une table de prestations mensuelle ***/

%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) D�but %SYSFUNC( datetime(),datetime. ) ;

/* Suppression de la table de prestations mensuelle si existante */
%IF %SYSFUNC ( exist( table_mois )) %THEN %DO ;
	PROC DELETE DATA = table_mois ;
	RUN ;
%END ;

PROC SQL; 
CREATE TABLE table_mois  as 
SELECT  	&liste_var_prs,  
		&liste_var_pha,
		&liste_var_med,
		pha.pha_act_qsn
FROM    &table_prs as p	    %croisement_tab_pop /*ne fait rien si pas de table de population a croiser*/
		INNER JOIN &table_pha as pha
			ON %CLE_TEC (p,pha)
		INNER JOIN orauser.liste_medicaments as med
			ON 	&CONDITION_JOINTURE_MEDICAMENTS
		LEFT JOIN &table_ete as etab
		ON %CLE_TEC(p, etab)
WHERE   p.flx_dis_dtd ="&FLX_DIS_DTD:0:0:0"dt
AND (p.exe_soi_dtd between "&DATE_EXE_DEB:0:0:0"dt and "&DATE_EXE_FIN:0:0:0"dt ) /* selection de la periode d execution du soin*/
AND (p.DPN_QLF <> 71 AND p.PRS_DPN_QLP <> 71) /*exclusion des soins externes transmis pour info*/
		AND (p.CPL_MAJ_TOP<2)                   /*exclusion des majorations*/
AND (etab.ETE_IND_TAA <> 1 OR etab.ETE_IND_TAA is missing) ; /* exclusion des ACE des sejours en hopitaux publics en T2A */

QUIT ;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles ****/

%MACRO COMPILATION() ;
%IF %SYSFUNC(exist( compil )) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change table_mois=compil;
quit;
%END ;
%ELSE %DO;
proc append base=compil data=table_mois FORCE;run;
proc delete data=table_mois;run;
%END;
%MEND COMPILATION ;

/*** Macro de suppression des regularisations ***/

%MACRO REGULARISATIONS( table_sortie );

/*liste des variables (hors variables quantite) sans les suffixes*/
%let liste_var_regul=%qsysfunc(tranwrd(%quote(&liste_var_prs),p.,)),%qsysfunc(tranwrd(%quote(&liste_var_pha),pha., )),%qsysfunc(tranwrd(%quote(&liste_var_med),med., ));
 
PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	distinct	
			&liste_var_regul,
			sum(pha_act_qsn) as pha_qt
FROM	compil 
GROUP BY	&liste_var_regul /* liste du GROUP_BY = liste du SELECT a l'exception des variables calculees pour eviter la generation de doublons */
HAVING CALCULATED	pha_qt>0 ;
QUIT ;
%MEND REGULARISATIONS;

/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO TAB_MEDICAMENTS( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;

/*jointure par code CIP13 ou code CIP7 selon l'annee*/
	%IF  &an<2014 %THEN %LET CONDITION_JOINTURE_MEDICAMENTS=(pha.pha_prs_ide=med.cip7);
	%ELSE %LET CONDITION_JOINTURE_MEDICAMENTS=(pha.pha_prs_c13=med.cip13);
		
				 
	/*DCIR archive*/
	%IF &an<&an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f_&an ;
		%LET table_pha = oravue.er_pha_f_&an ;
		%LET table_ete = oravue.er_ete_f_&an ;
	%END;
	/*DCIR courant*/
	%IF  &AN >= &an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f  ;
		%LET table_pha = oravue.er_pha_f  ;
		%LET table_ete = oravue.er_ete_f ;
	%END;

	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT &table_prs ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;
%PUT CONDITION_JOINTURE_MEDICAMENTS=&CONDITION_JOINTURE_MEDICAMENTS;
	 %CREA_TABLE(&FLX_DIS_DTD) ;
	 %COMPILATION() ;			
%END ;
     %REGULARISATIONS( &table_sortie ) ;
%MEND TAB_MEDICAMENTS;

/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;
 %TAB_MEDICAMENTS(	 Date_DEB = 201701,Date_FIN = 201912,table_sortie = libepico.table_medicaments_pha ) ;




/*********************************************************************************/
/************           SELECTION DES REMBOURSEMENTS DE MEDICAMENTS        *******/
/************                    DANS DCIR ARCHIVE ET DCIR - UCD           *******/
/*********************************************************************************/


 /* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France 
macro adaptee pour la selection des prestations associees a des delivrances de medicaments de la table ER_UCD_F*/

 /*********************************************************************************/
/* DONNEES en entree 
				oravue.er_prs_f_&an ou oravue.er_prs_f
				oravue.er_ucd_f_&an ou oravue.er_ucd_f
				oravue.IR_PHA_R
				orauser.liste_medicaments
				&table_population en entree (facultatif)
	

/* DONNEES en sortie
				&table_sortie a definir

/* PLAN du PROGRAMME ;
	  	- Definition par l'utilisateur des parametres suivants:
			* Choix de la librairie de donnees en sortie
			* options obs=0 ou MAX (respectivement pour tester le programme ou lancer la selection)
			* choix des codes de medicaments
			* liste des variables a selectionner
			* table de population a croiser (facultatif)
	  	- Macro outils
	  	- Selection des medicaments 
		- Copie de la table de population (facultatif) dans ORAUSER
	  	- Macro de selection des medicaments pour un mois de flux : CREA_TABLE
	  	- Macro de compilation des mois de flux : COMPILATION
	  	- Macro de suppression des regularisations : REGULARISATIONS
	  	- Macro TAB_MEDICAMENTS qui execute les macros CREA_TABLE et COMPILATION  pour chaque mois de flux et REGULARISATION sur la table finale
		- L'utilisateur lance la Macro TAB_MEDICAMENTS en choisissant
		    * le mois et annee de debut d'execution des soins (format aaaamm)
			* le mois et annee de fin d'execution des soins (format aaaamm)
			* le nom de la table de sortie
	 

/*********************************************************************************/


/*********************************************************************************/
/*****  Parametres a changer : repertoire,  liste des variables, des codes  *****/
/*********************************************************************************/

/*declaration de la librairie */
/* exemple : sous repertoire PGM_tmt  de l espace partage ESP_PART*/
/*
%let fichier = %SYSGET (HOME)/sasdata;
libname libepico "&fichier/REPEPICO/EXTRACTION";
  */

OPTIONS OBS  =MAX; /*pour tester la requete ou OPTIONS OBS = MAX pour l executer sur les
 donnees */

/*Criteres de selection des medicaments  a extraire du referentiel pharmacie (cf communique du 24 janvier 2012)*/
%LET codes =  ;
/* 		EXEMPLE 1 : Selection par UCD7
			pha_cip_ucd in( 9390830, 9390824, 9373406 ) 
		EXEMPLE 2 : Selection par code ATC  
			pha_atc_c03 like 'A10' and not(pha_atc_c07='A10BX06') 
		EXEMPLE 3 : Selection par principe actif
			pha_nom_pa like %METFORMINE%
		exemple 4 : Selection par UCD 7 ou ATC 7*
			pha_cip_ucd in( 9390830, 9390824, 9373406 ) or  pha_atc_C07  in ("J01AA02", "J01FA01" ,"J01MA01")	*/

/*liste des variables a conserver (Ne pas changer les prefixes p., ucd.,  ir., med.)*/

/* variables de la table des prestations*/
%LET liste_var_prs =
		p.ben_nir_psa,	
		p.ben_rng_gem,
		p.exe_soi_dtd,		
		p.ben_res_dpt, 
		p.org_aff_ben,
		p.pse_spe_cod,
		p.psp_spe_cod
		/*et autres variables selon le besoin*/;

/* variables de la table d actes affinee  = ici pharmacie ER_UCD_F*/
%LET liste_var_ucd=
		ucd.ucd_ucd_cod
		/*et autres variables selon le besoin*/;

/* variables du referentiel  = ici referenciel pharmacie IR_PHA_R*/
%LET liste_var_ir =
		ir.pha_atc_c03,  
		ir.pha_atc_c07,
		ir.pha_cip_ucd 
		/*et autres variables selon le besoin*/;

/* variables de la table medicament importee  = ici codes_med_all_v20220222 */
%LET liste_var_med = 
		med.ind_med,
		med.dci,
		med.specialite,
		med.atc7,
		med.ucd0,
		med.dosage,
		med.dose_par_bte_mg,
		med.nb_dose_bte,
		med.med_spe_epi,
		med.class_ae
		/*et autres variables selon le besoin*/;

/*1ere annee de la table ER_PRS_F*/
%LET an_DCIR = 2016 ; 

/*Facultatif: Pour extraire les remboursements de medicaments d'une population deja constituee, et non de la population francaise entiere
Attention: 1 ligne = 1 individu 
Variables obligatoires : BEN_NIR_PSA, BEN_RNG_GEM
Si vous n'avez pas de table de population au depart: %let table_pop=  , sinon =nom de la table*/
%LET table_pop = ;


/**********************************/
/********    MACRO OUTILS    ******/
/**********************************/

/* 9  cles techniques  */
%MACRO CLE_TEC ( p, d ) ;
&p..DCT_ORD_NUM = &d..DCT_ORD_NUM and
&p..FLX_DIS_DTD = &d..FLX_DIS_DTD and
&p..FLX_EMT_NUM = &d..FLX_EMT_NUM and
&p..FLX_EMT_ORD = &d..FLX_EMT_ORD and
&p..FLX_EMT_TYP = &d..FLX_EMT_TYP and
&p..FLX_TRT_DTD = &d..FLX_TRT_DTD and
&p..ORG_CLE_NUM = &d..ORG_CLE_NUM and
&p..prs_ORD_NUM = &d..prs_ORD_NUM and 
&p..REM_TYP_AFF = &d..REM_TYP_AFF
%MEND ;


/* Lorsque l on a besoin de fusionner une table personnelle avec une table oracle, il faut creer une table personnelle dans ORAUSER, 
   avant cela il faut s assurer qu une table avec le meme nom n existe pas deja ;  le cas echeant il faut supprimer cette table existante */
%MACRO KILL_ORAUSER ( table ) ;
%IF %SYSFUNC( exist( ORAUSER.&table )) %THEN %DO ;
	PROC SQL;
	DROP TABLE ORAUSER.&table;
	QUIT; 
%END ;
%MEND ;





/***************************************************************/
/****     CHOIX DES MEDICAMENTS DANS LE REFERENTIEL         ****/
/***************************************************************/

%KILL_ORAUSER( liste_medicaments ) ;

/*
PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	ORAVUE.IR_PHA_R
	WHERE 	&codes 
	;
QUIT ;
*/

/* Ou selon une table existante*/

/* Table chargee au debut du programme*/

/*
PROC SQL; 
	CREATE TABLE ORAUSER.liste_medicaments  AS
	SELECT	* 
	FROM 	orauser.CODES_MED_ALL_V20220222 where med_epi=1
	;
QUIT ;
*/

/***************************************************************/
/******      OPTION : CHOIX d'UNE TABLE de POPULATION     ******/
/***************************************************************/

%MACRO table_pop() ;
%IF &table_pop ne  %THEN %DO ;
	%PUT Table de population = &table_pop. ;
	%KILL_ORAUSER(pop) ;
	DATA orauser.pop ;
	SET &table_pop ;
	RUN ;
	%MACRO croisement_tab_pop() ;
		inner join orauser.pop pop
		on pop.ben_nir_psa=p.ben_nir_psa and pop.ben_rng_gem=p.ben_rng_gem
	%MEND ;
%END ;
%ELSE %DO ;
	%PUT no_table ;
	%MACRO croisement_tab_pop() ;
	%MEND ;
%END ;
%MEND ;
%table_pop ;





/***************************************************************/
/************        SELECTION DES PRESTATIONS       ***********/
/***************************************************************/ 
/*** Macro de creation d'une table de prestations mensuelle ***/

%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) D�but %SYSFUNC( datetime(),datetime. ) ;

/* Suppression de la table de prestations mensuelle si existante */
%IF %SYSFUNC ( exist( table_mois )) %THEN %DO ;
	PROC DELETE DATA = table_mois ;
	RUN ;
%END ;

PROC SQL;
CREATE TABLE table_mois  as 
SELECT  	&liste_var_prs,  
		&liste_var_ucd,
		&liste_var_med,
		ucd.ucd_dlv_nbr
FROM    &table_prs as p	    %croisement_tab_pop /*ne fait rien si pas de table de population a croiser*/
		INNER JOIN &table_ucd as ucd
			ON %CLE_TEC (p,ucd)
		INNER JOIN orauser.liste_medicaments as med
			ON 	&CONDITION_JOINTURE_MEDICAMENTS
		LEFT JOIN &table_ete as etab
		ON %CLE_TEC(p,etab)
WHERE   p.flx_dis_dtd ="&FLX_DIS_DTD:0:0:0"dt
AND (p.exe_soi_dtd between "&DATE_EXE_DEB:0:0:0"dt and "&DATE_EXE_FIN:0:0:0"dt ) /* selection de la periode d execution du soin*/
AND (p.DPN_QLF <> 71 AND p.PRS_DPN_QLP <> 71) /*exclusion des soins externes transmis pour info*/
		AND (p.CPL_MAJ_TOP<2)                   /*exclusion des majorations*/
AND (etab.ETE_IND_TAA <> 1 OR etab.ETE_IND_TAA is missing) ; /* exclusion des ACE des sejours en hopitaux publics en T2A */

QUIT ;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles ****/

%MACRO COMPILATION() ;
%IF %SYSFUNC(exist( compil )) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change table_mois=compil;
quit;
%END ;
%ELSE %DO;
proc append base=compil data=table_mois FORCE;run;
proc delete data=table_mois;run;
%END;
%MEND COMPILATION ;

/*** Macro de suppression des regularisations ***/

%MACRO REGULARISATIONS( table_sortie );

/*liste des variables (hors variables quantite) sans les suffixes*/
%let liste_var_regul=%qsysfunc(tranwrd(%quote(&liste_var_prs),p.,)),%qsysfunc(tranwrd(%quote(&liste_var_ucd),ucd., )),%qsysfunc(tranwrd(%quote(&liste_var_med),med., ));
 
PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	distinct	
			&liste_var_regul,
			sum(ucd_dlv_nbr) as ucd_qt
FROM	compil 
GROUP BY	&liste_var_regul /* liste du GROUP_BY = liste du SELECT a l'exception des variables calculees pour eviter la generation de doublons */
HAVING CALCULATED	ucd_qt>0 ;
QUIT ;
%MEND REGULARISATIONS;

/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO TAB_MEDICAMENTS( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;

/*jointure par code UCD selon l'annee*/
	%LET CONDITION_JOINTURE_MEDICAMENTS=(ucd.ucd_ucd_cod=med.ucd0);
		
				 
	/*DCIR archive*/
	%IF &an<&an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f_&an ;
		%LET table_ucd = oravue.er_ucd_f_&an ;
		%LET table_ete = oravue.er_ete_f_&an ;
	%END;
	/*DCIR courant*/
	%IF  &AN >= &an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f  ;
		%LET table_ucd = oravue.er_ucd_f  ;
		%LET table_ete = oravue.er_ete_f ;
	%END;

	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT &table_prs ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;
%PUT CONDITION_JOINTURE_MEDICAMENTS=&CONDITION_JOINTURE_MEDICAMENTS;
	 %CREA_TABLE(&FLX_DIS_DTD) ;
	 %COMPILATION() ;			
%END ;
     %REGULARISATIONS( &table_sortie ) ;
%MEND TAB_MEDICAMENTS;

/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;
 %TAB_MEDICAMENTS(	 Date_DEB = 201701,Date_FIN = 201912,table_sortie = libepico.table_medicaments_ucd ) ;






/*********************************************************************************/
/************           SELECTION DES REMBOURSEMENTS DE MEDICAMENTS        *******/
/************                    DANS PMSI MCO - UCD           *******/
/*********************************************************************************/
 
/* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France */
/* Permet la selection de sejours a partir de codes CIM-10 ; 
macro adaptee pour selection de sejours avec delivrance de medicaments en ATU */

/********************************************************************************/

/* liste des finess geographiques APHP, APHM et HCL a supprimer pour eviter les doublons */
%let finess_out = '130780521' '130783236' '130783293' '130784234' '130804297'
                  '600100101' '750041543' '750100018' '750100042' '750100075'
                  '750100083' '750100091' '750100109' '750100125' '750100166'
                  '750100208' '750100216' '750100232' '750100273' '750100299'
                  '750801441' '750803447' '750803454' '910100015' '910100023'
                  '920100013' '920100021' '920100039' '920100047' '920100054'
                  '920100062' '930100011' '930100037' '930100045' '940100027'
                  '940100035' '940100043' '940100050' '940100068' '950100016'
                  '690783154' '690784137' '690784152' '690784178' '690787478'
                  '830100558';


/******* creation de la liste des identifiants des sejours(eta_num/rsa_num) correspondant aux codes UCD souhaites *******/
%macro select_etanum_rsanum (aaaa=);
select distinct eta_num, rsa_num
from oravue.t_mco&aa.medatu 
where ucd_ucd_cod in (select ucd0 from orauser.liste_medicaments)

%mend select_etanum_rsanum;

/******* Generation d'un fichier comprenant pour les sejours selectionnes par la macro select_etanum_rsanum *******/
/******* des variables issues des tables T_MCOaaB, T_MCOaaC, T_MCOaaMEDATU, et T_MCOaaE                               *******/
/******* le fichier en sortie comprend une ligne par sejour                                                 *******/
%macro select_mco_atu (aaaa=);
%let aa = %substr(&aaaa.,3,2);

/**** tables T_MCOaaB (description du sejour hospitalier) + T_MCOaaC (table de chainage) ****/
proc sql;
    create table tmp1_bc_&aaaa. as
    select t1.eta_num, t1.rsa_num, t2.sej_num, t1.sej_nbj length=3,
           t1.nbr_dgn length=3, t1.nbr_rum length=3, t1.nbr_act length=3, 
           t1.ent_mod, t1.ent_prv, t1.sor_mod, t1.sor_des, t1.sor_ann, t1.sor_moi, 
           %if &aaaa. >= 2009 %then t2.exe_soi_dtd, t2.exe_soi_dtf, 
           t1.dgn_pal as dp, t1.dgn_rel as dr, t1.grg_ghm, t1.bdi_dep, t1.bdi_cod, 
           t1.cod_sex, t1.age_ann length=3, t1.age_jou length=3, t2.nir_ano_17,
           /* liste des codes retours */ 
           t2.fho_ret, t2.nai_ret, t2.nir_ret, t2.pms_ret, t2.sej_ret, t2.sex_ret
           %if &aaaa. >= 2006 %then , t2.dat_ret ;
           %if &aaaa. >= 2013 %then , t2.coh_nai_ret, t2.coh_sex_ret ;
    from oravue.t_mco&aa.b as t1,
         oravue.t_mco&aa.c as t2,
         (%select_etanum_rsanum(aaaa=&aaaa.)) as t3
    where (t1.eta_num = t3.eta_num and t1.rsa_num = t3.rsa_num) and
          (t2.eta_num = t3.eta_num and t2.rsa_num = t3.rsa_num)         
    order by t1.eta_num, t1.rsa_num;



/**** table T_MCOaaMEDATU (medicaments delivres) ****/
proc sql;
    create table tmp1_atu_&aaaa. as
    select t1.eta_num, t1.rsa_num, t1.DAT_DELAI, t1.INI_VAL_PRS, t1.UCD_UCD_COD, t1.adm_nbr
    from oravue.t_mco&aa.medatu as t1,
         (%select_etanum_rsanum(aaaa=&aaaa.)) as t3
    where (t1.eta_num = t3.eta_num and t1.rsa_num = t3.rsa_num)             
    order by t1.eta_num, t1.rsa_num;
quit;


/**** table T_MCOaaE (etablissements) ****/
proc sql;
    create table tmp1_e_&aaaa. as
    select eta_num, soc_rai, sta_eta
    from oravue.t_mco&aa.e
    order by eta_num;
quit;

/**** table FINALE : rassembler les differentes tables ****/
/**** un enregistrement par sejour hospitalier ****/
data tmp3_&aaaa.;
    merge tmp1_bc_&aaaa. (in= tab)
          tmp1_atu_&aaaa.
    by eta_num rsa_num;
    if tab;
run;

data tmp4_&aaaa.;
    merge tmp3_&aaaa. (in= tab)
          tmp1_e_&aaaa.;
    by eta_num;
    if tab;
run;

data mco_atu_&aaaa. (drop= tmp:);
    set tmp4_&aaaa. (rename= (sej_num = tmp_sejnum
                              %if &aaaa. >= 2009 %then %do;
                                  exe_soi_dtd = tmp_exesoidtd
                                  exe_soi_dtf = tmp_exesoidtf
                              %end;));
    year=&aaaa.; /* information sur l'annee de l'extraction*/
    /* creation de la variable cr_ok :1=ensemble des codes retours valides / 0=au moins 1 code retour errone */
    length cr_ok $ 1;
    if nir_ret = '0' and nai_ret = '0' and sex_ret = '0' and
       sej_ret = '0' and fho_ret = '0' and pms_ret = '0' 
       %if &aaaa. >= 2006 %then and dat_ret = '0' ;
       %if &aaaa. >= 2013 %then and coh_nai_ret = '0' and coh_sex_ret = '0' ;
        then cr_ok = '1';
    else do;
        cr_ok = '0';
        nir_ano_17 = 'XXXXXXXXXXXXXXXXX';
        tmp_sejnum = 'XXXX';
    end;

    /* le numero de sejour (SEJ_UM) est modifie pour les annees 2006 a 2008 afin de permettre l eventuel chainage 
    des sejours d un meme patient sur une periode incluant les annees 2009 et suivantes */ 
    length sej_num 4;
    %if &aaaa. = 2005 %then %do;
        if tmp_sejnum = 'XXXX' then call missing(sej_num);
        else sej_num = input(tmp_sejnum, $4.);
    %end;
    %else %if 2006 <= &aaaa. and &aaaa. <= 2008 %then %do;
        if tmp_sejnum = 'XXXX' then call missing(sej_num);
        else sej_num = input(tmp_sejnum, $4.) + 18263;
    %end;
    %else %if &aaaa. >= 2009 %then %do;
        if tmp_sejnum = 'XXXXX' then call missing(sej_num);
        else sej_num = input(tmp_sejnum, $5.);
    %end;

    /* dates entieres de soins : disponible a partir de 2009 */
    %if &aaaa. >= 2009 %then %do;
        length soin_dtd soin_dtf 6;
        soin_dtd = datepart(tmp_exesoidtd);
        soin_dtf = datepart(tmp_exesoidtf);
        format soin_dtd soin_dtf ddmmyy10.;
    %end;

    /* age = 0 */
    if missing(age_ann) and not missing(age_jou) then age_ann = 0;

    /* suppression des sejours en erreur (GHM commencant par 90) */
    if grg_ghm =: "90" then delete;
    
    /* suppression des Finess en doublon */
    if eta_num in (&finess_out.) then delete;
run;

/**** nettoyage des tables temporaires ****/
proc datasets lib=work nolist nodetails;
    delete tmp: ;
run;
%mend select_mco_atu ;


/*Lancement de la macro */

/* Annee d'inclusion des patients */
%select_mco_atu(aaaa=2018);

/* Annee anterieure pour definir prevalence incidence */
%select_mco_atu(aaaa=2017);

/* Annee posterieure pour suivi */
%select_mco_atu(aaaa=2019);


/*Union des 3 tables */
proc sql;
   CREATE TABLE libepico.med_atu_2017_19 AS 
   select * from mco_atu_2017
   UNION  
   select * from mco_atu_2018
   UNION 
   select * from mco_atu_2019;
quit;


/***********************************************************************************/
/************           CONSTITUTION TABLE FINAL DE DEL MEDICAMENTS  AE      *******/
/***********************************************************************************/



/* UNION DES TABLES INTERMEDIAIRES libepico.table_medicaments_pha et libepico.table_medicaments_ucd 
pour avoir une TABLE FINALE libepico.med_epi_2017_19 */

/* Note : table LIBEPICO.med_atu_2017_19 non fusionnee car vide (0 beneficiaire) */


Proc sort data=libepico.table_medicaments_pha; 
by ben_nir_psa ben_rng_gem;
run;

Proc sort data=libepico.table_medicaments_ucd;
by ben_nir_psa ben_rng_gem;
run;

data libepico.med_epi_2017_19;
merge libepico.table_medicaments_pha libepico.table_medicaments_ucd;
by ben_nir_psa ben_rng_gem;
run;