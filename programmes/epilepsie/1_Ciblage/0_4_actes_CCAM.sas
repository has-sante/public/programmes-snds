/*****************************************************************************************/
/* 	                              ETUDE PRATIQUE EPILEPSIE                               */
/*****************************************************************************************/


/*****************************************************************************************/
/* Nom du programme : actes_ccam */

/* Objectif : identifier les beneficiaires ayant eu un acte d'EEG */

/* Referentiel : cod_ccam dans data_ref.sas */

/* Repertoire : lib_sel
/* Table de sortie : &lib_sel..er_ccam */

/*****************************************************************************************/

/* Macro recuperee sur l'espace partage de SPF :
https://gitlab.com/healthdatahub/programmes-snds/-/tree/master/Sante_publique_France */
/* Macro adapte pour la selection des prestations associees 
a la realisation d'un acte CCAM en liberale (hors ACE) */


/*********************************************************************************/
/************           SELECTION DES ACTES EEG        *******/
/************            DANS    DCIR   - CAM         *******/
/*********************************************************************************/

/*********************************************************************************/
/* DONNEES en entree 
				oravue.er_prs_f_&an ou oravue.er_prs_f
				oravue.er_cam_f_&an ou oravue.er_cam_f
				&table_population en entree (facultatif)*/


/* DONNEES en sortie
				&table_sortie a definir

/* PLAN du PROGRAMME ;
	  	- Definition par l'utilisateur des parametres suivants:
			* Choix de la librairie de donnees en sortie
			* options obs=0 ou MAX (respectivement pour tester le programme ou lancer la selection)
			* choix des codes ccam
			* liste des variables a selectionner
			* table de population a croiser (facultatif)
	  	- Macro outils
	  	- Selection des codes ccam 
		- Copie de la table de population (facultatif) dans ORAUSER
	  	- Macro de selection des codes ccam pour un mois de flux : CREA_TABLE
	  	- Macro de compilation des mois de flux : COMPILATION
	  	- Macro de suppression des regularisations : REGULARISATIONS
	  	- Macro TAB_CCAM qui execute les macros CREA_TABLE et COMPILATION  pour chaque mois de flux et REGULARISATION sur la table finale
		- L'utilisateur lance la Macro TAB_CCAM en choisissant
		    * le mois et annee de debut d'execution des soins (format aaaamm)
			* le mois et annee de fin d'execution des soins (format aaaamm)
			* le nom de la table de sortie
	 

/*********************************************************************************/


/*********************************************************************************/
/*****  Parametres a changer : repertoire,  liste des variables, des codes  *****/
/*********************************************************************************/


  
OPTIONS OBS  =MAX; /* 0 pour tester la requete ou OPTIONS OBS = MAX pour l executer sur les
 donnees */


/*liste des variables a conserver (Ne pas changer les prefixes p., act.,  cod_act.)*/

/* variables de la table des prestations*/
%LET liste_var_prs =
		p.ben_nir_psa,	
		p.ben_rng_gem,
		p.exe_soi_dtd,		
		p.ben_res_dpt, 
		p.org_aff_ben,
		p.pse_spe_cod,
		p.psp_spe_cod
		/*et autres variables selon le besoin*/;

/* variables de la table d actes affinee  = ici er_cam_f*/
%LET liste_var_act =
       	act.cam_prs_ide
/*et autres variables selon le besoin*/;

/* variables de la table ccamt importee  = ici cod_ccam */
%LET liste_var_ref = 
		cod_act.ccam_gpe
		/*et autres variables selon le besoin*/;

/*1ere annee de la table ER_PRS_F*/
%LET an_DCIR = 2013 ; 

/*Facultatif: 
Attention: 1 ligne = 1 individu 
Variables obligatoires : BEN_NIR_PSA, BEN_RNG_GEM
Si vous n'avez pas de table de population au depart: %let table_pop=  , sinon =nom de la table*/
%LET table_pop = epi_cib.identite;


/**********************************/
/********    MACRO OUTILS    ******/
/**********************************/

/* 9  cles techniques  */
%MACRO CLE_TEC ( p, d ) ;
&p..DCT_ORD_NUM = &d..DCT_ORD_NUM and
&p..FLX_DIS_DTD = &d..FLX_DIS_DTD and
&p..FLX_EMT_NUM = &d..FLX_EMT_NUM and
&p..FLX_EMT_ORD = &d..FLX_EMT_ORD and
&p..FLX_EMT_TYP = &d..FLX_EMT_TYP and
&p..FLX_TRT_DTD = &d..FLX_TRT_DTD and
&p..ORG_CLE_NUM = &d..ORG_CLE_NUM and
&p..prs_ORD_NUM = &d..prs_ORD_NUM and 
&p..REM_TYP_AFF = &d..REM_TYP_AFF
%MEND ;


/* Lorsque l on a besoin de fusionner une table personnelle avec une table oracle, il faut creer une table personnelle dans ORAUSER, 
   avant cela il faut s assurer qu une table avec le meme nom n existe pas deja ;  le cas echeant il faut supprimer cette table existante */
%MACRO KILL_ORAUSER ( table ) ;
%IF %SYSFUNC( exist( ORAUSER.&table )) %THEN %DO ;
	PROC SQL;
	DROP TABLE ORAUSER.&table;
	QUIT; 
%END ;
%MEND ;



/***************************************************************/
/****  CHOIX DES CCAM DANS LE REFERENTIEL acte  ****/
/***************************************************************/

%KILL_ORAUSER( cod_ccam ) ;

/* Ou selon une table existante*/


PROC SQL; 
	CREATE TABLE ORAUSER.cod_ccam  AS
	SELECT	* 
	FROM 	cod_ccam /*where ccam_gpe='EEG'*/
	;
QUIT ;



/***************************************************************/
/******      OPTION : CHOIX d'UNE TABLE de POPULATION     ******/
/***************************************************************/

%MACRO table_pop() ;
%IF &table_pop ne  %THEN %DO ;
	%PUT Table de population = &table_pop. ;
	%KILL_ORAUSER(pop) ;
	DATA orauser.pop ;
	SET &table_pop ;
	*where ald_epi_18=1;
	RUN ;
	%MACRO croisement_tab_pop() ;
		inner join orauser.pop pop
		on pop.ben_nir_psa=p.ben_nir_psa and pop.ben_rng_gem=p.ben_rng_gem
	%MEND ;
%END ;
%ELSE %DO ;
	%PUT no_table ;
	%MACRO croisement_tab_pop() ;
	%MEND ;
%END ;
%MEND ;
%table_pop ;





/***************************************************************/
/************        SELECTION DES PRESTATIONS       ***********/
/***************************************************************/ 
/*** Macro de creation d'une table de prestations mensuelle ***/

%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) Debut %SYSFUNC( datetime(),datetime. ) ;

/* Suppression de la table de prestations mensuelle si existante */
%IF %SYSFUNC ( exist( table_mois )) %THEN %DO ;
	PROC DELETE DATA = table_mois ;
	RUN ;
	PROC DELETE DATA = tmp ;
	RUN ;

%END ;

PROC SQL; 

CREATE TABLE tmp  as 
SELECT  	&liste_var_prs,  
		&liste_var_act	,
		p.prs_act_nbr
FROM    &table_prs as p	    %croisement_tab_pop /*ne fait rien si pas de table de population a croiser*/
		INNER JOIN &table_act as act
			ON %CLE_TEC (p,act)
		LEFT JOIN &table_ete as etab
		ON %CLE_TEC(p, etab)
WHERE   p.flx_dis_dtd ="&FLX_DIS_DTD:0:0:0"dt
AND (p.exe_soi_dtd between "&DATE_EXE_DEB:0:0:0"dt and "&DATE_EXE_FIN:0:0:0"dt ) /* selection de la periode d execution du soin*/
AND (p.DPN_QLF <> 71 AND p.PRS_DPN_QLP <> 71) /*exclusion des soins externes transmis pour info*/
AND (etab.ETE_IND_TAA <> 1 OR etab.ETE_IND_TAA is missing) ; /* exclusion des ACE des sejours en hopitaux publics en T2A */

update tmp set cam_prs_ide=trim(cam_prs_ide);

create index cam_prs_ide on tmp(cam_prs_ide);

CREATE TABLE table_mois  as 
SELECT  t.*,
		&liste_var_ref
FROM    tmp t	   
		INNER JOIN cod_ccam as cod_act
			ON 	&CONDITION_JOINTURE_CCAM
;

QUIT ;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles ****/

%MACRO COMPILATION() ;
%IF %SYSFUNC(exist( compil )) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change table_mois=compil;
quit;
%END ;
%ELSE %DO;
proc append base=compil data=table_mois FORCE;run;
proc delete data=table_mois;run;
%END;
%MEND COMPILATION ;

/*** Macro de suppression des regularisations ***/

%MACRO REGULARISATIONS( table_sortie );

/*liste des variables (hors variables quantite) sans les suffixes*/
%let liste_var_regul=%qsysfunc(tranwrd(%quote(&liste_var_prs),p.,)),%qsysfunc(tranwrd(%quote(&liste_var_act),act., )),%qsysfunc(tranwrd(%quote(&liste_var_ref),cod_act., ));
 
PROC SQL ;
CREATE TABLE &table_sortie as
SELECT	distinct	
			&liste_var_regul,
			sum(prs_act_nbr) as act_nbr
FROM	compil 
GROUP BY	&liste_var_regul /* liste du GROUP_BY = liste du SELECT a l'exception des variables calculees pour eviter la generation de doublons */
HAVING CALCULATED	act_nbr>0 ;
QUIT ;
%MEND REGULARISATIONS;

/*** Execution des macros precedentes pour chaque mois de flux***/

%MACRO TAB_CCAM( Date_DEB= ,Date_FIN= ,table_sortie= ) ;
 
/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;

%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donnee*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compilee des mois extraits si ce n'est pas la 1ere execution*/
%IF %SYSFUNC(exist(compil)) %THEN %DO;
 	PROC DELETE DATA=compil;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le debut et la fin des soins */

/*BOUCLE SUR LES MOIS*/
%DO mois=&iterDeb %TO &iterFin + 7 ;
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;

/*jointure par code ccam */
	%LET CONDITION_JOINTURE_CCAM=(t.cam_prs_ide=cod_act.ccam);
		
				 
	/*DCIR archive*/
	%IF &an<&an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f_&an ;
		%LET table_act = oravue.er_cam_f_&an ;
		%LET table_ete = oravue.er_ete_f_&an ;
	%END;
	/*DCIR courant*/
	%IF  &AN >= &an_DCIR %THEN %DO;
		%LET table_prs = oravue.er_prs_f  ;
		%LET table_act = oravue.er_cam_f  ;
		%LET table_ete = oravue.er_ete_f ;
	%END;

	/*affichage dans le journal*/
	%PUT "Valeurs des parametres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%PUT &table_prs ;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;
%PUT CONDITION_JOINTURE_CCAM=&CONDITION_JOINTURE_CCAM;
	 %CREA_TABLE(&FLX_DIS_DTD) ;
	 %COMPILATION() ;			
%END ;
     %REGULARISATIONS( &table_sortie ) ;
%MEND TAB_CCAM;

/****************************************************************************/
/*lancement de la macro de selection des prestations                                                                       */
/****************************************************************************/
OPTIONS MPRINT ;
 %TAB_CCAM(	 Date_DEB = 201701,Date_FIN = 201912,table_sortie = epi_cib.er_ccam ) ;