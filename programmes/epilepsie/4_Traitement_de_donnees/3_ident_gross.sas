/***************************************************************************/
/*               ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                  */
/***************************************************************************/


/***************************************************************************/
/* Nom du programme : IDENT_GROSSESSE

/* Objectif : Identification des grossesses parmi les populations etudiees*/

/* Referentiels : Codes CIM-10 et Actes CCAM dans data_ref.sas

/* table de sortie : &lib_dmb..grossesse */

/***************************************************************************/


/* issues de grossesses hospitalisees */
/* ********************************** */

proc sql;

create table tmp_diag as 
	select distinct r.ben_idt_ano, r.eta_num, r.rsa_num, r.rss_num, age_ges, del_reg_ent, r.ent_dat, r.sor_dat
		,dgn_pal as dp, dp_gpe, dgn_rel as dr, dr_gpe
		, case when typ_diag = 'DAS' then diag	end as das
		, case when typ_diag = 'DAS' then cim_gpe end as das_gpe
	from &lib_dmb..hist_rss r
		left join &lib_dmb..hist_diag d on r.ben_idt_ano=d.ben_idt_ano and r.eta_num=d.eta_num and r.rsa_num=d.rsa_num and r.rss_num=d.rss_num
	where cim_gpe in ('NV','IG','GEU', 'MOL','AUT','FC','MN', 'IMG')
;
create index ben_idt_ano on tmp_diag(ben_idt_ano);
create index id_rss on tmp_diag (eta_num, rsa_num, rss_num);

create table tmp_hist_acte as 
	select distinct r.ben_idt_ano, r.eta_num, r.rsa_num, r.rss_num, age_ges, del_reg_ent, r.ent_dat, r.sor_dat, a.dte_acte
		, cdc_act as ccam, ccam_gpe,'MCO' as source
		, dgn_pal as dp, dp_gpe, dgn_rel as dr, dr_gpe
	from &lib_dmb..hist_rss r
		left join &lib_dmb..hist_act a on r.ben_idt_ano=a.ben_idt_ano and r.eta_num=a.eta_num and r.rsa_num=a.rsa_num and r.rss_num=a.rss_num
	where ccam_gpe in ('NV','IG','GEU')
;

create index ben_idt_ano on tmp_hist_acte(ben_idt_ano);
create index id_rss on tmp_hist_acte (eta_num, rsa_num, rss_num);

quit;


proc sql;
create table tmp_gross_diag as 
	select d.*, catx('',d.ben_idt_ano, d.eta_num, d.rsa_num, d.rss_num) as cle
	, max(case when ccam_gpe='NV' then 1 else 0 end) as act_nv
	, max(case when ccam_gpe='IG' then 1 else 0 end) as act_ig
	, max(case when ccam_gpe='GEU' then 1 else 0 end) as act_geu
	, max(dte_acte) format=ddmmyy10. as dte_acte
from tmp_diag d
	left join tmp_hist_acte a on a.ben_idt_ano=d.ben_idt_ano and a.eta_num=d.eta_num and a.rsa_num=d.rsa_num and a.rss_num=d.rss_num
group by d.ben_idt_ano, d.eta_num, d.rsa_num, d.rss_num, d.age_ges, d.del_reg_ent, d.ent_dat, d.sor_dat
		,d.dp, d.dp_gpe, d.dr,d.dr_gpe, d.das, d.das_gpe,cle
;

quit;


/* ajouter hosp gross reperer seulement par un acte */
proc sql;

create table tmp as 
	select  ben_idt_ano, eta_num, rsa_num, rss_num, age_ges, del_reg_ent, ent_dat, sor_dat
		, dp, dp_gpe, dr, dr_gpe, '' as das, '' as das_gpe
		, catx('',ben_idt_ano, eta_num, rsa_num, rss_num) as cle
		, max(case when ccam_gpe='NV' then 1 else 0 end) as act_nv
		, max(case when ccam_gpe='IG' then 1 else 0 end) as act_ig
		, max(case when ccam_gpe='GEU' then 1 else 0 end) as act_geu
		, max(dte_acte) format=ddmmyy10. as dte_acte 
	from tmp_hist_acte a
		where not exists (select * from tmp_diag d where d.ben_idt_ano=a.ben_idt_ano and d.eta_num=a.eta_num and d.rsa_num=a.rsa_num and d.rss_num=a.rss_num)
group by ben_idt_ano, eta_num, rsa_num, rss_num
;
insert into tmp_gross_diag
	select * from tmp; 

quit;

proc sort data=tmp_gross_diag;
	by cle;
run;
data grossesse_hosp;
set tmp_gross_diag;
by cle;

retain das_concat;
retain nb;
length das_concat $100.;

if first.cle then do;
	das_concat='';
	nb=0;
end;
das_concat=catx('',das,das_concat);
nb=nb+1;
if last.cle then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on grossesse_hosp(ben_idt_ano);
create index id_rss on grossesse_hosp (eta_num, rsa_num, rss_num);
quit;

proc sql;
alter table grossesse_hosp add ig num(1), issue_dte date format=ddmmyy10.;

update grossesse_hosp set ig=
	case when dp_gpe='IG' and das_gpe='IG' and act_ig=1 then 1 /* ivg */
	 when dp_gpe='IG' and das_gpe not in ('IG') and act_ig=1 then 2 /* img av 22s */
	 when das_gpe in ('IMG')then 3 /* img ap 22s */
	 when dp_gpe='IG' and act_ig=0 then 4 /* ivg */
	 when dp_gpe in ('FC','GEU','AUT','MOL') or (das_gpe in ('IG','FC','GEU','AUT','MOL') or das_gpe is null) and act_ig=1 then 5
	 when dp_gpe in ('FC','GEU','AUT','MOL') or (das_gpe in ('IG','FC','GEU','AUT','MOL') or das_gpe is null) and act_ig=0 then 6
	else 0 end 
;

update grossesse_hosp set issue_dte=max(dte_acte,ent_dat);

alter table grossesse_hosp drop cle;
quit;



/* issues de grossesses dcir (prs et actes en ville ou CE) */
/* ******************************************************* */
proc sql;
create table tmp_dcir as 
select ben_idt_ano, eta_num, exe_soi_dtd format=ddmmyy10. as exe_soi_dtd
		, cam_prs_ide, ccam_gpe , 'CAM' as source, catx('',ben_idt_ano, exe_soi_dtd,ccam_gpe) as cle
	from &lib_dmb..hist_act2
	where ccam_gpe in ('IG')
union
select distinct ben_idt_ano,etb_exe_fin, datepart(exe_soi_dtd) format=ddmmyy10.
		, put(prs_nat_ref,5.), 'IG','PRS', catx('',ben_idt_ano, exe_soi_dtd,'IG')
	from &lib_ext..ER_PRS_F 
	where prs_gpe='IVG'

;

quit;


proc sort data=tmp_dcir;
	by cle;
run;
data grossesse_dcir;
set tmp_dcir;
by cle;

retain ccam_concat;
retain prs_concat;

retain nb_ccam;
retain nb_prs;
length ccam_concat prs_concat $100.;

if first.cle then do;
	ccam_concat='';
	prs_concat='';
	nb_ccam=0;
	nb_prs=0;
end;

if source='CAM' then do;
	ccam_concat=catx('',cam_prs_ide,ccam_concat);
	nb_ccam=nb_ccam+1;
end;
if source='PRS' then do;
	prs_concat=catx('',cam_prs_ide,prs_concat);
	nb_prs=nb_prs+1;
end;

if last.cle then do;
	output;
end;

run;


/* ************ */
/* Interruption */
/* ************ */

proc sql;
/* concat des 2 sources d'info */
create table interruption as 
	select *
	from grossesse_hosp 
	where ig >0
;
alter table interruption add ccam_concat varchar(50), prs_concat varchar(50);

insert into interruption
	select ben_idt_ano, eta_num, '','',.,.,exe_soi_dtd, exe_soi_dtd,'','','','','','',0,1,0
	,exe_soi_dtd,''
	, max(nb_ccam,nb_prs), 1,exe_soi_dtd,ccam_concat, prs_concat
	from grossesse_dcir
;
quit;

/* fusion des doublons */

proc sort data=interruption;
	by ben_idt_ano issue_dte  ;
run;


data interruption;
	/*suppression des variables utilisees ci-apres 
	elles existent si le traitement a deja ete lance*/
	set interruption;
	drop dte_prec delai num_ig;
run;

proc sort data=interruption;
	by ben_idt_ano issue_dte ;
quit;

data interruption;
	/*Calcul du delai entre 2 grossesse (delai)
	=> generer les numeros d'ig (num_ig) par patiente
		meme ig si moins de 42 jours entre 2 issues identifiees
	*/
	set interruption;
	by ben_idt_ano;
	retain dte_prec;
	retain num_ig 0;
	dte_prec=lag(issue_dte);
	format dte_prec ddmmyy10.;
	if first.ben_idt_ano then do ;
		dte_prec=.; 
		num_ig=0;
	end;
	delai=issue_dte-dte_prec;
	if (issue_dte-dte_prec)>=0 and (issue_dte-dte_prec)<42 then do;
		num_ig=num_ig;
	end;
	else	num_ig=num_ig+1;
	
run;


proc sql;
create index ben_idt_ano on interruption(ben_idt_ano);

create table tmp as	
	select ben_idt_ano, num_ig,max(issue_dte) as issue_dte
	from interruption
	group by ben_idt_ano, num_ig;

create index ben_idt_ano on tmp(ben_idt_ano);

create table interruption_g as 
	select i.*, case when t.num_ig is null then 0 else 1 end as last_issue
	from interruption i
		left join tmp t on t.ben_idt_ano=i.ben_idt_ano and t.num_ig=i.num_ig and t.issue_dte=i.issue_dte
;
quit;


proc sql;

create table &lib_dmb..interruption_g as 
	select ben_idt_ano,num_ig, max(issue_dte) as issue_dte format=ddmmyy10.
	, max(ETA_NUM) as eta_num, max(RSA_NUM) as rsa_num, max(RSS_NUM) as rss_num, max(AGE_GES) as age_ges, max(DEL_REG_ENT) as del_reg_ent
	, max(ent_dat) as ent_dat format=ddmmyy10., max(sor_dat) as sor_dat format=ddmmyy10.
	, max(dp) as dp, max(dp_gpe) as dp_gpe, max(dr) as dr, max(dr_gpe) as dr_gpe, max(das) as das, max(das_gpe) as das_gpe
	, max(act_nv) as act_nv	, max(act_ig) as act_ig, max(act_geu) as act_geu
	, max(dte_acte) as dte_acte format=ddmmyy10.
	, max(das_concat) as das_concat
	, max(ccam_concat) as ccam_concat, max(prs_concat) as prs_concat 
from interruption_g
where last_issue=1
group by ben_idt_ano,num_ig

;

quit;



/* ************ */
/* grossesse    */
/* ************ */


proc sql;
create table tmp_gross as 
	select *
	from grossesse_hosp 
	where ig=0
;

quit;

/* fusion des doublons */

proc sort data=tmp_gross;
	by ben_idt_ano issue_dte  ;
run;


data tmp_gross;
	/*suppression des variables utilisees ci-apres 
	elles existent si le traitement a deja ete lance*/
	set tmp_gross;
	drop dte_prec delai num_g;
run;

proc sort data=tmp_gross;
	by ben_idt_ano issue_dte ;
quit;

data tmp_gross;
	/*Calcul du delai entre 2 grossesse (delai)
	=> generer les numeros de g (num_g) par patiente
		meme g si moins de 196 jours entre 2 issues identifiees
	*/
	set tmp_gross;
	by ben_idt_ano;
	retain dte_prec;
	retain num_g 0;
	dte_prec=lag(issue_dte);
	format dte_prec ddmmyy10.;
	if first.ben_idt_ano then do ;
		dte_prec=.; 
		num_g=0;
	end;
	delai=issue_dte-dte_prec;
	if (issue_dte-dte_prec)>=0 and (issue_dte-dte_prec)<196 then do;
		num_g=num_g;
	end;
	else	num_g=num_g+1;
	
run;


proc sql;
create index ben_idt_ano on tmp_gross(ben_idt_ano);

create table tmp as	
	select ben_idt_ano, num_g,max(issue_dte) as issue_dte
	from tmp_gross
	group by ben_idt_ano, num_g;

create index ben_idt_ano on tmp(ben_idt_ano);

create table hosp_g as 
	select i.*, case when t.num_g is null then 0 else 1 end as last_issue
	from tmp_gross i
		left join tmp t on t.ben_idt_ano=i.ben_idt_ano and t.num_g=i.num_g and t.issue_dte=i.issue_dte
;
quit;


/* correction last issue 

ne pas tenir compte des sejours apriori post partum 
1 seul cas ! :  ben_idt_ano='BW_fBwC21z6iVybzQ' 

si 1 sejour avec dp nv + acte accouchement => conserver
et 2d seulement das nv sans acte

*/

proc sql;

update hosp_g set last_issue=1
	where ben_idt_ano='BW_fBwC21z6iVybzQ' and dp_gpe='NV' ;

update hosp_g set last_issue=0
	where ben_idt_ano='BW_fBwC21z6iVybzQ' and dp_gpe is null ;

quit;

/*
proc sql;

create table verif as 
	select distinct g1.*
	from hosp_g g1
		inner join hosp_g g2 on g1.ben_idt_ano=g2.ben_idt_ano and g1.num_g=g2.num_g
	where g1.dp_gpe='NV' and g1.act_nv=1 and g1.last_issue=0
		and g2.dp_gpe is null and g2.act_nv=0 and g2.last_issue=1
;

quit;
*/




proc sql;

create table &lib_dmb..hosp_g as 
	select ben_idt_ano,num_g, max(issue_dte) as issue_dte format=ddmmyy10.
	, max(ETA_NUM) as eta_num, max(RSA_NUM) as rsa_num, max(RSS_NUM) as rss_num, max(AGE_GES) as age_ges, max(DEL_REG_ENT) as del_reg_ent
	, max(ent_dat) as ent_dat format=ddmmyy10., max(sor_dat) as sor_dat format=ddmmyy10.
	, max(dp) as dp, max(dp_gpe) as dp_gpe, max(dr) as dr, max(dr_gpe) as dr_gpe, max(das) as das, max(das_gpe) as das_gpe
	, max(act_nv) as act_nv	, max(act_ig) as act_ig, max(act_geu) as act_geu, max(ig) as ig
	, max(dte_acte) as dte_acte format=ddmmyy10.
	, max(das_concat) as das_concat
from hosp_g
where last_issue=1
group by ben_idt_ano,num_g

;
quit;



/* recherche des doublons entre accouchements et IG */


/* necessite de la date de debut pour supprimer les IG pdt une grossesse avec accouchement */

proc sql;
alter table &lib_dmb..hosp_g add dte_debut_gross date format=ddmmyy10.;

update &lib_dmb..hosp_g set dte_debut_gross =
	case when age_ges is not null then issue_dte-(age_ges*7+14)
		when age_ges is null and del_reg_ent is not null then issue_dte-(del_reg_ent+14)
		end
;
quit;
/* necessite du type de l'issue pour determiner la date de debut sans age gest ou ddr */
/* => que 7 cas NV => attribution de l'age gest = 39  */

proc sql;
update &lib_dmb..hosp_g set dte_debut_gross =
	issue_dte-(39*7+14)
	where dte_debut_gross is null
;
quit;




proc sql;

/* toper les IG a ne pas prendre en compte */

create table tmp_igexc as 
	select distinct i.ben_idt_ano,i.num_ig,i.issue_dte
	from &lib_dmb..hosp_g h
		inner join &lib_dmb..interruption_g i on i.ben_idt_ano=h.ben_idt_ano
	where i.issue_dte between h.dte_debut_gross and (h.issue_dte+10*7)
;

create index ben_idt_ano on tmp_igexc(ben_idt_ano);

alter table &lib_dmb..interruption_g add exclu num(1);


/* ig*/
update &lib_dmb..interruption_g i set exclu=.;

update &lib_dmb..interruption_g i set exclu=1
	where exists (select * from tmp_igexc t where t.ben_idt_ano=i.ben_idt_ano and t.num_ig=i.num_ig)
;
update &lib_dmb..interruption_g i set exclu=0 where exclu is null;


quit;



/* creation de la table grossesse a partir de la fusion des 2 sources (interruption et G hosp) */

proc sql;

create table grossesse as 
	select ben_idt_ano,num_g as num_init,issue_dte,eta_num,rsa_num,rss_num,age_ges,del_reg_ent,ent_dat,sor_dat,dp,dp_gpe,dr,dr_gpe,das,das_gpe,act_nv,act_ig,act_geu,ig,dte_acte,das_concat,dte_debut_gross,'' as ccam_concat, '' as prs_concat
	from &lib_dmb..hosp_g
union
	select ben_idt_ano,num_ig,issue_dte,eta_num,rsa_num,rss_num,age_ges,del_reg_ent,ent_dat,sor_dat,dp,dp_gpe,dr,dr_gpe,das,das_gpe,act_nv,act_ig,act_geu,1,dte_acte,das_concat,.,ccam_concat, prs_concat
	from &lib_dmb..interruption_g
	where exclu=0
;
quit;

/* numeroter les grossesses */


data &lib_dmb..grossesse;
	/*suppression des variables utilisees ci-apres 
	elles existent si le traitement a deja ete lance*/
	set &lib_dmb..grossesse;
	drop dte_prec delai num_g;
run;

proc sort data=grossesse;
	by ben_idt_ano issue_dte ;
quit;

data &lib_dmb..grossesse;
	set grossesse;
	by ben_idt_ano;
	retain dte_prec;
	retain num_g 0;
	dte_prec=lag(issue_dte);
	format dte_prec ddmmyy10.;
	if first.ben_idt_ano then do ;
		dte_prec=.; 
		num_g=0;
	end;
	delai=issue_dte-dte_prec;
	num_g=num_g+1;
	
run;



/* type issue de grossesse */

proc sql;

alter table &lib_dmb..grossesse add issue varchar(8);

update &lib_dmb..grossesse set issue=
	case when das_gpe='MN' then 'MN'
		when das_gpe='IMG' then 'IMG_AP22'
		when dp_gpe='NV' or das_gpe='NV' or act_nv=1 then 'NV'
		when dp_gpe='IG' and das_gpe='IG' and act_ig=1 then 'IVG'
		when dp_gpe='IG' and act_ig=0 then 'IVG_IMG'
		when dp_gpe='IG' and das_gpe not in ('IG') and act_ig=1 then 'IMG_AV22'
		when dp_gpe='FC' then 'FC'
		when dp_gpe='GEU' or act_geu=1 then 'GEU'
		when dp_gpe in ('MOL','AUT') then 'OTHER'
		when prs_concat is not null then 'IVG_V'
	end
;

update  &lib_dmb..grossesse set issue='IVG_V'
	where issue is null and ccam_concat is not null
  ;

/* exclusion des grossesses sans issue (123 => confirmation AD) */
alter table &lib_dmb..grossesse add exclu num(1);

update &lib_dmb..grossesse set exclu=1 where issue is null;
update &lib_dmb..grossesse set exclu=0 where exclu is null;

quit;


/* determiner date de debut de grossesse */


proc sql;
update &lib_dmb..grossesse set dte_debut_gross =
	case when age_ges is not null then issue_dte-(age_ges*7+14)
		when age_ges is null and del_reg_ent is not null then issue_dte-(del_reg_ent+14)
		end
	where dte_debut_gross is null

;
proc sql;
update &lib_dmb..grossesse set dte_debut_gross =issue_dte-(6*7-14)
	where dte_debut_gross is null and prs_concat is not null
;	
update &lib_dmb..grossesse set dte_debut_gross =issue_dte-(9*7-14)
	where dte_debut_gross is null and issue in ('GEU','IVG_V')
;	

quit;