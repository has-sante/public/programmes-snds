/**********************************************************************************************/
/*                    ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                                */
/**********************************************************************************************/


/**********************************************************************************************/
/* Nom du programme : soins_femmes

/* Objectif : creer la table de suivi de la grossesse et les variables indicatrices de suivi /

/* tables de sortie : &lib_dmb..periode_gross */

/**********************************************************************************************/


/* creation de la table periode_gross 
suivi mensuel : 1 an avant le debut de la grossesse + mois de grossesse
*/

proc sql;

create table &lib_dmb..periode_gross as 
	select g.ben_idt_ano, num_g, dte_debut_gross, issue_dte, issue
		, periode, (dte_debut_gross+p_deb) format=ddmmyy10. as debut_periode, min((dte_debut_gross+p_fin), issue_dte) format=ddmmyy10. as fin_periode	
	from &lib_dmb..grossesse g, cod_periode_g p 
	where exclu=0 and issue_dte between '01JAN2018'd and '31DEC2019'd
;

create index ben_idt_ano on &lib_dmb..periode_gross(ben_idt_ano);
create index debut_periode on &lib_dmb..periode_gross(debut_periode);
create index periode on &lib_dmb..periode_gross(periode);

/* enlever les periodes apres l'issue de grossesse */

delete from &lib_dmb..periode_gross
	where debut_periode>issue_dte;

quit;





/* calcul des indicatrices de recours aux soins dans chaque periode */
proc sql;
alter table &lib_dmb..periode_gross add
	nb_consult_neuro smallint, nb_consult_mg smallint,	nb_consult_urg smallint, nb_consult_ped smallint, nb_consult_tot smallint
	, nb_hdj_epi smallint, nb_hosp_epi smallint, nb_hosp smallint
	, nb_acte_eeg smallint,	nb_acte_irm	smallint
	, nb_med_ae smallint, nb_ind_med smallint, ind_med varchar(30)
	, nb_ind_fol smallint, ind_fol num(1)
	, prim num(1), sec num(1), ter num(1)
	, date_prim date format=ddmmyy10., date_sec date format=ddmmyy10., date_ter date format=ddmmyy10.
	, prim_mg num(1), prim_urg num(1), sec_eeg num(1), sec_neuro num(1), sec_irm num(1)
	, date_eeg date format=ddmmyy10., date_neuro date format=ddmmyy10.
	, sec_eeg_delai smallint,	sec_neuro_delai smallint,	sec_irm_delai smallint,	ter_delai smallint
	, dosage_ae num(1), neuro_fa num(1)
;
quit;


/* indicateur consult */
/* ***************** */

%macro nb_consult(var_s=, condition=);

proc sql;
create table tmp as 
	select p.ben_idt_ano, num_g,periode, count(distinct consult_dte) as nb
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		&condition
	group by p.ben_idt_ano, num_g, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index num_g on tmp(num_g);

update &lib_dmb..periode_gross p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_consult(var_s=nb_consult_mg, condition= and spe_medecin='MG');
%nb_consult(var_s=nb_consult_neuro, condition= and spe_medecin='NEURO');
%nb_consult(var_s=nb_consult_ped, condition= and spe_medecin='PED');
%nb_consult(var_s=nb_consult_tot, condition= );
%nb_consult(var_s=nb_consult_urg, condition= and prs_nat_ref='ATU');



/* indicateur hospit */
/* ***************** */
%macro nb_hosp(var_s=, condition=);

proc sql;
create table tmp as 
	select p.ben_idt_ano, num_g, periode, count(distinct ent_dat) as nb
	from &lib_dmb..hist_rss c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where ent_dat between debut_periode and fin_periode 
		&condition
	group by p.ben_idt_ano, num_g, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index num_g on tmp(num_g);

update &lib_dmb..periode_gross p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g  and t.periode=p.periode)
;
update &lib_dmb..periode_gross set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_hosp(var_s=nb_hdj_epi, condition= and nb_nuit=0 and epi=1);
%nb_hosp(var_s=nb_hosp_epi, condition= and nb_nuit>0 and epi=1);
%nb_hosp(var_s=nb_hosp, condition= );




/* indicateur actes */
/* ***************** */

%macro nb_act(var_s=, condition=);

proc sql;
create table tmp1 as 
	select distinct p.ben_idt_ano, num_g, periode, dte_acte 
	from &lib_dmb..hist_act h 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=h.ben_idt_ano 
	where dte_acte between debut_periode and fin_periode 
		&condition
union
	select distinct p.ben_idt_ano, num_g, periode, exe_soi_dtd as dte_acte
	from &lib_dmb..hist_act2 h 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=h.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
		&condition
;

create table tmp as
	select ben_idt_ano, num_g, periode, count(distinct dte_acte) as nb
	from tmp1
	group by ben_idt_ano, num_g, periode
;

create index ben_idt_ano on tmp(ben_idt_ano);
create index num_g on tmp(num_g);

update &lib_dmb..periode_gross p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g  and t.periode=p.periode)
;
update &lib_dmb..periode_gross set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_act(var_s=nb_acte_eeg, condition= and ccam_gpe='EEG');
%nb_act(var_s=nb_acte_irm, condition= and ccam_gpe='IMA');




/* indicateur med    */
/* ***************** */

/* med ae */
proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, ind_med, pha_prs_c13 as cod13 , exe_soi_dtd,sum(pha_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F a on a.ben_idt_ano=p.ben_idt_ano 
	where med_epi=1 
	group by p.ben_idt_ano,  ind_med, cod13,exe_soi_dtd
	having nb>0
;

insert into  tmp  
	select distinct p.ben_idt_ano, ind_med, input(ucd_ucd_cod,13.)  , exe_soi_dtd,sum(ucd_dlv_nbr) 
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F a on a.ben_idt_ano=p.ben_idt_ano 
	where med_epi=1 
	group by p.ben_idt_ano, ind_med, ucd_ucd_cod,exe_soi_dtd
	having sum(ucd_dlv_nbr)>0
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index exe_soi_dtd on tmp(exe_soi_dtd);


/* nombre de delivrance */
create table del_periode as 
	select p.ben_idt_ano,  num_g, periode, count(distinct exe_soi_dtd) as nb
	from tmp t
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, num_g, periode
;

create index ben_idt_ano on del_periode(ben_idt_ano);
create index num_g on del_periode(num_g);

update &lib_dmb..periode_gross p set nb_med_ae=
	(select nb from del_periode t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set nb_med_ae=0 where nb_med_ae is null;

/* nombre de ind_med */
create table indmed_periode as 
	select p.ben_idt_ano, num_g, periode, count(distinct t.ind_med) as nb
	from tmp t
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, num_g, periode
;

create index ben_idt_ano on indmed_periode(ben_idt_ano);
create index num_g on indmed_periode(num_g);

update &lib_dmb..periode_gross p set nb_ind_med=
	(select nb from indmed_periode t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set nb_ind_med=0 where nb_ind_med is null;


quit;

/* ind_med different par periode */

proc sql;

create table indmed_periode2 as 
	select distinct p.ben_idt_ano, num_g, periode, t.ind_med, catx('',p.ben_idt_ano, num_g,periode) as cle
	from tmp t
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
;
quit;

proc sort data=indmed_periode2;
	by cle;
run;
data tmp_indmed;
set indmed_periode2;
by cle;

retain indmed_cat;
retain nb;
length indmed_cat $50.;

if first.cle then do;
	indmed_cat='';
	nb=0;
end;
indmed_cat=catx('',ind_med,indmed_cat);
nb=nb+1;
if last.cle then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_indmed (ben_idt_ano);
create index periode on tmp_indmed (periode);

update &lib_dmb..periode_gross p set ind_med=
	(select indmed_cat from tmp_indmed t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;

quit;



/* med acide folique */
proc sql;


create table tmp as 
	select distinct p.ben_idt_ano, pha_prs_c13 as cod13 , exe_soi_dtd,sum(pha_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F a on a.ben_idt_ano=p.ben_idt_ano 
	where ind_med='AF' 
	group by p.ben_idt_ano, cod13,exe_soi_dtd
	having nb>0
;
quit;

proc sql;

/* nombre de delivrance */
create table del_periode as 
	select p.ben_idt_ano, num_g, periode, count(distinct exe_soi_dtd) as nb
	from tmp t
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, num_g, periode
;

create index ben_idt_ano on del_periode(ben_idt_ano);
create index periode on del_periode(periode);

update &lib_dmb..periode_gross p set nb_ind_fol=
	(select nb from del_periode t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set nb_ind_fol=0 where nb_ind_fol is null;

update &lib_dmb..periode_gross p set 
	ind_fol=case when nb_ind_fol>0 then 1 else 0 end;


quit;




/* soins primaires  */
/* **************** */
proc sql;

update &lib_dmb..periode_gross set
	prim=case when nb_consult_mg>0 or nb_consult_urg>0 or nb_consult_ped>0 then 1 else 0 end
	,prim_mg= case when nb_consult_mg>0 or nb_consult_ped>0 then 1 else 0 end
	,prim_urg= case when nb_consult_urg>0 then 1 else 0 end

;


create table soin as 
	select p.ben_idt_ano, num_g, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_mg>0 or nb_consult_urg>0 or nb_consult_ped>0
	group by p.ben_idt_ano, num_g, periode
;
create index ben_idt_ano on soin(ben_idt_ano);
create index num_g on soin(num_g);

update &lib_dmb..periode_gross p set date_prim=
	(select case when periode<12 then max_dte else min_dte end format=ddmmyy10. 
		from soin s where s.ben_idt_ano=p.ben_idt_ano and s.num_g=p.num_g and s.periode=p.periode )
;




quit;


/* soins secondaires */
/* ***************** */
proc sql;

update &lib_dmb..periode_gross set 
	sec=case when nb_consult_neuro>0 or nb_acte_eeg>0 then 1 else 0 end
	,sec_eeg=case when nb_acte_eeg>0 then 1 else 0 end
	,sec_neuro=case when nb_consult_neuro>0 then 1 else 0 end
	,sec_irm=case when nb_acte_irm>0 then 1 else 0 end
;

/* consult neuro */
create table soin as 
	select p.ben_idt_ano, num_g, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_neuro>0
	group by p.ben_idt_ano, num_g, periode
;

create index ben_idt_ano on soin(ben_idt_ano);
create index num_g on soin(num_g);

update &lib_dmb..periode_gross p set date_neuro=
	(select case when periode<12 then max_dte else min_dte end format=ddmmyy10. 
		from soin s where s.ben_idt_ano=p.ben_idt_ano and s.num_g=p.num_g and s.periode=p.periode )
;

update &lib_dmb..periode_gross set sec_neuro_delai=date_neuro-debut_periode;

/* eeg */
create table tmp1 as 
	select distinct p.ben_idt_ano, num_g, periode, dte_acte 
	from &lib_dmb..hist_act h 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=h.ben_idt_ano 
	where dte_acte between debut_periode and fin_periode 
		and ccam_gpe='EEG'
union
	select distinct p.ben_idt_ano, num_g, periode, exe_soi_dtd as dte_acte
	from &lib_dmb..hist_act2 h 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=h.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
		and ccam_gpe='EEG'
;

create table tmp_eeg as
	select ben_idt_ano, num_g, periode, min(dte_acte) as min_dte, max(dte_acte) as max_dte
	from tmp1 t
	group by ben_idt_ano, num_g, periode
;
create index ben_idt_ano on tmp_eeg(ben_idt_ano);
create index num_g on tmp_eeg(num_g);

update &lib_dmb..periode_gross p set date_eeg=
	(select case when periode<12 then max_dte else min_dte end format=ddmmyy10. 
		from tmp_eeg s where s.ben_idt_ano=p.ben_idt_ano and s.num_g=p.num_g and s.periode=p.periode )
;
update &lib_dmb..periode_gross set sec_eeg_delai=date_eeg-debut_periode;


/* synthese soins sec */
insert into soin
	select ben_idt_ano, num_g, periode, min_dte, max_dte
	from tmp_eeg
	group by ben_idt_ano, num_g, periode
;

create table tmp as 
	select ben_idt_ano, num_g, periode, min(min_dte) as min_dte, max(max_dte) as max_dte
	from soin
	group by ben_idt_ano, num_g, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index num_g on tmp(num_g);


update &lib_dmb..periode_gross p set date_sec=
	(select case when periode<12 then max_dte else min_dte end format=ddmmyy10. 
		from tmp s where s.ben_idt_ano=p.ben_idt_ano and s.num_g=p.num_g and s.periode=p.periode )
;



quit;



/* soins tertiaires  */
/* ***************** */

proc sql;

create table soin as 
	select p.ben_idt_ano, num_g, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_neuro>0 and spe_medecin='NEURO' and c.ter=1
	group by p.ben_idt_ano, num_g, periode
;
create index ben_idt_ano on soin(ben_idt_ano);
create index num_g on soin(num_g);

update &lib_dmb..periode_gross p set date_ter=
	(select case when periode<12 then max_dte else min_dte end format=ddmmyy10. 
		from soin s where s.ben_idt_ano=p.ben_idt_ano and s.num_g=p.num_g and s.periode=p.periode )
;


update &lib_dmb..periode_gross set 
	ter=case when date_ter is not null then 1 else 0 end
;


update &lib_dmb..periode_gross set ter_delai= date_ter-debut_periode 
;


quit;



/* ajouter la presence de class_co sur les periodes de suivi */

proc sql;

create table class_co as 
	select distinct ben_idt_ano, exe_soi_dtd as dte_debut, date_fin, class_co
	from &lib_dmb..med_gross
	where class_co is not null
;
insert into class_co
	select distinct ben_idt_ano, dte_acte, dte_fin, 'PROG'
	from &lib_dmb..implant
;

create index ben_idt_ano on class_co(ben_idt_ano);

create table seq_co as 
	select distinct s.ben_idt_ano, periode, c.class_co,  catx('',s.ben_idt_ano,periode) as cle
	from &lib_dmb..periode_soins s
		inner join class_co c on c.ben_idt_ano=s.ben_idt_ano
	where c.dte_debut<=fin_periode and c.date_fin>=debut_periode
	order by cle, c.class_co
;


quit;


/* concat class_co par sequence */
data new_co;
	set seq_co;
	by cle;
	retain rgp_co;
	retain nb_rgpco;

	length rgp_co $200.;

	if first.cle then do;
		rgp_co='';
		nb_rgpco=0;
	end;
	rgp_co=catx(',',rgp_co,class_co);
	nb_rgpco=nb_rgpco+1;

	if last.cle then do;
		rgp_co=substr(rgp_co,1,length(rgp_co));
		output;
	end;
run;


proc sql;

create index ben_idt_ano on new_co(ben_idt_ano);
create index periode on new_co(periode);
quit;

proc sql;
alter table &lib_dmb..periode_soins add class_co varchar(30), diu_imp num(1);

update &lib_dmb..periode_soins p set class_co=
	(select rgp_co from new_co n where p.ben_idt_ano=n.ben_idt_ano and p.periode=n.periode)	
;

quit;

proc sql;
alter table &lib_dmb..periode_soins add diu_imp num(1);

/* diu imp sur la periode */
create table per_diu as 
	select distinct s.ben_idt_ano, periode
	from &lib_dmb..periode_soins s
		inner join &lib_dmb..med_gross m on m.ben_idt_ano=s.ben_idt_ano
	where m.exe_soi_dtd<=fin_periode and m.date_fin>=debut_periode and m.ind_med='DIU'
;
create index ben_idt_ano on per_diu(ben_idt_ano);
create index periode on per_diu(periode);


update &lib_dmb..periode_soins s set diu_imp = 1
	where exists (select * from per_diu d where d.ben_idt_ano=s.ben_idt_ano and d.periode=s.periode)
;
update &lib_dmb..periode_soins s set diu_imp = 0 where diu_imp is null;

quit;


/* dosage ae */
proc sql;
create table tmp as 
	select p.ben_idt_ano, num_g,periode, count(distinct exe_soi_dtd) as nb
	from &lib_dmb..bio_gross b 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=b.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, num_g, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index num_g on tmp(num_g);

update &lib_dmb..periode_gross p set dosage_ae=1
	where exists (select * from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set dosage_ae=0 where dosage_ae is null;

quit;
/* dosage ae dans les 30j apres c neuro */




/* Acide folique dans les 30j apres c neuro */
/* dosage ae : examen bio dans les 30j apres c neuro */

proc sql;
/* consult neuro */
create table c_neuro as 
	select distinct p.ben_idt_ano, num_g, periode, consult_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_gross p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_neuro>0 
;
create index ben_idt_ano on c_neuro(ben_idt_ano);

alter table &lib_dmb..periode_gross add neuro_fa num(1), neuro_dosage num(1);

/* del af */

create table cneuro_fa as 
	select distinct n.*
	from &lib_dmb..med_gross m
		inner join c_neuro n on n.ben_idt_ano=m.ben_idt_ano
	where ind_med='AF' and exe_soi_dtd between consult_dte and consult_dte+30
;
create index ben_idt_ano on cneuro_fa(ben_idt_ano);
create index periode on cneuro_fa(periode);
create index num_g on cneuro_fa(num_g);


update &lib_dmb..periode_gross p set neuro_fa=1
	where exists (select * from cneuro_fa t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set neuro_fa=0 where neuro_fa is null;


/* dosage ae */
create table cneuro_ae as 
	select distinct n.*
	from &lib_dmb..bio_gross b 
		inner join c_neuro n on n.ben_idt_ano=b.ben_idt_ano
	where exe_soi_dtd between consult_dte and consult_dte+30 
;
create index ben_idt_ano on cneuro_ae(ben_idt_ano);
create index periode on cneuro_ae(periode);
create index num_g on cneuro_ae(num_g);

update &lib_dmb..periode_gross p set neuro_dosage=1
	where exists (select * from cneuro_ae t where t.ben_idt_ano=p.ben_idt_ano and t.num_g=p.num_g and t.periode=p.periode)
;
update &lib_dmb..periode_gross set neuro_dosage=0 where neuro_dosage is null;

quit;