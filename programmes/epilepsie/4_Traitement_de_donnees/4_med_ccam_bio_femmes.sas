/**************************************************************************************/
/*                    ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                       */
/*************************************************************************************/

/*************************************************************************************/
/* Nom du programme : MED_CCAM_BIO_FEMME

/* Objectif : Identifier des delivrances d'AF et de contraceptifs 
parmi les femmes en age de procreer ;
identifier les actes de contreles biologiques pharmacologiques /

/* Referentiels : Codes medicaments et Actes CCAM dans data_ref.sas

/* tables de sortie : &lib_dmb..med_gross, &lib_dmb..implant et &lib_dmb..bio_gross */

/*************************************************************************************/

/* ensemble des delivrances PHA  (pas d'UCD=> pas de del) */
proc sql;
create table &lib_dmb..MED_GROSS as 
	select distinct f.ben_idt_ano, cod13,nbj_expo, exe_soi_dtd, f.ind_med,f.psp_spe_cod,f.psp_stj_cod,f.class_co, sum(pha_act_qsn) as nb_bte
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F f  on f.ben_idt_ano=p.ben_idt_ano
		inner join cod_med c on c.cod7=f.pha_prs_ide or c.cod13=f.pha_prs_c13
	where sex=2 and age between 15 and 49 and (f.MED_CO=1 or f.MED_AF=1)
	group by f.ben_idt_ano, exe_soi_dtd, f.ind_med, f.class_co
	having sum(pha_act_qsn)>0
;

create index ben_idt_ano on &lib_dmb..MED_GROSS(ben_idt_ano);

alter table &lib_dmb..MED_GROSS add date_fin date format=ddmmyy10.;

update &lib_dmb..MED_GROSS set date_fin= exe_soi_dtd+(case when nbj_expo is null then nb_bte else nbj_expo*nb_bte end);

quit;

/* BIO */
proc sql;
create table &lib_dmb..BIO_GROSS as 
	select distinct f.ben_idt_ano, nabm, exe_soi_dtd, f.psp_spe_cod,f.psp_stj_cod, sum(bio_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_BIO_F f  on f.ben_idt_ano=p.ben_idt_ano
		inner join cod_bio c on c.nabm=f.bio_prs_ide 
	where sex=2 and age between 15 and 49 
	group by f.ben_idt_ano, exe_soi_dtd,f.psp_spe_cod,f.psp_stj_cod
	having sum(bio_act_qsn)>0
;


insert into &lib_dmb..BIO_GROSS
	select p.ben_idt_ano, nabm, ent_dat+del_dat_ent , 999, 999,sum(act_nbr)
	from &lib_dmb..patient p
		inner join &lib_dmb..identite i on i.ben_idt_ano=p.ben_idt_ano
		inner join &lib_ext..T_MCO_FLSTC f  on f.nir_ano_17=i.ben_nir_psa
		inner join cod_bio c on c.nabmc=substrn(f.nabm_cod,length(f.nabm_cod)-3,4) or c.nabmc0=substr(f.nabm_cod,4) 
	where p.sex=2 and p.age between 15 and 49
group by  p.ben_idt_ano, nabm, ent_dat 
;
create index ben_idt_ano on &lib_dmb..BIO_GROSS(ben_idt_ano);


quit;


/* recherche des actes implants contraceptifs */
/* par defaut tous les implant ont  class_co='PROG' */

proc sql;
/* actes ccam des sejours mco*/
create table &lib_dmb..implant as
	select distinct p.ben_idt_ano,cdc_act as ccam,dte_acte,nb_actes , source
	from &lib_dmb..patient p
		inner join &lib_dmb..hist_act a on a.ben_idt_ano=p.ben_idt_ano
	where p.sex=2 and p.age between 15 and 49 and ccam_gpe='IMP'
;

insert into &lib_dmb..implant 
	select distinct p.ben_idt_ano,cam_prs_ide,exe_soi_dtd,act_qte , source
	from &lib_dmb..patient p
		inner join &lib_dmb..hist_act2 a on a.ben_idt_ano=p.ben_idt_ano
	where p.sex=2 and p.age between 15 and 49 and ccam_gpe='IMP'
;

create index ben_idt_ano on &lib_dmb..implant(ben_idt_ano);
create index ccam on &lib_dmb..implant(ccam);
create index dte_acte on &lib_dmb..implant(dte_acte);
quit;

proc sql;

/* duree par defaut de 3 ans */
/* modifier date de fin si grossesse avant la fin theorique */

alter table &lib_dmb..implant add dte_fin date format=ddmmyy10.;

update &lib_dmb..implant set dte_fin = dte_acte+360*3;


create table tmp as 
	select distinct i.ben_idt_ano, i.dte_acte, min(dte_debut_gross) format=ddmmyy10. as debut_gross 
from &lib_dmb..implant i
		inner join &lib_dmb..grossesse g on g.ben_idt_ano=i.ben_idt_ano 
	where dte_debut_gross is not null and dte_debut_gross between i.dte_acte and i.dte_fin
	group by  i.ben_idt_ano, i.dte_acte
;
create index ben_idt_ano on tmp(ben_idt_ano);

update  &lib_dmb..implant i set dte_fin=
	(select debut_gross from tmp t where t.ben_idt_ano=i.ben_idt_ano and t.dte_acte=i.dte_acte)
	where exists (select * from tmp t2 where t2.ben_idt_ano=i.ben_idt_ano and t2.dte_acte=i.dte_acte)
;

quit;



/* ajouter la presence de class_co sur les sequences de tt AE */

proc sql;

create table class_co as 
	select distinct ben_idt_ano, exe_soi_dtd as dte_debut, date_fin, class_co, ind_med
	from &lib_dmb..med_gross
	where class_co is not null
;
insert into class_co
	select distinct ben_idt_ano, dte_acte, dte_fin, 'PROG', 'IMPL'
	from &lib_dmb..implant
;

create index ben_idt_ano on class_co(ben_idt_ano);

create table seq_co as 
	select distinct s.ben_idt_ano, num_seq, c.class_co, c.ind_med, catx('',s.ben_idt_ano,num_seq) as cle
	from &lib_dmb..sequence s
		inner join class_co c on c.ben_idt_ano=s.ben_idt_ano
	where c.dte_debut<=fin_seq and c.date_fin>=debut_seq
	order by cle, c.class_co
;


quit;


/* concat class_co par sequence */
data new_co;
	set seq_co;
	by cle;
	retain rgp_co;
	retain nb_rgpco;

	length rgp_co $200.;

	if first.cle then do;
		rgp_co='';
		nb_rgpco=0;
	end;
	rgp_co=catx(',',rgp_co,class_co);
	nb_rgpco=nb_rgpco+1;

	if last.cle then do;
		rgp_co=substr(rgp_co,1,length(rgp_co));
		output;
	end;
run;


proc sql;

create index ben_idt_ano on new_co(ben_idt_ano);
create index num_seq on new_co(num_seq);
quit;

proc sql;
alter table &lib_dmb..sequence add class_co varchar(30);
update &lib_dmb..sequence s set class_co=null;

update &lib_dmb..sequence s set class_co=
	(select rgp_co from new_co n where s.ben_idt_ano=n.ben_idt_ano and s.num_seq=n.num_seq)	
;

quit;