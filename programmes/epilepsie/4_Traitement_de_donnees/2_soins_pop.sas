/*****************************************************************************/
/*                ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                   */
/*****************************************************************************/

/*****************************************************************************/
/* Nom du programme : soins_pop

/* Objectif : constituer les periodes de suivi des patients et 
creer les indicateurs de suivi sur ces periodes */

/*referentiels generes dans data_ref.sas

/* table de sortie : &lib_dmb..periode_soins */

/*****************************************************************************/


/* generation de la table de suivi par periode */
/* decoupage mensuel de -6mois a +12 mois / a date index */

proc sql;

create table &lib_dmb..periode_soins as 
	select p.ben_idt_ano, yob, date_index,incident,pop1,pop2,index_ae
		, periode, (date_index+p_deb) format=ddmmyy10. as debut_periode, (date_index+p_fin) format=ddmmyy10. as fin_periode	
	from &lib_dmb..patient p, cod_periode c
;

create index ben_idt_ano on &lib_dmb..periode_soins(ben_idt_ano);
create index periode on &lib_dmb..periode_soins(periode);
create index debut_periode on &lib_dmb..periode_soins(debut_periode);

quit;


/* calcul des indicatrices de recours aux soins dans chaque periode */
proc sql;
alter table &lib_dmb..periode_soins add
	nb_consult_neuro smallint, nb_consult_mg smallint,	nb_consult_urg smallint, nb_consult_ped smallint, nb_consult_tot smallint
	, nb_hdj_epi smallint, nb_hosp_epi smallint, nb_hosp smallint
	, nb_acte_eeg smallint,	nb_acte_irm	smallint
	, nb_med_ae smallint, nb_ind_med smallint, ind_med varchar(50), duree_poly smallint
	, nb_ind_fol smallint, ind_fol num(1)
	, prim num(1), sec num(1), ter num(1)
	, date_prim date format=ddmmyy10., date_sec date format=ddmmyy10., date_ter date format=ddmmyy10.
	, prim_mg num(1), prim_urg num(1), sec_eeg num(1), sec_neuro num(1), sec_irm num(1)
	, sec_eeg_delai smallint,	sec_neuro_delai smallint,	sec_irm_delai smallint /* non calcules => peu d'interet du delai par rapport au debut de chaque periode */
	, ter_delai smallint
	, nb_consult_tele smallint
;
quit;


/* indicateur consult */
/* ***************** */

%macro nb_consult(var_s=, condition=);

proc sql;
create table tmp as 
	select p.ben_idt_ano, periode, count(distinct consult_dte) as nb
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		&condition
	group by p.ben_idt_ano, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index periode on tmp(periode);

alter table &lib_dmb..periode_soins add	&var_s smallint;

update &lib_dmb..periode_soins p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_consult(var_s=nb_consult_mg, condition= and spe_medecin='MG');
%nb_consult(var_s=nb_consult_neuro, condition= and spe_medecin='NEURO');
%nb_consult(var_s=nb_consult_ped, condition= and spe_medecin='PED');
%nb_consult(var_s=nb_consult_tot, condition= );
%nb_consult(var_s=nb_consult_urg, condition= and prs_nat_ref='ATU');

%nb_consult(var_s=nb_consult_mg_ter, condition= and spe_medecin='MG' and c.ter=1);
%nb_consult(var_s=nb_consult_neuro_ter, condition= and spe_medecin='NEURO' and c.ter=1);
%nb_consult(var_s=nb_consult_ped_ter, condition= and spe_medecin='PED' and c.ter=1);
%nb_consult(var_s=nb_consult_tot_ter, condition=  and c.ter=1);
%nb_consult(var_s=nb_consult_urg_ter, condition= and prs_nat_ref='ATU' and c.ter=1);

%nb_consult(var_s=nb_consult_mg_chu, condition= and spe_medecin='MG' and c.ter=0 and lieu_exe='CHU');
%nb_consult(var_s=nb_consult_neuro_chu, condition= and spe_medecin='NEURO' and c.ter=0 and lieu_exe='CHU');
%nb_consult(var_s=nb_consult_ped_chu, condition= and spe_medecin='PED' and c.ter=0 and lieu_exe='CHU');
%nb_consult(var_s=nb_consult_tot_chu, condition=  and c.ter=0 and lieu_exe='CHU');
%nb_consult(var_s=nb_consult_urg_chu, condition= and prs_nat_ref='ATU' and c.ter=0 and lieu_exe='CHU');

%nb_consult(var_s=nb_consult_mg_ch, condition= and spe_medecin='MG' and c.ter=0 and lieu_exe='CH');
%nb_consult(var_s=nb_consult_neuro_ch, condition= and spe_medecin='NEURO' and c.ter=0 and lieu_exe='CH');
%nb_consult(var_s=nb_consult_ped_ch, condition= and spe_medecin='PED' and c.ter=0 and lieu_exe='CH');
%nb_consult(var_s=nb_consult_tot_ch, condition=  and c.ter=0 and lieu_exe='CH');
%nb_consult(var_s=nb_consult_urg_ch, condition= and prs_nat_ref='ATU' and c.ter=0 and lieu_exe='CH');

%nb_consult(var_s=nb_consult_mg_vil, condition= and spe_medecin='MG' and c.ter=0 and lieu_exe in ('LIB','UNK'));
%nb_consult(var_s=nb_consult_neuro_vil, condition= and spe_medecin='NEURO' and c.ter=0 and lieu_exe in ('LIB','UNK'));
%nb_consult(var_s=nb_consult_ped_vil, condition= and spe_medecin='PED' and c.ter=0 and lieu_exe in ('LIB','UNK'));
%nb_consult(var_s=nb_consult_tot_vil, condition=  and c.ter=0 and lieu_exe in ('LIB','UNK'));
%nb_consult(var_s=nb_consult_urg_vil, condition= and prs_nat_ref='ATU' and c.ter=0 and lieu_exe in ('LIB','UNK'));


*%nb_consult(var_s=nb_consult_tele, condition= and code_consult='TELE' and spe_medecin='NEURO');
%nb_consult(var_s=nb_consult_tele, condition= and code_consult='TELE' );
%nb_consult(var_s=nb_consult_tele_ter, condition= and code_consult='TELE' and c.ter=1);
%nb_consult(var_s=nb_consult_tele_chu, condition= and code_consult='TELE' and c.ter=0 and lieu_exe='CHU');
%nb_consult(var_s=nb_consult_tele_ch, condition= and code_consult='TELE'  and c.ter=0 and lieu_exe='CH' );
%nb_consult(var_s=nb_consult_tele_vil, condition= and code_consult='TELE' and c.ter=0 and lieu_exe in ('LIB','UNK'));


/* indicateur hospit */
/* ***************** */
%macro nb_hosp(var_s=, condition=);

proc sql;
create table tmp as 
	select p.ben_idt_ano, periode, count(distinct ent_dat) as nb
	from &lib_dmb..hist_rss c 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where ent_dat between debut_periode and fin_periode 
		&condition
	group by p.ben_idt_ano, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index periode on tmp(periode);

update &lib_dmb..periode_soins p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_hosp(var_s=nb_hdj_epi, condition= and nb_nuit=0 and epi=1);
%nb_hosp(var_s=nb_hosp_epi, condition= and nb_nuit>0 and epi=1);
%nb_hosp(var_s=nb_hosp, condition= );




/* indicateur actes */
/* ***************** */

%macro nb_act(var_s=, condition=);

proc sql;
create table tmp1 as 
	select distinct p.ben_idt_ano, periode, dte_acte 
	from &lib_dmb..hist_act h 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=h.ben_idt_ano 
	where dte_acte between debut_periode and fin_periode 
		&condition
union
	select distinct p.ben_idt_ano, periode, exe_soi_dtd as dte_acte
	from &lib_dmb..hist_act2 h 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=h.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
		&condition
;

create table tmp as
	select ben_idt_ano, periode, count(distinct dte_acte) as nb
	from tmp1
	group by ben_idt_ano, periode 
;

create index ben_idt_ano on tmp(ben_idt_ano);
create index periode on tmp(periode);

update &lib_dmb..periode_soins p set &var_s=
	(select nb from tmp t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set &var_s=0 where &var_s is null;

quit;
%mend;

%nb_act(var_s=nb_acte_eeg, condition= and ccam_gpe='EEG');
%nb_act(var_s=nb_acte_irm, condition= and ccam_gpe='IMA');


/* indicateur med    */
/* ***************** */

/* med ae */
proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, ind_med, pha_prs_c13 as cod13 , exe_soi_dtd,sum(pha_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F a on a.ben_idt_ano=p.ben_idt_ano 
	where med_epi=1 
	group by p.ben_idt_ano,  ind_med, cod13,exe_soi_dtd
	having nb>0
;

insert into  tmp  
	select distinct p.ben_idt_ano, ind_med, input(ucd_ucd_cod,13.)  , exe_soi_dtd,sum(ucd_dlv_nbr) 
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F a on a.ben_idt_ano=p.ben_idt_ano 
	where med_epi=1 
	group by p.ben_idt_ano, ind_med, ucd_ucd_cod,exe_soi_dtd
	having sum(ucd_dlv_nbr)>0
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index exe_soi_dtd on tmp(exe_soi_dtd);


/* nombre de delivrance */
create table del_periode as 
	select p.ben_idt_ano,  periode, count(distinct exe_soi_dtd) as nb
	from tmp t
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, periode
;

create index ben_idt_ano on del_periode(ben_idt_ano);
create index periode on del_periode(periode);

update &lib_dmb..periode_soins p set nb_med_ae=
	(select nb from del_periode t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set nb_med_ae=0 where nb_med_ae is null;

/* nombre de ind_med */
create table indmed_periode as 
	select p.ben_idt_ano, periode, count(distinct t.ind_med) as nb
	from tmp t
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, periode
;

create index ben_idt_ano on indmed_periode(ben_idt_ano);
create index periode on indmed_periode(periode);

update &lib_dmb..periode_soins p set nb_ind_med=
	(select nb from indmed_periode t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set nb_ind_med=0 where nb_ind_med is null;


quit;

/* ind_med different par periode */

proc sql;

create table indmed_periode2 as 
	select distinct p.ben_idt_ano, periode, t.ind_med, catx('',p.ben_idt_ano, periode) as cle
	from tmp t
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
;
quit;

proc sort data=indmed_periode2;
	by cle;
run;
data tmp_indmed;
set indmed_periode2;
by cle;

retain indmed_cat;
retain nb;
length indmed_cat $50.;

if first.cle then do;
	indmed_cat='';
	nb=0;
end;
indmed_cat=catx('',ind_med,indmed_cat);
nb=nb+1;
if last.cle then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_indmed (ben_idt_ano);
create index periode on tmp_indmed (periode);


update &lib_dmb..periode_soins p set ind_med=
	(select indmed_cat from tmp_indmed t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;

quit;


/* nb jour en poly therapie */


proc sql;

create index ind_med on tmp_indmed (ind_med);

create table tmp as 
	select distinct p.ben_idt_ano,periode,debut_periode format=ddmmyy10.,fin_periode format=ddmmyy10.,debut_seqpoly,fin_seqpoly
		,case when debut_seqpoly>debut_periode then debut_seqpoly else debut_periode end format=ddmmyy10. as debut_expo
		,case when fin_seqpoly<fin_periode then fin_seqpoly else fin_periode end format=ddmmyy10.  as fin_expo
		,(case when fin_seqpoly<fin_periode then fin_seqpoly else fin_periode end)- (case when debut_seqpoly>debut_periode then debut_seqpoly else debut_periode end) as tps_expo
	from &lib_dmb..periode_soins p, &lib_dmb..sequence_poly m
	where p.ben_idt_ano=m.ben_idt_ano 
		and m.debut_seqpoly<=p.fin_periode and m.fin_seqpoly>=p.debut_periode	
		and poly=1 and duree>90
;


create table tmp2 as
	select ben_idt_ano, periode,sum(tps_expo) as tps
	from tmp
	group by ben_idt_ano, periode
;

create index ben_idt_ano on tmp2(ben_idt_ano);
create index periode on tmp2(periode);


update &lib_dmb..periode_soins p set duree_poly=
	(select tps from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set duree_poly=0 where duree_poly is null;

quit;




/* med acide folique */
proc sql;

alter table &lib_dmb..periode_soins add
 nb_ind_fol smallint, ind_fol num(1)
;

create table tmp as 
	select distinct p.ben_idt_ano, pha_prs_c13 as cod13 , exe_soi_dtd,sum(pha_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F a on a.ben_idt_ano=p.ben_idt_ano 
	where ind_med='AF' 
	group by p.ben_idt_ano, cod13,exe_soi_dtd
	having nb>0
;
quit;

proc sql;

/* nombre de delivrance */
create table del_periode as 
	select p.ben_idt_ano, periode, count(distinct exe_soi_dtd) as nb
	from tmp t
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=t.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
	group by p.ben_idt_ano, periode
;

create index ben_idt_ano on del_periode(ben_idt_ano);
create index periode on del_periode(periode);

update &lib_dmb..periode_soins p set nb_ind_fol=
	(select nb from del_periode t where t.ben_idt_ano=p.ben_idt_ano and t.periode=p.periode)
;
update &lib_dmb..periode_soins set nb_ind_fol=0 where nb_ind_fol is null;

update &lib_dmb..periode_soins p set 
	ind_fol=case when nb_ind_fol>0 then 1 else 0 end;


quit;


/* soins primaires  */
/* **************** */
proc sql;

update &lib_dmb..periode_soins set
	prim=case when nb_consult_mg>0 or nb_consult_urg>0 or nb_consult_ped>0 then 1 else 0 end
	,prim_mg= case when nb_consult_mg>0 or nb_consult_ped>0 then 1 else 0 end
	,prim_urg= case when nb_consult_urg>0 then 1 else 0 end

;


create table soin as 
	select p.ben_idt_ano, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_mg>0 or nb_consult_urg>0 or nb_consult_ped>0
	group by p.ben_idt_ano, periode
;
create index ben_idt_ano on soin(ben_idt_ano);
create index periode on soin(periode);

update &lib_dmb..periode_soins p set date_prim=
	(select case when periode<6 then max_dte else min_dte end format=ddmmyy10. 
		from soin s where s.ben_idt_ano=p.ben_idt_ano and s.periode=p.periode )
;




quit;


/* soins secondaires */
/* ***************** */
proc sql;

update &lib_dmb..periode_soins set 
	sec=case when nb_consult_neuro>0 or nb_acte_eeg>0 then 1 else 0 end
	,sec_eeg=case when nb_acte_eeg>0 then 1 else 0 end
	,sec_neuro=case when nb_consult_neuro>0 then 1 else 0 end
	,sec_irm=case when nb_acte_irm>0 then 1 else 0 end
;

create table soin as 
	select p.ben_idt_ano, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_neuro>0
	group by p.ben_idt_ano, periode
;

create table tmp1 as 
	select distinct p.ben_idt_ano, periode, dte_acte 
	from &lib_dmb..hist_act h 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=h.ben_idt_ano 
	where dte_acte between debut_periode and fin_periode 
		and ccam_gpe='EEG'
union
	select distinct p.ben_idt_ano, periode, exe_soi_dtd as dte_acte
	from &lib_dmb..hist_act2 h 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=h.ben_idt_ano 
	where exe_soi_dtd between debut_periode and fin_periode 
		and ccam_gpe='EEG'
;

insert into soin
	select ben_idt_ano, periode, min(dte_acte) as min_dte, max(dte_acte) as max_dte
	from tmp1 t
	group by ben_idt_ano, periode
;

create table tmp as 
	select ben_idt_ano, periode, min(min_dte) as min_dte, max(max_dte) as max_dte
	from soin
	group by ben_idt_ano, periode
;
create index ben_idt_ano on tmp(ben_idt_ano);
create index periode on tmp(periode);


update &lib_dmb..periode_soins p set date_sec=
	(select case when periode<6 then max_dte else min_dte end format=ddmmyy10. 
		from tmp s where s.ben_idt_ano=p.ben_idt_ano and s.periode=p.periode )
;

quit;



/* soins tertiaires  */
/* ***************** */

proc sql;

create table soin as 
	select p.ben_idt_ano, periode, min(consult_dte) as min_dte, max(consult_dte) as max_dte
	from &lib_dmb..consult c 
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where consult_dte between debut_periode and fin_periode 
		and nb_consult_neuro>0 and spe_medecin='NEURO' and c.ter=1
	group by p.ben_idt_ano, periode
;
create index ben_idt_ano on soin(ben_idt_ano);
create index periode on soin(periode);

update &lib_dmb..periode_soins p set date_ter=
	(select case when periode<6 then max_dte else min_dte end format=ddmmyy10. 
		from soin s where s.ben_idt_ano=p.ben_idt_ano and s.periode=p.periode )
;


update &lib_dmb..periode_soins set 
	ter=case when date_ter is not null then 1 else 0 end
;


update &lib_dmb..periode_soins p set ter_delai= date_ter-debut_periode 
;


quit;



/* attribution de l'etat de la sequence de TT (=> representation sequence) */
/* *********************************************************************** */

/* marquer les periodes apres DC */

proc sql;

alter table &lib_dmb..periode_soins add etat_tt varchar(2);

update &lib_dmb..periode_soins set etat_tt = null ;

create table tmp_dc as 
	select distinct s.ben_idt_ano ,periode
	from &lib_dmb..periode_soins s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where s.debut_periode>p.dcd_dte and p.dcd_dte is not null
;

create index ben_idt_ano on tmp_dc(ben_idt_ano);
create index periode on tmp_dc(periode);


update &lib_dmb..periode_soins p set 
	etat_tt = 'DC'
	where exists (select * from tmp_dc t where p.ben_idt_ano=t.ben_idt_ano and p.periode=t.periode)
;


quit;

/* bitherapie ou polytherapie */
proc sql;



update &lib_dmb..periode_soins set 
	etat_tt = case when duree_poly>0 then 'P'
		when duree_poly=0 and nb_ind_med>1 then 'B'
	end
where etat_tt is null
;

quit;


/* macro recherche mono a etiquetter
lancer les macro dans l'ordre */
%macro etat_mono(var_mono=);

proc sql;

create table tmp1 as 
	select ben_idt_ano ,min(periode) as periode
	from &lib_dmb..periode_soins
	where etat_tt is null and periode>=6 and ind_med is not null
		and duree_poly=0 and nb_ind_med=1
	group by ben_idt_ano
;

create index ben_idt_ano on tmp1(ben_idt_ano);
create index periode on tmp1(periode);


update &lib_dmb..periode_soins p set 
	etat_tt = &var_mono.
	where exists (select * from tmp1 t where p.ben_idt_ano=t.ben_idt_ano and p.periode=t.periode)
;


/* propager eiquette mono sur les periodes suivante pour le meme ind_med */

create table tmp1b as 
	select t.ben_idt_ano ,t.periode, p.ind_med
	from &lib_dmb..periode_soins p 
		inner join tmp1 t on p.ben_idt_ano=t.ben_idt_ano and p.periode=t.periode
;

create index ben_idt_ano on tmp1b(ben_idt_ano);
create index periode on tmp1b(periode);

create table tmp_m1 as 
	select distinct p2.ben_idt_ano , p2.periode
		from tmp1b p1  
		inner join &lib_dmb..periode_soins p2 on p1.ben_idt_ano=p2.ben_idt_ano
	where p2.periode>p1.periode and p1.ind_med=p2.ind_med
		and p2.etat_tt is null and p2.duree_poly=0 and p2.nb_ind_med=1
;
create index ben_idt_ano on tmp_m1(ben_idt_ano);
create index periode on tmp_m1(periode);

update &lib_dmb..periode_soins p set 
	etat_tt = &var_mono.
	where exists (select * from tmp_m1 t where p.ben_idt_ano=t.ben_idt_ano and p.periode=t.periode)
;


quit;

%mend;

%etat_mono(var_mono='M1');
%etat_mono(var_mono='M2');
%etat_mono(var_mono='M3');
%etat_mono(var_mono='M4');
%etat_mono(var_mono='M5');




/* attribution d'un numero de ligne de tt en fonction des changement d'etat */
/* a partir de la periode d'initiation */
/* ************************************************************************ */

proc sql;

create table tmp_periode as 
	select ben_idt_ano, periode, etat_tt
	from &lib_dmb..periode_soins
	where periode>=6 
	order by ben_idt_ano, periode
;
quit;

/* 1er phase on considere que chaque changement de TT est un changement de ligne*/
/* y compris pas de TT */

data ligne_tt;
set tmp_periode;
by ben_idt_ano;

retain l_tt;
retain first_num;

l_tt=lag(etat_tt);

if first.ben_idt_ano then do;
	first_num=0;
	l_tt='';
end;

if l_tt ne etat_tt then do;
	first_num=first_num+1;
end;

run;

/* recherche pour chaque periode le TT suivant */
/* => permettra de fusionner les lignes de TT identiques interrompues par 
1 seule periode sans TT */

proc sort data=ligne_tt;
	by ben_idt_ano  descending periode;
run;

data ligne_tt;
set ligne_tt;
by ben_idt_ano;

retain next_tt;

next_tt=lag(etat_tt);

run;

proc sort data=ligne_tt;
	by ben_idt_ano  periode;
run;

proc sql;

create index ben_idt_ano on ligne_tt(ben_idt_ano);
create index periode on ligne_tt(periode);
quit;


/* calcule du nombre de periode consecutive sur le meme TT*/
/* => permettra de fusionner les lignes de TT identiques interrompues par 
1 seule periode sans TT */

proc sql;
create table tmp as 
	select ben_idt_ano, first_num,count(*) as nb, min(periode) as min_per, max(periode) as max_per
	from ligne_tt
	group by ben_idt_ano, first_num
;
create index ben_idt_ano on tmp(ben_idt_ano);

quit;

proc sql;

alter table ligne_tt add nb_periode num(1),  per_n1 num(1);

update ligne_tt l set nb_periode=
	(select nb from tmp t where t.ben_idt_ano=l.ben_idt_ano and l.first_num=t.first_num)
;


update ligne_tt l set per_n1 =
	(select min_per from tmp t
	where t.ben_idt_ano=l.ben_idt_ano and l.periode between min_per and max_per
	)
;

quit;



/* numerotation des lignes de TT en fusionnant les lignes de TT identiques interrompues par 
1 seule periode sans TT */

proc sql;

create table tmp2 as 
	select ben_idt_ano, periode, etat_tt from ligne_tt
	where etat_tt ne '' or (etat_tt ='' and nb_periode>1) or (etat_tt ='' and nb_periode=1 and periode=18)
	order by ben_idt_ano, periode
;


quit;


data ligne_tt2;
set tmp2;
by ben_idt_ano;

retain l_tt;
retain scd_num;

l_tt=lag(etat_tt);

if first.ben_idt_ano then do;
	scd_num=0;
	l_tt='';
end;

if (periode<18 and l_tt ne etat_tt) or (periode=18 and etat_tt ne '' and l_tt ne etat_tt) then do;
	scd_num=scd_num+1;
end;

run;


proc sql;

create table periode_l2 as 
	select ben_idt_ano, scd_num, min(periode) as min_per, max(periode) as max_per
	from ligne_tt2
	group by ben_idt_ano, scd_num
;
create index ben_idt_ano on periode_l2(ben_idt_ano);

alter table ligne_tt add num_tt num(1),  per_n2 num(1);

update ligne_tt l set num_tt =
	(select scd_num from periode_l2 p 
	where p.ben_idt_ano=l.ben_idt_ano and l.periode between min_per and max_per
	)
;
update ligne_tt l set per_n2 =
	(select min_per from periode_l2 p 
	where p.ben_idt_ano=l.ben_idt_ano and l.periode between min_per and max_per
	)
;

quit;




/* numerotation des lignes de TT en fusionnant les lignes de TT identiques interrompues par 
1 ou pls periode(s) sans TT */

proc sql;

create table tmp3 as 
	select ben_idt_ano, periode, etat_tt from ligne_tt
	where etat_tt ne ''
	order by ben_idt_ano, periode
;


quit;


data ligne_tt3;
set tmp3;
by ben_idt_ano;

retain l_tt;
retain trz_num;

l_tt=lag(etat_tt);

if first.ben_idt_ano then do;
	trz_num=0;
	l_tt='';
end;

if l_tt ne etat_tt then do;
	trz_num=trz_num+1;
end;

run;


proc sql;

create table periode_l3 as 
	select ben_idt_ano, trz_num, min(periode) as min_per, max(periode) as max_per
	from ligne_tt3
	group by ben_idt_ano, trz_num
;
create index ben_idt_ano on periode_l3(ben_idt_ano);

alter table ligne_tt add num3_tt num(1),  per_n3 num(1);

update ligne_tt l set num3_tt =
	(select trz_num from periode_l3 p 
	where p.ben_idt_ano=l.ben_idt_ano and l.periode between min_per and max_per
	)
;
update ligne_tt l set per_n3 =
	(select min_per from periode_l3 p 
	where p.ben_idt_ano=l.ben_idt_ano and l.periode between min_per and max_per
	)
;

quit;



proc sql;

alter table &lib_dmb..periode_soins add ligne_n1 num(1), ligne_n2 num(1), ligne_n3 num(1),per_n1 num(1),per_n2 num(1), per_n3 num(1);

update &lib_dmb..periode_soins p set 
	ligne_n1= (select first_num from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)
	,ligne_n2= (select num_tt from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)
	,ligne_n3= (select num3_tt from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)

;

update &lib_dmb..periode_soins p set 
	per_n2= (select per_n2 from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)
	,per_n3= (select per_n3 from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)
;

update &lib_dmb..periode_soins p set 
	per_n1= (select per_n1 from ligne_tt l where l.ben_idt_ano=p.ben_idt_ano and l.periode=p.periode)
;


quit;

/* type de la premiere periode non vide */

proc sql;

create table tmp as 
	select ben_idt_ano,min(periode) as minper
	from &lib_dmb..periode_soins 
	where periode>=6 and etat_tt is not null
	group by ben_idt_ano
;
create index ben_idt_ano on tmp(ben_idt_ano);



create table tmp2 as 
	select p.ben_idt_ano, minper,etat_tt as first_etat_suivi
	from &lib_dmb..periode_soins p
		inner join tmp t on t.ben_idt_ano=p.ben_idt_ano and t.minper=p.periode
;

create index ben_idt_ano on tmp2(ben_idt_ano);

alter table &lib_dmb..periode_soins add minper num(1), first_etat_suivi varchar(2)
;

update &lib_dmb..periode_soins p set 
	minper= (select minper from tmp2 l where l.ben_idt_ano=p.ben_idt_ano );
update &lib_dmb..periode_soins p set 
	first_etat_suivi= (select first_etat_suivi from tmp2 l where l.ben_idt_ano=p.ben_idt_ano );




quit;


/* classe_ae*/

proc sql;

alter table &lib_dmb..periode_soins add class_ae varchar(30);

create table tmp as 
	select distinct p.ben_idt_ano, periode, c.class_ae, catx('',p.ben_idt_ano,periode) as cle
	from &lib_dmb..cure c
		inner join &lib_dmb..periode_soins p on p.ben_idt_ano=c.ben_idt_ano 
	where deb_cure_dte <= fin_periode and fin_cure_dte>=debut_periode and fin_periode
	order by cle
;
quit;

%_eg_conditional_dropds(new_ae);

/* concat ae par periode */
data new_ae;
	set tmp;
	by cle;
	retain rgp_ae;
	retain nb_rgpae;

	length rgp_ae $200.;

	if first.cle then do;
		rgp_ae='';
		nb_rgpae=0;
	end;
	rgp_ae=catx(',',rgp_ae,class_ae);
	nb_rgpae=nb_rgpae+1;

	if last.cle then do;
		rgp_ae=substr(rgp_ae,1,length(rgp_ae));
		output;
	end;
run;


proc sql;

create index ben_idt_ano on new_ae(ben_idt_ano);
create index periode on new_ae(periode);
quit;



proc sql;

update &lib_dmb..periode_soins s set class_ae=
	(select class_ae from new_ae n where n.ben_idt_ano=s.ben_idt_ano and n.periode=s.periode)
;

quit;


/*
proc sql;

drop table &lib_dmb..periode_soins;
create table &lib_dmb..periode_soins as 
	select * from periode_soins;
quit;

*/