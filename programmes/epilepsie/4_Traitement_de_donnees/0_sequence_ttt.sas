/*****************************************************************************/
/*                ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                   */
/*****************************************************************************/


/*****************************************************************************/
/* Nom du programme : sequence_ttt

/* Objectif : calculer les lignes de traitement antiepileptique
selon le dernier niveau de classe ATC, soit selon la molecule active*/

/* referentiel : liste de codes medicaments antiepileptiques

/* table de sortie : &lib_dmb..cure */
/*****************************************************************************/


/* creation cures de traitement epi par ind_med */
/************************************************/

/* ensemble des delivrances PHA et UCD */
proc sql;
create table tmp as 
	select distinct ben_idt_ano, exe_soi_dtd, ind_med,psp_spe_cod, sum(pha_act_qsn) as nb_bte
	from &lib_ext..ER_PHA_F a  
	where med_epi=1
	group by ben_idt_ano, exe_soi_dtd, ind_med
	having sum(pha_act_qsn)>0

;
insert into tmp 
	select distinct ben_idt_ano , exe_soi_dtd, ind_med,psp_spe_cod, sum(ucd_dlv_nbr) 
	from &lib_ext..ER_UCD_F a  
	where med_epi=1
	group by ben_idt_ano, exe_soi_dtd, ind_med
	having sum(ucd_dlv_nbr)>0

;

create index ben_idt_ano on tmp(ben_idt_ano);
quit;


/* recuperation des periodes d'hospitalisation de plus d'une nuit */
/* par defaut 1 delivrance = 30j d'exposition */
proc sql;

create table tmp2 as 
	select t.*,  r.ent_dat, r.sor_dat, catx('',t.ben_idt_ano,t.ind_med) as cle
	from tmp t
		left join &lib_dmb..hist_rss r on r.ben_idt_ano=t.ben_idt_ano
			and sor_dat>exe_soi_dtd and ent_dat<=exe_soi_dtd+30 
			and nb_nuit>1 and grg_ghm not like '28%'
	group by t.ben_idt_ano, t.ind_med,exe_soi_dtd, r.ent_dat, r.sor_dat
;

/* calcul du nombre de jour d'hospitalisation au cours de l'exposition */
/* ajout de ce nb de jour au temps d'exposition */

alter table tmp2 add nbj_hosp integer, fin_expo date format=ddmmyy10.;

update tmp2 set nbj_hosp=min(sor_dat,exe_soi_dtd+30)-max(ent_dat,exe_soi_dtd)
where ent_dat is not null;

update tmp2 set fin_expo=exe_soi_dtd+30+max(0,nbj_hosp);
quit;


/* creation des cures pour chaque type de traitement (ind_med) */
/* on considere qu'il y a continuite de TT s'il n'y a pas plus de 
7 jours entre la fin d'une exposition et la delivrance suivante */

data &lib_dmb..delivrance;
	set tmp2;
	by cle;

	retain num_cure; /* numero de la cure */
	retain num_del_cure; /* numero de delivrance dans la cure */
	retain num_delivrance; /* numero de delivrance / total */
	retain l_indmed ;
	retain l_exe_soi;
	retain l_finexpo;

	format l_exe_soi l_finexpo ddmmyy10.;

	l_exe_soi=lag(exe_soi_dtd);
	l_finexpo=lag(fin_expo);
	l_indmed=lag(ind_med);

	if first.cle then do;
		num_cure=1;
		num_del_cure=0;
		num_delivrance=0;
		l_finexpo=.;
		l_indmed=.;
		l_exe_soi=.;
	end;

	num_del_cure=num_del_cure+1;
	num_delivrance=num_delivrance+1;

	delai=exe_soi_dtd-l_finexpo;

	if delai>7 then do;
		num_cure=num_cure+1;
		num_del_cure=1;
	end;

run;


proc sql;
create index ben_idt_ano on &lib_dmb..delivrance(ben_idt_ano);
create index exe_soi_dtd on &lib_dmb..delivrance(exe_soi_dtd);
quit;


%_eg_conditional_dropds(&lib_dmb..cure);

/* Initialisation de la table cure */
proc sql;
create table &lib_dmb..cure as 
	select distinct ben_idt_ano,ind_med,num_cure,max(num_del_cure) as  nb_del,
		min(exe_soi_dtd) as deb_cure_dte format=ddmmyy10., max(fin_expo) as fin_cure_dte format=ddmmyy10.,
		max(exe_soi_dtd) as last_del_dte format=ddmmyy10.
	from &lib_dmb..delivrance
	group by ben_idt_ano,ind_med,num_cure
;

create index ben_idt_ano on &lib_dmb..cure(ben_idt_ano);

quit;

/* attribution du code class_ae en fonction de ind_med */
proc sql;

create table tmp as 
	select distinct ind_med, class_ae from cod_med where med_epi=1;

alter table &lib_dmb..cure add class_ae varchar(12);

update &lib_dmb..cure c set class_ae=
	(select distinct class_ae from tmp t where t.ind_med=c.ind_med)
;

quit;






/* first class_ae */
/* determine apres generation des cures*/

proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, class_ae
	from &lib_dmb..patient p
		inner join &lib_dmb..cure c on c.ben_idt_ano=p.ben_idt_ano and p.first_ae_dte=c.deb_cure_dte
;

quit;

proc sort data=tmp;
	by ben_idt_ano ;
run;

data tmp_classae;
set tmp;
by ben_idt_ano;

retain classae_cat;
retain nb;
length classae_cat $50.;

if first.ben_idt_ano then do;
	classae_cat='';
	nb=0;
end;
classae_cat=catx('',class_ae,classae_cat);
nb=nb+1;
if last.ben_idt_ano then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_classae (ben_idt_ano);

alter table &lib_dmb..patient add first_classae varchar(50);
update &lib_dmb..patient p set first_classae=
	(select classae_cat from tmp_classae t where t.ben_idt_ano=p.ben_idt_ano )
;

quit;






/* table initiale de generation des sequences*/
%_eg_conditional_dropds(sequence_deb);
proc sql;

create table sequence_deb as
	select distinct ben_idt_ano,deb_cure_dte as debut_seq, fin_cure_dte as fin_seq, ind_med as rgp
	FROM &lib_dmb..cure
	order by ben_idt_ano,deb_cure_dte,fin_cure_dte,ind_med
;
quit;


%sequence();
%clean_rgp();

%repeat_seq();

/* ********************************** */
/* relance apres interruption */

/*
%_eg_conditional_dropds(sequence_deb);
proc sql;

create table sequence_deb as
	select distinct ben_idt_ano, debut_seq, fin_seq, rgp
	FROM &lib_dmb..sequence_fin
	order by ben_idt_ano,debut_seq,fin_seq,rgp
;
quit;


%sequence();
%clean_rgp();

%repeat_seq();
*/


/* supprimer cle */
proc sql;

alter table epi_dmb.sequence_fin  drop  cle,rgp1,rgp2;
quit;


/* probleme sur dernier patient 
/* ajout cure manquantes */

proc sql;
insert into epi_dmb.sequence_fin 
select distinct ben_idt_ano,deb_cure_dte , fin_cure_dte ,  ind_med,1 
	FROM &lib_dmb..cure
	where ben_idt_ano='BzzzuqZUqVigFRMmg' and deb_cure_dte>='07JUL2017'd
;
quit;




/* reperer les sequences poly consecutives */
/* *************************************** */

proc sql;

create table sequence as 
	select *
	from epi_dmb.sequence_fin
	order by ben_idt_ano,debut_seq
;
quit;

data sequence;
	set sequence;
	by ben_idt_ano;

	retain num_seq; /* numero de la sequence */
	retain num_ss_seq; /* numero de ss sequence*/
	retain poly ;
	retain l_nbind;
	retain l_poly;
	retain l_finseq;

	format  l_finseq ddmmyy10.;

	if nb_rgp>1 then poly=1; else poly=0 ;

	l_finseq=lag(fin_seq);

	l_nbind=lag(nb_rgp);
	l_poly=lag(poly);

	if first.ben_idt_ano then do;
		num_seq=0;
		num_ss_seq=0;
		l_nbind=.;
		l_poly=.;
		l_finseq=.;
		l_nbind=.;
	end;


	if poly=l_poly then do;
		num_ss_seq=num_ss_seq+1;
		if debut_seq-l_finseq>1 then do;
			num_seq=num_seq+1;
			num_ss_seq=1;
		end;
	end;
	else do;
		num_seq=num_seq+1;
		num_ss_seq=1;
	end;
run;



proc sql ;
	create table sequence2 as 
	select ben_idt_ano, debut_seq, fin_seq, rgp, nb_rgp, poly, num_seq, num_ss_seq 
		, min(nb_rgp) as nb_ind_min, max(nb_rgp) as nb_ind_max, cat(ben_idt_ano,num_seq) as cle
	from sequence 
	group by cle
	order by cle ,num_ss_seq desc;
run;


data sequence2;
	set sequence2;
	by cle;

	retain list_rgp; /* numero de la sequence */
	retain fin_seq2 ;

	format  fin_seq2 ddmmyy10.;
	length list_rgp $200.;

	if first.cle then do;
		list_rgp='';
		fin_seq2=.;
	end;

	list_rgp=catx(',',list_rgp,rgp);
	fin_seq2=fin_seq;
run;

data sequence2;
	set sequence2;
	nb_list=count(list_rgp,",")+1;
run;


/* macro pour dedoublonner la liste des traitements */
/* ************************************************ */
%macro clean_rgp();
%_eg_conditional_dropds(tmp_rgp);
data tmp_rgp;
	set sequence2;
	/*keep ben_idt_ano debut_seq fin_seq rgp_fin cle ;*/
	where nb_list>1 ;

	nb=1;
	c1_rgp=strip(list_rgp);
	length rgp_fin $200.;
	do while (nb<=nb_list) ;
		if nb=nb_list then do;
			rgp_fin=strip(tranwrd(c1_rgp,',',''));
		end;
		else do;
			rgp_fin=strip(substr(c1_rgp,1,index(c1_rgp,',')-1));
		end;
		nb_rgp2=count(c1_rgp,cats(rgp_fin,','))+count(c1_rgp,rgp_fin);
		/*nb_rgp2=count(c1_rgp,rgp_fin);*/
		test=count(c1_rgp,cats(rgp_fin,','));
		output;
 		/*nb_rgp2=count(compbl(strip(c1_rgp)),rgp_fin) + 1;*/
		c1_rgp=strip(tranwrd(c1_rgp,cats(rgp_fin,','),""));
		nb=nb+nb_rgp2;
	end;
run;

/* regenerer la liste des traitements */
proc sql;
	create table tmp_rgp1 as
		select distinct ben_idt_ano, cle, rgp_fin
		from tmp_rgp
		order by ben_idt_ano, cle, rgp_fin;
quit;

proc sort data=tmp_rgp1;
	by cle rgp_fin;
run;

data tmp_rgp2;
	set tmp_rgp1;
	by cle;
	retain rgp;
	retain nb_rgp;

	length rgp $200.;

	if first.cle then do;
		rgp='';
		nb_rgp=0;
	end;
	rgp=catx(',',rgp,rgp_fin);
	nb_rgp=nb_rgp+1;

	if last.cle then do;
		rgp=substr(rgp,1,length(rgp));
		output;
	end;
run;

proc sql;

	create index ben_idt_ano on tmp_rgp2(ben_idt_ano);
	create index cle on tmp_rgp2(cle);
	alter table sequence2 add rgp2 varchar(200);
	update sequence2 s set rgp2 =
		(select rgp from tmp_rgp2 t
			where s.ben_idt_ano=t.ben_idt_ano and s.cle=t.cle )
		where exists (select rgp from tmp_rgp2 t2
			where s.ben_idt_ano=t2.ben_idt_ano and s.cle=t2.cle )
	;
	*update sequence2 s set rgp=rgp2 where rgp2 is not null;

quit;
%mend;

%clean_rgp();

proc sql;

create table &lib_dmb..sequence as 
	select ben_idt_ano, debut_seq, fin_seq, rgp, nb_rgp, poly, num_seq, num_ss_seq
	from  sequence
;
	create index ben_idt_ano on epi_dmb.sequence(ben_idt_ano);

quit;


proc sql;

create table &lib_dmb..sequence_poly as 
	select ben_idt_ano, poly, num_seq, min(debut_seq) format=ddmmyy10. as debut_seqpoly , max(fin_seq) format=ddmmyy10. as fin_seqpoly 
		, max(num_ss_seq) as nb_ss_seq, nb_ind_min, nb_ind_max
		,case when rgp2 is null then list_rgp else rgp2 end as list_indmed
		, count(rgp2,",")+1 as nb_ind_med
	from sequence2
	group by ben_idt_ano, poly, num_seq,nb_ind_min, nb_ind_max,list_indmed,nb_ind_med;

create index ben_idt_ano on epi_dmb.sequence_poly(ben_idt_ano);


/* duree de la sequence */
alter table &lib_dmb..sequence_poly add duree integer;

update &lib_dmb..sequence_poly set duree=fin_seqpoly-debut_seqpoly ;

quit;




/* class_ae sur la sequence */
/* ************************ */

/* recuperer les sequences poly avec au moins un ind_med avec une classe ae*/
proc sql;
create table tmp as 
	select distinct s.*
	from &lib_dmb..sequence s, cod_med c
	where s.rgp contains c.ind_med
		and c.class_ae is not null and s.class_ae is null and nb_rgp>1
;
quit;

/* redecouper la liste ind_med pour pouvoir reatribuer la classe_ae de chacun */
data tmp_rgp;
	set tmp;
	/*keep ben_idt_ano debut_seq fin_seq rgp_fin cle ;*/

	nb=1;
	c1_rgp=strip(rgp);
	length rgp_fin $200.;
	cle=catx('',ben_idt_ano,num_seq);
	do while (nb<=nb_rgp) ;
		if nb=nb_rgp then do;
			rgp_fin=strip(tranwrd(c1_rgp,',',''));
		end;
		else do;
			rgp_fin=strip(substr(c1_rgp,1,index(c1_rgp,',')-1));
		end;
		nb_rgp2=count(c1_rgp,cats(rgp_fin,','))+count(c1_rgp,rgp_fin);
		/*nb_rgp2=count(c1_rgp,rgp_fin);*/
		test=count(c1_rgp,cats(rgp_fin,','));
		output;
 		/*nb_rgp2=count(compbl(strip(c1_rgp)),rgp_fin) + 1;*/
		c1_rgp=strip(tranwrd(c1_rgp,cats(rgp_fin,','),""));
		nb=nb+nb_rgp2;
	end;
run;


proc sql;

alter table tmp_rgp add new_ae varchar (20);

update tmp_rgp t set new_ae=
	(select distinct class_ae from cod_med c where c.ind_med=t.rgp_fin and c.class_ae is not null)
;
quit;

proc sql;
create table new_ae as 
	select distinct ben_idt_ano, num_seq, new_ae, cle
	from tmp_rgp
	order by cle, new_ae
;
quit;

/* concat new_ae par sequence */
data new_ae;
	set new_ae;
	by cle;
	retain rgp_ae;
	retain nb_rgpae;

	length rgp_ae $200.;

	if first.cle then do;
		rgp_ae='';
		nb_rgpae=0;
	end;
	rgp_ae=catx(',',rgp_ae,new_ae);
	nb_rgpae=nb_rgpae+1;

	if last.cle then do;
		rgp_ae=substr(rgp_ae,1,length(rgp_ae));
		output;
	end;
run;


proc sql;

create index ben_idt_ano on new_ae(ben_idt_ano);
create index num_seq on new_ae(num_seq);
quit;

proc sql;
alter table &lib_dmb..sequence add class_ae varchar(30);
update &lib_dmb..sequence s set class_ae=
	(select distinct class_ae from cod_med c where c.ind_med=s.rgp and c.class_ae is not null)
;
update &lib_dmb..sequence s set class_ae=
	(select rgp_ae from new_ae n where s.ben_idt_ano=n.ben_idt_ano and s.num_seq=n.num_seq)	
	where class_ae is null
;

quit;