/********************************************************************/
/*           ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE               */
/********************************************************************/
/* Nom du programme : consult

/* Objectif : creer les tables de consultations */

/* tables de sortie : &lib_dmb..consult  */

/*****************************************************************/


%_eg_conditional_dropds(&lib_dmb..consult);

proc sql;

/* source prs */
create table &lib_dmb..consult as 
	select distinct ben_idt_ano, 'VILLE' as origine, datepart(exe_soi_dtd) format=ddmmyy10. as consult_dte, put(prs_nat_ref,5.) as prs_nat_ref, prs_gpe as code_consult
		, spe_gpe as spe_medecin, pse_stj_cod  as juri_medecin , stj_gpe as lieu_exe, etb_exe_fin format=$9.
	from &lib_ext..er_prs_f f
		left join cod_spe e on e.spe_cod_num=pse_spe_cod
		left join cod_stj j on j.stj_cod=pse_stj_cod
	where prs_gpe in ('TELE','FOR', 'CONS', 'VIS') 
		and (ETE_IND_TAA <> 1 OR ETE_IND_TAA is null)
;

alter table &lib_dmb..consult modify etb_exe_fin varchar(9);

/* source hosp */
insert into &lib_dmb..consult
	select distinct p.ben_idt_ano, 'HOSP' as origine, ENT_DAT as consult_dte, act_cod, 'H' as code_consult
		, spe_gpe as spe_medecin, ., 'CH', eta_num
	from &lib_ext..T_MCO_FCSTC fc 
		inner join &lib_dmb..identite i on i.ben_nir_psa=fc.nir_ano_17 
		inner join &lib_dmb..patient p on i.ben_idt_ano=p.ben_idt_ano 
		left join cod_spe e on e.spe_cod=exe_spe
	where ACT_COD in ('C','C   N','CS','CNP','C   F','CNP N','CS  N','CG','CS  F','CNP F','CG  F','C N','CG  N','C   1',
					'CNPSY','C2','C  F','CPSYL','C  N','C F','CNP 1','CSF','CN1','CG N','CNPN','CG F','C3','C F F'
					,'CSC','ATU','ACO','ADA','ADC','ADE','ADI','ATM'
					)
;

insert into &lib_dmb..consult
	select distinct p.ben_idt_ano, 'HOSP' as origine, ENT_DAT as consult_dte, act_cod, 'H' as code_consult
		, spe_gpe as spe_medecin, ., 'CH', eta_num
	from &lib_ext..T_MCO_FBSTC fb 
		inner join &lib_dmb..identite i on i.ben_nir_psa=fb.nir_ano_17 
		inner join &lib_dmb..patient p on i.ben_idt_ano=p.ben_idt_ano 
		left join cod_spe e on e.spe_cod=exe_spe
	where ACT_COD in ('C','C   N','CS','CNP','C   F','CNP N','CS  N','CG','CS  F','CNP F','CG  F','C N','CG  N','C   1',
					'CNPSY','C2','C  F','CPSYL','C  N','C F','CNP 1','CSF','CN1','CG N','CNPN','CG F','C3','C F F'
					,'CSC','ATU','ACO','ADA','ADC','ADE','ADI','ATM'
					)
;

create index ben_idt_ano on &lib_dmb..consult(ben_idt_ano);
create index consult_dte on &lib_dmb..consult(consult_dte);
create index origine on &lib_dmb..consult(origine);

quit;


/* type etb */

proc sql;

update &lib_dmb..consult c set lieu_exe='CHU'
	where origine='HOSP' and lieu_exe='CH'
		and etb_exe_fin in (select finess from cod_finess where chu=1 )
;

update &lib_dmb..consult c set lieu_exe='TER'
	where origine='HOSP' and lieu_exe='CH'
		and etb_exe_fin in (select finess from cod_finess where ter=1 )
;

quit;

proc sql;
alter table &lib_dmb..consult add ter num(1);

update &lib_dmb..consult c set ter=1
	where etb_exe_fin in (select finess from cod_finess where ter=1 )
		or etb_exe_fin in (select finess_cut from cod_finess where ter=1 )
;
update &lib_dmb..consult c set ter=0 where ter is null;

quit;