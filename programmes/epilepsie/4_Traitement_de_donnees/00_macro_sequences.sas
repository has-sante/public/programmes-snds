/*****************************************************************************/
/*                ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                   */
/*****************************************************************************/

/*****************************************************************************/
/* Nom du programme : 0_macro_sequence

/* Objectif : generer les sequences de traitement antiepileptique
sous l hypothese que une delivrance couvre 30 jours de traitement

/* Sortie : macro sequence, clean_rgp et repeat_seq

/*****************************************************************************/


/* macro de generation de sequence de traitements */
/* ********************************************** */

%macro sequence();
%_eg_conditional_dropds(tmp_per);

/* comparaison des lignes de sequences 2 a 2 */
/* regeneration des lignes de sequence en fonction du chevauchement ou pas 
 des sequences pour un meme patient */

proc sort data=sequence_deb;
	by ben_idt_ano debut_seq;
run;

data tmp_per;
	set sequence_deb end=eof;
	by ben_idt_ano;
	retain l_numano;
	retain l_deb;
	retain l_fin;
	retain l_rgp;

	format l_deb l_fin res_deb res_fin ddmmyy10.;

	l_numano=lag(ben_idt_ano);
	l_deb=lag(debut_seq);
	l_fin=lag(fin_seq);
	l_rgp=lag(rgp);


	if l_numano=ben_idt_ano then do;
		res_numano=ben_idt_ano;
		if debut_seq>l_fin then do;
			if traite=1 then do;
				traite=0;
			end;
			else do;
				res_deb=l_deb;
				res_fin=l_fin;
				res_rgp=l_rgp;
				output;
			end;
		end;
		else do;
			if debut_seq=l_deb then do;
				if fin_seq=l_fin then do;
					res_deb=l_deb; res_fin=l_fin; res_rgp=l_rgp;
					output;
					res_deb=l_deb; res_fin=l_fin; res_rgp=rgp;
					output ;
					traite=1;
				end;
				if l_fin<fin_seq then do;
					res_deb=l_deb; res_fin=l_fin; res_rgp=l_rgp;
					output;
					res_deb=l_deb; res_fin=l_fin; res_rgp=rgp;
					output ;
					res_deb=l_fin+1; res_fin=fin_seq; res_rgp=rgp;
					output ;
					traite=1;
				end;
				if l_fin>fin_seq then do;
					res_deb=l_deb; res_fin=fin_seq; res_rgp=l_rgp;
					output;
					res_deb=l_deb; res_fin=fin_seq; res_rgp=rgp;
					output ;
					res_deb=fin_seq+1; res_fin=l_fin; res_rgp=l_rgp;
					output ;
					traite=1;
				end;
			end;
			else do;
				if l_fin=fin_seq then do;
					res_deb=l_deb; res_fin=debut_seq-1; res_rgp=l_rgp;
					output;
					res_deb=debut_seq; res_fin=l_fin; res_rgp=l_rgp;
					output ;
					res_deb=debut_seq; res_fin=l_fin; res_rgp=rgp;
					output ;
					traite=1;
				end;				
				if l_fin<fin_seq then do;
					res_deb=l_deb; res_fin=debut_seq-1; res_rgp=l_rgp;
					output;
					res_deb=debut_seq; res_fin=l_fin; res_rgp=l_rgp;
					output ;
					res_deb=debut_seq; res_fin=l_fin; res_rgp=rgp;
					output ;
					res_deb=l_fin+1; res_fin=fin_seq; res_rgp=rgp;
					output ;
					traite=1;
				end;
				if l_fin>fin_seq then do;
					res_deb=l_deb; res_fin=debut_seq-1; res_rgp=l_rgp;
					output;
					res_deb=debut_seq; res_fin=fin_seq; res_rgp=l_rgp;
					output ;
					res_deb=debut_seq; res_fin=fin_seq; res_rgp=rgp;
					output ;
					res_deb=fin_seq+1; res_fin=l_fin; res_rgp=l_rgp;
					output ;
					traite=1;
				end;				
			end;
		end;
	end;
	else do;
		if eof then do;
			res_numano=ben_idt_ano;
			res_deb=debut_seq;
			res_fin=fin_seq;
			res_rgp=rgp;
			output;
			res_numano=l_numano;
			res_deb=l_deb;
			res_fin=l_fin;
			res_rgp=l_rgp;
			output;
		end;
		else do;
			res_numano=l_numano;
			res_deb=l_deb;
			res_fin=l_fin;
			res_rgp=l_rgp;
			output;
		end;
	end;

run;


/* -----------------------------------------------------------------------		
-- le programme ci-dessus a genere certaines lignes de sequences a tord
	la suite du programme sert a "nettoyer" ces lignes
-- ---------------------------------------------------------------------- */
%_eg_conditional_dropds(tmp_periode);
proc sql;

create table tmp_periode as
	select distinct res_numano as ben_idt_ano,res_deb as debut_seq, res_fin as fin_seq, res_rgp as rgp
	from tmp_per
	where res_numano is not null;

/* nb lignes (rgp) par periode*/
	create table tmp as 
		select ben_idt_ano,debut_seq,fin_seq,count(*) as nb
		from tmp_periode
		group by ben_idt_ano,debut_seq,fin_seq;

	create index ben_idt_ano on tmp(ben_idt_ano);     
	create index ben_idt_ano on tmp_periode(ben_idt_ano);    

	alter table tmp_periode add top smallint ,top2 smallint;
	
	update tmp_periode p set top =	(select t.nb from tmp t
			where p.ben_idt_ano=t.ben_idt_ano and t.debut_seq=p.debut_seq
				and t.fin_seq=p.fin_seq);		

	create table tmp as
		SELECT distinct t2.ben_idt_ano,t2.debut_seq,t2.fin_seq,t2.rgp,t1.debut_seq as debut_seq2,t1.fin_seq  as fin_seq2
		FROM tmp_periode t1,tmp_periode t2
		where t1.ben_idt_ano=t2.ben_idt_ano and t1.rgp=t2.rgp
			and t2.debut_seq between t1.debut_seq and t1.fin_seq and t2.top=1 and t1.top>1
			and (t2.debut_seq<>t1.debut_seq or t1.fin_seq<>t2.fin_seq)
	;
	create index ben_idt_ano on tmp(ben_idt_ano); 

	update tmp_periode p set debut_seq =
		(select min(fin_seq2)+1
			from tmp t
			where p.ben_idt_ano=t.ben_idt_ano and t.debut_seq=p.debut_seq
				and t.fin_seq=p.fin_seq and p.rgp=t.rgp
		)
		where exists (select * from tmp t where p.ben_idt_ano=t.ben_idt_ano and t.debut_seq=p.debut_seq
				and t.fin_seq=p.fin_seq and p.rgp=t.rgp);

	delete from tmp_periode where fin_seq<debut_seq;



	create table tmp2 as
		select p.ben_idt_ano,p.debut_seq,p2.debut_seq as debut_seq2,p.fin_seq,p2.fin_seq as fin_seq2, p.rgp
		from tmp_periode p2,tmp_periode p
		where p.ben_idt_ano=p2.ben_idt_ano 
			and p.rgp=p2.rgp 
            and (p.debut_seq=p2.debut_seq)
            and p.top=1 and p2.top>1
	;
	create index ben_idt_ano on tmp2(ben_idt_ano); 

	update tmp_periode p set top2=null;
	update tmp_periode p set top2=1
		where exists (select * from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.rgp=p.rgp 
			and t.debut_seq=p.debut_seq and t.fin_seq=p.fin_seq and p.top=1)
	;
	delete from tmp_periode where top2=1;



	create table tmp2 as
		select p.ben_idt_ano,p.debut_seq,p2.debut_seq as debut_seq2,p.fin_seq,p2.fin_seq as fin_seq2, p.rgp
		from tmp_periode p2,tmp_periode p
		where p.ben_idt_ano=p2.ben_idt_ano 
			and p.rgp=p2.rgp 
            and (p.fin_seq=p2.fin_seq)
            and p.top=1 and p2.top>1
	;
	create index ben_idt_ano on tmp2(ben_idt_ano); 

	update tmp_periode p set top2=1
		where exists (select * from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.rgp=p.rgp 
			and t.fin_seq=p.fin_seq and p.top=1)
	;
	delete from tmp_periode where top2=1;


	update tmp_periode p set fin_seq =
		(select t.debut_seq2-1 from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.rgp=p.rgp 
			and t.debut_seq=p.debut_seq and t.fin_seq=p.fin_seq and p.top=1)
		where exists (select t.debut_seq2-1 from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.rgp=p.rgp 
			and t.debut_seq=p.debut_seq and t.fin_seq=p.fin_seq and p.top=1)
	;



	create table tmp2 as
		select p.ben_idt_ano,p.debut_seq,p.fin_seq,p2.debut_seq as debut_seq2,p2.fin_seq as fin_seq2, p.rgp
		from tmp_periode p2,tmp_periode p
		where p.ben_idt_ano=p2.ben_idt_ano 
			and p.rgp=p2.rgp and p.top<>p2.top and p.top=1
            and (p.debut_seq<p2.fin_seq  and p.fin_seq>p2.debut_seq)
            and (p.debut_seq<>p2.debut_seq  or p.fin_seq<>p2.fin_seq)
	;
	create index ben_idt_ano on tmp2(ben_idt_ano); 

	update tmp_periode p set top2=null;
	update tmp_periode p set top2=1
		where exists (select * from tmp2 t where t.ben_idt_ano=p.ben_idt_ano and t.rgp=p.rgp 
			and t.fin_seq=p.fin_seq and  t.debut_seq=p.debut_seq)
	;
	delete from tmp_periode where top2=1;

	create table tmp_seq as
		select distinct ben_idt_ano,debut_seq,fin_seq,rgp as rgp1,catx('',ben_idt_ano,debut_seq,fin_seq) as cle
		from tmp_periode
	;
quit;

/* generation de la table finale des sequences */

%_eg_conditional_dropds(&lib_dmb..sequence_fin);
proc sort data=tmp_seq;
	by cle;
run;

data &lib_dmb..sequence_fin;
	set tmp_seq;
	by cle;
	retain rgp;
	retain nb_rgp;

	length rgp $200.;

	if first.cle then do;
		rgp='';
		nb_rgp=0;
	end;
	rgp=catx(',',rgp,rgp1);
	nb_rgp=nb_rgp+1;

	if last.cle then do;
		rgp=substr(rgp,1,length(rgp));
		output;
	end;
run;

proc sql;
	update  &lib_dmb..sequence_fin set nb_rgp=count(compbl(strip(rgp)),',')+1;
	create index ben_idt_ano on &lib_dmb..sequence_fin(ben_idt_ano);
	create index debut_seq on &lib_dmb..sequence_fin(debut_seq);
	alter table &lib_dmb..sequence_fin add rgp2 varchar(200);
quit;

%mend;


/* macro pour dedoublonner la liste des traitements */
/* ************************************************ */
%macro clean_rgp();
%_eg_conditional_dropds(tmp_rgp);
data tmp_rgp;
	set &lib_dmb..sequence_fin;
	/*keep ben_idt_ano debut_seq fin_seq rgp_fin cle ;*/
	where nb_rgp>1 ;

	nb=1;
	c1_rgp=strip(rgp);
	length rgp_fin $200.;
	cle=catx('',ben_idt_ano,debut_seq,fin_seq);
	do while (nb<=nb_rgp) ;
		if nb=nb_rgp then do;
			rgp_fin=strip(tranwrd(c1_rgp,',',''));
		end;
		else do;
			rgp_fin=strip(substr(c1_rgp,1,index(c1_rgp,',')-1));
		end;
		nb_rgp2=count(c1_rgp,cats(rgp_fin,','))+count(c1_rgp,rgp_fin);
		/*nb_rgp2=count(c1_rgp,rgp_fin);*/
		test=count(c1_rgp,cats(rgp_fin,','));
		output;
 		/*nb_rgp2=count(compbl(strip(c1_rgp)),rgp_fin) + 1;*/
		c1_rgp=strip(tranwrd(c1_rgp,cats(rgp_fin,','),""));
		nb=nb+nb_rgp2;
	end;
run;

/* regenerer la liste des traitements */
proc sql;
	create table tmp_rgp1 as
		select distinct ben_idt_ano, debut_seq, fin_seq, cle, rgp_fin
		from tmp_rgp
		order by ben_idt_ano, debut_seq, fin_seq, rgp_fin;
quit;

proc sort data=tmp_rgp1;
	by cle rgp_fin;
run;

data tmp_rgp2;
	set tmp_rgp1;
	by cle;
	retain rgp;
	retain nb_rgp;

	length rgp $200.;

	if first.cle then do;
		rgp='';
		nb_rgp=0;
	end;
	rgp=catx(',',rgp,rgp_fin);
	nb_rgp=nb_rgp+1;

	if last.cle then do;
		rgp=substr(rgp,1,length(rgp));
		output;
	end;
run;

proc sql;

	create index ben_idt_ano on tmp_rgp2(ben_idt_ano);
	create index debut_seq on tmp_rgp2(debut_seq);
	
	update &lib_dmb..sequence_fin s set rgp2 =
		(select rgp from tmp_rgp2 t
			where s.ben_idt_ano=t.ben_idt_ano and s.debut_seq=t.debut_seq and s.fin_seq=t.fin_seq)
		where exists (select rgp from tmp_rgp2 t2
			where s.ben_idt_ano=t2.ben_idt_ano and s.debut_seq=t2.debut_seq and s.fin_seq=t2.fin_seq)
	;
	update &lib_dmb..sequence_fin s set rgp=rgp2 where rgp2 is not null;

quit;
%mend;


/* macro qui permet de boucler sur la generation de sequences
tant qu'il y a des chevauchements de traitements */
%macro repeat_seq();
proc sql;
create table tmp2 as
	select p.ben_idt_ano,p.debut_seq,p.fin_seq,p2.debut_seq as debut_seq2,p2.fin_seq as fin_seq2, p.rgp
	from &lib_dmb..sequence_fin p2,&lib_dmb..sequence_fin p
	where p.ben_idt_ano=p2.ben_idt_ano 
		and p.debut_seq between p2.debut_seq and p2.fin_seq
		and (p.debut_seq<>p2.debut_seq  or p.fin_seq<>p2.fin_seq)
;

select count(*) into:doublon FROM tmp2 ;
%put &doublon;
quit;

%if &doublon>0 %then %do;
%do %until(&doublon=0);
%put &doublon;
	%_eg_conditional_dropds(sequence_deb);
	proc sql;
		create table sequence_deb as
			select ben_idt_ano,debut_seq,fin_seq,rgp
			from &lib_dmb..sequence_fin
			order by ben_idt_ano,debut_seq,fin_seq,rgp;
	quit;

	%sequence();
	%clean_rgp();

	proc sql;
		create table tmp2 as
		select p.ben_idt_ano,p.debut_seq,p.fin_seq,p2.debut_seq as debut_seq2,p2.fin_seq as fin_seq2, p.rgp
		from &lib_dmb..sequence_fin p2,&lib_dmb..sequence_fin p
		where p.ben_idt_ano=p2.ben_idt_ano 
			and p.debut_seq between p2.debut_seq and p2.fin_seq
			and (p.debut_seq<>p2.debut_seq  or p.fin_seq<>p2.fin_seq)
		;

	select count(*) into:doublon FROM tmp2 ;
	%put &doublon;
	quit;
%end;
%end;
%mend;