/***********************************************************************************************/
/*                         ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                            */
/***********************************************************************************************/

/***********************************************************************************************/
/* Nom du programme : stat_femmes */

/* Objectif : Analyser les donnees concernant la population des femmes en age de procreer 
et des femmes enceintes */

/* Sortie : fichier texte avec tables de resultats
/***********************************************************************************************/


/* Femmes en age de procreer */
/* ************************* */

ods TEXT= "Femme en age de procreer - Pop1"; 
proc tabulate data=femme_pop1;
	class  age incident /MISSING;
	table  age all,( incident all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age age_grp.;
run;


proc tabulate data=femme_pop1;
	class  age incident index_ae/MISSING;
	table  index_ae*(age all) all,( incident all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age age_grp.;
run;

ods TEXT= "Femme en age de procreer - Pop2"; 

proc tabulate data=femme_pop2;
	class  age incident /MISSING;
	table  age all,( incident all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age age_grp.;
run;

proc tabulate data=femme_pop2;
	class  age incident index_ae/MISSING;
	table  index_ae*(age all) all,( incident all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age age_grp.;
run;


/* age */
ods TEXT= "Age - Pop1"; 
proc tabulate data=femme_pop1;
	class incident;
	var age ;
	table  incident all, age='' *(F=NUMX10.1* MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop1';
run;

proc tabulate data=femme_pop1;
	class incident index_ae;
	var age ;
	table  incident*index_ae, age='' *(F=NUMX10.1* MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop1';
run;

ods TEXT= "Age - Pop2"; 
proc tabulate data=femme_pop2;
	class incident;
	var age ;
	table  incident all, age='' *(F=NUMX10.1*MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop2';
run;

proc tabulate data=femme_pop2;
	class incident index_ae;
	var age ;
	table  incident*index_ae, age='' *(F=NUMX10.1* MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop2';
run;


/* ae et co dans l'annee apres inclusion */
ods TEXT= "AE et CO - Pop1"; 

proc tabulate data=ae_co_pop1;
	class  class_ae class_co/MISSING;
	table  class_ae all,( class_co all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
run;
ods TEXT= "AE et CO - Pop2"; 

proc tabulate data=ae_co_pop2;
	class  class_ae class_co/MISSING;
	table  class_ae all,( class_co all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;



/* class_ae=EI dans l'annee apres inclusion */
proc sql;
create table ae_ei_pop1 as 
	select distinct ben_idt_ano, age
	from soins_pop1 s 
	where age_proc=1 and periode>=6 and class_ae in ('EI')
;
create index ben_idt_ano on ae_ei_pop1(ben_idt_ano);

create table ae_ei_pop2 as 
	select distinct ben_idt_ano, age
	from soins_pop2 s 
	where age_proc=1 and periode>=6 and class_ae in ('EI')
;
create index ben_idt_ano on ae_ei_pop2(ben_idt_ano);

quit;

ods TEXT= "AE =EI - Pop1"; 
proc tabulate data=ae_ei_pop1;
	class  age/MISSING;
	table  (age all), (F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age age_grp.;
run;

ods TEXT= "AE =EI - Pop2"; 
proc tabulate data=ae_ei_pop2;
	class  age/MISSING;
	table  (age all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age age_grp.;
run;


/* dui implant dans l'annee apres inclusion */
proc sql;
create table diu_pop1 as 
	select distinct s.ben_idt_ano, s.age
	from soins_pop1 s 
		inner join ae_ei_pop1 p on p.ben_idt_ano=s.ben_idt_ano
	where age_proc=1 and periode>=6 and diu_imp=1
;

create table diu_pop2 as 
	select distinct s.ben_idt_ano, s.age
	from soins_pop2 s 
		inner join ae_ei_pop2 p on p.ben_idt_ano=s.ben_idt_ano
	where age_proc=1 and periode>=6 and diu_imp=1
;
quit;

ods TEXT= "DUI/IMPL et AE=EI - Pop1"; 
proc tabulate data=diu_pop1;
	class  age / MISSING;
	table  (age all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age age_grp.;
run;

ods TEXT= "DUI/IMPL et AE=EI - Pop2"; 
proc tabulate data=diu_pop2;
	class  age / MISSING;
	table  (age all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age age_grp.;
run;




/* GROSSESSE */
/* ********* */

ods TEXT= "Grossesse - Pop1"; 
proc tabulate data=grossesse_pop1;
	class  issue / MISSING;
	table  (issue all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
quit;

ods TEXT= "Grossesse - Pop2"; 
proc tabulate data=grossesse_pop2;
	class  issue / MISSING;
	table  (issue all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;


ods TEXT= "Grossesse a domicile - Pop1"; 
proc tabulate data=grossesse_pop1;
	class  issue / MISSING;
	table  (issue all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	where dp='Z3900';
run;

ods TEXT= "Grossesse a domicile - Pop2"; 
proc tabulate data=grossesse_pop2;
	class  issue / MISSING;
	table  (issue all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	where dp='Z3900';
run;


/* consult neuro dans l'annee avant la grossesse */

proc sql;
create table g_neuro_pop1 as 
	select ben_idt_ano, num_g,dte_debut_gross, max(date_sec)  format=ddmmyy10. as min_dte_sec ,dte_debut_gross-max(date_sec) as max_delai
		, max(neuro_fa) as neuro_fa, max(neuro_dosage) as neuro_dosage 
	from periode_g_pop1 
	where sec_neuro=1 and periode<12
	group by ben_idt_ano, num_g,dte_debut_gross
;


create table g_neuro_pop2 as 
	select ben_idt_ano, num_g,dte_debut_gross,  max(date_sec)  format=ddmmyy10. as min_dte_sec, dte_debut_gross-max(date_sec) as max_delai
		, max(neuro_fa) as neuro_fa, max(neuro_dosage) as neuro_dosage 
	from periode_g_pop2 
	where sec_neuro=1 and periode<12
	group by ben_idt_ano, num_g,dte_debut_gross
;

quit;



ods TEXT= "C NEURO avant Grossesse - Pop1"; 
proc tabulate data=g_neuro_pop1;
	class  max_delai / MISSING;
	table  (max_delai all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format max_delai trim_n.;
run;

ods TEXT= "C NEURO avant Grossesse - Pop1"; 
proc tabulate data=g_neuro_pop2;
	class  max_delai / MISSING;
	table  (max_delai all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format max_delai trim_n.;
run;
*/

ods TEXT= "C NEURO avant Grossesse Delai - Pop1"; 
proc tabulate data=g_neuro_pop1;
	var max_delai ;
	table  max_delai *(F=NUMX10.1*MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop1';
run;

ods TEXT= "C NEURO avant Grossesse Delai - Pop2"; 
proc tabulate data=g_neuro_pop2;
	var max_delai ;
	table  max_delai *(F=NUMX10.1*MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop2';
run;


/* c NEURO et del AF */

ods TEXT= "Consult Neuro et delivrance AF - Pop1"; 
proc tabulate data=g_neuro_pop1;
	class  neuro_fa / MISSING;
	table  (neuro_fa all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Consult Neuro et delivrance AF - Pop2"; 
proc tabulate data=g_neuro_pop2;
	class  neuro_fa / MISSING;
	table  (neuro_fa all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;




/* c NEURO et dosage ae */

ods TEXT= "Consult Neuro et dosage AE - Pop1"; 
proc tabulate data=g_neuro_pop1;
	class  neuro_dosage / MISSING;
	table  (neuro_dosage all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Consult Neuro et dosage AE - Pop2"; 
proc tabulate data=g_neuro_pop2;
	class  neuro_dosage / MISSING;
	table  (neuro_dosage all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;



/* dosage ae */

proc sql;
create table dosage_pop1 as 
	select ben_idt_ano, num_g,max(dosage_ae) as max_dosage
	from periode_g_pop1 
	where periode<12
	group by ben_idt_ano, num_g
;


create table dosage_pop2 as 
	select ben_idt_ano, num_g,max(dosage_ae) as max_dosage
	from periode_g_pop2 
	where periode<12
	group by ben_idt_ano, num_g
;

quit;
ods TEXT= "Dosage AE dans l'annee avant - Pop1"; 
proc tabulate data=dosage_pop1;
	class  max_dosage / MISSING;
	table  (max_dosage all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Dosage AE dans l'annee avant - Pop2"; 
proc tabulate data=dosage_pop2;
	class  max_dosage / MISSING;
	table  (max_dosage all),(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;




/* consult neuro sur trimestre de grossesse */


proc sql;
create table trim_g_pop1 as 
	select ben_idt_ano, num_g
		, case when periode=11 then 'M-1'
			when periode between 12 and 14 then 'T1'
			when periode between 15 and 17 then 'T2'
			when periode between 18 and 21 then 'T3'
			else 'T9' end as trim_g
		, max(sec_neuro) as sec_neuro, max(ind_fol) as ind_fol 
	from periode_g_pop1 
	where periode>=11
	group by ben_idt_ano, num_g, trim_g
;

create table trim_g_pop2 as 
	select ben_idt_ano, num_g
		, case when periode=11 then 'M-1'
			when periode between 12 and 14 then 'T1'
			when periode between 15 and 17 then 'T2'
			when periode between 18 and 21 then 'T3'
			else 'T9' end as trim_g
		, max(sec_neuro) as sec_neuro, max(ind_fol) as ind_fol 
	from periode_g_pop2 
	where periode>=11
	group by ben_idt_ano, num_g, trim_g
;

quit;

ods TEXT= "C NEURO pendant trimestres de grossesse - Pop1"; 
proc tabulate data=trim_g_pop1;
	class  trim_g sec_neuro / MISSING;
	table  (trim_g all),(sec_neuro all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';

run;

ods TEXT= "C NEURO pendant trimestres de grossesse - Pop2"; 
proc tabulate data=trim_g_pop2;
	class  trim_g sec_neuro / MISSING;
	table  (trim_g all),(sec_neuro all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;


/* acide folique a M-1 et T1 */

proc sql;
create table m_fa_pop1 as 
	select distinct ben_idt_ano, num_g
	from periode_g_pop1 
	where periode=11 and ind_fol=1
;
create index ben_idt_ano on m_fa_pop1(ben_idt_ano);
create index num_g on m_fa_pop1(num_g);


create table mt1_fa_pop1 as 
	select distinct p.ben_idt_ano, p.num_g
	from periode_g_pop1 p
		inner join m_fa_pop1 m on p.ben_idt_ano=m.ben_idt_ano and p.num_g=m.num_g
	where periode between 12 and 14 and ind_fol=1
;


create table m_fa_pop2 as 
	select distinct ben_idt_ano, num_g
	from periode_g_pop2 
	where periode=11 and ind_fol=1
;
create index ben_idt_ano on m_fa_pop2(ben_idt_ano);
create index num_g on m_fa_pop2(num_g);


create table mt1_fa_pop2 as 
	select distinct p.ben_idt_ano, p.num_g
	from periode_g_pop2 p
		inner join m_fa_pop2 m on p.ben_idt_ano=m.ben_idt_ano and p.num_g=m.num_g
	where periode between 12 and 14 and ind_fol=1
;


quit;


ods TEXT= "Acide folique a M-1 et au 1er trimestres de grossesse - Pop1"; 
proc tabulate data=mt1_fa_pop1;
	class   num_g / MISSING;
	table (num_g all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';

run;

ods TEXT= "Acide folique a M-1 et au 1er trimestres de grossesse - Pop2"; 
proc tabulate data=mt1_fa_pop2;
	class   num_g / MISSING;
	table (num_g all)*(F=NUMX10.1*N F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;


/* complement 202302 */


/* consult neuro pendant la grossesse */

proc sql;
create table g_neuro_pop1 as 
	select ben_idt_ano, num_g,dte_debut_gross, max(date_sec)  format=ddmmyy10. as min_dte_sec ,dte_debut_gross-max(date_sec) as max_delai
		, max(neuro_fa) as neuro_fa, max(neuro_dosage) as neuro_dosage 
	from periode_g_pop1 
	where sec_neuro=1 and periode>=12
	group by ben_idt_ano, num_g,dte_debut_gross
;


create table g_neuro_pop2 as 
	select ben_idt_ano, num_g,dte_debut_gross,  max(date_sec)  format=ddmmyy10. as min_dte_sec, dte_debut_gross-max(date_sec) as max_delai
		, max(neuro_fa) as neuro_fa, max(neuro_dosage) as neuro_dosage 
	from periode_g_pop2 
	where sec_neuro=1 and periode>=12
	group by ben_idt_ano, num_g,dte_debut_gross
;

quit;