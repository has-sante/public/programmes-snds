/*********************************************************************************/
/*              ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                         */
/*********************************************************************************/

/*********************************************************************************/
/* Nom du programme : stat_patients_connus */

/* Objectif : Analyser les donnees concernant la population des patients connus*/

/* Sortie : fichier texte avec tables de resultats */
/*********************************************************************************/



/* SUIVI DANS L'ANNEE APRES L'INITIATION */
/* ************************************* */


proc sql;

create table suivi_pop1 as 
	select ben_idt_ano, incident, cl_age
		, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_consult_mg) as nb_consult_mg
		, sum(nb_consult_ped) as nb_consult_ped
		, sum(nb_consult_urg) as nb_consult_urg
		, sum(nb_consult_tot) as nb_consult_tot
		, sum(nb_hdj_epi) as nb_hdj_epi
		, sum(nb_acte_eeg) as nb_acte_eeg
		, sum(nb_consult_neuro_ter) as nb_consult_neuro_ter
		, sum(nb_consult_mg_ter) as nb_consult_mg_ter
		, sum(nb_consult_ped_ter) as nb_consult_ped_ter
		, sum(nb_consult_tot_ter) as nb_consult_tot_ter
		, sum(nb_consult_neuro_chu) as nb_consult_neuro_chu
		, sum(nb_consult_mg_chu) as nb_consult_mg_chu
		, sum(nb_consult_ped_chu) as nb_consult_ped_chu
		, sum(nb_consult_tot_chu) as nb_consult_tot_chu
		, sum(nb_consult_neuro_ch) as nb_consult_neuro_ch
		, sum(nb_consult_mg_ch) as nb_consult_mg_ch
		, sum(nb_consult_ped_ch) as nb_consult_ped_ch
		, sum(nb_consult_tot_ch) as nb_consult_tot_ch
		, sum(nb_consult_neuro_vil) as nb_consult_neuro_vil
		, sum(nb_consult_mg_vil) as nb_consult_mg_vil
		, sum(nb_consult_ped_vil) as nb_consult_ped_vil
		, sum(nb_consult_tot_vil) as nb_consult_tot_vil
	from soins_pop1
	where periode >=6
	group by ben_idt_ano, incident, cl_age
;

create table suivi_pop2 as 
	select distinct ben_idt_ano, incident,cl_age
		, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_consult_mg) as nb_consult_mg
		, sum(nb_consult_ped) as nb_consult_ped
		, sum(nb_consult_urg) as nb_consult_urg
		, sum(nb_consult_tot) as nb_consult_tot
		, sum(nb_hdj_epi) as nb_hdj_epi
		, sum(nb_acte_eeg) as nb_acte_eeg
		, sum(nb_consult_neuro_ter) as nb_consult_neuro_ter
		, sum(nb_consult_mg_ter) as nb_consult_mg_ter
		, sum(nb_consult_ped_ter) as nb_consult_ped_ter
		, sum(nb_consult_tot_ter) as nb_consult_tot_ter
		, sum(nb_consult_neuro_chu) as nb_consult_neuro_chu
		, sum(nb_consult_mg_chu) as nb_consult_mg_chu
		, sum(nb_consult_ped_chu) as nb_consult_ped_chu
		, sum(nb_consult_tot_chu) as nb_consult_tot_chu
		, sum(nb_consult_neuro_ch) as nb_consult_neuro_ch
		, sum(nb_consult_mg_ch) as nb_consult_mg_ch
		, sum(nb_consult_ped_ch) as nb_consult_ped_ch
		, sum(nb_consult_tot_ch) as nb_consult_tot_ch
		, sum(nb_consult_neuro_vil) as nb_consult_neuro_vil
		, sum(nb_consult_mg_vil) as nb_consult_mg_vil
		, sum(nb_consult_ped_vil) as nb_consult_ped_vil
		, sum(nb_consult_tot_vil) as nb_consult_tot_vil
	from soins_pop2
	where periode >=6
	group by ben_idt_ano, incident, cl_age
;

quit;


%macro med_suivi(pop=,ind=);

ods TEXT= "Repartition du nombre de consultations &ind. - &pop."; 

proc tabulate data=suivi_&pop.;
	 var &ind. ;
	class  cl_age ;
	table  cl_age all,  (&ind.) *(F=NUMX10.1*N SUM MEAN STD MIN MAX  P10 Q1 MEDIAN Q3 P90)
		/ BOX=&pop.;
	where incident=0;
run;
/*
proc tabulate data=suivi_&pop.;
	 var &ind. ;
	class  cl_age ;
	table  (&ind.) *(F=NUMX10.1*N SUM MEAN STD MIN MAX  P10 Q1 MEDIAN Q3 P90),cl_age all
		/ BOX=&pop.;
	where incident=0;
run;
*/

%mend;

%med_suivi(pop=pop1,ind=nb_consult_neuro);
%med_suivi(pop=pop1,ind=nb_consult_mg);
%med_suivi(pop=pop1,ind=nb_consult_ped);
%med_suivi(pop=pop1,ind=nb_consult_tot);
%med_suivi(pop=pop1,ind=nb_consult_urg);
%med_suivi(pop=pop1,ind=nb_hdj_epi);
%med_suivi(pop=pop1,ind=nb_acte_eeg);

%med_suivi(pop=pop2,ind=nb_consult_neuro);
%med_suivi(pop=pop2,ind=nb_consult_mg);
%med_suivi(pop=pop2,ind=nb_consult_ped);
%med_suivi(pop=pop2,ind=nb_consult_tot);
%med_suivi(pop=pop2,ind=nb_consult_urg);
%med_suivi(pop=pop2,ind=nb_hdj_epi);
%med_suivi(pop=pop2,ind=nb_acte_eeg);


%med_suivi(pop=pop1,ind=nb_consult_neuro_ter);
%med_suivi(pop=pop1,ind=nb_consult_mg_ter);
%med_suivi(pop=pop1,ind=nb_consult_ped_ter);
%med_suivi(pop=pop1,ind=nb_consult_tot_ter);

%med_suivi(pop=pop1,ind=nb_consult_neuro_chu);
%med_suivi(pop=pop1,ind=nb_consult_mg_chu);
%med_suivi(pop=pop1,ind=nb_consult_ped_chu);
%med_suivi(pop=pop1,ind=nb_consult_tot_chu);

%med_suivi(pop=pop1,ind=nb_consult_neuro_ch);
%med_suivi(pop=pop1,ind=nb_consult_mg_ch);
%med_suivi(pop=pop1,ind=nb_consult_ped_ch);
%med_suivi(pop=pop1,ind=nb_consult_tot_ch);

%med_suivi(pop=pop1,ind=nb_consult_neuro_vil);
%med_suivi(pop=pop1,ind=nb_consult_mg_vil);
%med_suivi(pop=pop1,ind=nb_consult_ped_vil);
%med_suivi(pop=pop1,ind=nb_consult_tot_vil);



%med_suivi(pop=pop2,ind=nb_consult_neuro_ter);
%med_suivi(pop=pop2,ind=nb_consult_mg_ter);
%med_suivi(pop=pop2,ind=nb_consult_ped_ter);
%med_suivi(pop=pop2,ind=nb_consult_tot_ter);

%med_suivi(pop=pop2,ind=nb_consult_neuro_chu);
%med_suivi(pop=pop2,ind=nb_consult_mg_chu);
%med_suivi(pop=pop2,ind=nb_consult_ped_chu);
%med_suivi(pop=pop2,ind=nb_consult_tot_chu);

%med_suivi(pop=pop2,ind=nb_consult_neuro_ch);
%med_suivi(pop=pop2,ind=nb_consult_mg_ch);
%med_suivi(pop=pop2,ind=nb_consult_ped_ch);
%med_suivi(pop=pop2,ind=nb_consult_tot_ch);

%med_suivi(pop=pop2,ind=nb_consult_neuro_vil);
%med_suivi(pop=pop2,ind=nb_consult_mg_vil);
%med_suivi(pop=pop2,ind=nb_consult_ped_vil);
%med_suivi(pop=pop2,ind=nb_consult_tot_vil);





/* stat sur les periodes */
/* ********************* */


/* Neurologue dans l'annee qui suit l'initiation */
proc sql;

create table sec_neuro1 as 
	select ben_idt_ano, incident,cl_age, sum(nb_consult_neuro) as nb_consult_neuro
	from soins_pop1
	where sec_neuro=1 and periode >=6 
	group by ben_idt_ano,incident, cl_age
;

create table sec_neuro2 as 
	select ben_idt_ano,incident, cl_age, sum(nb_consult_neuro) as nb_consult_neuro
	from soins_pop2
	where sec_neuro=1 and periode >=6
	group by ben_idt_ano, incident, cl_age
;
quit;


ods TEXT= "Au moins 1 C Neurologue dans l'annee qui suit l'initiation - Pop1"; 
proc tabulate data=sec_neuro1;
	class  cl_age  incident/ missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop1';
run;

proc tabulate data=sec_neuro1;
	 var nb_consult_neuro ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_consult_neuro='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;



ods TEXT= "Au moins 1 C Neurologue dans l'annee qui suit l'initiation - Pop2"; 
proc tabulate data=sec_neuro2;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop2';
run;

proc tabulate data=sec_neuro2;
	 var nb_consult_neuro ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_consult_neuro='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;




/* EEG dans l'annee qui suit l'initiation */
proc sql;

create table sec_eeg1 as 
	select distinct ben_idt_ano, incident,cl_age, count(*) as nb_eeg
	from soins_pop1
	where sec_eeg=1 and periode >=6
	group by ben_idt_ano, incident,index_ae, cl_age
;

create table sec_eeg2 as 
	select distinct ben_idt_ano,incident, cl_age, count(*) as nb_eeg
	from soins_pop2
	where sec_eeg=1 and periode >=6
	group by ben_idt_ano,incident,  cl_age
;
quit;


ods TEXT= "Au moins 1 EEG dans l'annee qui suit l'initiation - Pop1"; 
proc tabulate data=sec_eeg1;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop1';
run;

proc tabulate data=sec_eeg1;
	 var nb_eeg ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_eeg='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;



ods TEXT= "Au moins 1 EEG dans l'annee qui suit l'initiation - Pop2"; 
proc tabulate data=sec_eeg2;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop2';
run;

proc tabulate data=sec_eeg2;
	 var nb_eeg ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_eeg='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;




/* HDJ dans l'annee qui suit l'initiation */
proc sql;

create table hdj1 as 
	select distinct ben_idt_ano,incident, cl_age, sum(nb_hdj_epi) as nb_hdj
	from soins_pop1
	where nb_hdj_epi>=1 and periode >=6
	group by ben_idt_ano, incident, cl_age
;

create table hdj2 as 
	select distinct ben_idt_ano,incident, cl_age, sum(nb_hdj_epi) as nb_hdj
	from soins_pop2
	where nb_hdj_epi>=1 and periode >=6
	group by ben_idt_ano, incident, cl_age
;
quit;


ods TEXT= "Au moins 1 HDJ dans l'annee qui suit l'initiation - Pop1"; 
proc tabulate data=hdj1;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop1';
run;

proc tabulate data=hdj1;
	 var nb_hdj ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_hdj='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;


ods TEXT= "Au moins 1 HDJ dans l'annee qui suit l'initiation - Pop2"; 
proc tabulate data=hdj2;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop2';
run;

proc tabulate data=hdj2;
	 var nb_hdj ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_hdj='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;



/* URG dans l'annee qui suit l'initiation */
proc sql;

create table urg1 as 
	select distinct ben_idt_ano, incident,cl_age, sum(nb_consult_urg) as nb_urg
	from soins_pop1
	where prim_urg=1 and periode >=6
	group by ben_idt_ano, incident,index_ae, cl_age
;

create table urg2 as 
	select distinct ben_idt_ano,incident, cl_age, sum(nb_consult_urg) as nb_urg
	from soins_pop2
	where prim_urg=1 and periode >=6
	group by ben_idt_ano, incident, cl_age
;
quit;


ods TEXT= "Au moins 1 passage aux URG dans l'annee qui suit l'initiation - Pop1"; 
proc tabulate data=urg1;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop1';
run;

proc tabulate data=urg1;
	 var nb_urg ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_urg='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;


ods TEXT= "Au moins 1 passage aux URG dans l'annee qui suit l'initiation - Pop2"; 
proc tabulate data=urg2;
	class  cl_age incident / missing;
	table  cl_age all,incident*(F=NUMX10.1*N='' )
		/ BOX='Pop2';
run;

proc tabulate data=urg2;
	 var nb_urg ;
	class  cl_age incident / missing;
	table  (cl_age all),incident*nb_urg='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;