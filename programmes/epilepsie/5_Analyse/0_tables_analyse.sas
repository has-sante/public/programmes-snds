/**********************************************************************************************/
/*                    ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                                */
/**********************************************************************************************/

/**********************************************************************************************/
/* Nom du programme : tables_analyse */

/* Objectif : Transferer les tables dans l environnement de travail pour les analyses statistiques 
	et creer les variables permettant de filtrer sur la population d etude, sur la classe d age, 
	et sur l entree du patient dans le suivi (connu ou nouveau)

/* Tables de sortie : pop1, pop2, soins_pop1, soins_pop2, femmes _pop1, femmes_pop2, 
ae_co_pop1, ae_co_pop2, grossesse_pop1, grossesse_pop2, periode_g_pop1, periode_g_pop2 */
/**********************************************************************************************/


proc sql;
create table pop1 as 
	select *, case when age<15 then 1 else 0 end as enfant
		,put(age,agegpe.) as cl_age
		, case when first_classae like 'EI%' or first_classae like '% EI%' or first_classae like '%gluAED%' then 'EI'
			when first_classae like '%NEI%' then 'NEI'
			when first_classae like '%inh%' then 'inh' 
			else first_classae end as classe_ae
		, case when psp_first_ae like '%NEURO%' then 'NEURO'
			when psp_first_ae like '%PED%' then 'PED'
			when psp_first_ae like '%PSY%' then 'PSY'
			when psp_first_ae like '%MG%' then 'MG'
			when psp_first_ae is not null  then 'AUT' end as med_first
		, case when first_ae_18_dte is not null then 1 else 0 end as ae_18

	from &lib_dmb..patient where pop1=1;

create table pop2 as 
	select * , case when age<15 then 1 else 0 end as enfant
		,put(age,agegpe.) as cl_age
		, case when first_classae like 'EI%' or first_classae like '% EI%' or first_classae like '%gluAED%' then 'EI'
			when first_classae like '%NEI%' then 'NEI'
			when first_classae like '%inh%' then 'inh' 
			else first_classae end as classe_ae
		, case when psp_first_ae like '%NEURO%' then 'NEURO'
			when psp_first_ae like '%PED%' then 'PED'
			when psp_first_ae like '%PSY%' then 'PSY'
			when psp_first_ae like '%MG%' then 'MG'
			when psp_first_ae is not null  then 'AUT' end as med_first
		, case when first_ae_18_dte is not null then 1 else 0 end as ae_18

	from &lib_dmb..patient where pop2=1;

quit;

proc sql;
create table soins_pop1 as 
	select s.*, put(age,agegpe.) as cl_age, p.sex,age
		, case when age between 15 and 49 and p.sex=2 then 1 else 0 end as age_proc
	from &lib_dmb..periode_soins s
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop1=1 ;

create table soins_pop2 as 
	select s.* , put(age,agegpe.) as cl_age, p.sex,age
		, case when age between 15 and 49 and p.sex=2 then 1 else 0 end as age_proc
	from &lib_dmb..periode_soins s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop2=1 ;

quit;


/* femme en age de procreer */



proc sql;

create table femme_pop1 as 
	select *
	from &lib_dmb..patient 
	where pop1=1 and sex=2 and age between 15 and 49;

create table femme_pop2 as 
	select * 
	from &lib_dmb..patient 
	where pop2=1 and sex=2 and age between 15 and 49;


/* ae et co dans l'annee apres inclusion */
create table ae_co_pop1 as 
	select distinct p.ben_idt_ano, age, class_ae,class_co
	from &lib_dmb..periode_soins s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop1=1 and sex=2 and age between 15 and 49
		and periode>=6 and class_ae is not null and class_co is not null
;

create table ae_co_pop2 as 
	select distinct p.ben_idt_ano, age, class_ae,class_co
	from &lib_dmb..periode_soins s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop2=1 and sex=2 and age between 15 and 49
		and periode>=6 and class_ae is not null and class_co is not null
;
quit;

/* grossesse */
proc sql;
create table grossesse_pop1 as 
	select s.*, put(age,age_grp.) as cl_age
	from &lib_dmb..grossesse s
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop1=1 and sex=2 and age between 15 and 49 and exclu=0 
		and issue_dte between '01JAN2018'd and '31DEC2019'd
;

create table grossesse_pop2 as 
	select s.* , put(age,age_grp.) as cl_age
	from &lib_dmb..grossesse s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop2=1 and sex=2 and age between 15 and 49 and exclu=0
		and issue_dte between '01JAN2018'd and '31DEC2019'd
;



create table periode_g_pop1 as 
	select s.*, put(age,age_grp.) as cl_age
	from &lib_dmb..periode_gross s
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop1=1 and sex=2 and age between 15 and 49
;


create table periode_g_pop2 as 
	select s.* , put(age,age_grp.) as cl_age
	from &lib_dmb..periode_gross s 
		inner join &lib_dmb..patient p on p.ben_idt_ano=s.ben_idt_ano
	where p.pop2=1 and sex=2 and age between 15 and 49;

quit;

