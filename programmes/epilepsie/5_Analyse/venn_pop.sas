/*********************************************************************************/
/*              ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                         */
/*********************************************************************************/

/*********************************************************************************/
/* Nom du programme : venn_pop */

/* Objectif : Construction d'un diagramme de Venn pour les populations d'étude */

/* Sortie : fichier text avec diagrammes de venn
/*********************************************************************************/


/*********************************************************************/
/* Venn Diagram Macro */
/*********************************************************************/

%macro venn( data = 
,A=
,B=
,C=
,D=
,venn_diagram =  /* Select whether you want a 2 Way, 3 Way or 4 Way Venn Diagram. EG for 2 way enter 2. Valid values are 2,3 and 4 */
,GroupA =  /* Define group name 1, mandatory */
,GroupB =  /* Define group name 2, mandatory */
,GroupC =  /* Define group name 3, mandatory for 3 and 4 way diagrams */
,GroupD = /* Define group name 4, mandatory for 4 way diagrams*/
,cutoffA =  /* Set the P Value cut-off */
,cutoffB =  
,cutoffC = 
,cutoffD =  
,out_location = 
,outputfilename = Venn_test /* Define the filename for the graphic file */
,drilldownfilename = Drilldown /* Define the filename for the drill down data file */
,numpop= 
,nb_pop=
);

%let outputfilename = Venn_test&numpop.;


/* Calculate the category for each observation in the dataset 
This has to be done differently for 2,3 and 4 way diagrams */

data data_reformatted;
set &data;
run;
/* Counting the overlap */
data data_reformatted2;
set data_reformatted;
%IF &venn_diagram = 2 %THEN %DO ;
	if &A ne . and &B ne . then do;
		if &A &cutoffA and &B &cutoffB then AB = 1;
		else AB = 0; 
	end;
	if &A ne . then do; 
		if &A &cutoffA and AB ne 1 then A1 = 1;
		else A1 = 0;
	end;
	if &B ne . then do;
		if &B &cutoffB and AB ne 1 then B1 = 1;
		else B1 = 0;
	end;
%end;

%ELSE %IF &venn_diagram = 3 %THEN %DO;
	if &A ne . and &B ne . and &C ne . then do;
		if &A &cutoffA and &B &cutoffB and &C &cutoffC then ABC = 1;
		else ABC = 0;
	end;
	if &A ne . and &B ne . then do;
		if &A &cutoffA and &B &cutoffB and ABC ne 1 then AB = 1;
		else AB = 0; 
	end;
	if &A ne . and &C ne . then do;
		if &A &cutoffA and &C &cutoffC and ABC ne 1 then AC = 1;
		else AC = 0; 
	end;
	if &B ne . and &C ne . then do;
		if &B &cutoffB and &C &cutoffC and ABC ne 1 then BC = 1;
		else BC = 0; 
	end;
	if &A ne . then do;
		if &A &cutoffA and AB ne 1 and AC ne 1 and ABC ne 1 then A1 = 1; 
		else A1 = 0;
	end;
	if &B ne . then do;
		if &B &cutoffB and AB ne 1 and BC ne 1 and ABC ne 1 then B1 = 1; 
		else B1 = 0;
	end;
	if &C ne . then do;
		if &C &cutoffC and AC ne 1 and BC ne 1 and ABC ne 1 then C1 = 1; 
		else C1 = 0;
	end;
%END;

%ELSE %IF &venn_diagram=4 %THEN %DO;

	if &A ne . and &B ne . and &C ne . and &D ne . then do;
		if &A &cutoffA and &B &cutoffB and &C &cutoffC and &D &cutoffD then ABCD = 1;
		else ABCD = 0;
	end;
	if &A ne . and &B ne . and &C ne . then do;	
		if &A &cutoffA and &B &cutoffB and &C &cutoffC and ABCD ne 1 then ABC = 1;
		else ABC = 0;
	end;
	if &A ne . and &B ne . and &D ne . then do;
		if &A &cutoffA and &B &cutoffB and &D &cutoffD and ABCD ne 1 then ABD = 1;
		else ABD = 0;
	end;
	if &A ne . and &C ne . and &D ne . then do;
		if &A &cutoffA and &C &cutoffC and &D &cutoffD and ABCD ne 1 then ACD = 1;
		else ACD = 0;
	end; 
	if &B ne . and &C ne . and &D ne . then do;
		if &B &cutoffB and &C &cutoffC and &D &cutoffD and ABCD ne 1 then BCD = 1;
		else BCD = 0;
	end;
	if &A ne . and &B ne . then do;
		if &A &cutoffA and &B &cutoffB and ABC ne 1 and ABD ne 1 and ABCD ne 1 then AB = 1;
		else AB = 0; 
	end;
	if &A ne . and &C ne . then do;
		if &A &cutoffA and &C &cutoffC and ABC ne 1 and ACD ne 1 and ABCD ne 1 then AC = 1;
		else AC = 0; 
	end;
	if &A ne . and &D ne . then do;
		if &A &cutoffA and &D &cutoffD and ABD ne 1 and ACD ne 1 and ABCD ne 1 then AD =1;
		else AD = 0; 
	end;
	if &B ne . and &C ne . then do;
		if &B &cutoffB and &C &cutoffC and ABC ne 1 and BCD ne 1 and ABCD ne 1 then BC = 1;
		else BC = 0; 
	end;
	if &B ne . and &D ne . then do;
		if &B &cutoffB and &D &cutoffD and ABD ne 1 and BCD ne 1 and ABCD ne 1 then BD = 1;
		else BD = 0; 
	end;
	if &C ne . and &D ne . then do;
		if &C &cutoffC and &D &cutoffD and ACD ne 1 and BCD ne 1 and ABCD ne 1 then CD = 1;
		else CD = 0; 
	end;
	if &A ne . then do;
		if &A &cutoffA and AB ne 1 and AC ne 1 and AD ne 1 and ABC ne 1 and ABD ne 1 and ACD ne 1 and ABCD ne 1 then A1 = 1;
 		else A1 = 0;
	end;
	if &B ne . then do;
		if &B &cutoffB and AB ne 1 and BC ne 1 and BD ne 1 and ABC ne 1 and ABD ne 1 and BCD ne 1 and ABCD ne 1 then B1 = 1;
		else B1 = 0;
	end;
	if &C ne . then do;
		if &C &cutoffC and AC ne 1 and BC ne 1 and CD ne 1 and ABC ne 1 and ACD ne 1 and BCD ne 1 and ABCD ne 1 then C1 = 1;
		else C1 = 0;
	end;
	if &D ne . then do;
		if &D &cutoffD and AD ne 1 and BD ne 1 and CD ne 1 and ABD ne 1 and ACD ne 1
		and BCD ne 1 and ABCD ne 1 then D1 = 1;
 		else D1 = 0;
	end;
%END;

run;



/*
COUNTING THE ELEMENTS IN EACH GROUP 
After the Macro identifies the elements in each group it uses PROC UNIVARIATE
to sum up the number of elements in each group.
The total number of element within the diagram i.e. the union of Groups A, B, C, 
and D, and the total number of elements in the dataset i.e. the universal set are 
then calculated. This is used to identify the number of elements that fall outside the union. 
*/

proc univariate data = Data_reformatted2 noprint;
var AB A1 B1
%if &venn_diagram > 2 %then %do;
	ABC AC BC C1
%end;
%if &venn_diagram > 3 %then %do;
	ABCD ABD ACD BCD AD BD CD D1
%end;
;

output out = data_sum sum = sum_AB sum = sum_A1 sum = sum_B1
%if &venn_diagram > 2 %then %do;
	sum = sum_ABC
	sum = sum_AC
	sum = sum_BC
	sum = sum_C1
%end;
%if &venn_diagram > 3 %then %do;
	sum = sum_ABCD sum = sum_ABD sum =sum_ACD 
	sum = sum_BCD sum = sum_AD sum = sum_BD 
	sum = sum_CD sum = sum_D1
%end;
;
run;

/* Counting the number in the universal set */
proc sql noprint;
	create table id_count as
	select count(ben_idt_ano) as count_id
	from data_reformatted;
quit;





/* Counting the number inside the union */
data data_sum2;
	set data_sum;
	totalinside = sum(sum_AB, sum_A1, sum_B1
		%if &venn_diagram > 2 %then %do;
			,sum_ABC, sum_AC, sum_BC, sum_C1
		%end;
		%if &venn_diagram > 3 %then %do;
			,sum_ABCD, sum_ABD, sum_ACD, sum_BCD, sum_AD, sum_BD, sum_CD, sum_D1
		%end;
	);
run;

/*
COUNTING THE ELEMENTS THAT FALL OUTSIDE OF THE UNION
Using the fetch function the values of the total number of elements within the 
union and the universal set are fetched from the appropriate datasets and
assigned to a macro-variable. The total number of elements that fall outside the diagram is then calculated by using %eval to evaluate the arithmetic expression of the number 
of elements in the universal set - the number of elements within the union. 
*/

/* Calculating the total number of unique ids - so that I can calculate the number that falls outside of the groups */

proc sql noprint;
	select count_id into: TN
	from id_count;
quit;

/* Calculating the total number of values that fall within the groups */
proc sql noprint;
	select totalinside into: TI
	from data_sum2;
quit;

/* Calculating the total numbers that fall outside all of the groups */
%let TO = %eval(&TN -&TI);

/* Assigning the sums to macro variables */
proc sql noprint;
	select sum_A1, sum_B1, sum_AB into :A, :B, :AB
	from data_sum2;
quit; 

%if &venn_diagram > 2 %then %do;
proc sql noprint;
select sum_C1, sum_AC, sum_BC, sum_ABC into :C, :AC, :BC, :ABC
from data_sum2;
quit; 
%end;

%if &venn_diagram > 3 %then %do;
proc sql noprint;
select sum_D1, sum_AD, sum_BD, sum_CD, sum_ABD, sum_ACD, sum_BCD, sum_ABCD into :D, :AD, :BD, :CD, :ABD, :ACD, :BCD, :ABCD
from data_sum2;
quit; 
%end;

/* The rest of the macro needs to be done seperately for 2, 3 and 4 way plots */
data test;
do x = 1 to 100;
y = x;
output;
end;
run;


/* ************** 2 WAY VENN DIAGRAMS ************** */
%if &venn_diagram=2 %then %do;
proc template;
define statgraph Venn2Way;
begingraph / drawspace=datavalue;
/* Plot */
layout overlay / yaxisopts = (display = NONE) xaxisopts = (display = NONE);
scatterplot x=x y=y / markerattrs=(size = 0);
/* Venn Diagram (Circles) */
drawoval x=36 y=50 width=45 height=60/display=all fillattrs=(color=red) transparency=0.75   WIDTHUNIT= Percent HEIGHTUNIT= Percent;
drawoval x=63 y=50 width=45 height=60/display=all fillattrs=(color=blue)
transparency=0.75 WIDTHUNIT= Percent HEIGHTUNIT= Percent;
/* Numbers */
drawtext "&A" / x=33 y=50 anchor=center;
drawtext "&AB" / x=50 y=50 anchor=center;
drawtext "&B" / x=66 y=50 anchor=center;
drawtext "Outside Union -&TO" / x=50 y=10 anchor=center width = 30;
/*Labels */
drawtext "&GroupA" / x=30 y=15 anchor=center width = 30;
drawtext "&GroupB" / x=70 y=15 anchor=center width = 30;
endlayout;
endgraph;
end;
run;

ods graphics on / reset = all border = off width=16cm height=12cm imagefmt = png imagename = "&outputfilename" ; 
/* ods listing gpath = "&out_location" image_dpi = 200; */
proc sgrender data=test template=Venn2Way;
run;
ods listing close;
ods graphics off;
%end;

/* ************** 3 WAY VENN DIAGRAMS ************** */
%if &venn_diagram = 3 %then %do;
proc template;
define statgraph Venn3Way;
begingraph / drawspace=datavalue;
/* Plot */
layout overlay / yaxisopts = (display = NONE) xaxisopts = (display = NONE);
scatterplot x=x y=y / markerattrs=(size = 0);
/* Venn Diagram (Circles) */
drawoval x=37 y=40 width=45 height=60 / display=all fillattrs=(color=CXB70185) outlineattrs=(color=CXB70185)  transparency=0.5 WIDTHUNIT= Percent HEIGHTUNIT= Percent;
drawoval x=63 y=40 width=45 height=60/ display=all fillattrs=(color=CX855AF2) outlineattrs=(color=CX855AF2) transparency=0.5 WIDTHUNIT= Percent HEIGHTUNIT= Percent;
drawoval x=50 y=65 width=45 height=60 / display=all fillattrs=(color=CXC9E24D) outlineattrs=(color=CXC9E24D) transparency=0.5 WIDTHUNIT= Percent HEIGHTUNIT= Percent;
/* Numbers */
drawtext "&A" / x=32 y=35 anchor=center;
drawtext "&AB" / x=50 y=30 anchor=center;
drawtext "&B" / x=68 y=35 anchor=center;
drawtext "&ABC" / x=50 y=50 anchor=center;
drawtext "&AC" / x=37 y=55 anchor=center;
drawtext "&BC" / x=63 y=55 anchor=center;
drawtext "&C" / x=50 y=75 anchor=center;
drawtext "Population &numpop. : &nb_pop." / y=3 x=50 anchor=center width = 30;
/* Labels */
drawtext "&GroupA" / x=30 y=7 anchor=center width = 30;
drawtext "&GroupB" / x=70 y=7 anchor=center width = 30;
drawtext "&GroupC" / x=50 y=98 anchor=center width = 30;
endlayout;
endgraph;
end;
run;

ods graphics on / reset = all border = off width=16cm height=12cm imagefmt = png imagename = "&outputfilename"; 
/* ods listing gpath = "&out_location" image_dpi = 200; */

proc sgrender data=test template=Venn3Way;
run;
ods listing close;
ods graphics off;
%end;

/*************** 4 WAY VENN DIAGRAMS ***************/
%if &venn_diagram = 4 %then %do;

proc template;
define statgraph Venn4Way;
begingraph / drawspace=datavalue;
/* Plot */
layout overlay / yaxisopts = (display = NONE) xaxisopts = (display= NONE);
scatterplot x=x y=y / markerattrs=(size = 0);
/* Venn Diagram (Ellipses) */
drawoval x=28 y=39 width=26 height=100/display=all fillattrs=(color=red transparency=0.85) outlineattrs=(color=red) transparency=0.50 WIDTHUNIT= Percent HEIGHTUNIT= Percent rotate = 45;
drawoval x=72 y=39 width=26 height=100/display=all fillattrs=(color=green
transparency=0.85) outlineattrs=(color=green) transparency=0.50 WIDTHUNIT= Percent 
WEIGHTUNIT= Percent rotate = 315;
drawoval x=57 y=54 width=26 height=100/display=all fillattrs=(color=blue transparency=0.85) outlineattrs=(color=blue) transparency=0.50 WIDTHUNIT= Percent HEIGHTUNIT= Percent rotate = 335;
drawoval x=43 y=54 width=26 height=100/display=all fillattrs=(color=yellow transparency=0.85) outlineattrs=(color=yellow) transparency=0.50 WIDTHUNIT= Percent HEIGHTUNIT= Percent rotate = 25;
/* Numbers */
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&A" / x=13 y=60 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&B" / x=35 y=80 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&C" / x=65 y=80 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&D" / x=87 y=60 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&AB" / x=36 y=45 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&AC" / x=41 y=16 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&AD" / x=50 y=6 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&BC" / x=50 y=55 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&BD" / x=59 y=16 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&CD" / x=64 y=45 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&ABC" / x=43 y=30 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&BCD" / x=57 y=30 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&ACD" / x=46 y=12 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&BCD" / x=52 y=12 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 6pt weight = bold) "&ABCD" / x=50 y=21 anchor=center;
drawtext textattrs = GRAPHVALUETEXT(size = 7pt weight = bold) "Outside Union - &TO" / x=70 y=5 anchor=center width = 30;
/* Labels */
drawtext textattrs = GRAPHVALUETEXT(size = 7pt weight = bold) "&GroupA" / x=6 y=20 anchor=center width = 30;
drawtext textattrs = GRAPHVALUETEXT(size = 7pt weight = bold) "&GroupB" / x=6 y=85 anchor=center width = 30;
drawtext textattrs = GRAPHVALUETEXT(size = 7pt weight = bold) "&GroupC" / x=82 y=85 anchor=center width = 30;
drawtext textattrs = GRAPHVALUETEXT(size = 7pt weight = bold) "&GroupD" / x=82 y=20 anchor=center width = 30;
endlayout;
endgraph;
end;
run;

ods graphics on / reset = all border = off width=16cm height=12cm imagefmt = png magename = "&outputfilename" ; 
/* ods listing gpath = "&out_location" image_dpi = 200 */
;
proc sgrender data=test template=Venn4Way;
run;

ods listing close;
ods graphics off;
%end;
%mend venn;





/*********************************************************************/
/* Appel de la macro */
/*********************************************************************/


/* Preparation des donnees pour obtenir un diagramme de VENN */

proc sql;
create table pop1_v as 
	select ben_idt_ano, ald_epi_18 as ald,hosp_epi_18 as hosp, med_epi_18 as med
	from &lib_dmb..patient
	where pop1=1;

;

select count(*) format=8. into :nb_pop1 
	from &lib_dmb..patient
	where pop1=1;

;

create table pop2_v as 
	select ben_idt_ano, ald_epi_18 as ald,hosp_epi_18 as hosp, med_epispe_18 as med
	from &lib_dmb..patient
	where pop2=1;

;

select count(*) format=8. into :nb_pop2
	from &lib_dmb..patient
	where pop2=1;

;

quit;


/* Construction des diagrammes de VENN */

ods text="pop 1";
proc tabulate data=pop1_v;
	class ald hosp med /MISSING;
	table  (ald *hosp all='Total'),(med all='Total')*(F=numfr8.*N='')	/ BOX='';
run;

options noquotelenmax;
%venn(data= pop1_v, venn_diagram = 3
	,A=ald, B=hosp, C=med
	,GroupA=ALD,GroupB=HOSP,GroupC=MED
	,numpop=1, nb_pop=&nb_pop1.
);



ods text="pop 2";
proc tabulate data=pop2_v;
	class ald hosp med /MISSING;
	table  (ald *hosp all='Total'),(med all='Total')*(F=numfr8.*N='')	/ BOX='';
run;

options noquotelenmax;
%venn(data= pop2_v, venn_diagram = 3
	,A=ald, B=hosp, C=med
	,GroupA=ALD,GroupB=HOSP,GroupC=MED
	,numpop=2, nb_pop=&nb_pop2.
);



ods TEXT= "Pop1"; 
proc tabulate data=pop1;
	class   incident new_med_epi cl_age/MISSING;
	table  cl_age all,( incident*new_med_epi all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Pop2"; 
proc tabulate data=pop2;
	class   incident new_med_epi cl_age/MISSING;
	table  cl_age all,( incident*new_med_epi all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
run;