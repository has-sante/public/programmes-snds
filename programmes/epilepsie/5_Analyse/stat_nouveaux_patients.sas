/*********************************************************************************/
/*              ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                         */
/*********************************************************************************/


/*********************************************************************************/
/* Nom du programme : stat_nouveaux_patients

/* Objectif : Analyser les donnees concernant la population des nouveaux patients
et des nouveaux utilisateurs d'AE */

/* Sortie : fichier texte avec tables de resultats */
/*********************************************************************************/


/* incident  */

ods TEXT= "Incident - Pop1"; 
proc tabulate data=pop1;
	class  age incident /MISSING;
	table  age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age agegpe.;
run;


proc tabulate data=pop1;
	class  age incident index_ae/MISSING;
	table  index_ae*age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age agegpe.;
run;

ods TEXT= "Incident - Pop2"; 

proc tabulate data=pop2;
	class  age incident /MISSING;
	table  age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age agegpe.;
run;

proc tabulate data=pop2;
	class  age incident index_ae/MISSING;
	table  index_ae*age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age agegpe.;
run;
/* sex  */

ods TEXT= "Sexe/Age - Pop1"; 
proc tabulate data=pop1;
	class  age sex /MISSING;
	table  age all,( sex all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age agegpe.;
run;

proc tabulate data=pop1;
	class  sex age /MISSING;
	table  age all,( sex all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
	format age age_grp.;
run;

ods TEXT= "Sexe/AGe - Pop2"; 

proc tabulate data=pop2;
	class  sex age /MISSING;
	table  age all,( sex all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age agegpe.;
run;



proc tabulate data=pop2;
	class  sex age /MISSING;
	table  age all,( sex all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
	format age age_grp.;
run;


/* age */
ods TEXT= "Age - Pop1"; 
proc tabulate data=pop1;
	class enfant;
	var age ;
	table  enfant, age='' *(F=NUMX10.1* MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop1';
run;
proc tabulate data=pop1;
	class cl_age;
	var  age ;
	table cl_age, age='' *(F=NUMX10.1* MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop1';
run;



ods TEXT= "Age - Pop2"; 
proc tabulate data=pop2;
	class enfant;
	var age ;
	table  enfant, age='' *(F=NUMX10.1*MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop2';
run;

proc tabulate data=pop2;
	class cl_age;
	var  age ;
	table cl_age, age='' *(F=NUMX10.1*MIN MAX MEDIAN Q1 Q3 N MEAN STD)
		/ BOX='Pop2';
run;



/* first ae */
/*
ods TEXT= "First AE ind_med - Pop1"; 
proc tabulate data=pop1;
	class  first_indmed incident /MISSING;
	table  first_indmed all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "First AE ind_med - Pop2"; 
proc tabulate data=pop2;
	class  first_indmed incident /MISSING;
	table  first_indmed all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*PCTN='%T')
		/ BOX='Pop2';
run;
*/
ods TEXT= "First AE Prescripteur - Pop1"; 
proc tabulate data=pop1;
	class  psp_first_ae incident /MISSING;
	table  psp_first_ae all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "First AE Prescripteur - Pop2"; 
proc tabulate data=pop2;
	class  psp_first_ae incident /MISSING;
	table  psp_first_ae all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
run;

proc sort data=pop1;
by cl_age;
run;

proc sort data=pop2;
by cl_age;
run;
ods TEXT= "First AE Prescripteur - Pop1"; 
proc tabulate data=pop1;
	class  med_first incident index_ae cl_age/MISSING;
	table  med_first all,( incident*index_ae all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	by cl_age;
run;

ods TEXT= "First AE Prescripteur - Pop2"; 
proc tabulate data=pop2;
	class  med_first incident index_ae cl_age/MISSING;
	table  med_first all,( incident*index_ae all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	by cl_age;
run;

/* stat sur les periodes */
/* ********************* */


/* Neurologue dans les 3 mois qui precedent l'initiation */
proc sql;

create table sec_neuro1 as 
	select ben_idt_ano, incident,index_ae, cl_age, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_consult_tele) as nb_consult_tele
	from soins_pop1
	where sec_neuro=1 and periode in (3,4,5)
	group by ben_idt_ano, incident,index_ae, cl_age
;

create table sec_neuro2 as 
	select ben_idt_ano, incident,index_ae,cl_age, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_consult_tele) as nb_consult_tele
	from soins_pop2
	where sec_neuro=1 and periode in (3,4,5)
	group by ben_idt_ano, incident,index_ae, cl_age
;
quit;


/* complement 01/2023 */
/* ******************************************************** */
proc sql;

create table suivi_incidpop1 as 
	select ben_idt_ano, incident,index_ae, cl_age
		, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_acte_eeg) as nb_acte_eeg
		, sum(nb_acte_irm) as nb_acte_irm
		, sum(nb_consult_tele) as nb_consult_tele

		, sum(nb_consult_neuro_ter) as nb_consult_neuro_ter
		, sum(nb_consult_tele_ter) as nb_consult_tele_ter

		, sum(nb_consult_neuro_chu) as nb_consult_neuro_chu
		, sum(nb_consult_tele_chu) as nb_consult_tele_chu

		, sum(nb_consult_neuro_ch) as nb_consult_neuro_ch
		, sum(nb_consult_tele_ch) as nb_consult_tele_ch

		, sum(nb_consult_neuro_vil) as nb_consult_neuro_vil
		, sum(nb_consult_tele_vil) as nb_consult_tele_vil

	from soins_pop1
	where periode in (3,4,5)
	group by ben_idt_ano, incident,index_ae, cl_age
;

create table suivi_incidpop2 as 
	select ben_idt_ano, incident,index_ae,cl_age
		, sum(nb_consult_neuro) as nb_consult_neuro
		, sum(nb_acte_eeg) as nb_acte_eeg
		, sum(nb_acte_irm) as nb_acte_irm
		, sum(nb_consult_tele) as nb_consult_tele

		, sum(nb_consult_neuro_ter) as nb_consult_neuro_ter
		, sum(nb_consult_tele_ter) as nb_consult_tele_ter

		, sum(nb_consult_neuro_chu) as nb_consult_neuro_chu
		, sum(nb_consult_tele_chu) as nb_consult_tele_chu

		, sum(nb_consult_neuro_ch) as nb_consult_neuro_ch
		, sum(nb_consult_tele_ch) as nb_consult_tele_ch

		, sum(nb_consult_neuro_vil) as nb_consult_neuro_vil
		, sum(nb_consult_tele_vil) as nb_consult_tele_vil
	from soins_pop2
	where periode in (3,4,5)
	group by ben_idt_ano, incident,index_ae, cl_age
;
quit;



%macro med_suivi(pop=,ind=);

ods TEXT= "Repartition du nombre de consultations &ind. - &pop."; 

proc tabulate data=suivi_incid&pop.;
	class  cl_age ;
	table  (cl_age='' all),(F=NUMX10.1*N )
		/ BOX=&pop.;
	where incident=1 and &ind.>0;
run;
/*

proc tabulate data=suivi_incid&pop.;
	 var &ind. ;
	class  cl_age ;
	table  cl_age all,  (&ind.) *(F=NUMX10.1*N SUM MEAN STD MIN MAX  P10 Q1 MEDIAN Q3 P90)
		/ BOX=&pop.;
	where incident=1;
run;

*/

%mend;

%med_suivi(pop=pop1,ind=nb_consult_neuro);
%med_suivi(pop=pop1,ind=nb_consult_neuro_ter);
*%med_suivi(pop=pop1,ind=nb_consult_tele_ter);
%med_suivi(pop=pop1,ind=nb_consult_neuro_chu);
*%med_suivi(pop=pop1,ind=nb_consult_tele_chu);
%med_suivi(pop=pop1,ind=nb_consult_neuro_ch);
*%med_suivi(pop=pop1,ind=nb_consult_tele_ch);
%med_suivi(pop=pop1,ind=nb_consult_neuro_vil);
*%med_suivi(pop=pop1,ind=nb_consult_tele_vil);

%med_suivi(pop=pop2,ind=nb_consult_neuro);
%med_suivi(pop=pop2,ind=nb_consult_neuro_ter);
*%med_suivi(pop=pop2,ind=nb_consult_tele_ter);
%med_suivi(pop=pop2,ind=nb_consult_neuro_chu);
*%med_suivi(pop=pop2,ind=nb_consult_tele_chu);
%med_suivi(pop=pop2,ind=nb_consult_neuro_ch);
*%med_suivi(pop=pop2,ind=nb_consult_tele_ch);
%med_suivi(pop=pop2,ind=nb_consult_neuro_vil);
*%med_suivi(pop=pop2,ind=nb_consult_tele_vil);










/* ******************************************************** */



proc sql;
/* lieu exercice neuro */
create table lieu_neuro as 
	select p.ben_idt_ano, p.pop1, p.pop2,p.incident,p.index_ae,periode, put(age,agegpe.) as cl_age, lieu_exe, count(distinct consult_dte) as nb
	from &lib_dmb..patient p
		inner join &lib_dmb..periode_soins s on s.ben_idt_ano=p.ben_idt_ano 
		inner join &lib_dmb..consult c  on c.ben_idt_ano=s.ben_idt_ano
	where consult_dte between debut_periode and fin_periode 
		and spe_medecin='NEURO' and periode in (3,4,5)
	group by p.ben_idt_ano, p.pop1, p.pop2, periode,p.index_ae, lieu_exe,cl_age,p.incident
;

quit;

ods TEXT= "Au moins 1 C Neurologue dans les 3 mois qui precedent l'initiation - Pop1"; 
proc tabulate data=sec_neuro1;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
run;

proc tabulate data=sec_neuro1;
	 var nb_consult_neuro nb_consult_tele;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all, incident*nb_consult_neuro='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;

ods TEXT= "Au moins 1 C Neurologue and tele dans les 3 mois qui precedent l'initiation - Pop1"; 

proc tabulate data=sec_neuro1;
	 var nb_consult_neuro nb_consult_tele;
	class incident cl_age index_ae / missing;
	table  cl_age all, incident*nb_consult_tele='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;


ods TEXT= "Lieu d'exercice des neurologues (C les 3 mois qui precedent l'initiation) - Pop1"; 
proc tabulate data=lieu_neuro;
	var nb;
	class  cl_age lieu_exe index_ae / missing;
	table  index_ae*lieu_exe  all,cl_age*nb*(F=NUMX10.1*SUM F=NUMX10.1*COLPCTSUM='%T')
		/ BOX='Pop1';
	where pop1=1 and incident=1;
run;


ods TEXT= "Lieu d'exercice des neurologues (C les 3 mois qui precedent l'initiation) - Pop1"; 
proc tabulate data=lieu_neuro;
	var nb;
	class  cl_age lieu_exe / missing;
	table  lieu_exe  all,cl_age*nb*(F=NUMX10.1*SUM F=NUMX10.1*COLPCTSUM='%T')
		/ BOX='Pop1';
	where pop1=1 and incident=1;
run;




ods TEXT= "Au moins 1 C  Neurologue dans les 3 mois qui precedent l'initiation - Pop2"; 
proc tabulate data=sec_neuro2;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
run;

proc tabulate data=sec_neuro2;
	 var nb_consult_neuro;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all, incident*nb_consult_neuro='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;

ods TEXT= "Au moins 1 C Neurologue and tele dans les 3 mois qui precedent l'initiation - Pop2"; 

proc tabulate data=sec_neuro2;
	 var nb_consult_neuro nb_consult_tele ;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all, incident*nb_consult_tele='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;



ods TEXT= "Lieu d'exercice des neurologues (C les 3 mois qui precedent l'initiation) - Pop2"; 
proc tabulate data=lieu_neuro;
	var nb;
	class  cl_age lieu_exe index_ae / missing;
	table  index_ae*lieu_exe  all,cl_age*nb*(F=NUMX10.1*SUM F=NUMX10.1*COLPCTSUM='%T')
		/ BOX='Pop2';
	where pop2=1 and incident=1;
run;

ods TEXT= "Lieu d'exercice des neurologues (C les 3 mois qui precedent l'initiation) - Pop1"; 
proc tabulate data=lieu_neuro;
	var nb;
	class  cl_age lieu_exe / missing;
	table  lieu_exe  all,cl_age*nb*(F=NUMX10.1*SUM F=NUMX10.1*COLPCTSUM='%T')
		/ BOX='Pop2';
	where pop2=1 and incident=1;
run;


/* EEG dans les 3 mois qui precedent l'initiation */
proc sql;

create table sec_eeg1 as 
	select ben_idt_ano, incident, cl_age,index_ae
	from soins_pop1
	where sec_eeg=1 and periode in (3,4,5)
;

create table sec_eeg2 as 
	select distinct ben_idt_ano, incident,cl_age,index_ae
	from soins_pop2
	where sec_eeg=1 and periode in (3,4,5)
;
quit;

ods TEXT= "Au moins 1 EEG dans les 3 mois qui precedent l'initiation - Pop1"; 
proc tabulate data=sec_eeg1;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Au moins 1 EEG dans les 3 mois qui precedent l'initiation - Pop2"; 
proc tabulate data=sec_eeg2;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
run;



/* IRM dans les 3 mois qui precedent l'initiation */
proc sql;

create table sec_irm1 as 
	select ben_idt_ano, incident, cl_age,index_ae
	from soins_pop1
	where sec_irm=1 and periode in (3,4,5)
;

create table sec_irm2 as 
	select distinct ben_idt_ano, incident,cl_age,index_ae
	from soins_pop2
	where sec_irm=1 and periode in (3,4,5)
;
quit;

ods TEXT= "Au moins 1 IRM dans les 3 mois qui precedent l'initiation - Pop1"; 
proc tabulate data=sec_irm1;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
run;

ods TEXT= "Au moins 1 IRM dans les 3 mois qui precedent l'initiation - Pop2"; 
proc tabulate data=sec_irm2;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all,( incident all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
run;


/* TELECONSULTATION dans les 3 mois qui precedent l'initiation */


proc sql;

create table cons_tele1 as 
	select ben_idt_ano, incident, cl_age,index_ae
		, sum(nb_consult_tele) as nb
	from soins_pop1
	where nb_consult_tele>0 and periode in (3,4,5)
	group by ben_idt_ano, incident, cl_age,index_ae
;

create table cons_tele1_all as 
	select ben_idt_ano, incident,periode, cl_age,index_ae
		, sum(nb_consult_tele) as nb
	from soins_pop1
	where nb_consult_tele>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;

create table cons_tot1 as 
	select ben_idt_ano, incident,periode, cl_age,index_ae
		, sum(nb_consult_tot) as nb
	from soins_pop1
	where nb_consult_tot>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;
create table cons_urg1 as 
	select ben_idt_ano, incident,periode, cl_age,index_ae
		, sum(nb_consult_urg) as nb
	from soins_pop1
	where nb_consult_urg>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;

create table cons_tele2 as 
	select ben_idt_ano, incident,cl_age,index_ae
		, sum(nb_consult_tele) as nb
	from soins_pop2
	where nb_consult_tele>0 and periode in (3,4,5)
	group by ben_idt_ano, incident, cl_age,index_ae
;

create table cons_tele2_all as 
	select ben_idt_ano, incident,cl_age, periode,index_ae
		, sum(nb_consult_tele) as nb
	from soins_pop2
	where nb_consult_tele>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;


create table cons_tot2 as 
	select ben_idt_ano, incident,periode, cl_age,index_ae
		, sum(nb_consult_tot) as nb
	from soins_pop2
	where nb_consult_tot>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;
create table cons_urg2 as 
	select ben_idt_ano, incident,periode, cl_age,index_ae
		, sum(nb_consult_urg) as nb
	from soins_pop2
	where nb_consult_urg>0 
	group by incident,ben_idt_ano,  cl_age, periode,index_ae
;
quit;


ods TEXT= "Au moins 1 tele consulation dans les 3 mois qui precedent l'initiation - Pop1"; 

proc tabulate data=cons_tele1;
	 var nb;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all, incident*nb='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop1';
run;

ods TEXT= "Au moins 1 tele consulation dans les 3 mois qui precedent l'initiation - Pop2"; 

proc tabulate data=cons_tele2;
	 var  nb;
	class incident cl_age index_ae / missing;
	table  index_ae*cl_age all, incident*nb='' *(F=NUMX10.1*N SUM MEAN STD MIN MAX MEDIAN Q1 Q3)
		/ BOX='Pop2';
run;


ods TEXT= "Patient avec au moins 1 tele consulation, par periode - Pop1"; 

proc tabulate data=cons_tele1_all;
	 var nb;
	class incident periode cl_age / missing;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*N)
		/ BOX='Pop1';
run;


ods TEXT= "Nombre de tele consulation par periode - Pop1"; 

proc tabulate data=cons_tele1_all;
	 var nb;
	class incident periode cl_age/ missing ;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*SUM)
		/ BOX='Pop1';
run;

ods TEXT= "Nb de consulations par periode - Pop1"; 

proc tabulate data=cons_tot1;
	 var nb;
	class incident periode cl_age / missing;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*SUM)
		/ BOX='Pop1';
run;



ods TEXT= "Patients avec au moins 1 tele consulation, par periode - Pop2"; 

proc tabulate data=cons_tele2_all;
	 var nb;
	class incident periode cl_age / missing;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*N)
		/ BOX='Pop2';
run;



ods TEXT= "Nb tele consulations par periode - Pop2"; 

proc tabulate data=cons_tele2_all;
	 var nb;
	class incident periode cl_age / missing;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*SUM)
		/ BOX='Pop2';
run;


ods TEXT= "Nb de consulations par periode - Pop2"; 

proc tabulate data=cons_tot2;
	 var nb;
	class incident periode cl_age / missing;
	table  incident*cl_age all, periode*nb='' *(F=NUMX10.1*SUM)
		/ BOX='Pop2';
run;



/* les TT */

/*Proportion de sujets ayant eu une initiation d'AE durant l'annee precedant
la premiere consultation en centre expert apres la date index */


proc sql;

/* denominateur */
create table ter_pop1 as 
	select ben_idt_ano,cl_age,incident,index_ae,ter, min(date_ter) format=ddmmyy10. as min_ter_dte
	from  soins_pop1 
	where ter=1 and periode>=6
	group by ben_idt_ano,cl_age,incident,index_ae,ter
;
create index ben_idt_ano on ter_pop1(ben_idt_ano);

create table ter_pop2 as 
	select ben_idt_ano,cl_age,incident,index_ae,ter, min(date_ter) format=ddmmyy10. as min_ter_dte
	from  soins_pop2 
	where ter=1 and periode>=6
	group by ben_idt_ano,cl_age,incident,index_ae,ter
;
create index ben_idt_ano on ter_pop2(ben_idt_ano);


/* numerateur initiation de TT */

create table ter_init_pop1 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae,ter
	from ter_pop1 p
		inner join &lib_dmb..cure c on c.ben_idt_ano=p.ben_idt_ano 
	where deb_cure_dte between min_ter_dte-360 and min_ter_dte-1
;

create table ter_init_pop2 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae,ter
	from ter_pop2 p
		inner join &lib_dmb..cure c on c.ben_idt_ano=p.ben_idt_ano 
	where deb_cure_dte between min_ter_dte-360 and min_ter_dte-1
;
quit;

proc sql;

/* numerateur consultation neuro */

create table ter_neuro_pop1 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae
	from ter_pop1 p
		inner join &lib_dmb..consult c on c.ben_idt_ano=p.ben_idt_ano 
	where spe_medecin='NEURO' and consult_dte between min_ter_dte-360 and min_ter_dte-1
;

create table ter_neuro_pop2 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae
	from ter_pop2 p
		inner join &lib_dmb..consult c on c.ben_idt_ano=p.ben_idt_ano 
	where spe_medecin='NEURO' and consult_dte between min_ter_dte-360 and min_ter_dte-1
;

quit;


proc sql;

/* numerateur periode en polytherapie */

create table ter_poly_pop1 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae
	from ter_pop1 p
		inner join &lib_dmb..sequence_poly s on s.ben_idt_ano=p.ben_idt_ano 
	where poly=1 and duree>90 and debut_seqpoly<min_ter_dte and fin_seqpoly>=(min_ter_dte-360)
;

create table ter_poly_pop2 as
	select distinct p.ben_idt_ano,cl_age,incident,index_ae
	from ter_pop2 p
		inner join &lib_dmb..sequence_poly s on s.ben_idt_ano=p.ben_idt_ano 
	where poly=1 and duree>90 and debut_seqpoly<min_ter_dte and fin_seqpoly>=(min_ter_dte-360)
;

quit;

ods TEXT= " Incident avec passage en centre TER - Pop1"; 

proc tabulate data=ter_pop1;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1;
run;


ods TEXT= " Incident avec passage en centre TER + initiation AE dans l'annee avant - Pop1"; 

proc tabulate data=ter_init_pop1;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1;
run;

ods TEXT= " Incident avec passage en centre TER + C NEURO dans l'annee avant - Pop1"; 

proc tabulate data=ter_neuro_pop1;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1;
run;

ods TEXT= " Incident avec passage en centre TER + polytherapie dans l'annee avant - Pop1"; 

proc tabulate data=ter_poly_pop1;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1;
run;

ods TEXT= " Incident avec passage en centre TER - Pop2"; 

proc tabulate data=ter_pop2;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1;
run;


ods TEXT= " Incident avec passage en centre TER + initiation AE dans l'annee avant - Pop2"; 

proc tabulate data=ter_init_pop2;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1;
run;

ods TEXT= " Incident avec passage en centre TER + C NEURO dans l'annee avant - Pop2"; 

proc tabulate data=ter_neuro_pop2;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1;
run;

ods TEXT= " Incident avec passage en centre TER + polytherapie dans l'annee avant - Pop2"; 

proc tabulate data=ter_poly_pop2;
	class  index_ae cl_age / missing;
	table  cl_age all, index_ae*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1;
run;


/* repartition des incident par classe ae */

ods TEXT= " Repartition des incidents selon la classe AE de l'initiation - Pop1"; 

proc tabulate data=pop1;
	class  index_ae cl_age classe_ae /*first_classae*// missing;
	table  (cl_age*(classe_ae all)) , (index_ae all)*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1;
run;


ods TEXT= " Repartition des incidents selon la classe AE de l'initiation - Pop2"; 

proc tabulate data=pop2;
	class  index_ae cl_age classe_ae / missing;
	table  (cl_age*(classe_ae all)), (index_ae all)*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1;
run;



/* initiation vpa ou cbz */

ods TEXT= " Initiation par VPA ou CBZ - Pop1"; 

proc tabulate data=pop1;
	class  index_ae cl_age / missing;
	table  cl_age all , (index_ae all)*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1 and (first_indmed like '%VPA%' or first_indmed like '%CBZ%');
run;


ods TEXT= " Initiation par VPA ou CBZ - Pop2"; 

proc tabulate data=pop2;
	class  index_ae cl_age / missing;
	table  cl_age all , (index_ae all)*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1 and (first_indmed like '%VPA%' or first_indmed like '%CBZ%');
run;




/* initiation vpa ou cbz */

ods TEXT= " Initiation  LP - Pop1"; 

proc tabulate data=pop1;
	class  index_ae cl_age first_lp / missing;
	table  cl_age all , (index_ae*(first_lp all))*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where incident=1 and (first_indmed like '%VPA%' or first_indmed like '%CBZ%');
run;


ods TEXT= " Initiation  LP - Pop2"; 

proc tabulate data=pop2;
	class  index_ae cl_age first_lp / missing;
	table  cl_age all , (index_ae*(first_lp all))*(F=NUMX10.1*N F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where incident=1 and (first_indmed like '%VPA%' or first_indmed like '%CBZ%');
run;


/* nombre de patients avec au moins une periode poly */

proc sql;

create table poly_pop1 as 
	select cl_age,index_ae, count(distinct ben_idt_ano) as nb
	from soins_pop1
	where incident=1 and etat_tt='P' and periode>=6
	group by cl_age,index_ae
;


create table poly_pop1_2 as 
	select cl_age,index_ae, count(distinct ben_idt_ano) as nb
	from soins_pop1
	where incident=1 and etat_tt='P' and periode>=6 and ligne_n2 between 1 and 4
	group by cl_age,index_ae
;

create table poly_pop2 as 
	select cl_age,index_ae, count(distinct ben_idt_ano) as nb
	from soins_pop2
	where incident=1 and etat_tt='P' and periode>=6 
	group by cl_age,index_ae
;

create table poly_pop2_2 as 
	select cl_age,index_ae, count(distinct ben_idt_ano) as nb
	from soins_pop2
	where incident=1 and etat_tt='P' and periode>=6 and ligne_n2 between 1 and 4
	group by cl_age,index_ae
;

quit;


/***************************************************************************/
/*Complement a propos du prescripteur : ajout du lieu d exercice juridique */
/* prescripteur new med epi */

proc sort data=pop1;
by age;
run;

ods TEXT= "Prescripteur new med epi - Pop1"; 
proc tabulate data=pop1;
	class  med_first stj_first_ae  /MISSING;
	table  med_first all,(  stj_first_ae all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop1';
	where new_med_epi=1;
	by age;
	format age agegpe. ;
run;


proc sort data=pop2;
by age;
run;
ods TEXT= "Prescripteur new med epi - Pop2"; 
proc tabulate data=pop2;
	class  med_first stj_first_ae /MISSING;
	table  med_first all,(  stj_first_ae all)*(F=NUMX10.1*N='' F=NUMX10.1*COLPCTN='%T')
		/ BOX='Pop2';
	where new_med_epi=1;
	by age;
	format age agegpe. ;
run;