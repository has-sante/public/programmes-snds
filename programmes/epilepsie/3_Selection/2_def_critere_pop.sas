/****************************************************************************************************/
/*                         ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                                 */
/****************************************************************************************************/



/****************************************************************************************************/
/* Nom du programme : DEF_CRITERE_POP

/* Objectif : 
* creer les tables d'ALD 
* reconstituer toutes les variables indicatrices de l'inclusion/exclusion des patients dans les populations d'etude 
* definir les parametres permettant d'identifier les nouveaux patients des patients connus
* identifier la premiere delivrance d'AE et le medecin prescripteur */

/* tables de sortie : &lib_dmb..hist_ald ; creations des nouvelles variables dans &lib_dmb..patient */

/****************************************************************************************************/


/* recopie de table patient initiale (sans indicateur)*/
proc sql;

create table &lib_dmb..patient as 
	select * from &lib_dmb..patient_init;

create index ben_idt_ano on &lib_dmb..patient (ben_idt_ano);

quit;


* *********************************** ;
* historique des ald d'interet ;
* *********************************** ;

proc sql;
	create table hist_ald as 
	select distinct ben_idt_ano,cim_gpe, med_mtf_cod as cim10, imb_ald_num as ald_num,
		datepart(imb_ald_dtd) as ald_deb format=ddmmyy10.,datepart(imb_ald_dtf) as ald_fin format=ddmmyy10.,
		cat(ben_idt_ano,cim_gpe) as cle
	from &lib_ext..IR_IMB_R 
	where imb_etm_nat in (0,41,43,45,47) and cim_gpe is not null
;

update hist_ald set ald_fin='31DEC2099'd where ald_fin='01JAN1600'd;
quit;



/* fusion des periodes d'ald qui se chevauchent */

proc sort data=hist_ald;
	by cle ald_deb;
run;

data hist_ald;
	set hist_ald;
	by cle;

	retain num_ald; /* numero l'ald */
	retain num_per_ald; /* numero de la periode d'ald */
	retain l_groupe ;
	retain l_deb;
	retain l_fin;

	format l_deb l_fin ddmmyy10.;

	l_deb=lag(ald_deb);
	l_fin=lag(ald_fin);
	l_groupe=lag(cim_gpe);

	if first.cle then do;
		num_ald=1;
		num_per_ald=0;
		l_fin=.;
		l_groupe=.;
		l_deb=.;
	end;

	num_per_ald=num_per_ald+1;

	delai=ald_deb-l_fin;

	if delai>1 then do;
		num_ald=num_ald+1;
		num_per_ald=1;
	end;

run;

/* synthese des ald par periode */
proc sql;

create table &lib_dmb..hist_ald as 
	select ben_idt_ano,cim_gpe, num_ald as ald_periode, min(ald_deb) as ald_deb format=ddmmyy10., max(ald_fin) as ald_fin format=ddmmyy10.
	from hist_ald
	group by ben_idt_ano, cim_gpe, num_ald

;

create index ben_idt_ano on &lib_dmb..hist_ald (ben_idt_ano);
create index cim_gpe on &lib_dmb..hist_ald (cim_gpe);

quit;



/* macro de generation des tops ald 
recherche ald selon les parametres
	var_ald : nom de la variable "top ald"
	condition : criteres de selection de l'ald dans la table des ald
*/
%macro search_ald(var_ald=, condition=);

%_eg_conditional_dropds(tmp);

proc sql;

	create table tmp_ald as 
	select distinct p.ben_idt_ano, 1 as top_ald
	from &lib_dmb..patient p
	inner join &lib_dmb..hist_ald a on a.ben_idt_ano=p.ben_idt_ano
	where &condition
	;
	create index ben_idt_ano on tmp_ald(ben_idt_ano);

	create table patient as 
	select p.*, case when top_ald is null then 0 else 1 end as &var_ald
	from &lib_dmb..patient p
	left join tmp_ald t on t.ben_idt_ano=p.ben_idt_ano 
	;

quit;


proc sql;
	drop table &lib_dmb..patient ;
	create table &lib_dmb..patient as 
		select * from patient;

create index ben_idt_ano on &lib_dmb..patient(ben_idt_ano);

quit;


%mend;

* generation des top ald dans la table identite;
%search_ald(var_ald=ald_epi_18, condition= a.cim_gpe in ('EPI') and ald_deb<='31DEC2018'd and ald_fin>='01JAN2018'd);
%search_ald(var_ald=ald_oh, condition= a.cim_gpe in ('OH') and ald_deb<='31DEC2019'd and ald_fin>='01JAN2017'd);
%search_ald(var_ald=ald_dln, condition= a.cim_gpe in ('DLN') and ald_deb<='31DEC2019'd and ald_fin>='01JAN2017'd);
%search_ald(var_ald=ald_mig, condition= a.cim_gpe in ('MIG') and ald_deb<='31DEC2019'd and ald_fin>='01JAN2017'd);
%search_ald(var_ald=ald_tb, condition= a.cim_gpe in ('TB') and ald_deb<='31DEC2019'd and ald_fin>='01JAN2017'd);
%search_ald(var_ald=ald_anx, condition= a.cim_gpe in ('ANX') and ald_deb<='31DEC2019'd and ald_fin>='01JAN2017'd);



/* date index ald epi */

proc sql;

create table ald_dte as 
select distinct p.ben_idt_ano, min(ald_deb) format=ddmmyy10. as first_ald_epi
from &lib_dmb..patient p
inner join &lib_dmb..hist_ald a on a.ben_idt_ano=p.ben_idt_ano
where a.cim_gpe in ('EPI') and p.ald_epi_18=1 
group by p.ben_idt_ano
;
create index ben_idt_ano on ald_dte(ben_idt_ano);

alter table &lib_dmb..patient add date_index_ald_epi date format=ddmmyy10., new_ald_epi num(1);

update &lib_dmb..patient p set date_index_ald_epi=
	(select max(first_ald_epi, '01JAN2018'd) from ald_dte a where p.ben_idt_ano=a.ben_idt_ano)
;

update &lib_dmb..patient p set new_ald_epi=
	case when ald_epi_18=0 or date_index_ald_epi<='01JAN2018'd then 0 else 1 end;
;


update &lib_dmb..patient p set new_ald_epi=null
where date_index_ald_epi is null
;


quit;


* *********************************** ;
* historique des TT d'int ;
* *********************************** ;


* top inclusion/exclusion med autre ;
* ********************************* ;

/* macro des tops medicaments selon les parametres
	var_hosp : nom de la variable "top hospit"
	condition : criteres de selection sejour dans la table des diag
	lim : nombre de delivrances necessaire pour valider le top
*/

/* nb : realise avant d'avoir assez de place pour generer la table DELIVRANCE  */

%macro search_med(var_med=, condition=, lim=);

* nb del minimum  ;
%if %length(&lim)=0 %then %do;
	%let lim_del=1;
%end;
%else %do;
	%let lim_del=&lim;
%end;

proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, pha_prs_c13 as cod13 , exe_soi_dtd,sum(pha_act_qsn) as nb
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F a on a.ben_idt_ano=p.ben_idt_ano 
	where &condition
	group by p.ben_idt_ano, cod13,exe_soi_dtd
	having nb>0
;

insert into  tmp  
	select distinct p.ben_idt_ano, input(ucd_ucd_cod,13.)  , exe_soi_dtd,sum(ucd_dlv_nbr) 
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F a on a.ben_idt_ano=p.ben_idt_ano 
	where &condition
	group by p.ben_idt_ano, ucd_ucd_cod,exe_soi_dtd
	having sum(ucd_dlv_nbr)>0
;

create table tmp_med as 
	select ben_idt_ano,count(distinct exe_soi_dtd) as nb_del, sum(nb) as nb, 1 as top_med
	from tmp
	group by ben_idt_ano
;

create index ben_idt_ano on tmp_med(ben_idt_ano);

alter table &lib_dmb..patient drop &var_med, nb_&var_med;
create table patient as 
	select p.*
		,case when top_med is null or nb_del< &lim_del then 0 else 1 end as &var_med
		,case when top_med is null then 0 else nb_del end as nb_&var_med
	from &lib_dmb..patient p
	left join tmp_med t on t.ben_idt_ano=p.ben_idt_ano
;


quit;

proc sql;

drop table &lib_dmb..patient;
create table &lib_dmb..patient as 
	select * from patient;

create index ben_idt_ano on &lib_dmb..patient(ben_idt_ano);

quit;

%mend;



* med spe epi;
%search_med(var_med=med_epispe_18, condition= year(exe_soi_dtd)=2018 and med_spe_epi=1);
* med epi non spe;
%search_med(var_med=med_epinonspe_18, condition= year(exe_soi_dtd)=2018 and med_spe_epi=0 and med_epi=1);

%search_med(var_med=med_oh, condition= a.med_oh=1, lim=3);
%search_med(var_med=med_dln, condition= a.med_dln=1, lim=3);
%search_med(var_med=med_mig, condition= a.med_mig=1, lim=3);
%search_med(var_med=med_tb, condition= a.med_tb=1, lim=1);
%search_med(var_med=med_anx, condition= a.med_anx=1, lim=3);


*** autres criteres exclusion med non spe **** ;
* ******************************************** ;
proc sql;
alter table epi_cib.identite drop med_clon_18, med_pgb_18, med_pgbpsy_18,med_gbp_18,med_tbpsy_18;
quit;
* CLON;
%search_med(var_med=med_clon_18, condition= year(exe_soi_dtd)=2018 and ind_med in ('CLON'));

* PGB  ;
%search_med(var_med=med_pgb_18, condition= year(exe_soi_dtd)=2018 and ind_med in ('PGB'));
* PGB +- psy ;
%search_med(var_med=med_pgbpsy_18, condition= year(exe_soi_dtd)=2018 and ind_med in ('PGB') and psp_spe_cod in (33,75));

* GBP ;
%search_med(var_med=med_gbp_18, condition= year(exe_soi_dtd)=2018 and ind_med in ('GBP'));

* CBZ ou LTG ;
%search_med(var_med=med_tbpsy_18, condition= year(exe_soi_dtd)=2018 and ind_med in ('CBZ','LTG') and psp_spe_cod in (33,75));


* CLON en monotherapie ;
proc sql;

create table tmp_med1 as 
	select distinct ben_idt_ano 
	from &lib_ext..ER_PHA_F a  
	where ind_med in ('CLON') and year(exe_soi_dtd)=2018
	group by ben_idt_ano
	having sum(pha_act_qsn)>0

;
insert into tmp_med1  
	select distinct ben_idt_ano 
	from &lib_ext..ER_UCD_F a  
	where ind_med in ('CLON') and year(exe_soi_dtd)=2018
	group by ben_idt_ano
	having sum(ucd_dlv_nbr)>0

;

create index ben_idt_ano on tmp_med1(ben_idt_ano);

create table tmp2 as 
	select a.ben_idt_ano, a.pha_prs_c13,a.exe_soi_dtd
	from &lib_ext..ER_PHA_F a
	inner join tmp_med1 t on a.ben_idt_ano=t.ben_idt_ano
	where ind_med not in ('CLON') and med_epi=1
	group by a.ben_idt_ano, a.pha_prs_c13,a.exe_soi_dtd
	having sum(pha_act_qsn)>0
;

insert into tmp2  
	select a.ben_idt_ano, input(a.ucd_ucd_cod,13.),a.exe_soi_dtd
	from &lib_ext..ER_UCD_F a
	inner join tmp_med1 t on a.ben_idt_ano=t.ben_idt_ano
	where ind_med not in ('CLON') and med_epi=1
	group by a.ben_idt_ano, a.ucd_ucd_cod,a.exe_soi_dtd
	having sum(ucd_dlv_nbr)>0
;


create index ben_idt_ano on tmp2(ben_idt_ano);

create table tmp_monoclon as 
	select distinct a.ben_idt_ano
	from tmp_med1 a
	where not exists (select * from tmp2 t where a.ben_idt_ano=t.ben_idt_ano)
;
create index ben_idt_ano on tmp_monoclon(ben_idt_ano);

alter table &lib_dmb..patient add med_clonmono num(1);

update &lib_dmb..patient p set med_clonmono=1
	where exists (select * from tmp_monoclon m where m.ben_idt_ano=p.ben_idt_ano);

update &lib_dmb..patient set med_clonmono=0
	where med_clonmono is null;

quit;


* CLON + (PGB ou GBP) le meme jour ;
proc sql;

create table tmp_med1 as 
	select distinct ben_idt_ano,  exe_soi_dtd 
	from &lib_ext..ER_PHA_F a  
	where year(exe_soi_dtd)=2018 and ind_med in ('CLON')
	group by ben_idt_ano
	having sum(pha_act_qsn)>0

;
insert into tmp_med1  
	select distinct ben_idt_ano,  exe_soi_dtd 
	from &lib_ext..ER_UCD_F a  
	where year(exe_soi_dtd)=2018 and ind_med in ('CLON')
	group by ben_idt_ano
	having sum(ucd_dlv_nbr)>0

;

create index ben_idt_ano on tmp_med1(ben_idt_ano);
create index exe_soi_dtd on tmp_med1(exe_soi_dtd);

create table tmp2 as 
	select a.ben_idt_ano, a.pha_prs_c13,a.exe_soi_dtd
	from &lib_ext..ER_PHA_F a
	inner join tmp_med1 t on a.ben_idt_ano=t.ben_idt_ano
	where a.exe_soi_dtd=t.exe_soi_dtd and ind_med in ('PGB','GBP')
	group by a.ben_idt_ano, a.pha_prs_c13,a.exe_soi_dtd
	having sum(pha_act_qsn)>0
;
insert into tmp2  
	select a.ben_idt_ano, input(a.ucd_ucd_cod,13.),a.exe_soi_dtd
	from &lib_ext..ER_UCD_F a
	inner join tmp_med1 t on a.ben_idt_ano=t.ben_idt_ano
	where a.exe_soi_dtd=t.exe_soi_dtd and ind_med in ('PGB','GBP')
	group by a.ben_idt_ano, a.ucd_ucd_cod,a.exe_soi_dtd
	having sum(ucd_dlv_nbr)>0
;
create index ben_idt_ano on tmp2(ben_idt_ano);

create table tmp_med as 
	select distinct a.ben_idt_ano
	from tmp2 a
	inner join tmp_med1 t on a.ben_idt_ano=t.ben_idt_ano
;
create index ben_idt_ano on tmp_med(ben_idt_ano);

alter table &lib_dmb..patient add med_clonsimult_18 num(1);

update &lib_dmb..patient p set med_clonsimult_18=1
	where exists (select * from tmp_med m where m.ben_idt_ano=p.ben_idt_ano);

update &lib_dmb..patient set med_clonsimult_18=0
	where med_clonsimult_18 is null;

quit;


* date index epi ;
* first med epi en 2018;
* first med ae;

%macro medepi(var_med=, condition=);

proc sql;

create table tmp1 as 
	select ben_idt_ano, pha_prs_c13, exe_soi_dtd 
	from &lib_ext..ER_PHA_F
	where &condition and year(exe_soi_dtd)=2018 
	group by ben_idt_ano, pha_prs_c13, exe_soi_dtd 
	having sum(pha_act_qsn)>0
;
insert into tmp1  
	select ben_idt_ano, input(ucd_ucd_cod,13.), exe_soi_dtd 
	from &lib_ext..ER_UCD_F
	where &condition and year(exe_soi_dtd)=2018 
	group by ben_idt_ano, ucd_ucd_cod, exe_soi_dtd 
	having sum(ucd_dlv_nbr)>0
;
create table tmp_med as 
	select ben_idt_ano, min(exe_soi_dtd) format=ddmmyy10. as first_18_dte
	from tmp1
	group by ben_idt_ano
;
create index ben_idt_ano on tmp_med(ben_idt_ano);


create table tmp2 as 
	select ben_idt_ano, pha_prs_c13, exe_soi_dtd 
	from &lib_ext..ER_PHA_F
	where &condition
	group by ben_idt_ano, pha_prs_c13, exe_soi_dtd 
	having sum(pha_act_qsn)>0
;
insert into tmp2  
	select ben_idt_ano, input(ucd_ucd_cod,13.), exe_soi_dtd 
	from &lib_ext..ER_UCD_F
	where &condition
	group by ben_idt_ano, ucd_ucd_cod, exe_soi_dtd 
	having sum(ucd_dlv_nbr)>0
;

create table tmp_first as 
	select ben_idt_ano, min(exe_soi_dtd) format=ddmmyy10. as first_dte
	from tmp2
	group by ben_idt_ano
;
create index ben_idt_ano on tmp_first(ben_idt_ano);



alter table &lib_dmb..patient add &var_med._18_dte date format=ddmmyy10.,&var_med._dte date format=ddmmyy10.;

update &lib_dmb..patient p set &var_med._18_dte=null,&var_med._dte=null;

update &lib_dmb..patient p set &var_med._18_dte=
	(select first_18_dte from tmp_med m where m.ben_idt_ano=p.ben_idt_ano);
update &lib_dmb..patient p set &var_med._dte=
	(select first_dte from tmp_first m where m.ben_idt_ano=p.ben_idt_ano);


quit;


%mend;
%medepi(var_med=first_ae, condition= med_epi=1);
%medepi(var_med=first_aespe, condition=  med_spe_epi=1);


proc sql;

alter table &lib_dmb..patient add med_epi_18 num(1), new_med_epi num(1);

update &lib_dmb..patient set 
	med_epi_18=max(med_epispe_18,med_epinonspe_18)
;

update &lib_dmb..patient set new_med_epi= 
	case when med_epi_18=0 then 0
	 when (med_epi_18=1 and first_ae_dte<first_ae_18_dte) then 0 else 1 end;
;


update &lib_dmb..patient set new_med_epi= null
where first_ae_dte is null
;

quit;


/* first indmed */

proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, ind_med
	from &lib_dmb..patient p
		inner join &lib_dmb..cure c on c.ben_idt_ano=p.ben_idt_ano and p.first_ae_dte=c.deb_cure_dte
;

quit;

proc sort data=tmp;
	by ben_idt_ano ;
run;

data tmp_indmed;
set tmp;
by ben_idt_ano;

retain indmed_cat;
retain nb;
length indmed_cat $50.;

if first.ben_idt_ano then do;
	indmed_cat='';
	nb=0;
end;
indmed_cat=catx('',ind_med,indmed_cat);
nb=nb+1;
if last.ben_idt_ano then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_indmed (ben_idt_ano);

alter table &lib_dmb..patient add first_indmed varchar(50);
update &lib_dmb..patient p set first_indmed=
	(select indmed_cat from tmp_indmed t where t.ben_idt_ano=p.ben_idt_ano )
;

quit;





/* type prescripteur de la 1ere del */
proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, exe_soi_dtd ,  spe_gpe as spe_medecin, cat(p.ben_idt_ano,exe_soi_dtd) as cle
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
		left join cod_spe e on e.spe_cod_num=psp_spe_cod
	where med_epi=1 
union
	select distinct p.ben_idt_ano, exe_soi_dtd,  spe_gpe , cat(p.ben_idt_ano,exe_soi_dtd)
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
		left join cod_spe e on e.spe_cod_num=psp_spe_cod
	where med_epi=1 
;


quit;


proc sort data=tmp;
	by cle spe_medecin;
run;
data tmp_psp;
set tmp;
by cle;

retain psp_cat;
retain nb;
length psp_cat $50.;

if first.cle then do;
	psp_cat='';
	nb=0;
end;
psp_cat=catx('',spe_medecin,psp_cat);
nb=nb+1;
if last.cle then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_psp (ben_idt_ano);

alter table &lib_dmb..patient add psp_first_ae varchar(50);
update &lib_dmb..patient p set psp_first_ae=
	(select psp_cat from tmp_psp t where t.ben_idt_ano=p.ben_idt_ano )
;

quit;


/* MAJ Janvier 2023 
Lieu d'exercice du prescripteur de la primo prescription
*/


/* type prescripteur de la 1ere del */
proc sql;

create table tmp as 
	select distinct p.ben_idt_ano, exe_soi_dtd ,  put(psp_stj_cod,stjfmt.) as stj_med, cat(p.ben_idt_ano,exe_soi_dtd) as cle
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
	where med_epi=1 
union
	select distinct p.ben_idt_ano, exe_soi_dtd,  put(psp_stj_cod,stjfmt.) as stj_med , cat(p.ben_idt_ano,exe_soi_dtd)
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
	where med_epi=1 
;


quit;


proc sort data=tmp;
	by cle stj_med;
run;
data tmp_stj;
set tmp;
by cle;

retain stj_cat;
retain nb;
length stj_cat $50.;

if first.cle then do;
	stj_cat='';
	nb=0;
end;
stj_cat=catx('',stj_med,stj_cat);
nb=nb+1;
if last.cle then do;
	output;
end;

run;


proc sql;

create index ben_idt_ano on tmp_stj (ben_idt_ano);

alter table &lib_dmb..patient add stj_first_ae varchar(50);
update &lib_dmb..patient p set stj_first_ae=
	(select stj_cat from tmp_stj t where t.ben_idt_ano=p.ben_idt_ano )
;

quit;




/* lp a l'initiation  */
proc sql;

create table tmp1 as 
	select p.ben_idt_ano, max(lp) as max_lp
	from &lib_dmb..patient p
		inner join &lib_ext..ER_PHA_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
	where med_epi=1 
	group by p.ben_idt_ano
union
	select p.ben_idt_ano, max(lp)
	from &lib_dmb..patient p
		inner join &lib_ext..ER_UCD_F f on p.ben_idt_ano=f.ben_idt_ano and f.exe_soi_dtd=p.first_ae_dte
		left join cod_spe e on e.spe_cod_num=psp_spe_cod
	where med_epi=1 
	group by p.ben_idt_ano
;

create table tmp as 
	select ben_idt_ano, max(max_lp) as lp
	from tmp1
	group by ben_idt_ano;


create index ben_idt_ano on tmp (ben_idt_ano);

alter table &lib_dmb..patient add first_lp num(1);
update &lib_dmb..patient p set first_lp=
	 (select lp from tmp t where t.ben_idt_ano=p.ben_idt_ano )
;
update &lib_dmb..patient p set first_lp=0 where first_lp is null;

quit;


* *********************************** ;
* top sur les hospitalisations  ;
* *********************************** ;

%macro search_hosp(var_hosp=, condition=);

proc sql ;


create table tmp_hosp as 
	select distinct p.ben_idt_ano, 1 as top_hosp
	from &lib_dmb..patient p
	inner join &lib_dmb..hist_diag a on a.ben_idt_ano=p.ben_idt_ano 
	where &condition
;
create index ben_idt_ano on tmp_hosp(ben_idt_ano);

create table patient as 
	select p.*, case when top_hosp is null then 0 else 1 end as &var_hosp
	from &lib_dmb..patient p
	left join tmp_hosp t on t.ben_idt_ano=p.ben_idt_ano 
;


quit;

proc sql;

drop table &lib_dmb..patient;
create table &lib_dmb..patient as 
	select * from patient;

create index ben_idt_ano on &lib_dmb..patient(ben_idt_ano);

quit;
%mend;


%search_hosp(var_hosp=hosp_epi_18, condition= cim_gpe in ('EPI') and ent_dat<='31DEC2018'd and sor_dat>='01JAN2018'd);
%search_hosp(var_hosp=hosp_oh, condition=  cim_gpe in ('OH') and typ_diag in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_dln, condition=  cim_gpe in ('DLN') and typ_diag in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_mig, condition=  cim_gpe in ('MIG') and typ_diag in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_tb, condition=  cim_gpe in ('TB') and typ_diag in ('DPS','DPU','DRS','DRU'));
%search_hosp(var_hosp=hosp_anx, condition=  cim_gpe in ('ANX') and typ_diag in ('DPS','DPU','DRS','DRU'));


/* date index hosp epi first hosp epi */
proc sql;

create table hosp_dte as 
select distinct p.ben_idt_ano, min(ent_dat) format=ddmmyy10. as hosp_epi_18_dte
	from &lib_dmb..patient p
	inner join &lib_dmb..hist_diag a on a.ben_idt_ano=p.ben_idt_ano 
where a.cim_gpe in ('EPI') and p.hosp_epi_18=1 and ent_dat<='31DEC2018'd and sor_dat>='01JAN2018'd
group by p.ben_idt_ano
;
create index ben_idt_ano on hosp_dte(ben_idt_ano);

create table first_hosp_dte as 
select distinct p.ben_idt_ano, min(ent_dat) format=ddmmyy10. as first_hosp_epi_dte
	from &lib_dmb..patient p
	inner join &lib_dmb..hist_diag a on a.ben_idt_ano=p.ben_idt_ano 
where a.cim_gpe in ('EPI') and p.hosp_epi_18=1 
group by p.ben_idt_ano
;
create index ben_idt_ano on first_hosp_dte(ben_idt_ano);


alter table &lib_dmb..patient add date_index_hosp date format=ddmmyy10., first_hosp_epi date format=ddmmyy10., new_hosp num(1);

update &lib_dmb..patient p set date_index_hosp=
	(select max(hosp_epi_18_dte, '01JAN2018'd) from hosp_dte a where p.ben_idt_ano=a.ben_idt_ano)
;
update &lib_dmb..patient p set first_hosp_epi=
	(select first_hosp_epi_dte from first_hosp_dte a where p.ben_idt_ano=a.ben_idt_ano)
;


update &lib_dmb..patient p set new_hosp=null;

update &lib_dmb..patient p set new_hosp=
	case when (hosp_epi_18=0 or first_hosp_epi<date_index_hosp) then 0 else 1 end;
;

update &lib_dmb..patient p set new_hosp=null
	where date_index_hosp is null;

quit;


*** autres criteres exclusion ea - EEG **** ;
* ***************************************** ;

proc sql;

create table tmp_eeg as 
	select distinct p.ben_idt_ano, 1 as top_eeg
	from &lib_dmb..patient p
	inner join &lib_ext..er_cam_f a on a.ben_idt_ano=p.ben_idt_ano 
	where first_ae_18_dte is not null and a.exe_soi_dtd between first_ae_18_dte-360 and first_ae_18_dte+360
	group by p.ben_idt_ano
;

create index ben_idt_ano on tmp_eeg(ben_idt_ano);

create table patient as 
	select p.*
		,case when t.top_eeg is null then 0 else 1 end as eeg_12m_18
	from &lib_dmb..patient p
	left join tmp_eeg t on t.ben_idt_ano=p.ben_idt_ano
;


quit;

proc sql;

drop table &lib_dmb..patient;
create table &lib_dmb..patient as 
	select * from patient;

create index ben_idt_ano on &lib_dmb..patient(ben_idt_ano);

quit;


proc sql;

alter table &lib_dmb..patient add date_index date format=ddmmyy10.,incident num(1), age smallint;

update &lib_dmb..patient set 
	date_index = case when med_epi_18=1 or hosp_epi_18=1 then min(date_index_hosp,first_ae_18_dte)
				when (med_epi_18=0 or med_epi_18 is null) and (hosp_epi_18=0 or hosp_epi_18 is null) and ald_epi_18=1 then date_index_ald_epi
				end
;


alter table &lib_dmb..patient add index_ae num(1);
update &lib_dmb..patient set 
	index_ae = case when first_ae_18_dte=date_index then 1 else 0 end;



update &lib_dmb..patient set 
	incident= case when date_index>='01JAN2018'd 
		and (new_hosp=0 or new_med_epi=0 or new_ald_epi=0)
			then 0 else 1 end
; 

update &lib_dmb..patient set 
	/*age=round((date_index-input(cat('15/',mob,'/',yob),ddmmyy10.))/365.25)*/
	age=year(date_index)-input(yob,4.);

quit;



/* definition des populations d'etude */

/*
pop 1 - estimation haute
pop 2 - estimation basse 
*/

/* Note : critere EEG finalement non utilise */

proc sql;
alter table &lib_dmb..patient add 
	epi num(1), oh num(1), dln num(1), mig num(1), tb num(1), anx num(1), other_ind num(1)
	, dln2 num(1), tb2 num(1), anx2 num(1), nb_epi_18 smallint, unk num(1)
	, pop1 num(1), pop2 num(1)
;



update &lib_dmb..patient set 
	epi=max(ald_epi_18,hosp_epi_18)
	,nb_epi_18=sum(nb_med_epispe_18,nb_med_epinonspe_18)
	,oh=max(ald_oh,hosp_oh,med_oh)
	,dln=max(ald_dln,hosp_dln,med_dln)
	,mig=max(ald_mig,hosp_mig,med_mig)
	,tb=max(ald_tb,hosp_tb,med_tb)
	,anx=max(ald_anx,hosp_anx,med_anx)
	,dln2=case when med_clonsimult_18 then 1 else 0 end
	,anx2=case when med_clon_18=0 and med_pgbpsy_18=1 then 1 else 0 end
	,tb2=case when med_clon_18=0 and med_pgbpsy_18=0 and  med_tbpsy_18=1 then 1 else 0 end
;

update &lib_dmb..patient set other_ind=max(oh, dln, mig,tb,anx,dln2, anx2,tb2); 



update &lib_dmb..patient set 
	unk=case when med_clonmono=1 and other_ind=0 then 1 else 0 end
;

quit;



proc sql;
update &lib_dmb..patient set 
	pop1=case when id_fiable=1 and (epi=1 or (med_epi_18=1 and other_ind=0 and unk=0))  then 1 else 0 end;

update &lib_dmb..patient set 
	pop2=case when id_fiable=1 and (epi=1 or med_epispe_18=1) then 1 else 0 end;


quit;


proc tabulate data=&lib_dmb..patient;
	class ald_epi_18 hosp_epi_18 med_epi_18/ missing;
	table  ald_epi_18*hosp_epi_18 all,(med_epi_18 all)*(F=numfr8.*N='N');
	where pop1=1;
run;


proc tabulate data=&lib_dmb..patient;
	class ald_epi_18 hosp_epi_18 med_epispe_18/ missing;
	table  ald_epi_18*hosp_epi_18 all,(med_epispe_18 all)*(F=numfr8.*N='N');
	where pop2=1;
run;

proc tabulate data=&lib_dmb..patient;
	class pop1 pop2 / missing;
	table pop1 all,(pop2 all )*(F=numfr8.*N='N');
run;