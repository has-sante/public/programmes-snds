/****************************************************************************************************/
/*                         ETUDE PRATIQUE PRISE EN CHARGE EPILEPSIE                                 */
/****************************************************************************************************/



/****************************************************************************************************/
/* Nom du programme : IDENT_POP_MAJ

/* Objectif : creer les variables de verification des donnees identifiantes des patients
mois de naissance, sexe, date de deces, commune de residence 

/* tables de sortie : &lib_dmb..patient ; &lib_dmb..identite 

/****************************************************************************************************/



%_eg_conditional_dropds(&lib_dmb..patient);
%_eg_conditional_dropds(&lib_dmb..identite);
proc sql;
/* /!\ pop3 initiale devient pop2 lors de l'analyse   */
	create table &lib_dmb..patient as 
		(select distinct ben_idt_ano from &lib_sel..patient
		where pop1=1 or pop3=1); 

	create index ben_idt_ano on &lib_dmb..patient(ben_idt_ano);

/* identites des patients exposes */
	create table &lib_dmb..identite as 
		(select distinct i.* from &lib_sel..patient p
			inner join &lib_sel..identite i on i.ben_idt_ano=p.ben_idt_ano
		where pop1=1 or pop3=1);

	create index ben_idt_ano on &lib_dmb..identite(ben_idt_ano);
	create index idx_ident on &lib_dmb..identite(ben_nir_psa, ben_rng_gem);



quit;

/* check valeur aberrante 
proc sql;

select min(ben_nai_ann) as min_yob, max(ben_nai_ann) as max_yob
	, min(ben_nai_moi) as min_mob, max(ben_nai_moi) as max_mob
	, min(ben_sex_cod) as min_sex, max(ben_sex_cod) as max_sex
	, min(ben_dcd_dte) as min_dc, max(ben_dcd_dte) as max_dc
from &lib_ext..ir_ben_r 
;


select distinct ben_nai_ann
from &lib_ext..ir_ben_r 
;
select distinct ben_nai_moi
from &lib_ext..ir_ben_r 
;
select distinct ben_sex_cod
from &lib_ext..ir_ben_r 
;
quit;
*/

/* recherche donnees demographique patient */
proc sql;

alter table &lib_dmb..identite add  
		yob varchar(4), mob varchar(2), sex num(1) , commune varchar(5), nb_gem num(1), last_id_maj num(1)
		,dcd_ben_dte date format=ddmmyy10., dcd_prs_dte date format=ddmmyy10.,  dcd_mco_dte date format=ddmmyy10.
		,nb_prs integer, last_prs_dte date  format=ddmmyy10.
; 

*update &lib_dmb..identite i set yob=null, mob=null, sex=null,dcd_ben_dte =null;


/* annee de naissance */
/* ****************** */
update &lib_dmb..identite i set yob =
	(select distinct ben_nai_ann from &lib_ext..ir_ben_r r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and BEN_NAI_ANN not in ('0000', '1600' ) 
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	
	)
;
/* ir_ben_r archive si yob is null*/
update &lib_dmb..identite i set yob =
	(select distinct ben_nai_ann from &lib_ext..ir_ben_r_arc r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and BEN_NAI_ANN not in ('0000', '1600' )
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
	where i.yob is null
;
quit;


proc sql;

create table tmp as 
	select i.ben_idt_ano, i.ben_nir_psa, i.ben_rng_gem, max_trt_dtd
	from &lib_dmb..identite i
		inner join &lib_ext..ir_ben_r r on i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
		where max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )
	union
	select i.ben_idt_ano, i.ben_nir_psa, i.ben_rng_gem, max_trt_dtd
	from &lib_dmb..identite i
		inner join &lib_ext..ir_ben_r_arc r on i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
		where max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )

;

proc sql;
create table tmp2 as 
	select ben_idt_ano, ben_nir_psa, ben_rng_gem, max(datepart(max_trt_dtd)) as max_trt_dtd format=ddmmyy10.
	from tmp
	group ben_idt_ano, ben_nir_psa, ben_rng_gem
;
create index ben_idt_ano on tmp2(ben_idt_ano);
create index id_ben on tmp2(ben_nir_psa,ben_rng_gem);


alter table &lib_dmb..identite add last_id_maj num(1);
update &lib_dmb..identite i set last_id_maj = null;
update &lib_dmb..identite i set last_id_maj = 1
	where exists (select * from tmp2 t where i.ben_nir_psa=t.ben_nir_psa  and i.ben_rng_gem=t.ben_rng_gem 
				and max_trt_dtd=(select max(max_trt_dtd) from tmp2 t2 where t2.ben_idt_ano=t.ben_idt_ano)
				)
;
update &lib_dmb..identite i set last_id_maj = 0 where last_id_maj is null;

quit;


proc sql;

/* mois de naissance */
/* ****************** */
update &lib_dmb..identite i set mob =
	(select distinct ben_nai_moi from &lib_ext..ir_ben_r r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and BEN_NAI_MOI between '01' and '12'
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
;
/* ir_ben_r archive si mob is null*/
update &lib_dmb..identite i set mob =
	(select distinct ben_nai_moi from &lib_ext..ir_ben_r_arc r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and BEN_NAI_MOI between '01' and '12'
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
	where i.mob is null
;

quit;


proc sql;

/* code sexe */
/* ********* */
update &lib_dmb..identite i set sex =
	(select distinct ben_sex_cod from &lib_ext..ir_ben_r r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and ben_sex_cod not in (0)
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	
	)
;
/* ir_ben_r archive si mob is null*/
update &lib_dmb..identite i set sex =
	(select distinct ben_sex_cod from &lib_ext..ir_ben_r_arc r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and ben_sex_cod not in (0)
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
	where i.sex is null
;

quit;


proc sql;

/* commune de residence */
/* ******************** */

update &lib_dmb..identite i set commune =
	(select distinct cat(substr(ben_res_dpt,2,2),ben_res_com) from &lib_ext..ir_ben_r r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
;
/* ir_ben_r archive si mob is null*/
update &lib_dmb..identite i set commune = 
	(select distinct cat(substr(ben_res_dpt,2,2),ben_res_com) from &lib_ext..ir_ben_r_arc r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
	where i.commune is null
;

quit;



proc sql;

/* date de dc */
update &lib_dmb..identite i set dcd_ben_dte =
	(select datepart(ben_dcd_dte) from &lib_ext..ir_ben_r r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and datepart(r.ben_dcd_dte)<>'01JAN1600'd 
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	
	)
;
/* ir_ben_r archive si date_dc is null*/
update &lib_dmb..identite i set dcd_ben_dte =
	(select datepart(ben_dcd_dte) from &lib_ext..ir_ben_r_arc r 
		where i.ben_nir_psa=r.ben_nir_psa and i.ben_rng_gem=r.ben_rng_gem 
			and datepart(r.ben_dcd_dte)<>'01JAN1600'd 
			and i.dcd_ben_dte is null
			and max_trt_dtd=(select max(max_trt_dtd) from &lib_ext..ir_ben_r_arc r2 
						where i.ben_nir_psa=r2.ben_nir_psa and i.ben_rng_gem=r2.ben_rng_gem )	

	)
	where i.dcd_ben_dte is null
;

quit;

proc sql;
/* DC PRS */

create table tmp as 
( select i.ben_nir_psa,i.ben_rng_gem,i.ben_idt_ano,
		max(-1+intnx("days",input('15'||'/'||substr(ben_dcd_ame,5,2)||'/'||substr(ben_dcd_ame,1,4),ddmmyy10.), 1) ) as ben_dcd_dte
	from &lib_dmb..identite i, &lib_ext..ER_PRS_F f
	where i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem and ben_dcd_ame<>'000101'
	group by i.ben_nir_psa,i.ben_rng_gem,i.ben_idt_ano
);

create table prs as
 select ben_nir_psa,ben_rng_gem,ben_idt_ano,
		max(ben_dcd_dte) as ben_dcd_dte format=ddmmyy10.
	from tmp
	group by ben_nir_psa,ben_rng_gem,ben_idt_ano
;
create index idx_ident on prs(ben_nir_psa, ben_rng_gem);

update &lib_dmb..identite i set dcd_prs_dte =
	(select ben_dcd_dte
	from prs t 
	where i.ben_nir_psa=t.ben_nir_psa and i.ben_rng_gem=t.ben_rng_gem
	)
;


/* check : aucun date de dc prs sans date de dc ir_ben_r */
quit;


/* dc mco si date_dc */
proc sql;

update &lib_dmb..identite i set dcd_mco_dte =null;

create table tmp as 
	select distinct i.ben_nir_psa, sor_dat format=ddmmyy10.
	FROM &lib_ext..T_MCO_B b,
		&lib_dmb..identite i
	WHERE i.ben_nir_psa=b.nir_ano_17  
		and b.sor_mod='9' 
;
create index ben_nir_psa on tmp(ben_nir_psa);

update &lib_dmb..identite i set dcd_mco_dte =
	(select max(sor_dat) from tmp t where i.ben_nir_psa=t.ben_nir_psa);
quit;


/* derniere prestation et nb prestations sur periode d'etude */
proc sql;

create table tmp as 
( select i.ben_nir_psa,i.ben_rng_gem,i.ben_idt_ano
		,count(*) as nb_prs
		,max(datepart(exe_soi_dtd)) as last_prs_dte 
	from &lib_dmb..identite i, &lib_ext..ER_PRS_F f
	where i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem 
	group by i.ben_nir_psa,i.ben_rng_gem,i.ben_idt_ano
);

create table prs as
 select ben_nir_psa,ben_rng_gem,ben_idt_ano
		,sum(nb_prs) as nb_prs 		
		,max(last_prs_dte) as last_prs_dte  format=ddmmyy10.
	from tmp
	group by ben_nir_psa,ben_rng_gem,ben_idt_ano
;

create index idx_ident on prs(ben_nir_psa, ben_rng_gem);

update &lib_dmb..identite i set nb_prs =
	(select nb_prs
	from prs t 
	where i.ben_nir_psa=t.ben_nir_psa and i.ben_rng_gem=t.ben_rng_gem
	)
;

update &lib_dmb..identite i set last_prs_dte =
	(select last_prs_dte
	from prs t 
	where i.ben_nir_psa=t.ben_nir_psa and i.ben_rng_gem=t.ben_rng_gem
	)
;


quit;


proc sql;

alter table &lib_dmb..identite add  nb_gem num(1);
/* pls identifiants patient => jumeaux ? a exclure des etudes */
create table tmp as 
	select ben_nir_psa, count(distinct ben_idt_ano) as nb
	from &lib_dmb..identite
	group by ben_nir_psa
;
create index ben_nir_psa on tmp(ben_nir_psa);

update &lib_dmb..identite i set nb_gem =
	(select nb
	from tmp t 
	where i.ben_nir_psa=t.ben_nir_psa 
	)
;
quit;


/* ***************************** */
/*      TAble PATIENT            */
/* ***************************** */


proc sql;

/* indicateurs demographique */
create table tmp as 
	select ben_idt_ano,nb_id,count(distinct yob) as nb_yob
		,count(distinct mob) as nb_mob,count(distinct sex) as nb_sex
		,max(nb_gem) as nb_gem,sum(nb_prs) as nb_prs
	from &lib_dmb..identite i 
	group by ben_idt_ano,nb_id
;
create index ben_idt_ano on tmp(ben_idt_ano);

create table patient as 
	select p.*, nb_id,nb_yob,nb_mob,nb_sex, nb_gem,nb_prs
	from &lib_dmb..patient p
		left join tmp t on t.ben_idt_ano=p.ben_idt_ano
;
create index ben_idt_ano on patient (ben_idt_ano);
	
alter table patient add 		
		yob varchar(4), mob varchar(2), sex num(1),commune varchar(5)
		,dcd_dte date format=ddmmyy10., last_prs_dte date  format=ddmmyy10.,
		dcd_dte_pb integer, dcd smallint, nb_gem num(1)
; 

update patient p set yob =
	(select distinct i.yob from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and p.nb_yob=1 and i.yob is not null)
where nb_yob=1
;


update patient p set mob =
	(select distinct mob from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and p.nb_mob=1 and i.mob is not null)
where nb_mob=1
;

update patient p set sex =
	(select distinct sex from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and p.nb_sex=1 and i.sex is not null)
where nb_sex=1
;

/* choix commune patient avec prestation et "plus petite" si pls (nb : pas besoin d'etre precis pour cette etude) */
update patient p set commune =
	(select min(distinct commune) from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and nb_prs is not null)
;
/* commune des sans prestations */
update patient p set commune =
	(select min(distinct commune) from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and nb_prs is null)
where commune is null
;
quit;

proc sql;

drop table &lib_dmb..patient;
create table &lib_dmb..patient as 
	select * from patient
;
create index ben_idt_ano on &lib_dmb..patient (ben_idt_ano);
quit;

/*
proc sql;
select ben_idt_ano, count(distinct dcd_ben_dte) as nb_dcd_dte
from &lib_dmb..identite
group by ben_idt_ano
having nb_dcd_dte>1
;
quit;
*/
proc sql;

/* date de dc du patient */
update &lib_dmb..patient p set dcd_dte =
	(select max(dcd_ben_dte) from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and i.dcd_ben_dte is not null)
;

/* mco si non repertorie avant*/
update &lib_dmb..patient p set dcd_dte =
	(select distinct dcd_mco_dte from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano and i.dcd_mco_dte is not null and p.dcd_dte is null)
	where p.dcd_dte is null and exists (select * from &lib_dmb..identite i2 
		where p.ben_idt_ano=i2.ben_idt_ano and i2.dcd_mco_dte is not null and p.dcd_dte is null)
;


/* top dcd */
update &lib_dmb..patient p set dcd =null;
update &lib_dmb..patient p set dcd=1 where dcd_dte is not null;
update &lib_dmb..patient p set dcd=0 where dcd is null;


/* pb sur date de dc ? prestation apres date de deces ?*/
update &lib_dmb..patient p set last_prs_dte =
	(select max(last_prs_dte) from &lib_dmb..identite i 
		where p.ben_idt_ano=i.ben_idt_ano)
;

update &lib_dmb..patient set dcd_dte_pb=null;
update &lib_dmb..patient set dcd_dte_pb=1
	where last_prs_dte-dcd_dte>30;
update &lib_dmb..patient set dcd_dte_pb=0
	where dcd_dte_pb is null;

quit;



proc sql;

/* !!!! nous n'avons pas toutes les prestations => controle limit� */
/* recherche des patients qui ont des prestations au dela de 30j apres le dc suppos� */
/* si somme prs_act_nbr=0 => regulation => pas de pb */
create table tmp as 
( select p.ben_idt_ano,PRS_NAT_REF,sum(prs_act_nbr) as nb
	from &lib_dmb..identite i, &lib_ext..ER_PRS_F f,&lib_dmb..patient p
	where i.ben_idt_ano=p.ben_idt_ano and i.ben_nir_psa=f.ben_nir_psa and i.ben_rng_gem=f.ben_rng_gem
		and datepart(exe_soi_dtd)-dcd_dte >30 and dcd_dte_pb=1 and datepart(exe_soi_dtd)>dcd_dte
	group by  p.ben_idt_ano,PRS_NAT_REF
);


create table prs_dc as 
	select ben_idt_ano,PRS_NAT_REF,sum(nb) as nb
	from tmp
	where nb>0
	group by ben_idt_ano,PRS_NAT_REF
;

create index ben_idt_ano on prs_dc(ben_idt_ano);

update &lib_dmb..patient p set dcd_dte_pb=0
where dcd_dte_pb=1 and ben_idt_ano and not exists (select * from prs_dc d where d.ben_idt_ano=p.ben_idt_ano );
quit;


/*  fiabilite id */
proc sql;
alter table &lib_dmb..patient add id_fiable num(1);

update &lib_dmb..patient set id_fiable=null;
update &lib_dmb..patient set id_fiable=0
	where nb_yob<>1 or nb_sex<>1 or nb_gem<>1 or (dcd_dte<'01JAN2018'd and dcd_dte is not null) 
		or nb_id>=10 or nb_prs=.
;
update &lib_dmb..patient set id_fiable=1 where id_fiable is null;

quit;


proc sql;
/* date de fin de suivi (deces ou 31/12/2019)*/
alter table &lib_dmb..patient add fup_dte date format=ddmmyy10.;

update &lib_dmb..patient set fup_dte=min('31DEC2019'd,dcd_dte);


quit;


/* copie initiale table patient*/
proc sql;

create table &lib_dmb..patient_init as 
	select * from &lib_dmb..patient;

quit;