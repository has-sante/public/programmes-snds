/**************************************************************************************************************************************************************
	 						ETUDE SUR L'ETAT DES PRATIQUES DE RECOURS A L'IRM DU MEMBRE INFERIEUR 
	
	SELECTION DES ACTES D'IMAGERIE ET AMBULATOIRES (= HORS HOSPITALISATION), DES CONSULTATIONS / TELECONSULTATIONS ET DES HOSPITALISATIONS EN RAPPORT AVEC LE MEMBRE INFERIEUR 6 MOIS APRES L'IRM INDEX
	
	1 - EXTRACTION DCIR DES CONSULTATIONS ET DES ACTES IMAGERIE ET AMBULATOIRES DANS LES 6 MOIS SUIVANT L'IRM INDEX
	2 - EXTRACTION PMSI DES CONSULTATIONS, DES ACTES IMAGERIE ET AMBULATOIRES ET DES HOSPITALISATIONS DANS LES 6 MOIS SUIVANT L'IRM INDEX
	3 - CARACTERISATION DES TYPES DE CONSULTATIONS, D'ACTES D'IMAGERIE ET D'AMBULATOIRES ET DES HOSPITALISATIONS
		- TABLE sasdata1.IMI_suivi_post_IRM_S2_2021
	
**************************************************************************************************************************************************************/


/***************************************************************D�fintion des macrovariables**********************************************************************/
/*P�riode d'�tude:*/
%let per=S2;

/*Delai de suivi en jour � prendre en compte apr�s l'IRM*/
%let delai=180;

/*Actes � prendre en compte DCIR et PMSI*/
%let acte_imagerie='NFQK001','NFQK002','NFQK003','NFQK004','NZQH001','NZQK002','PBQM001','PBQM002','PBQM003','PBQM004','NZQN001','NZQJ001','NAQK015','NAQK007','NAQK023','NAQK071','NAQK049','NEQK010','NEQK035','NEQK012','NBQK001','NCQK001','NGQK001','NGQK002','NDQK001','NDQK002','NDQK003','NDQK004';
%let acte_ambu='NFMP001','NZHB002','NZHH004','NZHH001','NZJB001','NZLB001','NZLH002','NZLH001','NZMP007','NFMP002';
/*D�tail actes imagerie*/
%let radio_genou='NFQK001','NFQK002','NFQK003','NFQK004';
%let scanner='NZQH001','NZQK002';
%let echo='PBQM001','PBQM002','PBQM003','PBQM004';
%let IRM='NZQN001','NZQJ001';
%let radio_bassin='NAQK015','NAQK007','NAQK023','NAQK071','NAQK049';
%let radio_femur='NEQK010','NEQK035','NEQK012';
%let radio_cuisse='NBQK001';
%let radio_jambe='NCQK001';
%let radio_cheville='NGQK001','NGQK002';
%let radio_pied='NDQK001','NDQK002','NDQK003','NDQK004';
/*D�tail actes ambu (= hors hospitalisation)*/
%let contention='NFMP001';
%let ponction='NZHB002','NZHH004','NZHH001';
%let evacuation='NZJB001'; 
%let injection_sans='NZLB001';
%let injection_avec='NZLH002','NZLH001';
%let attelle='NFMP002';
%let app_rigide='NZMP007';


/*DCIR*/
%let date_debflux=202108;
%let date_finflux=202301;
%let anneem=2021;
%let consult=1098,1099,1101,1102,1103,1105,1109,1110,1111,1112,1113,1114,1115,1117,1118,1140,1168;
%let tele_consult=1096,1157,1164,1191,1192;

/*PMSI*/
%let anneep=21;
%let anneep1=22_09;/*Donn�es PMSI M9 2022 (ann�e enti�re pas disponible)*/
%let consultp='C','C   N','C   F','CS','CS  N','CS  F','G','G   F','G   N','GS','GS  N','GS  F','CA','CA  N','CA  F','CSC','CSC N','CSC F','CDE','CDE F','CDE N','CNP','CNP F','CNP N',
'APU','APU N','APU F','APY','APY N','APY F','APC','APC N ','APC F','CCX','CCX N','CCX F','CCP','CCP N','CCP F';
%let tele_consultp='TTE','TTE N','TTE F','TCP','TCP N','TCP F','TLC','TLC F','TLC N','TC','TC  N','TC  F','TCG','TCG F','TCG N';
%let fin_geo ='130780521', '130783236', '130783293', '130784234', '130804297','600100101','750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166','750100208', '750100216', 
'750100232', '750100273', '750100299' , '750801441', '750803447', '750803454', '910100015','910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037','930100045', '940100027', 
'940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137','690784152', '690784178', '690787478', '830100558' ;
%let ghm_hospit='08C45','08C34','08C38','08C24','08C53','08C54';
/*D�tail ghm hospit*/
%let meniscectomie='08C45';
%let ligament='08C34';
%let arthroscopie='08C38';
%let prothese='08C24';
%let trauma='08C53';
%let non_trauma='08C54';

/********************************************************************************************************************************************************************/
/********************************************************************************************************************************************************************/

/*Calcul pour chaque couple possible ben_nir_psa||ben_rng_gem du d�lai maximum � prendre en compte apr�s l'IRM pour le suivi*/
proc sql;
create table id_patient_suivi as select distinct a.ben_nir_psa, a.ben_rng_gem,a.ben_idt_ano, b.exe_soi_dtd 
from sasdata1.IMI_id_patient_&per._&anneem  a
inner join sasdata1.IMI_patient_first_irm_&per._&anneem  b on (a.ben_idt_ano=b.ben_idt_ano);
quit;

data id_patient_suivi;
set id_patient_suivi;
format date_limite DATETIME20.;
date_limite=exe_soi_dtd+(&delai*24*3600);/*On regarde les consultations jusqu'� 6 mois apr�s (180 jours)*/
run;

proc sql;
  drop table orauser.id_patient_suivi;
  CREATE TABLE orauser.id_patient_suivi AS SELECT * from id_patient_suivi;
QUIT;

/********************************************************************************************************************************************************************/

/***************************************************************************************************************
			1 - EXTRACTION DCIR DES CONSULTATIONS ET DES ACTES IMAGERIE ET AMBULATOIRES DANS LES 6 MOIS SUIVANT L'IRM INDEX
*****************************************************************************************************************/

/**************************************************************************
Filtres DCIR
---------------------------------------------------------------------------
Exclusion de l'activit� des ES Publics et des hospitalisations (ES ex-OQN)
Exclusion des lignes de consultations avec quantit� <=0
Exclusion des "m�decins fictifs"
*************************************************************************/

%macro extract(date);

/*Extraction des consultations et teleconsultations de chaque patient dans les 6 mois pr�c�dent l'IRM et ajout du dpt PS (table da_pra_r)*/
proc sql;
%connectora;

CREATE TABLE consult_dcir_post_&date AS SELECT * from connection to oracle 
(select b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.PSE_SPE_COD, b.PRS_NAT_REF, b.EXE_SOI_DTD,a.EXE_SOi_DTD as date_irm, a.ben_idt_ano,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num,
sum(b.PRS_ACT_QTE) as PRS_ACT_QTE
      FROM ER_PRS_F b 
      inner join id_patient_suivi a on (a.BEN_NIR_PSA = b.BEN_NIR_PSA AND a.BEN_RNG_GEM = b.BEN_RNG_GEM )
      left join ER_ETE_F c on 		  
      	b.FLX_DIS_DTD = c.FLX_DIS_DTD and b.FLX_TRT_DTD = c.FLX_TRT_DTD and b.FLX_EMT_TYP = c.FLX_EMT_TYP and b.FLX_EMT_NUM = c.FLX_EMT_NUM and b.FLX_EMT_ORD = c.FLX_EMT_ORD and b.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = c.DCT_ORD_NUM and b.PRS_ORD_NUM = c.PRS_ORD_NUM and b.REM_TYP_AFF = c.REM_TYP_AFF
      WHERE b.flx_dis_dtd=to_date(&date.01,'YYYYMMDD')
      AND a.EXE_SOI_DTD < b.EXE_SOI_DTD 
      AND a.DATE_LIMITE >= b.EXE_SOI_DTD 
      AND b.CPL_MAJ_TOP < 2 
      AND b.DPN_QLF NOT IN (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      AND PSE_STJ_COD not in (99) /*exclusion des m�decins fictifs*/
      AND b.BSE_PRS_NAT IN (&consult, &tele_consult) /*codes consultations et t�l�consultations*/
GROUP BY b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.PSE_SPE_COD, b.PRS_NAT_REF, b.EXE_SOI_DTD,a.EXE_SOi_DTD, a.ben_idt_ano,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num);
disconnect from oracle;
quit;

PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM
FROM oravue.DA_PRA_R
WHERE DTE_ANN_TRT = "&an1" AND dte_moi_fin = "&mois1" ;
QUIT;
  
proc sql;
create table consult_dcir_post_&date as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from consult_dcir_post_&date a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;

/*Extraction des actes de chaque patient dans les 6 mois pr�c�dent l'IRM et ajout du dpt PS (table da_pra_r)*/
proc sql;
%connectora;

create table acte_dcir_post_&date as SELECT * from connection to oracle 
(select b.ben_nir_psa, b.ben_rng_gem, b.PFS_EXE_NUM,b.pse_spe_cod,b.EXE_SOI_DTD,b.prs_nat_ref,d.cam_prs_ide as code_ccam,a.EXE_SOi_DTD as date_irm,a.ben_idt_ano,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num,
(SUM(CASE WHEN b.CPL_MAJ_TOP <2 THEN b.PRS_ACT_QTE ELSE 0 END)) as prs_act_qte
from er_prs_f b 
  inner join id_patient_suivi a on (a.BEN_NIR_PSA = b.BEN_NIR_PSA AND a.BEN_RNG_GEM = b.BEN_RNG_GEM )
  inner join er_cam_f d on
	d.FLX_DIS_DTD = b.FLX_DIS_DTD and d.FLX_TRT_DTD = b.FLX_TRT_DTD and d.FLX_EMT_TYP = b.FLX_EMT_TYP and d.FLX_EMT_NUM = b.FLX_EMT_NUM and d.FLX_EMT_ORD = b.FLX_EMT_ORD and d.ORG_CLE_NUM = b.ORG_CLE_NUM and
  	d.DCT_ORD_NUM = b.DCT_ORD_NUM and d.PRS_ORD_NUM = b.PRS_ORD_NUM and d.REM_TYP_AFF = b.REM_TYP_AFF
left join ER_ETE_F c on 		  
	b.FLX_DIS_DTD = c.FLX_DIS_DTD and b.FLX_TRT_DTD = c.FLX_TRT_DTD and b.FLX_EMT_TYP = c.FLX_EMT_TYP and b.FLX_EMT_NUM = c.FLX_EMT_NUM and b.FLX_EMT_ORD = c.FLX_EMT_ORD and b.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = c.DCT_ORD_NUM and b.PRS_ORD_NUM = c.PRS_ORD_NUM and b.REM_TYP_AFF = c.REM_TYP_AFF
WHERE b.flx_dis_dtd=to_date(&date.01,'YYYYMMDD')
      AND a.EXE_SOI_DTD < b.EXE_SOI_DTD 
      AND a.DATE_LIMITE >= b.EXE_SOI_DTD 
      AND b.PSE_STJ_COD not in (99) /*exclusion des m�decins fictifs*/
      and b.DPN_QLF not in (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      and d.cam_prs_ide in (&acte_imagerie,&acte_ambu) and d.CAM_ACT_COD = '1' /*s�lection actes, activt�=1 */
group by  b.ben_nir_psa, b.ben_rng_gem, b.PFS_EXE_NUM,b.pse_spe_cod,b.EXE_SOI_DTD,b.prs_nat_ref,d.cam_prs_ide,a.EXE_SOi_DTD,a.ben_idt_ano,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num);
disconnect from oracle;
quit;

PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM
FROM oravue.DA_PRA_R
WHERE DTE_ANN_TRT = "&an1" AND dte_moi_fin = "&mois1" ;
QUIT;
  
proc sql;
create table acte_dcir_post_&date as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from acte_dcir_post_&date a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;

%mend;

%macro lance;
%do i=&date_debflux %to &date_finflux;
	%let an1=%sysfunc(substr(&i,1,4));
	%let mois1=%eval(%sysfunc(substr(&i,5,2))-1);
	%if %length(&mois1)=1 %then %do;
		%let mois1=%sysfunc(cats(0,&mois1));
	%end;
    	%if &i=202113 %then %do;
        	%let i=202201;
    	%end;
        %if &i=202201 %then %do;
            %let an1=2021;
            %let mois1=12;
    	%end;
    	%if &i=202213 %then %do;
	    %let i=202301;
    	%end;
    	%if &i=202301 %then %do;
	     %let an1=2022;
	     %let mois1=12;
    	%end;
%extract(&i);
%end;
%mend;

%lance;

data consult_dcir;
set consult_dcir_post:;
run;

data acte_dcir;
set acte_dcir_post:;
run;

/**********************************Nettoyage table Consultations***************************************************************/

/* Exclusion de l'activit� des �tablissements publics ex-DG (ACE) et des consultations lors d'hospitalisations (ES ex-OQN) */
proc sql;
create table consult_dcir as select * 
from consult_dcir
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data consult_dcir;
set consult_dcir;
if prs_ppu_sec=1 or mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
run;

proc sql;
create table consult_dcir as select * 
from consult_dcir
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;

/*Suppression des lignes de consultations si qte <= 0 */
proc sql;
create table consult_dcir as select distinct ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, PRS_NAT_REF,EXE_SOI_DTD,date_irm,  ETE_CAT_COD, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM,sum(PRS_ACT_QTE) as PRS_ACT_QTE 
from consult_dcir
group by ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, PRS_NAT_REF,EXE_SOI_DTD, date_irm, ETE_CAT_COD, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM
having (calculated prs_act_qte)>0;
quit;


/**********************************Nettoyage table Actes***************************************************************/

/* Exclusion de l'activit� des �tablissements publics ex-DG (ACE) et des actes lors d'hospitalisations (ES ex-OQN) */
proc sql;
create table acte_dcir as select * 
from acte_dcir
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data acte_dcir;
set acte_dcir;
if prs_ppu_sec=1 or mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
run;

proc sql;
create table acte_dcir as select * 
from acte_dcir
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;

/*Suppression des lignes d'actes si qte <= 0 */
proc sql;
create table acte_dcir as select distinct ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD,EXE_SOI_DTD,date_irm,  ETE_CAT_COD, ETB_EXE_FIN, code_ccam,PFS_COD_POS,PFS_EXC_COM,sum(PRS_ACT_QTE) as PRS_ACT_QTE 
from acte_dcir
group by ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, EXE_SOI_DTD, date_irm, ETE_CAT_COD, ETB_EXE_FIN, code_ccam,PFS_COD_POS,PFS_EXC_COM
having (calculated prs_act_qte)>0;
quit;

/***************************************************************************************************************
		2 - EXTRACTION PMSI DES CONSULTATIONS, DES ACTES IMAGERIE ET AMBULATOIRES ET DES HOSPITALISATIONS DANS LES 6 MOIS SUIVANT L'IRM INDEX
*****************************************************************************************************************/

/**************************************************************************
Filtres PMSI
---------------------------------------------------------------------------
Exclusion des finess geographiques APHP/APHM/HCL (donn�es en doublon sinon)
Exclusion des patients avec des codes retour incorrects (<>0) 
Donn�es 2021 et 2022
Pour l'hospitalisation: 
	- Exclusion des ghm en erreur (90)
	- Exclusion des prestations inter-�tablissement
*************************************************************************/

/*MCO ACE consult*/
proc sql; 
/*annnee n*/
create table consult_pmsi_ace_mco_&anneep as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD,sum(ACT_NBR) as prs_act_qte
from  oravue.t_mco&anneep.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.EXE_SPE, c.ACT_COD;
/*annnee n+1*/
create table consult_pmsi_ace_mco_&anneep1 as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD,sum(ACT_NBR) as prs_act_qte
from  oravue.t_mco&anneep1.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep1.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.EXE_SPE, c.ACT_COD;
quit; 

/*SSR ACE consult*/
proc sql; 
/*annee n*/
create table consult_pmsi_ace_ssr_&anneep as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD,sum(ACT_NBR) as prs_act_qte
from  oravue.t_ssr&anneep.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_ssr&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.EXE_SPE, c.ACT_COD;
/*annee n+1*/
create table consult_pmsi_ace_ssr_&anneep1 as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD,sum(ACT_NBR) as prs_act_qte
from  oravue.t_ssr&anneep1.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_ssr&anneep1.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.EXE_SPE, c.ACT_COD;
quit; 

data consult_pmsi_ace;
set consult_pmsi_ace_mco_&anneep consult_pmsi_ace_mco_&anneep1 consult_pmsi_ace_ssr_&anneep consult_pmsi_ace_ssr_&anneep1;
run;

proc sql; 
create table consult_pmsi_ace as select distinct ben_idt_ano,finess_exec,exe_soi_dtd, date_irm,pse_spe_cod, ACT_COD,sum(prs_act_qte) as prs_act_qte from  consult_pmsi_ace
group by ben_idt_ano,finess_exec,exe_soi_dtd, date_irm,pse_spe_cod, ACT_COD;
quit;

/*MCO ACE actes*/
proc sql; 
/*annee n*/
create table acte_pmsi_ace_&anneep as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,c.ccam_cod as code_ccam
from  oravue.t_mco&anneep.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep.fmstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and c.ccam_cod in (&acte_imagerie,&acte_ambu) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.ccam_cod;
/*annee n+1*/
create table acte_pmsi_ace_&anneep1 as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,c.ccam_cod as code_ccam
from  oravue.t_mco&anneep1.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep1.fmstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and c.ccam_cod in (&acte_imagerie,&acte_ambu) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.EXE_SOI_DTD < c.EXE_SOI_DTD AND b.DATE_LIMITE >= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo,c.exe_soi_dtd, b.exe_soi_dtd,b.ben_idt_ano,c.ccam_cod;
quit; 

data acte_pmsi_ace;
set acte_pmsi_ace_&anneep acte_pmsi_ace_&anneep1;
run;

proc sql; 
create table acte_pmsi_ace as select distinct ben_idt_ano,finess_exec,exe_soi_dtd, date_irm,code_ccam,count(*) as prs_act_qte from acte_pmsi_ace 
group by ben_idt_ano,finess_exec,exe_soi_dtd, date_irm,code_ccam;
quit;

/*MCO HOSPITALISATIONS ACTES (imagerie)*/
proc sql;
/*annee n*/
create table acte_pmsi_hosp_&anneep as select distinct
a.nir_ano_17 as ben_nir_psa, a.exe_soi_dtd as date_entree, a.exe_soi_dtf, e.eta_num_geo as finess_exec, c.NBR_EXE_ACT,c.ENT_DAT_DEL,c.CDC_ACT as code_ccam,b.sej_typ
from oravue.t_mco&anneep.c as a
	inner join oravue.t_mco&anneep.a as c on  (c.eta_num=a.eta_num and c.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep.um as e on  (e.eta_num=a.eta_num and e.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep.b as b on  (b.eta_num=a.eta_num and b.rsa_num=a.rsa_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and e.RUM_ORD_NUM=1 /*On s�lectionne le 1er RUM du s�jour pour r�cup�rer le finess g�ographique*/
and c.CDC_ACT in (&acte_imagerie) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.grg_ghm not like "90%" /*exclusion CM 90*/
and a.NIR_RET='0' and a.NAI_RET='0' and a.SEX_RET='0' and a.SEJ_RET='0' and a.FHO_RET='0' and a.PMS_RET='0' and a.DAT_RET='0' and a.COH_NAI_RET='0' and a.COH_SEX_RET='0';/* On ne garde que les patients avec des codes retours corrects */
/*annee n+1*/
create table acte_pmsi_hosp_&anneep1 as select distinct
a.nir_ano_17 as ben_nir_psa, a.exe_soi_dtd as date_entree, a.exe_soi_dtf, e.eta_num_geo as finess_exec, c.NBR_EXE_ACT,c.ENT_DAT_DEL,c.CDC_ACT as code_ccam,b.sej_typ
from oravue.t_mco&anneep1.c as a
	inner join oravue.t_mco&anneep1.a as c on  (c.eta_num=a.eta_num and c.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep1.um as e on  (e.eta_num=a.eta_num and e.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep1.b as b on  (b.eta_num=a.eta_num and b.rsa_num=a.rsa_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and e.RUM_ORD_NUM=1 /*On s�lectionne le 1er RUM du s�jour pour r�cup�rer le finess g�ographique*/
and c.CDC_ACT in (&acte_imagerie) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.grg_ghm not like "90%" /*exclusion CM 90*/
and a.NIR_RET='0' and a.NAI_RET='0' and a.SEX_RET='0' and a.SEJ_RET='0' and a.FHO_RET='0' and a.PMS_RET='0' and a.DAT_RET='0' and a.COH_NAI_RET='0' and a.COH_SEX_RET='0';/* On ne garde que les patients avec des codes retours corrects */

quit;

/*calcul date de l'acte*/
data acte_pmsi_hosp;
set acte_pmsi_hosp_&anneep acte_pmsi_hosp_&anneep1;
format exe_soi_dtd DATETIME20.;
date_acte=datepart(date_entree)+ent_dat_del;
exe_soi_dtd=dhms(date_acte,0,0,0);
if exe_soi_dtd>exe_soi_dtf then exe_soi_dtd=exe_soi_dtf;
if sej_typ='B' then delete;/*suppression des PIE*/
drop exe_soi_dtf date_entree date_acte ent_dat_del sej_typ;
run;
 
proc sql;
create table acte_pmsi_hosp as select distinct a.ben_nir_psa, a.exe_soi_dtd, a.finess_exec, a.NBR_EXE_ACT,a.code_ccam,b.exe_soi_dtd as date_irm,b.ben_idt_ano
from acte_pmsi_hosp a
 	inner join id_patient_suivi as b on (a.ben_nir_psa=b.ben_nir_psa)
where b.exe_soi_dtd < a.EXE_SOI_DTD AND b.DATE_LIMITE >= a.EXE_SOI_DTD;

create table acte_pmsi_hosp as select distinct ben_idt_ano,exe_soi_dtd, finess_exec, code_ccam,date_irm,sum(nbr_exe_act) as prs_act_qte
from acte_pmsi_hosp 
group by ben_idt_ano,exe_soi_dtd, finess_exec, code_ccam,date_irm;
quit;

/*MCO HOSPITALISATIONS GHM*/
proc sql;
/*annee n*/
create table ghm_pmsi_hosp_&anneep as select distinct
a.nir_ano_17 as ben_nir_psa, a.exe_soi_dtd,a.exe_soi_dtf, e.eta_num_geo as finess_exec, c.grg_ghm,c.sej_typ
from oravue.t_mco&anneep.c as a
	inner join oravue.t_mco&anneep.b as c on  (c.eta_num=a.eta_num and c.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep.um as e on  (e.eta_num=a.eta_num and e.rsa_num=a.rsa_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and e.RUM_ORD_NUM=1 /*On s�lectionne le 1er RUM du s�jour pour r�cup�rer le finess g�ographique*/
and substr(c.grg_ghm,1,5) in (&ghm_hospit) /*S�lection hospit*/
and c.grg_ghm not like "90%" /*exclusion CM 90*/
and a.NIR_RET='0' and a.NAI_RET='0' and a.SEX_RET='0' and a.SEJ_RET='0' and a.FHO_RET='0' and a.PMS_RET='0' and a.DAT_RET='0' and a.COH_NAI_RET='0' and a.COH_SEX_RET='0';/* On ne garde que les patients avec des codes retours corrects */
/*annee n+1*/
create table ghm_pmsi_hosp_&anneep1 as select distinct
a.nir_ano_17 as ben_nir_psa, a.exe_soi_dtd,a.exe_soi_dtf, e.eta_num_geo as finess_exec, c.grg_ghm,c.sej_typ
from oravue.t_mco&anneep1.c as a
	inner join oravue.t_mco&anneep1.b as c on  (c.eta_num=a.eta_num and c.rsa_num=a.rsa_num)
	inner join oravue.t_mco&anneep1.um as e on  (e.eta_num=a.eta_num and e.rsa_num=a.rsa_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and e.RUM_ORD_NUM=1 /*On s�lectionne le 1er RUM du s�jour pour r�cup�rer le finess g�ographique*/
and substr(c.grg_ghm,1,5) in (&ghm_hospit) /*S�lection hospit*/
and c.grg_ghm not like "90%" /*exclusion CM 90*/
and a.NIR_RET='0' and a.NAI_RET='0' and a.SEX_RET='0' and a.SEJ_RET='0' and a.FHO_RET='0' and a.PMS_RET='0' and a.DAT_RET='0' and a.COH_NAI_RET='0' and a.COH_SEX_RET='0';/* On ne garde que les patients avec des codes retours corrects */
quit;

data ghm_pmsi_hosp;
set ghm_pmsi_hosp_&anneep ghm_pmsi_hosp_&anneep1;
run;
 
proc sql;
create table ghm_pmsi_hosp as select distinct a.ben_nir_psa, a.exe_soi_dtd, a.finess_exec, a.grg_ghm,b.exe_soi_dtd as date_irm,b.ben_idt_ano
from ghm_pmsi_hosp a inner join id_patient_suivi as b on (a.ben_nir_psa=b.ben_nir_psa)
where b.exe_soi_dtd < a.EXE_SOI_DTD and a.exe_soi_dtf>b.exe_soi_dtd AND b.DATE_LIMITE >= a.EXE_SOI_DTD and a.sej_typ ne 'B';

create table ghm_pmsi_hosp as select distinct ben_idt_ano,exe_soi_dtd, finess_exec, grg_ghm, date_irm,count(*) as prs_act_qte
from ghm_pmsi_hosp
group by ben_idt_ano,exe_soi_dtd, finess_exec, grg_ghm, date_irm;
quit;

/***************************************************************************************************************
			3 - CARACTERISATION DES TYPES DE CONSULTATIONS, D'ACTES D'IMAGERIE ET D'AMBULATOIRES ET DES HOSPITALISATIONS
*****************************************************************************************************************/

/*Concat�nation des tables DCIR et ACE PMSI:
- Harmonisation DCIR / PMSI sur codage lettres cl� et finess => on met les finess sur 9 caract�res au lieu de 8; si finess pas retrouv� dans la table datasante, on laisse le finess sur 8 caract�res
- Correction si sp�cialit� (pse_spe_cod) manquante et si prs_nat_ref correspond � une consultation / t�l�consultation de MG => On met la sp�cialit� � 1 (=MG)
- Cr�ation variables type et detail de la prestation
- Cr�ation variable code prestation (Acte CCAM, code consult ou GHM)
- Cr�ation de la variable dpt ex�cutant: : Dpt �tablissement si finess ex�cutant rempli (et pas en erreur) / Dpt du PS sinon*/

proc sql;
create table consult_dcir as select a.*, b.finess as finessgeo_exec
from consult_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);

create table acte_dcir as select a.*, b.finess as finessgeo_exec
from acte_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);
quit;

data IMI_suivi_post_IRM_&per._&anneem;
set consult_dcir (in=a) acte_dcir (in=b) consult_pmsi_ace (in=c) acte_pmsi_ace (in=d) ghm_pmsi_hosp (in=e) acte_pmsi_hosp (in=f);

if (e or f) then type='PMSI_HOSP';
if (c or d) then type='PMSI_ACE';
if (a or b) then type='DCIR';

if type='DCIR' then do;
finess_exec=finessgeo_exec;
if etb_exe_fin ne '' and finessgeo_exec='' then finess_exec=etb_exe_fin;
end;

if type='PMSI_ACE' then do;
if act_cod in ('C','C   N','C   F') then prs_nat_ref=1111;
if act_cod in ('CS','CS  N','CS  F') then prs_nat_ref=1112;
if act_cod in ('G','G   F','G   N') then prs_nat_ref=1110;
if act_cod in ('GS','GS  N','GS  F') then prs_nat_ref=1109;
if act_cod in ('CA','CA  N','CA  F') then prs_nat_ref=1115;
if act_cod in ('CSC','CSC N','CSC F') then prs_nat_ref=1114;
if act_cod in ('CDE','CDE F','CDE N') then prs_nat_ref=1140;
if act_cod in ('CNP','CNP F','CNP N') then prs_nat_ref=1113;
if act_cod in ('APU','APU N','APU F') then prs_nat_ref=1101;
if act_cod in ('APY','APY N','APY F') then prs_nat_ref=1102;
if act_cod in ('APC','APC N ','APC F') then prs_nat_ref=1103;
if act_cod in ('CCX','CCX N','CCX F') then prs_nat_ref=1105;
if act_cod in ('TTE','TTE N','TTE F') then prs_nat_ref=1096;
if act_cod in ('TCP','TCP N','TCP F') then prs_nat_ref=1157;
if act_cod in ('TLC','TLC F','TLC N') then prs_nat_ref=1164;
if act_cod in ('CCP','CCP F','CCP N') then prs_nat_ref=1168;
if act_cod in ('TC','TC  N','TC  F') then prs_nat_ref=1191;
if act_cod in ('TCG','TCG F','TCG N') then prs_nat_ref=1192;
end;

/*Correction pse_spe_cod si mqt et prs_nat_ref = consult de MG*/
if pse_spe_cod in (.,99,0) and prs_nat_ref in (1111,1110,1192) then pse_spe_cod=1;

/*Cr�ation variable type prestation*/
format type_prestation $20.;
if prs_nat_ref in (&tele_consult, &consult) then type_prestation='consult';
if code_ccam in (&acte_imagerie) then type_prestation='imagerie';
if code_ccam in (&acte_ambu) then type_prestation='ambu';
if substr(grg_ghm,1,5) in (&ghm_hospit) then type_prestation='hospit';

/*Cr�ation variable d�tail prestation*/
format presta_detail $20.;
if prs_nat_ref in (&tele_consult, &consult) then do;
if pse_spe_cod in (1,22,23) then presta_detail='C_MG';
else if pse_spe_cod in (14) then presta_detail='C_rhumato';
else if pse_spe_cod in (41) then presta_detail='C_ortho';
else if pse_spe_cod in (31) then presta_detail='C_MPR';
else presta_detail='C_autre';
end;
if code_ccam in (&radio_genou) then presta_detail='radio_genou';
if code_ccam in (&scanner) then presta_detail='scanner';
if code_ccam in (&echo) then presta_detail='echo';
if code_ccam in (&IRM) then presta_detail='IRM';
if code_ccam in (&contention) then presta_detail='contention';
if code_ccam in (&attelle) then presta_detail='attelle';
if code_ccam in (&app_rigide) then presta_detail='app_rigide';
if code_ccam in (&ponction) then presta_detail='ponction';
if code_ccam in (&evacuation) then presta_detail='evacuation';
if code_ccam in (&injection_sans) then presta_detail='injection_sans';
if code_ccam in (&injection_avec) then presta_detail='injection_avec';
if substr(grg_ghm,1,5) in (&meniscectomie) then presta_detail='meniscectomie';
if substr(grg_ghm,1,5) in (&ligament) then presta_detail='ligament';
if substr(grg_ghm,1,5) in (&arthroscopie) then presta_detail='arthroscopie';
if substr(grg_ghm,1,5) in (&prothese) then presta_detail='prothese';
if substr(grg_ghm,1,5) in (&trauma) then presta_detail='trauma';
if substr(grg_ghm,1,5) in (&non_trauma) then presta_detail='non_trauma';
if code_ccam in (&radio_bassin) then presta_detail='radio_bassin';
if code_ccam in (&radio_femur) then presta_detail='radio_femur';
if code_ccam in (&radio_cuisse) then presta_detail='radio_cuisse';
if code_ccam in (&radio_jambe) then presta_detail='radio_jambe';
if code_ccam in (&radio_cheville) then presta_detail='radio_cheville';
if code_ccam in (&radio_pied) then presta_detail='radio_pied';

/*Cr�ation variable code prestation*/
format code $13.;
if (b or d or f) then code=code_ccam;
if (a or c) then code=compress(put(prs_nat_ref,8.));
if e then code=grg_ghm;

/*Cr�ation variable dpt ex�cutant*/
format dpt_exec $3.;
if finess_exec ne '' then do;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);
else dpt_exec=substr(finess_exec,1,2);
end;
if substr(finess_exec,1,4) ='2001' then dpt_exec='2A';
if substr(finess_exec,1,4) ='2002' then dpt_exec='2B';
if finess_exec=' ' or substr(finess_exec,1,2)='99' then do;
if substr(pfs_cod_pos,1,3)  in ('201','200') then dpt_exec='2A'; 
else if substr(pfs_cod_pos,1,3) in ('202','206') then dpt_exec='2B'; 
else if substr(pfs_cod_pos,1,3) = '980' then dpt_exec=substr(pfs_cod_pos,1,3);
else if substr(pfs_cod_pos,1,2) in ('97','98') then dpt_exec=SUBSTR(PFS_COD_POS,1,2) || substr(PFS_EXC_COM,1,1);
else dpt_exec=substr(pfs_cod_pos,1,2);
end;
if dpt_exec in ('981','') then dpt_exec='99';

drop prs_nat_ref act_cod code_ccam grg_ghm finessgeo_exec etb_exe_fin pse_spe_cod pfs_exe_num PFS_COD_POS PFS_EXC_COM;
run;


proc sql;
create table sasdata1.IMI_suivi_post_IRM_&per._&anneem as select distinct BEN_IDT_ANO,TYPE,TYPE_PRESTATION,PRESTA_DETAIL,CODE,EXE_SOI_DTD,date_irm,DPT_EXEC,FINESS_EXEC,ETE_CAT_COD,
sum(PRS_ACT_QTE) as prs_act_qte from IMI_suivi_post_IRM_&per._&anneem
group by BEN_IDT_ANO,TYPE,TYPE_PRESTATION,PRESTA_DETAIL,CODE,EXE_SOI_DTD,date_irm,DPT_EXEC,FINESS_EXEC,ETE_CAT_COD;
quit;

data sasdata1.IMI_suivi_post_IRM_&per._&anneem;
set sasdata1.IMI_suivi_post_IRM_&per._&anneem;
/*Cr�ation variable d�lai entre l'IRM et la consult ou l'acte*/
delai_post_irm=datepart(exe_soi_dtd)-datepart(date_irm);
drop date_irm;
run;


/*V�rification*/
proc means data=sasdata1.IMI_suivi_post_IRM_&per._&anneem n min q1 median q3 max mean std;
var delai_post_irm;
run;

proc freq data=sasdata1.IMI_suivi_post_IRM_&per._&anneem;
table type type_prestation presta_detail code ete_cat_cod prs_act_qte delai_post_irm finess_exec dpt_exec exe_soi_dtd;
run;

/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.id_patient_suivi;
quit;





