/**************************************************************************************************************************************************************
	 						ETUDE SUR L'ETAT DES PRATIQUES DE RECOURS A L'IRM DU MEMBRE INFERIEUR 
	
	DETERMINATION DU MEDECIN DEMANDEUR PROBABLE DE L'IRM A PARTIR DES CONSULTATIONS ANTERIEURES (JUSQU'A 3 MOIS AVANT)
	
	1 - EXTRACTION DES CONSULTATIONS ET TELECONSULTATIONS DCIR DANS LES 3 MOIS PRECEDENT L'IRM INDEX
	2 - EXTRACTION DES CONSULTATIONS ET TELECONSULTATIONS PMSI ACE DANS LES 3 MOIS PRECEDENT L'IRM INDEX
	3 - CARACTERISATION DES TYPES DE CONSULTATIONS ANTERIEURES A L'IRM et DETERMINATION DU MEDECIN DEMANDEUR
		- TABLE sasdata1.IMI_demandeur_irm_s2_2021 
	
**************************************************************************************************************************************************************/

/***************************************************************D�fintion des macrovariables**********************************************************************/
/*P�riode d'�tude:*/
%let per=S2;

/*Delai de suivi en jour � prendre en compte avant l'IRM*/
%let delai=90;

/*DCIR*/
%let date_debflux=202105;
%let date_finflux=202207;
%let anneem=2021;
%let consult=1098,1099,1101,1102,1103,1105,1109,1110,1111,1112,1113,1114,1115,1117,1118,1140,1168;
%let tele_consult=1096,1157,1164,1191,1192;

/*PMSI*/
%let anneep=21;
%let consultp='C','C   N','C   F','CS','CS  N','CS  F','G','G   F','G   N','GS','GS  N','GS  F','CA','CA  N','CA  F','CSC','CSC N','CSC F','CDE','CDE F','CDE N','CNP','CNP F','CNP N',
'APU','APU N','APU F','APY','APY N','APY F','APC','APC N ','APC F','CCX','CCX N','CCX F','CCP','CCP N','CCP F';
%let tele_consultp='TTE','TTE N','TTE F','TCP','TCP N','TCP F','TLC','TLC F','TLC N','TC','TC  N','TC  F','TCG','TCG F','TCG N';
%let fin_geo ='130780521', '130783236', '130783293', '130784234', '130804297','600100101','750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166','750100208', '750100216', 
'750100232', '750100273', '750100299' , '750801441', '750803447', '750803454', '910100015','910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037','930100045', '940100027', 
'940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137','690784152', '690784178', '690787478', '830100558' ;

/********************************************************************************************************************************************************************/

/*Calcul pour chaque couple possible ben_nir_psa||ben_rng_gem du d�lai maximum � prendre en compte avant l'IRM pour d�terminer le m�decin demandeur*/
proc sql;
create table id_patient_demandeur as select distinct a.ben_nir_psa, a.ben_rng_gem,a.ben_idt_ano, b.exe_soi_dtd 
from sasdata1.IMI_id_patient_&per._&anneem  a
inner join sasdata1.IMI_patient_first_irm_&per._&anneem  b on (a.ben_idt_ano=b.ben_idt_ano);
quit;

data id_patient_demandeur;
set id_patient_demandeur;
format date_limite DATETIME20.;
date_limite=exe_soi_dtd-(&delai*24*3600);/*On regarde les consultations jusqu'� 3 mois avant (90 jours)*/
run;

proc sql;
  drop table orauser.id_patient_demandeur;
  CREATE TABLE orauser.id_patient_demandeur AS SELECT * from id_patient_demandeur;
QUIT;

/***************************************************************************************************************
			1 - EXTRACTION DES CONSULTATIONS ET TELECONSULTATIONS DCIR DANS LES 3 MOIS PRECEDENT L'IRM INDEX
*****************************************************************************************************************/

/**************************************************************************
Filtres DCIR
---------------------------------------------------------------------------
Exclusion de l'activit� des ES Publics et des hospitalisations (ES ex-OQN)
Exclusion des lignes de consultations avec quantit� <=0
Exclusion des "m�decins fictifs"
*************************************************************************/

/*Extraction des consultations et teleconsultations de chaque patient dans les 3 mois pr�c�dent l'IRM et ajout du dpt du PS (table DA_PRA_R)*/

%macro extract(date);

proc sql;
%connectora;

CREATE TABLE consult_dcir_dem_&date AS SELECT * from connection to oracle 
(select distinct b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.PSE_SPE_COD, b.PRS_NAT_REF, b.EXE_SOI_DTD,a.EXE_SOi_DTD as date_irm, a.ben_idt_ano,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num,
sum(b.PRS_ACT_QTE) as PRS_ACt_QTE
      FROM ER_PRS_F b 
      inner join id_patient_demandeur a on (a.BEN_NIR_PSA = b.BEN_NIR_PSA AND a.BEN_RNG_GEM = b.BEN_RNG_GEM )
      left join ER_ETE_F c on 		  
      	b.FLX_DIS_DTD = c.FLX_DIS_DTD and b.FLX_TRT_DTD = c.FLX_TRT_DTD and b.FLX_EMT_TYP = c.FLX_EMT_TYP and b.FLX_EMT_NUM = c.FLX_EMT_NUM and b.FLX_EMT_ORD = c.FLX_EMT_ORD and b.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = c.DCT_ORD_NUM and b.PRS_ORD_NUM = c.PRS_ORD_NUM and b.REM_TYP_AFF = c.REM_TYP_AFF
      WHERE b.flx_dis_dtd=to_date(&date.01,'YYYYMMDD')
      AND a.EXE_SOI_DTD > b.EXE_SOI_DTD 
      AND a.DATE_LIMITE <= b.EXE_SOI_DTD 
      AND b.CPL_MAJ_TOP < 2 
      AND b.DPN_QLF NOT IN (71)
      AND PSE_STJ_COD not in (99) /*exclusion des m�decins fictifs*/
      AND b.BSE_PRS_NAT IN (&consult, &tele_consult) /*codes consultations et t�l�consultations*/
GROUP BY b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.PSE_SPE_COD, b.PRS_NAT_REF, b.EXE_SOI_DTD,a.EXE_SOi_DTD, a.ben_idt_ano,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num);
disconnect from oracle;
quit;

PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM
FROM oravue.DA_PRA_R
WHERE DTE_ANN_TRT ="&an1" AND dte_moi_fin = "&mois1" ;
QUIT;
  
proc sql;
create table consult_dcir_dem_&date as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from consult_dcir_dem_&date a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;
%mend;

%macro lance;
%do i=&date_debflux %to &date_finflux;
	%let an1=%sysfunc(substr(&i,1,4));
	%let mois1=%eval(%sysfunc(substr(&i,5,2))-1);
	%if %length(&mois1)=1 %then %do;
		%let mois1=%sysfunc(cats(0,&mois1));
	%end;
    	%if &i=202113 %then %do;
        	%let i=202201;
    	%end;
        %if &i=202201 %then %do;
            %let an1=2021;
            %let mois1=12;
    	%end;
%extract(&i);
%end;
%mend;
%lance;

data consult_dcir;
set consult_dcir_dem:;
run;

/* Exclusion de l'activit� des �tablissements publics ex-DG (ACE) et des consultations lors d'hospitalisations (ES ex-OQN) */
proc sql;
create table consult_dcir as select * 
from consult_dcir
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data consult_dcir;
set consult_dcir;
if prs_ppu_sec=1 or mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
run;

proc sql;
create table consult_dcir as select * 
from consult_dcir
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;

/*Suppression des lignes de consultations si qte <= 0 */
proc sql;
create table consult_dcir as select distinct 
ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, PRS_NAT_REF,EXE_SOI_DTD,date_irm, ETE_CAT_COD, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM, sum(PRS_ACT_QTE) as PRS_ACT_QTE 
from consult_dcir
group by ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, PRS_NAT_REF,EXE_SOI_DTD, date_irm, ETE_CAT_COD, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM
having (calculated prs_act_qte)>0;
quit;


/***************************************************************************************************************
			2 - EXTRACTION DES CONSULTATIONS ET TELECONSULTATIONS PMSI ACE DANS LES 3 MOIS PRECEDENT L'IRM INDEX
*****************************************************************************************************************/

/**************************************************************************
Filtres PMSI
---------------------------------------------------------------------------
Exclusion des finess geographiques APHP/APHM/HCL (donn�es en doublon sinon)
Exclusion des patients avec des codes retour incorrects (<>0)
*************************************************************************/

/*MCO*/
proc sql; 
create table consult_pmsi_ace_mco as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD
from oravue.t_mco&anneep.cstc as a 
		inner join orauser.id_patient_demandeur as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD;
quit; 

/*SSR*/
proc sql; 
create table consult_pmsi_ace_ssr as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.exe_soi_dtd as date_irm,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD
from oravue.t_ssr&anneep.cstc as a 
		inner join orauser.id_patient_demandeur as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_ssr&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp) /*actes de consultations et t�l�consultations*/
and b.EXE_SOI_DTD > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD;
quit; 

data consult_pmsi_ace;
set consult_pmsi_ace_mco consult_pmsi_ace_ssr;
run;

proc sql; 
create table consult_pmsi_ace as select distinct ben_idt_ano,finess_exec,exe_soi_dtd, date_irm,pse_spe_cod, ACT_COD
from  consult_pmsi_ace;
quit; 


/***************************************************************************************************************
			3 - CARACTERISATION DES TYPES DE CONSULTATIONS ANTERIEURES A L'IRM ET DETERMINATION DU MEDECIN DEMANDEUR
*****************************************************************************************************************/

/*Concat�nation des tables DCIR et ACE PMSI:
- Harmonisation DCIR / PMSI sur codage lettres cl� et finess => on met les finess DCIR sur 9 caract�res au lieu de 8 => si finess pas retrouv� dans la table datasante, on laisse le finess sur 8 caract�res
- Correction si sp�cialit� (pse_spe_cod) manquante et si prs_nat_ref correspond � une consultation / t�l�consultation de MG => On met la sp�cialit� � 1 (=MG)
- S�paration des consultations/t�l�consultations selon la sp�cialit� de l'ex�cutant: MG, rhumato, ortho, MPR, autre
- Calcul du d�lai entre IRM et consultation
- Cr�ation de la variable dpt ex�cutant: : Dpt �tablissement si finess ex�cutant rempli (et pas en erreur) / Dpt du PS sinon*/

proc sql;
create table consult_dcir as select a.*, b.finess as finessgeo_exec
from consult_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);
quit;

data consult_avt_irm;
set consult_dcir (in=a drop=prs_act_qte) consult_pmsi_ace (in=b);

format type $8.;

if a then type='DCIR';
if b then type='PMSI_ACE';

if type='DCIR' then do;
finess_exec=finessgeo_exec;
if etb_exe_fin ne '' and finessgeo_exec='' then finess_exec=etb_exe_fin;
end;

if type='PMSI_ACE' then do;
if act_cod in ('C','C   N','C   F') then prs_nat_ref=1111;
if act_cod in ('CS','CS  N','CS  F') then prs_nat_ref=1112;
if act_cod in ('G','G   F','G   N') then prs_nat_ref=1110;
if act_cod in ('GS','GS  N','GS  F') then prs_nat_ref=1109;
if act_cod in ('CA','CA  N','CA  F') then prs_nat_ref=1115;
if act_cod in ('CSC','CSC N','CSC F') then prs_nat_ref=1114;
if act_cod in ('CDE','CDE F','CDE N') then prs_nat_ref=1140;
if act_cod in ('CNP','CNP F','CNP N') then prs_nat_ref=1113;
if act_cod in ('APU','APU N','APU F') then prs_nat_ref=1101;
if act_cod in ('APY','APY N','APY F') then prs_nat_ref=1102;
if act_cod in ('APC','APC N ','APC F') then prs_nat_ref=1103;
if act_cod in ('CCX','CCX N','CCX F') then prs_nat_ref=1105;
if act_cod in ('TTE','TTE N','TTE F') then prs_nat_ref=1096;
if act_cod in ('TCP','TCP N','TCP F') then prs_nat_ref=1157;
if act_cod in ('TLC','TLC F','TLC N') then prs_nat_ref=1164;
if act_cod in ('CCP','CCP F','CCP N') then prs_nat_ref=1168;
if act_cod in ('TC','TC  N','TC  F') then prs_nat_ref=1191;
if act_cod in ('TCG','TCG F','TCG N') then prs_nat_ref=1192;
end;

/*Correction pse_spe_cod si mqt et prs_nat_ref = consult de MG*/
if pse_spe_cod in (.,99,0) and prs_nat_ref in (1111,1110,1192) then pse_spe_cod=1;

format consult $20.;
if prs_nat_ref in (&consult,&tele_consult) then do;
if pse_spe_cod in (1,22,23) then consult='C_MG';
else if pse_spe_cod in (14) then consult='C_rhumato';
else if pse_spe_cod in (41) then consult='C_ortho';
else if pse_spe_cod in (31) then consult='C_MPR';
else consult='C_autre';
end;

delai_consult=datepart(date_irm)-datepart(exe_soi_dtd);

format dpt_exec $3.;
if finess_exec ne '' then do;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);
else dpt_exec=substr(finess_exec,1,2);
end;
if substr(finess_exec,1,4) ='2001' then dpt_exec='2A';
if substr(finess_exec,1,4) ='2002' then dpt_exec='2B';
if finess_exec=' ' or substr(finess_exec,1,2)='99' then do;
if substr(pfs_cod_pos,1,3)  in ('201','200') then dpt_exec='2A'; 
else if substr(pfs_cod_pos,1,3) in ('202','206') then dpt_exec='2B'; 
else if substr(pfs_cod_pos,1,3) = '980' then dpt_exec=substr(pfs_cod_pos,1,3);
else if substr(pfs_cod_pos,1,2) in ('97','98') then dpt_exec=SUBSTR(PFS_COD_POS,1,2) || substr(PFS_EXC_COM,1,1);
else dpt_exec=substr(pfs_cod_pos,1,2);
end;
if dpt_exec in ('981','') then dpt_exec='99';

drop finessgeo_exec etb_exe_fin act_cod pfs_exe_num PFS_COD_POS PFS_EXC_COM;
run;


/*Pour chaque "type" de consultation (variable consult), on garde la consultation la plus proche de la date d'IRM pour chaque patient */
proc sort data=consult_avt_irm;by ben_idt_ano consult exe_soi_dtd;run;

data consult_avt_irm;
set consult_avt_irm;
by ben_idt_ano consult exe_soi_dtd;
if last.consult;
run;

/*v�rification*/
proc freq data=consult_avt_irm;
table prs_nat_ref consult pse_spe_cod ete_cat_cod delai_consult type exe_soi_dtd finess_exec dpt_exec ;
run;

/*On cr�e une table avec une ligne par patient et le "type" de consultation en colonne*/
proc transpose data=consult_avt_irm out=demandeur_&per._&anneem (drop=_name_)prefix=delai_;by ben_idt_ano notsorted;var delai_consult;id consult;run;


/*Cr�ation de variables pour les analyses:
- M�decin demandeur:
	Si C/TLC ortho, rhumato ou MPR dans les 3 mois: sp�cialit� de la consultation ou t�l�consultation la plus proche de l'IRM
	Si aucune C/TLC de rhumato, ortho ou MPR dans les 3 mois: "MG" si consult ou t�l�consult de MG dans les 3 mois
	Sinon: "autre sp�cialit�" si C/TLC d'une autre sp�cialit� dans les 3 mois
- delai_irm
- Construction de variables en 0/1 pour pr�sence/absence de consultations ou t�l�consultation de MPR / ortho / rhumato / MG / autre 
- mg_only: consultation / t�l�consultation uniquement d'un MG
- CS: pr�sence d'une consultation / t�l�consultation en ortho, rhumato ou MPR
- mg_sans_CS: consultation / t�l�consultation MG sans consultation / t�l�consultation en ortho, rhumato ou MPR
*/

data demandeur_&per._&anneem;
set demandeur_&per._&anneem;

format demandeur $10.;
delai_min=min(delai_c_ortho,delai_c_mpr,delai_c_rhumato);
if delai_min ne . then do;
if delai_min=delai_c_ortho  then do;demandeur='ortho';delai_irm=delai_min;end;
if delai_min=delai_c_rhumato  then do;demandeur='rhumato';delai_irm=delai_min;end;
if delai_min=delai_c_mpr  then do;demandeur='mpr';delai_irm=delai_min;end;
end;

if demandeur='' and delai_c_mg ne .  then do;demandeur='mg';delai_irm=delai_c_mg;end;
if demandeur='' and delai_c_autre ne . then do;demandeur='autre';delai_irm=delai_c_autre;end;

mg_only=0;cs=0;mg_sans_cs=0;
if demandeur='mg' and delai_c_autre = . then mg_only=1;
if demandeur in ('ortho','rhumato','mpr') then cs=1;
if cs=0 and delai_c_mg ne .  then mg_sans_cs=1;

if delai_c_ortho ne .  then c_ortho=1;else c_ortho=0;
if delai_c_rhumato ne . then c_rhumato=1;else c_rhumato=0;
if delai_c_mpr ne .  then c_mpr=1;else c_mpr=0;
if delai_c_mg ne .  then c_mg=1;else c_mg=0;
if delai_c_autre ne . then c_autre=1;else c_autre=0;

drop delai_min delai_c_ortho  delai_c_rhumato  delai_c_mpr  delai_c_mg  delai_c_autre ;
run;


/*On ajoute le d�partement du m�decin demandeur et la provenance des donn�es (PMSI / DCIR)  dans la table*/
%macro ajout(var=);
proc transpose data=consult_avt_irm out=demandeur_&per._&anneem._&var (drop=_name_) prefix=delai_;by ben_idt_ano &var notsorted;var delai_consult;id consult;run;

data demandeur_&per._&anneem._&var;
set demandeur_&per._&anneem._&var;
format demandeur $10.;
delai_min=min(delai_c_ortho,delai_c_mpr,delai_c_rhumato);
if delai_min ne . then do;
if delai_min=delai_c_ortho then demandeur='ortho';
if delai_min=delai_c_rhumato then demandeur='rhumato';
if delai_min=delai_c_mpr  then demandeur='mpr';
end;
if demandeur='' and delai_c_mg ne . then demandeur='mg';
if demandeur='' and delai_c_autre ne . then demandeur='autre';
keep ben_idt_ano demandeur &var ;
run;

proc sort data= demandeur_&per._&anneem._&var;by ben_idt_ano demandeur;run;
proc sort data= demandeur_&per._&anneem;by ben_idt_ano demandeur;run;

data demandeur_&per._&anneem;
merge demandeur_&per._&anneem (in=a) demandeur_&per._&anneem._&var;
by ben_idt_ano demandeur;
if a;
run;

%mend;
%ajout (var=dpt_exec);
%ajout (var=type);


/*Ajout dans la table de l'ensemble des patients (ben_idt_ano) de la population d'�tude => pour certains patients, on ne retrouve aucune consultation dans les 3 mois pr�c�dents*/
proc sort data=sasdata1.IMI_patient_first_irm_&per._&anneem out=id (keep=ben_idt_ano); by ben_idt_ano;run;
proc sort data=demandeur_&per._&anneem (rename=dpt_exec=dpt_demandeur); by ben_idt_ano;run;

data sasdata1.IMI_demandeur_irm_&per._&anneem;
merge demandeur_&per._&anneem (in=a) id (in=b);
by ben_idt_ano;
sans_C=0;
if b and not a then sans_C=1;
if sans_C=1 then do;demandeur='';mg_only=.;cs=.;mg_sans_CS=.;c_ortho=.;c_mpr=.;c_rhumato=.;c_mg=.;c_autre=.;end;
run;

/*v�rification*/
proc freq data=sasdata1.IMI_demandeur_irm_&per._&anneem ;
table mg_only cs mg_sans_cs demandeur sans_C delai_irm c_ortho c_rhumato c_autre c_mg c_mpr dpt_demandeur type/ missing;
run;

proc means data=sasdata1.IMI_demandeur_irm_&per._&anneem n min p5 p10 p20 q1 p30 p40 median p60 p70 q3 p80 p90 p95 p99 max mean std;
var delai_irm;
run;

/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.id_patient_demandeur;
quit;

