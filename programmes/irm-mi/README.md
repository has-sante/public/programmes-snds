# Etat des pratiques de recours à l'IRM du membre inférieur

## Description
La Haute Autorité de Santé et le Conseil National Professionnel de Radiologie et d'Imagerie Médicale ont publié en juin 2022 des recommandations sur la pertinence de l'imagerie en cas de gonalgie non traumatique. 
Suite à cette publication, et pour appuyer les messages clés de ces recommandations, une étude à partir du SNDS a été réalisée pour établir un état des lieux des pratiques de recours à l’IRM du membre inférieur sur le 2nd semestre 2021.

Ce projet a été mené par la Mission Data, en collaboration avec le Service des Bonnes Pratiques. Il a démarré en octobre 2022 et a été finalisé durant le 1er semestre 2023. Les résultats ont été publiés en septembre 2023.

Ci-dessous les principaux résultats tirés de la fiche état des pratiques publiée sur le site de la HAS:

![Alt text](IRM.PNG)

## Liens utiles

### Vers les documents de la HAS
- [Présentation de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/has-103-version_presentation_etude.pdf)
- [Rapport de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/has-103_rapport_irm_membre_inferieur_cd_2023-05-17_vd.pdf)
- [Fiche état des pratiques](https://www.has-sante.fr/jcms/p_3463561/fr/fiche-etat-des-pratiques-irm-du-membre-inferieur)
- [Fiches Pertinence de l’imagerie en cas de gonalgie chez l’adulte](https://www.has-sante.fr/jcms/p_3278662/fr/pertinence-de-l-imagerie-en-cas-de-gonalgie-chez-l-adulte), dont lien vers l'ensemble des documents

### Vers les programmes
L'ensemble des programmes de traitement se trouve [ici](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi).
- [01_IMI_extraction_population.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/01_IMI_extraction_population.sas)
- [02_IMI_demandeur_irm.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/02_IMI_demandeur_irm.sas)
- [03_IMI_parcours_soins_pre_irm.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/03_IMI_parcours_soins_pre_irm.sas)
- [04_IMI_parcours_soins_post_irm.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/04_IMI_parcours_soins_post_irm.sas)
- [05_IMI_tableaux_restitution.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/05_IMI_tableaux_restitution.sas)

## Contexte technique

### Données et logiciels utilisés
Les données utilisées (DCIR et PMSI MCO et SSR) sont issues du SNDS. L’accès aux données du SNDS s’est fait via le portail de l’assurance maladie, au titre de l’accès permanent dont dispose la HAS. 

L'extraction de données et les analyses ont été réalisées avec le logiciel SAS.

## Contacts
Pour toute question sur la mise en place de cette étude et les programmes associés, n'hésitez pas à contacter la mission data: data@has-sante.fr

## Détails techniques

### Flux de données
Le flux de données ci-dessous accompagne les programmes mis à disposition sur le dépôt.

Cliquez sur les éléments du graphique pour accéder directement aux programmes ou à la description des tables.

> 📝 **Notes :**
> - Le nom des programmes débute par leur numéro d'ordre de lancement, suivi du préfixe "IMI_"
> - Le nom des tables débute par le préfixe "IMI_" et se termine par la période de l'étude (ici "S2_2021")

```mermaid

   flowchart TB

    Data0(SNDS PORTAIL CNAM) --> Prog1[01_IMI_EXTRACTION_POPULATION.SAS]

    subgraph "SELECTION POPULATION"
        Prog1[01_IMI_EXTRACTION_POPULATION.SAS] --> Tab1(IMI_PATIENT_FIRST_IRM_S2_2021.SAS7BDAT)
        Prog1[01_IMI_EXTRACTION_POPULATION.SAS] --> Tab2(IMI_ID_PATIENT_S2_2021.SAS7BDAT)
    end

    Tab1(IMI_PATIENT_FIRST_IRM_S2_2021.SAS7BDAT) --> Data1(POPULATION D'ETUDE)
    Tab2(IMI_ID_PATIENT_S2_2021.SAS7BDAT) --> Data1(Population d'étude)
    Data1(POPULATION D'ETUDE) --> Prog2[02_IMI_DEMANDEUR_IRM.SAS]
    Data1(POPULATION D'ETUDE) --> Prog3[03_IMI_SUIVI_PRE_IRM.SAS]
    Data1(POPULATION D'ETUDE) --> Prog4[04_IMI_SUIVI_POST_IRM.SAS]

    subgraph "TRAITEMENT DE DONNEES"
        Prog2[02_IMI_DEMANDEUR_IRM.SAS] --> Tab3(IMI_DEMANDEUR_IRM_S2_2021.SAS7BDAT)
        Prog3[03_IMI_PARCOURS_SOINS_PRE_IRM.SAS] --> Tab4(IMI_SUIVI_IRM_S2_2021.SAS7BDAT)
        Prog4[04_IMI_PARCOURS_SOINS_POST_IRM.SAS] --> Tab5(IMI_SUIVI_POST_IRM_S2_2021.SAS7BDAT)
    end

    Tab3(IMI_DEMANDEUR_IRM_S2_2021.SAS7BDAT) --> Data2(DONNEES A ANALYSER)
    Tab4(IMI_SUIVI_PRE_IRM_S2_2021.SAS7BDAT) --> Data2(DONNEES A ANALYSER)
    Tab5(IMI_SUIVI_POST_IRM_S2_2021.SAS7BDAT) --> Data2(DONNEES A ANALYSER)
    Data2(DONNEES A ANALYSER) --> Prog5[05_IMI_TABLEAUX_RESTITUTION.SAS]

    subgraph "ANALYSE STATISTIQUE"
        Prog5[05_IMI_TABLEAUX_RESTITUTION.SAS]
    end

     Prog5[05_IMI_TABLEAUX_RESTITUTION.SAS] --> Data3(RESULTATS)

    click Tab1 "./index.html#imi-patient-first-irm-periode"
    click Tab2 "./index.html#imi-id-patient-periode"
    click Tab3 "./index.html#imi-demandeur-irm-periode"
    click Tab4 "./index.html#imi-suivi-pre-irm-periode"
    click Tab5 "./index.html#imi-suivi-post-irm-periode"

    click Prog1 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/01_IMI_extraction_population.sas"
    click Prog2 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/02_IMI_demandeur_irm.sas"
    click Prog3 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/03_IMI_parcours_soins_pre_irm.sas"
    click Prog4 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/04_IMI_parcours_soins_post_irm.sas"
    click Prog5 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/irm-mi/05_IMI_tableaux_restitution.sas"

    style Tab1 fill:#ffe9a6,stroke:#f66
    style Tab2 fill:#ffe9a6,stroke:#f66
    style Tab3 fill:#ffe9a6,stroke:#f66
    style Tab4 fill:#ffe9a6,stroke:#f66
    style Tab5 fill:#ffe9a6,stroke:#f66
    style Data0 fill:#E5A099, stroke:#CC5D52
    style Data1 fill:#E5A099, stroke:#CC5D52
    style Data2 fill:#E5A099, stroke:#CC5D52
    style Data3 fill:#E5A099, stroke:#CC5D52
```
### Description des tables principales

#### IMI_PATIENT_FIRST_IRM_PERIODE
Table contenant les caractéristiques des patients inclus dans l'étude et des informations sur leur IRM index

| Variable    | Libellé                                                                                                                               |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| BEN_IDT_ANO | Identifiant du patient                                                                                                                |
| BEN_SEX_COD | Sexe du patient                                                                                                                       |
| AGE         | Age du patient                                                                                                                        |
| DPT_PATIENT | Département du patient                                                                                                                |
| TYPE        | Provenance des données: DCIR / PMSI_ACE                                                                                               |
| EXE_SOI_DTD | Date de l'IRM index                                                                                                                   |
| CODE_CCAM   | Code CCAM de l'IRM index (NZQN001 / NZQJ001)                                                                                          |
| DPT_EXEC    | Département de l'exécutant (Dpt de l'établissement ou du professionnel de santé si finess de l'établissement exécutant non renseigné) |
| FINESS_EXEC | Finess de l'établissement exécutant (peut être non renseigné pour le DCIR)                                                            |
| ETE_CAT_COD | Catégorie de l'établissement exécutant (Données DCIR uniquement; peut être non renseigné)                                             |

#### IMI_ID_PATIENT_PERIODE
Table contenant l'ensemble des couples possibles d'identifiants patients (BEN_NIR_PSA||BEN_RNG_GEM) pour les patients inclus dans l'étude

| Variable    | Libelle                                                                 |
| ----------- | ----------------------------------------------------------------------- |
| BEN_IDT_ANO | Identifiant unique du patient                                           |
| BEN_IDT_TOP | Top identifiant patient: 1 si ben_idt_ano égal au NIR patient / 0 sinon |
| BEN_NIR_PSA | Identifiant patient (n'est pas forcément unique pour un patient)        |
| BEN_RNG_GEM | Rang gémellaire du patient                                              |

#### IMI_DEMANDEUR_IRM_PERIODE
Table contenant la spécialité du médecin demandeur de l'IRM index et des infos sur les consultation des patients inclus dans l'étude dans les 3 mois précédent l'IRM

| Variable           | Libelle                                                                                                                                                                                                |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| BEN_IDT_ANO        | Identifiant du patient                                                                                                                                                                                 |
| TYPE               | Provenance des données: DCIR / PMSI_ACE                                                                                                                                                                |
| DEMANDEUR       | Spécialité du médecin demandeur de l'IRM index (cf. construction de la variable dans le protocole): ortho / rhumato / mpr / mg / autre                                                       |
| DPT_DEMANDEUR   | Département du médecin demandeur de l'IRM index                                                                                        |
| DELAI_IRM | Délai en jour entre l'IRM index et la demande de l'examen                                                                                                                                              |
| SANS_C             | Patient sans médecin demandeur trouvé dans les 3 mois précédents l'IRM: 1 si pas de médecin demandeur / 0 sinon                                                                                                  |
| C_MPR              | Présence d'au moins une consultation / téléconsultation de MPR dans les 3 mois précédent l'IRM: 1 si oui / 0 sinon                                                                                     |
| C_ORTHO            | Présence d'au moins une consultation / téléconsultation d'orthopédie dans les 3 mois précédent l'IRM: 1 si oui / 0 sinon                                                                               |
| C_RHUMATO          | Présence d'au moins une consultation / téléconsultation de rhumatologie dans les 3 mois précédent l'IRM: 1 si oui / 0 sinon                                                                            |
| C_MG               | Présence d'au moins une consultation / téléconsultation de médecine générale dans les 3 mois précédent l'IRM: 1 si oui / 0 sinon                                                                       |
| C_AUTRE            | Présence d'au moins une consultation / téléconsultation d'une autre spécialité dans les 3 mois précédent l'IRM (spécialité autre que MPR,rhumato, ortho, MG): 1 si oui / 0 sinon                       |
| CS                 | Présence d'au moins une consultation / téléconsultation de MPR, ortho ou rhumato dans les 3 mois précédent l'IRM: 1 si oui / 0 sinon                                                                   |
| MG_ONLY            | Présence d'au moins une consultation / téléconsultation de médecine générale sans aucune autre consultation / téléconsultation de spécialité dans les 3 mois précédents l'IRM: 1 si oui / 0 sinon      |
| MG_SANS_CS         | Présence d'au moins une consultation / téléconsultation de médecine générale sans aucune consultation / téléconsultation de MPR, ortho ou rhumato dans les 3 mois précédents l'IRM: 1 si oui / 0 sinon |

#### IMI_SUIVI_PRE_IRM_PERIODE
Table contenant les consultations, les actes et hospitalisations d'intérêt dans les 6 mois précédent l'IRM index pour les patients inclus dans l'étude

| Variable        | Libelle                                                                                                                                                                                                                                                                                                                                                                  |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| BEN_IDT_ANO     | Identifiant du patient                                                                                                                                                                                                                                                                                                                                                   |
| TYPE            | Provenance des données: DCIR / PMSI_ACE / PMSI_HOSP                                                                                                                                                                                                                                                                                                                      |
| TYPE_PRESTATION | Type de prestation : imagerie / ambu / hospit / consult                                                                                                                                                                                                                                                                                                                  |
| PRESTA_DETAIL   | Prestation détaillée: radio_genou / scanner / echo / IRM / contention / attelle / app_rigide / ponction / evacuation / injection_sans / injection_avec / meniscectomie / ligament / arthroscopie / prothese / trauma / non_trauma / C_MG / C_MPR / C_ortho / C_rhumato / C_autre / radio_bassin / radio_femur / radio_cuisse / radio_jambe / radio_cheville / radio_pied |
| CODE            | Code CCAM de l'acte ou code de la consultation (en norme PS5) ou code GHM de l'hospitalisation                                                                                                                                                                                                                                                                           |
| EXE_SOI_DTD     | Date de la prestation (correspond à la date d'entrée pour l'hospitalisation)                                                                                                                                                                                                                                                                                             |
| DELAI_PRE_IRM   | Délai en jour entre la prestation et l'IRM index                                                                                                                                                                                                                                                                                                                  |
| PRS_ACT_QTE     | Nombre d'actes / consultations / hospitalisations                                                                                                                                                                                                                                                                                                                        |
| DPT_EXEC        | Département de l'exécutant (Dpt de l'établissement ou du professionnel de santé si finess de l'établissement exécutant non renseigné ou en erreur)                                                                                                                                                                                                                       |
| FINESS_EXEC     | Finess de l'établissement exécutant (peut être non renseigné pour le DCIR)                                                                                                                                                                                                                                                                                               |
| ETE_CAT_COD     | Catégorie de l'établissement exécutant (Données DCIR uniquement; peut être non renseigné)                                                                                                                                                                                                                                                                                |
#### IMI_SUIVI_POST_IRM_PERIODE
Table contenant les consultations, les actes et hospitalisations d'intérêt dans les 6 mois suivant l'IRM index pour les patients inclus dans l'étude

| Variable        | Libelle                                                                                                                                                                                                                                                                                                                                                                  |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| BEN_IDT_ANO     | Identifiant du patient                                                                                                                                                                                                                                                                                                                                                   |
| TYPE            | Provenance des données: DCIR / PMSI_ACE / PMSI_HOSP                                                                                                                                                                                                                                                                                                                      |
| TYPE_PRESTATION | Type de prestation : imagerie / ambu / hospit / consult                                                                                                                                                                                                                                                                                                                  |
| PRESTA_DETAIL   | Prestation détaillée: radio_genou / scanner / echo / IRM / contention / attelle / app_rigide / ponction / evacuation / injection_sans / injection_avec / meniscectomie / ligament / arthroscopie / prothese / trauma / non_trauma / C_MG / C_MPR / C_ortho / C_rhumato / C_autre / radio_bassin / radio_femur / radio_cuisse / radio_jambe / radio_cheville / radio_pied |
| CODE            | Code CCAM de l'acte ou code de la consultation (en norme PS5) ou code GHM de l'hospitalisation                                                                                                                                                                                                                                                                           |
| EXE_SOI_DTD     | Date de la prestation (correspond à la date d'entrée pour l'hospitalisation)                                                                                                                                                                                                                                                                                             |
| DELAI_POST_IRM  | Délai en jour entre la prestation et l'IRM index                                                                                                                                                                                                                                                                                                                  |
| PRS_ACT_QTE     | Nombre d'actes / consultations / hospitalisations                                                                                                                                                                                                                                                                                                                        |
| DPT_EXEC        | Département de l'exécutant (Dpt de l'établissement ou du professionnel de santé si finess de l'établissement exécutant non renseigné ou en erreur)                                                                                                                                                                                                                       |
| FINESS_EXEC     | Finess de l'établissement exécutant (peut être non renseigné pour le DCIR)                                                                                                                                                                                                                                                                                               |
| ETE_CAT_COD     | Catégorie de l'établissement exécutant (Données DCIR uniquement; peut être non renseigné)                                                                                                                                                                                                                                                                                |