/**************************************************************************************************************************************************************
							ETUDE SUR L'ETAT DES PRATIQUES DE RECOURS A L'IRM DU MEMBRE INFERIEUR 
	 
	
CREATION DES TABLEAUX DE RESTITUTION DES RESULTATS:
	1 - DESCRIPTION DES PATIENTS ET DE L'IRM INDEX
	2 - MEDECIN DEMANDEUR DE L'IRM INDEX
	3 - SUIVI PRE ET POST IRM INDEX

****************************************************************************************************************************************************************/

/***************************************************************D�finition des macrovariables**********************************************************************/
%let per=S2;/*p�riode*/
%let anneem=2021;/*ann�e*/

data _null_;/*date de traitement*/
call symput("datetrt",left(put("&sysdate"d,ddmmyy10.)));
run;

proc sql noprint;
select count(distinct ben_idt_ano) into:nb_patient from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem; /*nb patients*/
quit;

/***************************************************************D�finition des formats**********************************************************************/
proc format;

/*1 - DESCRIPTION DES PATIENTS ET DE L'IRM INDEX*/
value $desc_pat
'1'="Nb patients"
'2'="Age moyen"
'3'="Age m�dian"
'4'="% Hommes"
'5'="% IRM en lib�ral"
'6'="% IRM en ACE"
'7'="Q1 Age"
'8'="Q3 Age"
'9'="IQR"
;

/*2 - MEDECIN DEMANDEUR DE L'IRM INDEX*/
value $dem0_
'0'="3 mois avant l'IRM index"
;

value $dem1_
'1'="Consultation de MPR, d'orthop�die ou de rhumatologie"
'2'="Consultation d'orthop�die"
'3'="Consultation de rhumatologie"
'4'="Consultation de MPR"
'5'="Consultation de MG sans consultation de MPR, d'orthop�die ou de rhumatologie"
'6'="Consultation de MG seulement"
'7'="Aucune consultation m�dicale retrouv�e"
;

value $dem2_
'1'="M�decin g�n�raliste"
'2'="Orthop�diste"
'3'="Rhumatologue"
'4'="MPR"
'5'="Autre sp�cialit�"
'6'="M�decin demandeur non retrouv�"
'7'="Total"
;

/*3 - SUIVI PRE ET POST IRM INDEX*/
value $suivi0_
'0'="6 mois avant l'IRM index"
'1'="6 mois apr�s l'IRM index"
'2'="Total"
;

value $suivi1_
'1'="Consultation de m�decine g�n�rale"
'2'="Consultation d'orthop�die"
'3'="Consultation de rhumatologie"
'4'="Consultation de MPR"
'5'="Consultation m�dicale d'une autre sp�cialit�"
;

value $suivi2_
'1'="Radiographie du genou"
'2'="Radiographie du bassin"
'3'="Radiographie de l'articulation coxof�morale"
'4'="Radiographie de la cuisse"
'5'="Radiographie de la jambe"
'6'="Radiographie de la cheville"
'7'="Radiographie du pied"
'8'="Scanner du membre inf�rieur"
'9'="Echographie articulaire"
'10'="IRM du membre inf�rieur"
'11'="IRM du membre inf�rieur, d�lai> 7jr"
;

value $suivi3_
'1'="Confection d'une contention souple du genou"
'2'="Confection d'une attelle de posture ou de mobilisation du genou"
'3'="Confection d'un appareil rigide d'immobilisation du membre inf�rieur prenant le genou"
'4'="Ponction d'une articulation du membre inf�rieur"
'5'="Evacuation de collection articulaire du membre inf�rieur"
'6'="Injection th�rapeutique dans une articulation du membre inf�rieur sans guidage"
'7'="Injection th�rapeutique dans une articulation du membre inf�rieur avec guidage"
;

value $suivi4_
'1'="M�niscectomie sous arthroscopie"
'2'="Interventions sur les ligaments crois�s"
'3'="Autres arthroscopies du genou"
'4'="Proth�ses de genou"
'5'="Interventions sur le genou pour traumatismes"
'6'="Interventions sur le genou pour des affections autres que traumatiques"
;

value $suivi5_
'1'="Patient sans consultation ortho, rhumato, MPR"
'2'="Patient sans consultation ortho, rhumato, MPR, MG"
'3'="Patient sans aucune consultation retrouv�e"
'4'="Patient sans radiographie du genou"
'5'="Patient avec au moins un acte de radiographie du bassin, articulation coxof�morale, cuisse, jambe, cheville, pied"
'6'="Patient sans radiographie du genou mais avec au moins un acte de radiographie du bassin, articulation coxof�morale, cuisse, jambe, cheville, pied"
'7'="Patient sans radiographie du MI (y/c radiographie du genou)"
'8'="Patient avec au moins une autre IRM du membre inf�rieur"
'9'="Patient avec au moins une autre IRM du membre inf�rieur dans un d�lai> 7jr par rapport � l'IRM de r�r�rence"
'10'="Patient sans imagerie du MI compl�mentaire � l'IRM index (radio, �cho, scanner ou autre IRM)"
'11'="Patient sans acte th�rapeutique (articulaire, MI ou genou) en ambulatoire"
'12'="Patient sans hospitalisation pour pathologies du genou"
'13'="Patient sans acte th�rapeutique ambulatoire ni hospitalisation en rapport avec le genou"
;

value $suivi6_
'7'="Au moins l'un de ces actes"
'8'="Au moins l'un de ces actes et pas de radiographie du genou"
'9'="Aucune radiographie du MI (y/c radiographie du genou)"
;

run;


/****************************************************************************************************************************************************************/

ods excel file="./IMI_res.xlsx"  options(start_at="B2" sheet_interval='none');
ods text="ETUDE SUR L'ETAT DES PRATIQUES DE RECOURS A L'IRM DU MEMBRE INFERIEUR";
ods text="IRM r�alis�es sur le 2nd semestre 2021 - suivi 6 mois avant et apr�s l'IRM - patients >= 18 ans";
ods text=" ";
ods text="Date de traitement: &datetrt";
ods text=" ";
ods text=" ";

/***************************************************************************
			1 - DESCRIPTION DES PATIENTS ET DE L'IRM INDEX
***************************************************************************/

/*Table utilis�e: sasdata1.IMI_PATIENT_FIRST_IRM_S2_2021*/

ods text=" ";
ods text="1 - DESCRIPTION DES PATIENTS ET DE L'IRM INDEX";
ods text=" ";

proc sql noprint;
create table t1 as select '1' as indic,count(distinct ben_idt_ano) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
create table t2 as select '2' as indic,round(mean(age),0.1) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
create table t3 as select '3' as indic,round(median(age),0.1) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
create table t4 as select '4' as indic,round((sum(ben_sex_cod=1)*100)/&nb_patient,0.1) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
create table t5 as select '5' as indic,round((sum(type='DCIR')*100)/&nb_patient,0.1) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
create table t6 as select '6' as indic,round((sum(type='PMSI_ACE')*100)/&nb_patient,0.1) as valeur from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
quit;

/*Pour calcul Q1 / Q3 Age*/
Proc MEANS Data=sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem noprint;
var age;
output out=t7_ (drop=_type_ _freq_) q1=q1 q3=q3 qrange=qrange;
run;

proc transpose data=t7_ out=t7;run;

data t7;
set t7;
if _name_="q1" then indic="7";
if _name_="q3" then indic="8";
if _name_="qrange" then indic="9";
drop _label_ _name_;
rename col1=valeur;
run;

data res;
set t1 t2 t3 t4 t5 t6 t7;
run;

proc report data=res nowd  split='*'; 
columns  indic valeur;
define indic/format=$desc_pat. "Indicateurs" order=data;
define valeur/ "Valeur";
run;


/***************************************************************************
			2 - MEDECIN DEMANDEUR DE L'IRM INDEX
***************************************************************************/

/*Table utilis�e: sasdata1.IMI_DEMANDEUR_IRM_S2_2021*/

ods text=" ";
ods text="2 - MEDECIN DEMANDEUR DE L'IRM INDEX";


ods text=" ";
ods text="Type de consultations / t�l�consultations 3 mois avant l'IRM";
proc sql noprint;
create table t1 as select '0' as suivi,'1' as indic,sum(cs=1) as nb,round((sum(cs=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t2 as select '0' as suivi,'2' as indic,sum(c_ortho=1) as nb,round((sum(c_ortho=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t3 as select '0' as suivi,'3' as indic,sum(c_rhumato=1) as nb,round((sum(c_rhumato=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t4 as select '0' as suivi,'4' as indic,sum(c_mpr=1) as nb,round((sum(c_mpr=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t5 as select '0' as suivi,'5' as indic,sum(mg_sans_cs=1) as nb,round((sum(mg_sans_cs=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t6 as select '0' as suivi,'6' as indic,sum(mg_only=1) as nb,round((sum(mg_only=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
create table t7 as select '0' as suivi,'7' as indic,sum(sans_c=1) as nb,round((sum(sans_c=1)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem;
quit;

data res;
set t1 t2 t3 t4 t5 t6 t7;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct) ;
define suivi/across "A0"X format=$dem0_. order=data;
define indic/format=$dem1_. "Consultations/T�l�consultations" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
run;

ods text=" ";
ods text="M�decin demandeur probable de l'IRM";
proc sql noprint;
create table t1 as select '1' as indic,sum(demandeur='mg') as nb,round((sum(demandeur='mg')*100)/&nb_patient,0.1) as pct,sum(demandeur='mg' and type='PMSI_ACE') as nb_hosp,round((sum(demandeur='mg' and type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp,round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur='mg';
create table t2 as select '2' as indic,sum(demandeur='ortho') as nb,round((sum(demandeur='ortho')*100)/&nb_patient,0.1) as pct,sum(demandeur='ortho' and type='PMSI_ACE') as nb_hosp,round((sum(demandeur='ortho' and type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp,round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur='ortho';
create table t3 as select '3' as indic,sum(demandeur='rhumato') as nb,round((sum(demandeur='rhumato')*100)/&nb_patient,0.1) as pct,sum(demandeur='rhumato' and type='PMSI_ACE') as nb_hosp,round((sum(demandeur='rhumato' and type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp,round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur='rhumato';
create table t4 as select '4' as indic,sum(demandeur='mpr') as nb,round((sum(demandeur='mpr')*100)/&nb_patient,0.1) as pct,sum(demandeur='mpr' and type='PMSI_ACE') as nb_hosp,round((sum(demandeur='mpr' and type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp,round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur='mpr';
create table t5 as select '5' as indic,sum(demandeur='autre') as nb,round((sum(demandeur='autre')*100)/&nb_patient,0.1) as pct,sum(demandeur='autre' and type='PMSI_ACE') as nb_hosp,round((sum(demandeur='autre' and type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp,round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur='autre';
create table t6 as select '6' as indic,sum(demandeur=' ') as nb,round((sum(demandeur=' ')*100)/&nb_patient,0.1) as pct from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur=' ';
create table t7 as select '7' as indic,&nb_patient as nb,round((&nb_patient*100)/&nb_patient,0.1) as pct,sum(type='PMSI_ACE') as nb_hosp,round((sum(type='PMSI_ACE')*100)/&nb_patient,0.1) as pct_hosp, round(mean(delai_irm),0.1) as moy,round(median(delai_irm),0.1) as med from sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem where demandeur ne ' ';
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=sasdata1.IMI_DEMANDEUR_IRM_&per._&anneem noprint ;
var delai_irm;
output out=t&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;
%q1_q3 (num=1,condition=where demandeur='mg');
%q1_q3 (num=2,condition=where demandeur='ortho');
%q1_q3 (num=3,condition=where demandeur='rhumato');
%q1_q3(num=4,condition=where demandeur='mpr');
%q1_q3 (num=5,condition=where demandeur='autre');
%q1_q3 (num=7,condition=);


data res;
set t1 t2 t3 t4 t5 t6 t7;
run;

proc report data=res nowd  split='*'; 
columns  indic nb pct nb_hosp pct_hosp moy med q1_ q3_ iqr;
define indic/format=$dem2_. "M�decins demandeurs" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_hosp/ "Nb patients avec m�decin demandeur hospitalier" ;
define pct_hosp/ "% patients";
define moy/ "D�lai moyen en jr avt IRM";
define med/ "D�lai m�dian en jr avt IRM";
define q1_/"Q1 D�lai IRM";
define q3_/"Q3 D�lai IRM";
define iqr/"IQR D�lai IRM";
run;


/***************************************************************************
			3 - SUIVI PRE ET POST IRM INDEX
***************************************************************************/

/*Tables utilis�es: sasdata1.IMI_SUIVI_PRE_IRM_S2_2021 et sasdata1.IMI_SUIVI_POST_IRM_S2_2021*/

/*Cr�ation de tables:
- first_presta_pre_irm: Table qui contient par patient et par type de prestation: le nombre de prestations, la date et le d�lai de la derni�re prestation avant l'IRM 
- first_presta_post_irm: Table qui contient par patient et par type de prestation: le nombre de prestations, la date et le d�lai de la 1�re prestation apr�s l'IRM */

proc sql;
create table first_presta_pre_irm as select distinct ben_idt_ano, presta_detail,min(delai_pre_irm) as delai_pre_irm,sum(prs_act_qte) as prs_act_qte,max(exe_soi_dtd) as first_date
format=DATETIME20. from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem group by ben_idt_ano, presta_detail;

create table first_presta_post_irm as select distinct ben_idt_ano, presta_detail,min(delai_post_irm) as delai_post_irm,sum(prs_act_qte) as prs_act_qte,min(exe_soi_dtd) as first_date
format=DATETIME20. from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem group by ben_idt_ano, presta_detail;
quit;

/*
- first_presta_pre_irm_h7j: M�me table que pr�c�demment mais avec suppression des IRM effectu�es dans un d�lai de 7 jours par rapport � l'IRM index (pour calcul des IRM r�p�t�es)
- first_presta_post_irm_h7j: M�me table que pr�c�demment mais avec suppression des IRM effectu�es dans un d�lai de 7 jours par rapport � l'IRM index (pour calcul des IRM r�p�t�es)
*/


data IMI_pre;
set sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem;
if presta_detail='IRM' and delai_pre_irm<=7 then delete;
run;

data IMI_post;
set sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem;
if presta_detail='IRM' and delai_post_irm<=7 then delete;
run;

proc sql;
create table first_presta_pre_irm_h7j as select distinct ben_idt_ano, presta_detail,min(delai_pre_irm) as delai_pre_irm,sum(prs_act_qte) as prs_act_qte,max(exe_soi_dtd) as first_date
format=DATETIME20. from IMI_pre group by ben_idt_ano, presta_detail;

create table first_presta_post_irm_h7j as select distinct ben_idt_ano, presta_detail,min(delai_post_irm) as delai_post_irm,sum(prs_act_qte) as prs_act_qte,min(exe_soi_dtd) as first_date
format=DATETIME20. from IMI_post group by ben_idt_ano, presta_detail;
quit;

/************************************************************************************/

ods text=" ";
ods text="3 - SUIVI PRE ET POST IRM INDEX";

ods text=" ";
ods text="Focus consultations / t�l�consultations 6 mois avant et apr�s l'IRM";

/*Avant l'IRM*/
proc sql noprint;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="C_MG";
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="C_ortho";
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="C_rhumato";
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="C_MPR";
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="C_autre";
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_pre_irm noprint ;
var delai_pre_irm;
output out=t&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="C_MG");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="C_ortho");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="C_rhumato");
%q1_q3(num=4,condition=where PRESTA_DETAIL="C_MPR");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="C_autre");



/*Apr�s l'IRM*/
proc sql noprint;
create table s1 as select '1' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="C_MG";
create table s2 as select '1' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="C_ortho";
create table s3 as select '1' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="C_rhumato";
create table s4 as select '1' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="C_MPR";
create table s5 as select '1' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="C_autre";
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_post_irm noprint ;
var delai_post_irm;
output out=s&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data s&num;
merge s&num s&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="C_MG");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="C_ortho");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="C_rhumato");
%q1_q3(num=4,condition=where PRESTA_DETAIL="C_MPR");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="C_autre");


data res;
set t1 t2 t3 t4 t5 s1 s2 s3 s4 s5;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct moy_c med_c moy_d med_d q1_ q3_ iqr) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi1_. "Consultations/T�l�consultations" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_d/ "D�lai moyen en jr par rapport � l'IRM";
define med_d/ "D�lai m�dian en jr par rapport � l'IRM";
define q1_/"Q1 D�lai";
define q3_/"Q3 D�lai";
define iqr/"IQR D�lai";
run;


ods text=" ";
ods text="Focus imagerie (articulaire, du MI ou du genou) 6 mois avant et apr�s l'IRM";

/*Avant l'IRM*/
proc sql noprint;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_genou";
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_bassin";
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_femur";
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_cuisse";
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_jambe";
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_cheville";
create table t7 as select '0' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="radio_pied";
create table t8 as select '0' as suivi, '8' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="scanner";
create table t9 as select '0' as suivi, '9' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="echo";
create table t10 as select '0' as suivi, '10' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="IRM";
create table t11 as select '0' as suivi, '11' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm_h7j where PRESTA_DETAIL="IRM";
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_pre_irm noprint ;
var delai_pre_irm;
output out=t&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="radio_genou");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="radio_bassin");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="radio_femur");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="radio_cuisse");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="radio_jambe");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="radio_cheville");
%q1_q3 (num=7,condition=where PRESTA_DETAIL="radio_pied");
%q1_q3 (num=8,condition=where PRESTA_DETAIL="scanner");
%q1_q3 (num=9,condition=where PRESTA_DETAIL="echo");
%q1_q3 (num=10,condition=where PRESTA_DETAIL="IRM");

Proc MEANS Data=first_presta_pre_irm_h7j noprint ;
var delai_pre_irm;
output out=t11_ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
where PRESTA_DETAIL="IRM";
run;

data t11;
merge t11 t11_;
run;


/*Apr�s l'IRM*/
proc sql noprint;
create table s1 as select '1' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_genou";
create table s2 as select '1' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_bassin";
create table s3 as select '1' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_femur";
create table s4 as select '1' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_cuisse";
create table s5 as select '1' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_jambe";
create table s6 as select '1' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_cheville";
create table s7 as select '1' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="radio_pied";
create table s8 as select '1' as suivi, '8' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="scanner";
create table s9 as select '1' as suivi, '9' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="echo";
create table s10 as select '1' as suivi, '10' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="IRM";
create table s11 as select '1' as suivi, '11' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm_h7j where PRESTA_DETAIL="IRM";

quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_post_irm noprint ;
var delai_post_irm;
output out=s&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data s&num;
merge s&num s&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="radio_genou");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="radio_bassin");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="radio_femur");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="radio_cuisse");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="radio_jambe");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="radio_cheville");
%q1_q3 (num=7,condition=where PRESTA_DETAIL="radio_pied");
%q1_q3 (num=8,condition=where PRESTA_DETAIL="scanner");
%q1_q3 (num=9,condition=where PRESTA_DETAIL="echo");
%q1_q3 (num=10,condition=where PRESTA_DETAIL="IRM");


Proc MEANS Data=first_presta_post_irm_h7j noprint ;
var delai_post_irm;
output out=s11_ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
where PRESTA_DETAIL="IRM";
run;

data s11;
merge s11 s11_;
run;



data res;
format indic $2.;
set t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct moy_c med_c moy_d med_d q1_ q3_ iqr) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi2_. "Imageries" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_d/ "D�lai moyen en jr par rapport � l'IRM";
define med_d/ "D�lai m�dian en jr par rapport � l'IRM";
define q1_/"Q1 D�lai";
define q3_/"Q3 D�lai";
define iqr/"IQR D�lai";
run;


ods text=" ";
ods text="Focus actes th�rapeutiques (du MI ou du genou) en ambulatoire";

/*Avant l'IRM*/
proc sql noprint;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="contention";
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="attelle";
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="app_rigide";
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="ponction";
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="evacuation";
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="injection_sans";
create table t7 as select '0' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="injection_avec";
quit;


/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_pre_irm noprint ;
var delai_pre_irm;
output out=t&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="contention");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="attelle");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="app_rigide");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="ponction");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="evacuation");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="injection_sans");
%q1_q3 (num=7,condition=where PRESTA_DETAIL="injection_avec");


/*Apr�s l'IRM*/
proc sql noprint;
create table s1 as select '1' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="contention";
create table s2 as select '1' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="attelle";
create table s3 as select '1' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="app_rigide";
create table s4 as select '1' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="ponction";
create table s5 as select '1' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="evacuation";
create table s6 as select '1' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="injection_sans";
create table s7 as select '1' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="injection_avec";
quit;

%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_post_irm noprint ;
var delai_post_irm;
output out=s&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data s&num;
merge s&num s&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="contention");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="attelle");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="app_rigide");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="ponction");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="evacuation");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="injection_sans");
%q1_q3 (num=7,condition=where PRESTA_DETAIL="injection_avec");


data res;
set t1 t2 t3 t4 t5 t6 t7 s1 s2 s3 s4 s5 s6 s7;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct moy_c med_c moy_d med_d q1_ q3_ iqr) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi3_. "Actes th�rapeutiques (articulaires, du MI ou du genou) en ambulatoire" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_d/ "D�lai moyen en jr par rapport � l'IRM";
define med_d/ "D�lai m�dian en jr par rapport � l'IRM";
define q1_/"Q1 D�lai";
define q3_/"Q3 D�lai";
define iqr/"IQR D�lai";
run;


ods text=" ";
ods text="Focus hospitalisations pour pathologies du genou";

/*Avant l'IRM*/
proc sql noprint;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="meniscectomie";
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="ligament";
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="arthroscopie";
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="prothese";
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="trauma";
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_pre_irm),0.1) as moy_d,round(median(delai_pre_irm),0.1) as med_d from first_presta_pre_irm where PRESTA_DETAIL="non_trauma";
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_pre_irm noprint ;
var delai_pre_irm;
output out=t&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="meniscectomie");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="ligament");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="arthroscopie");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="prothese");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="trauma");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="non_trauma");


/*Apr�s l'IRM*/
proc sql noprint;
create table s1 as select '1' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="meniscectomie";
create table s2 as select '1' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="ligament";
create table s3 as select '1' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="arthroscopie";
create table s4 as select '1' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="prothese";
create table s5 as select '1' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="trauma";
create table s6 as select '1' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,round(mean(delai_post_irm),0.1) as moy_d,round(median(delai_post_irm),0.1) as med_d from first_presta_post_irm where PRESTA_DETAIL="non_trauma";
quit;

/*Calcul Q1 Q3 d�lai*/
%macro q1_q3 (num=,condition=);
Proc MEANS Data=first_presta_post_irm noprint ;
var delai_post_irm;
output out=s&num._ (drop=_type_ _freq_) q1=q1_ q3=q3_ qrange=iqr;
&condition;
run;

data s&num;
merge s&num s&num._;
run;
%mend;
%q1_q3 (num=1,condition=where PRESTA_DETAIL="meniscectomie");
%q1_q3 (num=2,condition=where PRESTA_DETAIL="ligament");
%q1_q3 (num=3,condition=where PRESTA_DETAIL="arthroscopie");
%q1_q3 (num=4,condition=where PRESTA_DETAIL="prothese");
%q1_q3 (num=5,condition=where PRESTA_DETAIL="trauma");
%q1_q3 (num=6,condition=where PRESTA_DETAIL="non_trauma");


data res;
set t1 t2 t3 t4 t5 t6 s1 s2 s3 s4 s5 s6;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct moy_c med_c moy_d med_d q1_ q3_ iqr) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi4_. "Hospitalisations pour pathologies du genou" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_d/ "D�lai moyen en jr par rapport � l'IRM";
define med_d/ "D�lai m�dian en jr par rapport � l'IRM";
define q1_/"Q1 D�lai";
define q3_/"Q3 D�lai";
define iqr/"IQR D�lai";
run;


ods text=" ";
ods text="Synth�se du suivi pre et post IRM";


/*Avant l'IRM*/
%macro tt (nb=,type=);
proc sql;
create table selec as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where (&type);
create table list_pat as select distinct ben_idt_ano from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
quit;

data selec;
merge selec (in=a) list_pat (in=b);
by ben_idt_ano;
if b and not a;
run;

proc sql;
create table t&nb as select '0' as suivi, "&nb" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from selec ;
quit;
%mend;

%tt(nb=1,type=presta_detail in ("C_ortho","C_rhumato","C_MPR"));
%tt(nb=2,type=presta_detail in ("C_ortho","C_rhumato","C_MPR","C_MG"));
%tt(nb=3,type=type_prestation="consult");
%tt(nb=4,type=presta_detail="radio_genou");
%tt(nb=7,type=presta_detail in ("radio_genou","radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied"));
%tt(nb=10,type=type_prestation="imagerie");
%tt(nb=11,type=type_prestation="ambu");
%tt(nb=12,type=type_prestation="hospit");
%tt(nb=13,type=type_prestation in ("hospit","ambu"));

proc sql;
create table t5 as select '0' as suivi, "5" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table t6_1 as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table t6_2 as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where presta_detail in ("radio_genou");
create table t8 as select '0' as suivi, "8" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where presta_detail='IRM';
create table t9 as select '0' as suivi, "9" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem where presta_detail='IRM' and delai_pre_irm>7;
quit;

data t6;
merge t6_1 (in=a) t6_2 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t6 as select '0' as suivi, "6" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t6;
quit;

/*Apr�s l'IRM*/
%macro tt (nb=,type=);
proc sql;
create table selec as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where (&type);
create table list_pat as select distinct ben_idt_ano from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
quit;

data selec;
merge selec (in=a) list_pat (in=b);
by ben_idt_ano;
if b and not a;
run;

proc sql;
create table s&nb as select '1' as suivi, "&nb" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from selec ;
quit;
%mend;

%tt(nb=1,type=presta_detail in ("C_ortho","C_rhumato","C_MPR"));
%tt(nb=2,type=presta_detail in ("C_ortho","C_rhumato","C_MPR","C_MG"));
%tt(nb=3,type=type_prestation="consult");
%tt(nb=4,type=presta_detail="radio_genou");
%tt(nb=7,type=presta_detail in ("radio_genou","radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied"));
%tt(nb=10,type=type_prestation="imagerie");
%tt(nb=11,type=type_prestation="ambu");
%tt(nb=12,type=type_prestation="hospit");
%tt(nb=13,type=type_prestation in ("hospit","ambu"));

proc sql;
create table s5 as select '1' as suivi, "5" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table s6_1 as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table s6_2 as select distinct ben_idt_ano from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where presta_detail in ("radio_genou");
create table s8 as select '1' as suivi, "8" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where presta_detail='IRM';
create table s9 as select '1' as suivi, "9" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem where presta_detail='IRM' and delai_post_irm>7;
quit;

data s6;
merge s6_1 (in=a) s6_2 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table s6 as select '1' as suivi, "6" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from s6;
quit;


/*Total*/
data IMI_SUIVI_PRE_POST_IRM;
set sasdata1.IMI_SUIVI_PRE_IRM_&per._&anneem sasdata1.IMI_SUIVI_POST_IRM_&per._&anneem;
if delai_post_irm ne . then delai=delai_post_irm;
if delai_pre_irm ne . then delai=delai_pre_irm;
run;

proc sql;
create table IMI_SUIVI_PRE_POST_IRM_h7j as select distinct ben_idt_ano, type_prestation, presta_detail from IMI_SUIVI_PRE_POST_IRM where delai>7;
create table IMI_SUIVI_PRE_POST_IRM as select distinct ben_idt_ano, type_prestation, presta_detail from IMI_SUIVI_PRE_POST_IRM;
quit;

%macro tt (nb=,type=);
proc sql;
create table selec as select distinct ben_idt_ano from IMI_SUIVI_PRE_POST_IRM where (&type);
create table list_pat as select distinct ben_idt_ano from sasdata1.IMI_PATIENT_FIRST_IRM_&per._&anneem;
quit;

data selec;
merge selec (in=a) list_pat (in=b);
by ben_idt_ano;
if b and not a;
run;

proc sql;
create table z&nb as select '2' as suivi, "&nb" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from selec ;
quit;
%mend;

%tt(nb=1,type=presta_detail in ("C_ortho","C_rhumato","C_MPR"));
%tt(nb=2,type=presta_detail in ("C_ortho","C_rhumato","C_MPR","C_MG"));
%tt(nb=3,type=type_prestation="consult");
%tt(nb=4,type=presta_detail="radio_genou");
%tt(nb=7,type=presta_detail in ("radio_genou","radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied"));
%tt(nb=10,type=type_prestation="imagerie");
%tt(nb=11,type=type_prestation="ambu");
%tt(nb=12,type=type_prestation="hospit");
%tt(nb=13,type=type_prestation in ("hospit","ambu"));

proc sql;
create table z5 as select '2' as suivi, "5" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from IMI_SUIVI_PRE_POST_IRM where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table z6_1 as select distinct ben_idt_ano from IMI_SUIVI_PRE_POST_IRM where presta_detail in ("radio_bassin","radio_femur","radio_cuisse","radio_jambe","radio_cheville","radio_pied");
create table z6_2 as select distinct ben_idt_ano from IMI_SUIVI_PRE_POST_IRM where presta_detail in ("radio_genou");
create table z8 as select '2' as suivi, "8" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from IMI_SUIVI_PRE_POST_IRM where presta_detail='IRM';
create table z9 as select '2' as suivi, "9" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from IMI_SUIVI_PRE_POST_IRM_h7j where presta_detail='IRM';
quit;

data z6;
merge z6_1 (in=a) z6_2 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table z6 as select '2' as suivi, "6" as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from z6;
quit;

data res;
format indic $2.;
set t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 z1 z2 z3 z4 z5 z6 z7 z8 z9 z10 z11 z12 z13;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi5_. "Indicateurs" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
run;

ods excel close;

