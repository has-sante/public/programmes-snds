/**************************************************************************************************************************************************************
	 						ETUDE SUR L'ETAT DES PRATIQUES DE RECOURS A L'IRM DU MEMBRE INFERIEUR 

EXTRACTION DE LA POPULATION D'ETUDE:


	1 - EXTRACTION DES IRM DE LA PERIODE D'ETUDE
		EXTRACTION IRM � partir du DCIR
		EXTRACTION IRM � partir du PMSI
	2 - SELECTION DES IDENTIFIANTS PATIENT 
		SELECTION PMSI
		SELECTION DCIR
		SELECTION DE TOUS LES IDENTIFIANTS PATIENT POSSIBLES POUR UN MEME PATIENT
		- TABLE sasdata1.IMI_id_patient_S2_2021 
	3 - SELECTION DE L'IRM INDEX POUR CHAQUE PATIENT INCLUS DANS L'ETUDE
		- TABLE sasdata1.IMI_patient_first_irm_S2_2021
	
**************************************************************************************************************************************************************/

/***************************************************
Rappel p�rim�tre de l'�tude:
-----------------------------
IRM r�alis�es en lib�ral (DCIR) ou en ACE (PMSI) 
Hors hospitalisation 
Hors passages aux ugences
Patients >= 18 ans
2�me semestre 2021
***************************************************/

/***************************************************************D�fintion des macrovariables**********************************************************************/

/*P�riode d'�tude*/
%let per=S2;

/*DCIR*/
%let date_debm=202107;
%let date_finm=202112;
%let anneem=2021;

/*PMSI*/
%let anneep=21;
%let mois_deb=7;
%let mois_fin=12;
%let fin_geo ='130780521', '130783236', '130783293', '130784234', '130804297','600100101','750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166','750100208', '750100216', 
'750100232', '750100273', '750100299' , '750801441', '750803447', '750803454', '910100015','910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037','930100045', '940100027', 
'940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137','690784152', '690784178', '690787478', '830100558' ;

/********************************************************************************************************************************************************************/

/***************************************************************************
			1 - EXTRACTION DES IRM DE LA PERIODE D'ETUDE
***************************************************************************/


/**************************************************************************EXTRACTION IRM � partir du DCIR*************************************************************************/

/**************************************************************************
Filtres DCIR
---------------------------------------------------------------------------
Exclusion des NIR Fictifs
Exclusion de l'activit� des ES Publics (ACE)
Exclusion des IRM r�alis�es lors de s�jours hospitaliers en ES ex-OQN
Exclusion des IRM r�alis�es lors d'un passage aux urgences ES ex-OQN
Exclusion des lignes d'actes avec quantit� <=0
*************************************************************************/

%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) D�but %SYSFUNC( datetime(),datetime. ) ;

/*S�lection des IRM sur la p�riode d'�tude*/
proc sql;
create table extract as 
SELECT a.exe_soi_dtd, a.ben_res_dpt, a.ben_res_com, a.ben_nir_psa, a.ben_rng_gem, a.BEN_SEX_COD, a.pfs_exe_num,a.dpn_qlf, (SUM(CASE WHEN a.CPL_MAJ_TOP <2 THEN a.PRS_ACT_QTE ELSE 0 END)) as prs_act_qte, a.prs_nat_ref,
case when a.BEN_AMA_COD between 1000 AND 1011 then 0
     when a.BEN_AMA_COD between 1012 AND 1023 then 1
     else a.BEN_AMA_COD end as age, /*calcul de l'�ge*/
b.cam_prs_ide as code_ccam,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num
from oravue.er_prs_f a 
inner join oravue.er_cam_f b on
	a.FLX_DIS_DTD = b.FLX_DIS_DTD and a.FLX_TRT_DTD = b.FLX_TRT_DTD and a.FLX_EMT_TYP = b.FLX_EMT_TYP and a.FLX_EMT_NUM = b.FLX_EMT_NUM and a.FLX_EMT_ORD = b.FLX_EMT_ORD and a.ORG_CLE_NUM = b.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = b.DCT_ORD_NUM and a.PRS_ORD_NUM = b.PRS_ORD_NUM and a.REM_TYP_AFF = b.REM_TYP_AFF
left join oravue.ER_ETE_F c on 		  
	a.FLX_DIS_DTD = c.FLX_DIS_DTD and a.FLX_TRT_DTD = c.FLX_TRT_DTD and a.FLX_EMT_TYP = c.FLX_EMT_TYP and a.FLX_EMT_NUM = c.FLX_EMT_NUM and a.FLX_EMT_ORD = c.FLX_EMT_ORD and a.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = c.DCT_ORD_NUM and a.PRS_ORD_NUM = c.PRS_ORD_NUM and a.REM_TYP_AFF = c.REM_TYP_AFF
where a.FLX_DIS_DTD="&flx_dis_dtd:0:0:0"dt
      and a.exe_soi_dtd between ("&date_exe_deb:0:0:0"dt) and ("&date_exe_fin:0:0:0"dt) /*s�lection p�riode �tude*/
      AND a.BEN_CDI_NIR NOT IN ('01','05','06','08','09','11','12','13','14','15','48','46','47')  /*exclusion des nir fictifs*/
      and a.DPN_QLF not in (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      and b.cam_prs_ide in ('NZQN001','NZQJ001') and b.CAM_ACT_COD = '1' /*s�lection actes d'IRM du membre inf�rieur, activt�=1 */
group by a.exe_soi_dtd, a.ben_res_dpt, a.ben_res_com, a.ben_nir_psa, a.ben_rng_gem, a.BEN_SEX_COD,  a.pfs_exe_num,a.dpn_qlf, a.prs_nat_ref,a.BEN_AMA_COD,b.cam_prs_ide,c.ETE_IND_TAA,c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num;
quit;

/*Extraction du dpt des PS (table da_pra) et jointure avec la table des IRM*/
PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM,input(DTE_MOI_FIN,2.) as dte_fin
FROM ORAVUE.DA_PRA_R
WHERE DTE_ANN_TRT = "&an" AND (calculated DTE_FIN) = &mois1 ;/*mois de traitement*/
QUIT;
  
proc sql;
create table extract as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from extract a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;


/*S�lection des ATU sur la p�riode d'�tude pour suppression des IRM r�alis�es aux urgences (ES ex-OQN)*/
proc sql;
create table extract_atu as 
SELECT a.exe_soi_dtd,a.ben_nir_psa,a.ben_rng_gem,(SUM(CASE WHEN a.CPL_MAJ_TOP <2 THEN a.PRS_ACT_QTE ELSE 0 END)) as prs_act_qte
from oravue.er_prs_f a 
left join oravue.ER_ETE_F c on 		  
	a.FLX_DIS_DTD = c.FLX_DIS_DTD and a.FLX_TRT_DTD = c.FLX_TRT_DTD and a.FLX_EMT_TYP = c.FLX_EMT_TYP and a.FLX_EMT_NUM = c.FLX_EMT_NUM and a.FLX_EMT_ORD = c.FLX_EMT_ORD and a.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	a.DCT_ORD_NUM = c.DCT_ORD_NUM and a.PRS_ORD_NUM = c.PRS_ORD_NUM and a.REM_TYP_AFF = c.REM_TYP_AFF 
where a.FLX_DIS_DTD="&flx_dis_dtd:0:0:0"dt
      and a.exe_soi_dtd between ("&date_exe_deb:0:0:0"dt) and ("&date_exe_fin:0:0:0"dt)
      and a.DPN_QLF not in (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information */
      and a.prs_nat_ref=2238 /*S�lection code ATU*/
 group by a.exe_soi_dtd,a.ben_nir_psa,a.ben_rng_gem
 having (calculated prs_act_qte>0);
quit;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles (proc append)****/
%MACRO COMPILATION() ;
%IF %SYSFUNC(exist(extract_irm_&per._&anneem)) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change extract=extract_irm_&per._&anneem;
quit;
%END ;
%ELSE %DO;
proc append base=extract_irm_&per._&anneem data=extract FORCE;run;
proc delete data=extract;run;
%END;

%IF %SYSFUNC(exist(extract_atu_&per._&anneem)) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change extract_atu=extract_atu_&per._&anneem;
quit;
%END ;
%ELSE %DO;
proc append base=extract_atu_&per._&anneem data=extract_atu FORCE;run;
proc delete data=extract_atu;run;
%END;
%MEND COMPILATION ;


/*** Ex�cution pour chaque mois de flux***/
%MACRO TAB_PRS( Date_DEB= ,Date_FIN= ) ;

/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;
%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donn�e*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compil�e des mois extraits si ce n'est pas la 1�re ex�cution*/
%IF %SYSFUNC(exist(extract_irm_&per._&anneem)) %THEN %DO;
 	PROC DELETE DATA=extract_irm_&per._&anneem;
	RUN ;
%END;

%IF %SYSFUNC(exist(extract_atu_&per._&anneem)) %THEN %DO;
 	PROC DELETE DATA=extract_atu_&per._&anneem;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le d�but et la fin des soins */

/*BOUCLE SUR LES MOIS*/
	%DO mois=&iterDeb %TO &iterFin + 7 ;/*flux sur p�riode + 6 mois*/
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;
	%LET mois1 = %SYSFUNC( month( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1)))) ;/*s�lection mois dans DA_PRA_R*/
				 
	/*affichage dans le journal*/
	%PUT "Valeurs des param�tres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%put mois= &mois1;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;

	%CREA_TABLE(&FLX_DIS_DTD) ;
	%COMPILATION() ;			
%END ;

%MEND TAB_PRS;

/*lancement de la macro*/
OPTIONS MPRINT ;
 %TAB_PRS(Date_DEB = &date_debm,Date_FIN = &date_finm) ;


/* Exclusion de l'activit� des �tablissements publics ex-DG (ACE) => activit� r�cup�r�e dans le PMSI */
proc sql;
create table extract_irm_&per._&anneem as select * 
from extract_irm_&per._&anneem
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data extract_irm_&per._&anneem;
set extract_irm_&per._&anneem;
if prs_ppu_sec=1 then delete;
run;

/* Exclusion des IRM r�alis�es lors d'une hospitalisation dans un �tablissement priv� (ex-OQN) => hors p�rim�tre */
proc sql;
create table extract_irm_&per._&anneem as select * 
from extract_irm_&per._&anneem
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;

data extract_irm_&per._&anneem;
set extract_irm_&per._&anneem;
if mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
drop ete_ghs_num ete_ind_taa prs_ppu_sec dpn_qlf prs_nat_ref mdt_cod;
run;

/*Exclusion IRM DCIR si passages aux urgences dans la journ�e (ES ex-OQN) (m�me jour, m�me patient) => hors p�rim�tre*/
proc sort data=extract_irm_&per._&anneem; by ben_nir_psa ben_rng_gem exe_soi_dtd;run; 
proc sort data=extract_atu_&per._&anneem; by ben_nir_psa ben_rng_gem exe_soi_dtd;run; 

data extract_irm_dcir_&per._&anneem;
merge extract_irm_&per._&anneem (in=a) extract_atu_&per._&anneem (in=b);
by ben_nir_psa ben_rng_gem exe_soi_dtd;
if a and not b;
run;

/*Suppression des IRM si qte <= 0 */
proc sql;
create table extract_irm_dcir_&per._&anneem as select distinct exe_soi_dtd, pfs_exe_num,ben_res_dpt, ben_res_com,ben_nir_psa, ben_rng_gem, BEN_SEX_COD, age,code_ccam, PFS_COD_POS,PFS_EXC_COM,ETE_CAT_COD, etb_exe_fin,
sum (PRS_ACT_QTE) as nb_act 
from extract_irm_dcir_&per._&anneem 
group by exe_soi_dtd, pfs_exe_num,ben_res_dpt, ben_res_com, ben_nir_psa, ben_rng_gem, BEN_SEX_COD, age, code_ccam, PFS_COD_POS,PFS_EXC_COM,ETE_CAT_COD, etb_exe_fin
having (calculated nb_act >0);
quit;

 
/**************************************************************************EXTRACTION IRM � partir du PMSI (ACE)*************************************************************************/

/**************************************************************************
Filtres PMSI
---------------------------------------------------------------------------
Exclusion des finess geographiques APHP/APHM/HCL (donn�es en doublon sinon)
Exclusion des patients avec des codes retour incorrects (<>0)
Exclusion des IRM r�alis�es lors d'un passage aux urgences ES ex-DGF
S�lection du S2 2021 sur la date de l'acte du fichier RSF-M (variable dispo depuis 2020)
*************************************************************************/

proc sql; 
create table extract_irm_ace as select distinct 
a.nir_ano_17 as ben_nir_psa,b.eta_num_geo as finess_exec,b.ccam_cod as code_ccam,b.exe_soi_dtd,input(c.cod_sex,1.) as ben_sex_cod,
case when (substr(c.cod_post,1,2) in ('00','99') or substr(c.cod_post,1,1) not in ('0','1','2','3','4','5','6','7','8','9')) then '99'
     when (substr(c.cod_post,1,2) not in ('97','98')) then SUBSTR(c.cod_post,1,2) 
     else SUBSTR(c.cod_post,1,3) end as dpt_patient,
case when c.age_jou ne . then 0  else c.age_ann end as age /*calcul de l'�ge*/
from  oravue.t_mco&anneep.cstc as a 
		inner join oravue.t_mco&anneep.fmstc as b on a.eta_num=b.eta_num and b.seq_num=a.seq_num
		inner join oravue.t_mco&anneep.fastc as c  on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where b.ccam_cod in ('NZQN001','NZQJ001') and b.acv_act='1' /*S�lection IRM membre inf�rieur, activit� =1 */
and a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" ;/* On ne garde que les patients avec des codes retours corrects */
quit; 

/*Exclusion IRM PMSI ACE si passage aux urgences (m�me jour et m�me patient) => hors p�rim�tre*/
proc sql; 
create table extract_atu_ace as select distinct a.nir_ano_17 as ben_nir_psa,
case when (c.exe_soi_dtd ne .) then c.exe_soi_dtd
           else b.exe_soi_dtd end as exe_soi_dtd
from  oravue.t_mco&anneep.cstc as a 
		inner join oravue.t_mco&anneep.fbstc as b on a.eta_num=b.eta_num and b.seq_num=a.seq_num
		inner join oravue.t_mco&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where (b.act_cod='ATU' or c.act_cod='ATU') /*S�lection factures avec ATU RSF-B ou RSF-C*/
and a.eta_num not in (&fin_geo) /*exclusion des finess geographique APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" ;/* On ne garde que les patients avec des codes retours corrects */
quit; 

proc sort data=extract_irm_ace;by ben_nir_psa exe_soi_dtd;run;
proc sort data=extract_atu_ace;by ben_nir_psa exe_soi_dtd;run;

data extract_irm_ace;
merge extract_irm_ace (in=a) extract_atu_ace (in=b);
if a and not b;
by ben_nir_psa exe_soi_dtd;
run;

/*S�lection des IRM du S2 2021*/
proc sql;
create table extract_irm_ACE_&per._&anneem as select ben_nir_psa,finess_exec,code_ccam,exe_soi_dtd,ben_sex_cod,dpt_patient,age
from extract_irm_ACE
where month(datepart(exe_soi_dtd))>=&mois_deb and month(datepart(exe_soi_dtd<=&mois_fin))
group by ben_nir_psa,finess_exec,code_ccam,exe_soi_dtd,ben_sex_cod,dpt_patient,age;
quit;

/***************************************************************************
 			2 - SELECTION DES IDENTIFIANTS PATIENTS
***************************************************************************/
 
/*********************************************************************************SELECTION PMSI****************************************************************************************************/

/*On ne garde que les ben_nir_psa retrouv�s dans IR_BEN_R (car besoin de suivre le patient avant et apr�s l'IRM)
On conserve les ben_idt_top=0
On conserve infos sur age du patient pour correction si manquant dans le flux*/

proc sql;
  drop table orauser.extract_irm_ACE_&per._&anneem;
  CREATE TABLE orauser.extract_irm_ACE_&per._&anneem AS SELECT distinct ben_nir_psa from extract_irm_ACE_&per._&anneem;
QUIT;
  
 PROC SQL;
    CREATE table extract_irbenr_ace AS SELECT b.ben_nir_psa, b.BEN_RNG_GEM, b.ben_idt_ano,b.BEN_NAI_ANN
    FROM orauser.extract_irm_ACE_&per._&anneem a inner JOIN ORAVUE.IR_BEN_R b ON (a.ben_nir_psa = b.BEN_NIR_PSA)
    order by b.ben_nir_psa,b.ben_idt_top desc,b.ben_rng_gem, b.max_trt_dtd desc, b.ben_dte_ins desc, b.ben_dte_maj desc;
QUIT;

/*On supprime les �ventuels doublons de patients sur le ben_nir_psa*/
proc sort data=extract_irbenr_ace nodupkey;by ben_nir_psa;run;

/*Ajout du ben_idt_ano dans la table des ACE => suppression des patients non retrouv�s dans IR_BEN_R*/
proc sql;
create table extract_irm_ACE_&per._&anneem 
as select a.* , b.BEN_RNG_GEM, b.ben_idt_ano,&anneem-input(ben_nai_ann,4.) as age_irbenr
from extract_irm_ACE_&per._&anneem  a
inner join extract_irbenr_ace b on (a.ben_nir_psa=b.ben_nir_psa);
quit; 


/*********************************************************************************SELECTION DCIR****************************************************************************************************/

/*On ne garde que les couples ben_nir_psa||ben_rng_gem retrouv�s dans IR_BEN_R (car besoin de suivre le patient avant et apr�s l'IRM)
On conserve les ben_idt_top=0
On conserve infos sur �ge pour correction si manquant dans le flux*/

proc sql;
  drop table orauser.extract_irm_DCIR_&per._&anneem;
  CREATE TABLE orauser.extract_irm_DCIR_&per._&anneem AS SELECT distinct ben_nir_psa,ben_rng_gem from extract_irm_DCIR_&per._&anneem;
QUIT;
  
 PROC SQL;
    CREATE table extract_irbenr_dcir AS SELECT b.ben_nir_psa, b.BEN_RNG_GEM, b.ben_idt_ano,b.BEN_NAI_ANN
    FROM orauser.extract_irm_DCIR_&per._&anneem a inner JOIN ORAVUE.IR_BEN_R b ON (a.ben_nir_psa = b.BEN_NIR_PSA and a.ben_rng_gem = b.ben_rng_gem)
    order by b.ben_nir_psa,b.ben_idt_top desc,b.ben_rng_gem, b.max_trt_dtd desc, b.ben_dte_ins desc, b.ben_dte_maj desc;
QUIT;

/*On supprime les �ventuels doublons de patients sur le ben_nir_psa||ben_rng_gem*/
proc sort data=extract_irbenr_dcir nodupkey;by ben_nir_psa ben_rng_gem;run;

/*Ajout du ben_idt_ano dans la table DCIR => suppression des patients non retrouv�s dans IR_BEN_R*/
proc sql;
create table extract_irm_DCIR_&per._&anneem 
as select a.* , b.ben_idt_ano,&anneem-input(ben_nai_ann,4.) as age_irbenr
from extract_irm_DCIR_&per._&anneem  a
inner join extract_irbenr_dcir b on (a.ben_nir_psa=b.ben_nir_psa and a.ben_rng_gem=b.ben_rng_gem);
quit; 

/**************************************************SELECTION DE TOUS LES COUPLES BEN_NIR_PSA||BEN_RNG_GEM POSSIBLES POUR UN MEME BEN_IDT_ANO **********************************************************/

/*Cr�ation d'une table unique PMSI ACE et DCIR
Filtres
---------------------------------------------------------------------------
Exclusion des patients non retrouv�s dans la table IR_BEN_R (cf. parties pr�c�dentes)
Exclusion des patients < 18 ans apr�s correction des �ges manquants avec IR_BEN_R
*************************************************************************/

data extract_irm_&per._&anneem;
set extract_irm_DCIR_&per._&anneem (in=a drop=nb_act pfs_exe_num) extract_irm_ACE_&per._&anneem (in=b);
format type $8.;
if a then type='DCIR';
if b then type='PMSI_ACE';
if age in (9999,.) then age=age_irbenr; /*correction des �ge manquants du flux avec ceux de IR_BEN_R*/
if age<18 then delete; /* on ne garde que les patients adultes*/
run;

/*Recherche de tous les couples ben_nir_psa||ben_rng_gem possibles pour notre population dans IR_BEN_R*/
proc sql;
drop table orauser.id_patient;
create table orauser.id_patient as select distinct ben_idt_ano from extract_irm_&per._&anneem;
quit;

proc sql ;
create table sasdata1.IMI_id_patient_&per._&anneem as 
select distinct a.ben_idt_ano, a.ben_nir_psa, a.ben_rng_gem, a.ben_idt_top
FROM ORAVUE.IR_BEN_R a
inner join orauser.id_patient b on (a.ben_idt_ano = b.ben_idt_ano) 
order by ben_idt_ano;
quit;

proc freq data=sasdata1.IMI_id_patient_&per._&anneem;
table ben_idt_top ben_rng_gem;
run;


/***************************************************************************
3 - SELECTION DE L'IRM INDEX POUR CHAQUE PATIENT INCLUS DANS L'ETUDE
***************************************************************************/

/*Harmonisation donn�es DCIR avec PMSI:
on met les finess ex�cutants sur 9 caract�res au lieu de 8 => si finess pas retrouv� dans la table datasante, on laisse le finess sur 8 caract�res
On met le dpt du patient sur 3 caract�re si DOM/TOM + en 99 si format incorrect (+ correction dpt Corse et Monaco)
Cr�ation dpt ex�cutant: Dpt �tablissement si finess ex�cutant rempli (et pas en erreur) / Dpt du PS sinon*/

proc sql;
create table extract_irm_&per._&anneem as select a.*, b.finess as finessgeo_exec
from extract_irm_&per._&anneem a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);
quit;

data extract_irm_&per._&anneem;
set extract_irm_&per._&anneem;

if type='DCIR' then do;
finess_exec=finessgeo_exec;
if etb_exe_fin ne '' and finessgeo_exec='' then finess_exec=etb_exe_fin;

format dpt_patient $3.;
if substr(ben_res_dpt,1,2)='97' then  ben_res_dpt='097';
if substr(ben_res_dpt,1,2)='98' then  ben_res_dpt='098';
if ben_res_dpt ='209' then ben_res_dpt='020';
if substr(ben_res_dpt,1,1)='0' then do ; dpt_patient=substr(BEN_RES_DPT,2,2);end;else do; dpt_patient='99';end;
code_com=cats(dpt_patient,BEN_RES_COM);
if dpt_patient in ('97','98')  then dpt_patient=substr(code_com,1,3);
end;
if dpt_patient in ('981') then dpt_patient='980';
if dpt_patient in ('') then dpt_patient='99';

format dpt_exec $3.;
if finess_exec ne '' then do;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);
else dpt_exec=substr(finess_exec,1,2);
end;
if substr(finess_exec,1,4) ='2001' then dpt_exec='2A';
if substr(finess_exec,1,4) ='2002' then dpt_exec='2B';
if finess_exec=' ' or substr(finess_exec,1,2)='99' then do;
if substr(pfs_cod_pos,1,3)  in ('201','200') then dpt_exec='2A'; 
else if substr(pfs_cod_pos,1,3) in ('202','206') then dpt_exec='2B'; 
else if substr(pfs_cod_pos,1,3) = '980' then dpt_exec=substr(pfs_cod_pos,1,3);
else if substr(pfs_cod_pos,1,2) in ('97','98') then dpt_exec=SUBSTR(PFS_COD_POS,1,2) || substr(PFS_EXC_COM,1,1);
else dpt_exec=substr(pfs_cod_pos,1,2);
end;
if dpt_exec in ('981','') then dpt_exec='99';

drop finessgeo_exec etb_exe_fin ben_res_com code_com ben_res_dpt pfs_cod_pos pfs_exc_com;
run;

/*On cr�e une table avec les infos du patient et de l'IRM index => 1�re IRM du patient dans la p�riode d'�tude*/
Proc sort data=extract_irm_&per._&anneem; by ben_idt_ano EXE_SOI_DTD;run;

data sasdata1.IMI_patient_first_irm_&per._&anneem;
set extract_irm_&per._&anneem;
by ben_idt_ano EXE_SOI_DTD;
if first.ben_idt_ano;
drop age_irbenr ben_nir_psa ben_rng_gem;
run;

/*verifications de chaque variable*/
proc freq data=sasdata1.IMI_patient_first_irm_&per._&anneem;
table exe_soi_dtd BEN_SEX_COD age code_ccam ETE_CAT_COD finess_exec dpt_patient dpt_exec type;
run;

/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.extract_irm_ACE_&per._&anneem;
  drop table orauser.extract_irm_DCIR_&per._&anneem;
  drop table orauser.id_patient;
quit;




