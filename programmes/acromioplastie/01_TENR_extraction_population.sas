/**************************************************************************************************************************************************************
	 				DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D�UNE ACROMIOPLASTIE ISOLEE
EXTRACTION DE LA POPULATION D'ETUDE: 

	1 - EXTRACTION DES SEJOURS DE CHIRURGIE (PMSI MCO)
	2 - SELECTION DU SEJOUR DE CHIRURGIE DE REFERENCE POUR CHAQUE PATIENT
	3 - EXCLUSION DES PATIENTS AYANT ETE HOSPITALISES DANS LES 18 MOIS PRECEDENT LA CHIRURGIE	
	4 - SELECTION DE TOUS LES IDENTIFIANTS PATIENT POSSIBLES POUR UN MEME PATIENT
		
	Tables produites par le programme:	
		- TABLE sasdata1.TENR_patient_chir_S2_2022
		- TABLE sasdata1.TENR_id_patient_S2_2022
	
**************************************************************************************************************************************************************/

/***************************************************
Rappel p�rim�tre de l'�tude:
-----------------------------
S�jour avec au moins un acte de chirurgie d'une tendinopathie de la coiffe des rotateurs non rompue, � l'exclusion des s�jours avec �galement des actes chirurgicaux de tendinopathie de l'�paule rompue
Exclusion des s�jours avec diagnostic d'�paule traumatique S40* � S49* en DP / DA / DR 
Exclusion des patients ayant eu une hospitalisation en CMD 08, 21, 26 dans les 18 mois avant le s�jour de r�f�rence
En �tablissement MCO
Sur le 2�me semestre 2022
Patients >= 40 ans
***************************************************/

/***************************************************************D�finition des macrovariables**********************************************************************/

/*P�riode d'�tude*/
%let per=S2;
%let anneem=2022;
%let anneep=22;
%let anneep1=21;
%let mois_deb=7;

/*Finess g�o APHP/APHM/HCL*/
%let fin_geo ='130780521', '130783236', '130783293', '130784234', '130804297','600100101','750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166','750100208', '750100216', 
'750100232', '750100273', '750100299' , '750801441', '750803447', '750803454', '910100015','910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037','930100045', '940100027', 
'940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137','690784152', '690784178', '690787478', '830100558' ;

/*Actes, diagnostics et CMD*/
%let acte_chir_non_rompue='MEMA006','MEMA017','MEMC003','MEMC005','MEMA011','MEMC001';
%let diag_trauma='S40','S41','S42','S43','S44','S45','S46','S47','S48','S49';
%let acte_chir_rompue='MJMA003','MEFC002','MEFA003','MEJC001','MJEA006','MJEA010','MJEC002','MJEC001','MJDC001','MJDA001';
%let cmd='08','21','26';


/********************************************************************************************************************************************************************/

/*******************************************************************************************************************************
			1 - EXTRACTION DES SEJOURS DE CHIRURGIE
*******************************************************************************************************************************/

/**************************************************************************
Filtres sp�cifiques PMSI MCO
---------------------------------------------------------------------------
Exclusion des finess geographiques APHP/APHM/HCL (donn�es en doublon sinon)
Exclusion des patients avec des codes retour incorrects (<>0)
Exclusion des s�jours en CM90
Exclusion des s�jours en prestation inter-�tablissements
*************************************************************************/

/*1 - On s�lectionne les s�jours avec actes de chirurgie de la coiffe des rotateurs non rompue + actes correspondant*/
proc sql;
create table selec_chir_non_rompue as select distinct eta_num,rsa_num, cdc_act as code_ccam, NBR_EXE_ACT
from oravue.t_mco&anneep.a 
where CDC_ACT in (&acte_chir_non_rompue) and acv_act='1' /*s�lection actes chirurgie non rompue, activit� =1 */
order by eta_num, rsa_num; 
quit;

/*2 - On s�lectionne les s�jours avec actes de chirurgie de l'�paule rompue*/
proc sql;
create table selec_chir_rompue as select distinct eta_num,rsa_num 
from oravue.t_mco&anneep.a 
where CDC_ACT in (&acte_chir_rompue) and acv_act='1' /*s�lection actes chirurgie rompue, activit� =1 */
order by eta_num, rsa_num; 
quit;

/*3 - On s�lectionne les s�jours avec diagnostics d'�paules traumatiques*/
proc sql;
/*DP ou DR du s�jour*/
create table selec_dp_dr_sej_trauma as select distinct eta_num,rsa_num 
from oravue.t_mco&anneep.b 
where substr(dgn_pal,1,3) in (&diag_trauma) or substr(dgn_rel,1,3) in (&diag_trauma)
order by eta_num, rsa_num; 
/*DP ou DR du RUM*/
create table selec_dp_dr_rum_trauma as select distinct eta_num,rsa_num
from oravue.t_mco&anneep.um
where substr(dgn_pal,1,3) in (&diag_trauma) or substr(dgn_rel,1,3) in (&diag_trauma)
order by eta_num, rsa_num; 
/*DA*/
create table selec_da_trauma as select distinct eta_num,rsa_num
from oravue.t_mco&anneep.d
where substr(ass_dgn,1,3) in (&diag_trauma)
order by eta_num, rsa_num; 
quit;

data selec_diag_trauma;
merge selec_dp_dr_sej_trauma selec_dp_dr_rum_trauma selec_da_trauma;
by eta_num rsa_num;
run;

/*4 - On s�lectionne les s�jours des patients de plus de 40 ans entr�s sur le 2nd semestre 2022, hors CM90, hors finess geo APHP/HCL/APHM, hors PIE, hors codes retour incorrects + s�lection des variables utiles pour l'�tude*/
proc sql;
create table selec_sej as select distinct b.eta_num,b.rsa_num,b.sej_typ,b.cod_sex as sexe,
case when b.age_jou ne . then 0  
    else b.age_ann end as age,
case when (substr(b.cod_post,1,2) in ('00','99') or substr(b.cod_post,1,1) not in ('0','1','2','3','4','5','6','7','8','9')) then '99'
     when (substr(b.cod_post,1,2) not in ('97','98','20')) then substr(b.cod_post,1,2) 
     when (substr(b.cod_post,1,3) in ('201','200')) then '2A'
     when (substr(b.cod_post,1,3) in ('202','206')) then '2B'
     else substr(b.cod_post,1,3) end as dpt_patient,
c.exe_soi_dtd as date_entree,c.exe_soi_dtf as date_sortie,c.nir_ano_17 as ben_nir_psa,
um.eta_num_geo as finess_exec
from oravue.t_mco&anneep.c as c
inner join oravue.t_mco&anneep.b as b on (c.eta_num=b.eta_num and c.rsa_num=b.rsa_num)
inner join oravue.t_mco&anneep.um as um on  (c.eta_num=um.eta_num and c.rsa_num=um.rsa_num)
where c.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' /* On ne garde que les patients avec des codes retours corrects */
and b.grg_ghm not like "90%"  /*exclusion CM 90*/
and um.RUM_ORD_NUM=1 /*s�lection du 1er RUM du s�jour pour r�cup�rer le finess g�ographique*/
order by eta_num, rsa_num; 
quit;

/*exclusion PIE, s�jours des patients <40 ans, s�jours entr�s avt 1er semestre 2022*/
data selec_sej_ok;
set selec_sej;
if sej_typ='B' or month(datepart(date_entree))<&mois_deb or year(datepart(date_entree)) ne &anneem or age<40 then delete;
drop sej_typ;
run;

/*5 - On regroupe les tables cr��es pr�c�demment => cr�ation d'une table des identifiants de s�jours � inclure*/
proc sort data=selec_chir_non_rompue out=selec_chir_non_rompue_ (keep=eta_num rsa_num) nodupkey;by eta_num rsa_num;run;
proc sort data=selec_sej_ok out=selec_sej_ok_ (keep=eta_num rsa_num) nodupkey;by eta_num rsa_num;run;

data num_sej_chir_epaule_ok;
merge selec_chir_non_rompue_ (in=a) selec_chir_rompue (in=b) selec_diag_trauma (in=c) selec_sej_ok_ (in=d);
by eta_num rsa_num;
if a and d and not b and not c;
run;

/*6 - Construction de la table de travail � partir des identifiants s�jours s�lectionn�s: ajout et cr�ation des variables � garder pour l'analyse*/
data sej_chir_epaule_ok_&per._&anneem;
merge num_sej_chir_epaule_ok (in=a) selec_chir_non_rompue selec_sej_ok;
by eta_num rsa_num;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);/*cr�ation var dpt finess ex�cutant*/
else dpt_exec=substr(finess_exec,1,2);
if a;
run;

/*si plusieurs codes CCAM de chirurgie pour un s�jour, on les agr�ge sur une seule variable pour avoir une ligne par s�jour (code1 - code2 - ...) dans la table de travail*/
proc sort data=sej_chir_epaule_ok_&per._&anneem out=ccam(keep=eta_num rsa_num code_ccam);by eta_num rsa_num code_ccam;run;

data ccam;
set ccam;
format actes_ccam $100.;
by eta_num rsa_num code_ccam;
retain actes_ccam ;
if first.rsa_num then actes_ccam=code_ccam;
else actes_ccam=catx('-',actes_ccam,code_ccam) ;
if last.rsa_num then output;
keep eta_num rsa_num actes_ccam;
run;

/*on somme le nombre d'actes par s�jour*/
proc sql;
create table sej_chir_epaule_&per._&anneem as select distinct eta_num,rsa_num,ben_nir_psa,age,sexe,dpt_patient,date_entree,date_sortie,finess_exec, dpt_exec,sum(nbr_exe_act) as nb_actes
from sej_chir_epaule_ok_&per._&anneem
group by eta_num,rsa_num,ben_nir_psa,age,sexe,dpt_patient,date_entree,date_sortie,finess_exec, dpt_exec
order by eta_num,rsa_num;
quit;

/*table de travail*/
data sej_chir_epaule_&per._&anneem ;
merge sej_chir_epaule_&per._&anneem ccam;
by eta_num rsa_num;
run;

/*******************************************************************************************************************************
			2 - SELECTION DU SEJOUR DE CHIRURGIE DE REFERENCE POUR CHAQUE PATIENT
*******************************************************************************************************************************/

/********************************************************************************
Apr�s v�rifications: 
---------------------------------------------------------------------------
- Pas de jumeaux (pas de ben_rng_gem >1 pour les ben_nir_psa s�lectionn�s dans le PMSI)
- Pas de NIR fictif
- 16 ben_nir_psa non retrouv�s dans IR_BEN_R dont 14 hospitalis�s dans un �tab ex-OQN qui devraient donc �tre forc�ment dans le DCIR... D�cision de consid�rer ces ben_nir_psa comme des identifiants erron�s et de les supprimer de notre s�lection (car impossible � suivre dans le parcours pr�-op si les identifiants sont erron�s) 
- On conserve les ben_idt_top=0 (6 patients)
**********************************************************************************/

/* 1- S�lection du ben_idt_ano (identifiant patient) de nos patients � partir de la table ir_ben_r*/
proc sql;
  drop table orauser.extract_id;
  create table orauser.extract_id as select distinct ben_nir_psa from sej_chir_epaule_&per._&anneem;
quit;

proc sql;
    create table extract_irbenr as select distinct b.ben_nir_psa, b.ben_rng_gem, b.ben_idt_ano, b.ben_idt_top, b.ben_cdi_nir
    from orauser.extract_id a 
    inner join oravue.ir_ben_r b on (a.ben_nir_psa = b.ben_nir_psa)
    order by b.ben_nir_psa,b.ben_idt_top desc,b.ben_rng_gem, b.max_trt_dtd desc, b.ben_dte_ins desc, b.ben_dte_maj desc;
quit;

/* 2- Ajout du ben_idt_ano dans la table de travail => suppression des patients non retrouv�s dans IR_BEN_R*/
proc sort data=extract_irbenr out=extract_irbenr_ nodupkey;by ben_nir_psa;run;

proc sql;
create table sej_chir_epaule_ok_&per._&anneem 
as select a.* , b.ben_idt_ano
from sej_chir_epaule_&per._&anneem  a
inner join extract_irbenr_ b on (a.ben_nir_psa=b.ben_nir_psa);
quit; 

/* 3 - On s�lectionne la 1�re chirurgie de l'�paule pour chaque patient (si plusieurs chirurgies pour un m�me patient sur le 2nd semestre 2022) => Chirurgie de r�f�rence*/
proc sort data=sej_chir_epaule_ok_&per._&anneem;by ben_idt_ano date_entree date_sortie;run;

data patient_chir_&per._&anneem;
set sej_chir_epaule_ok_&per._&anneem;
by ben_idt_ano date_entree date_sortie;
if first.ben_idt_ano;
run;


/*******************************************************************************************************************************
			3 - EXCLUSION DES PATIENTS AYANT ETE HOSPITALISES DANS LES 18 MOIS PRECEDENT LA CHIRURGIE
*******************************************************************************************************************************/

/*****************************************************************************
Exclusion:
-------------------------------------------------------------------------
Patients ayant �t� hospitalis�s dans les 18 mois pr�c�dent le s�jour de chirurgie r�f�rence => 540 jours entre date d'entr�e du s�jour de chirurgie de r�f�rence et date de sortie du s�jour ant�rieur 
En CMD 08, 21, 26 
*****************************************************************************/

/* 1 - S�lection des hospitalisations PMSI MCO 2021 et 2022*/
proc sql;
/*2022*/
create table selec_hosp_18m1 as select distinct c.nir_ano_17 as ben_nir_psa,c.eta_num,c.rsa_num,c.exe_soi_dtf as date_hosp_prec,b.grg_ghm as ghm_prec
from oravue.t_mco&anneep.c as c
inner join oravue.t_mco&anneep.b as b on (c.eta_num=b.eta_num and c.rsa_num=b.rsa_num)
where c.eta_num not in (&fin_geo) 
and c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0'
and b.grg_ghm not like "90%"  
order by eta_num, rsa_num; 
/*2021*/
create table selec_hosp_18m2 as select distinct c.nir_ano_17 as ben_nir_psa,c.exe_soi_dtf as date_hosp_prec,b.grg_ghm as ghm_prec
from oravue.t_mco&anneep1.c as c
inner join oravue.t_mco&anneep1.b as b on (c.eta_num=b.eta_num and c.rsa_num=b.rsa_num)
where c.eta_num not in (&fin_geo) 
and c.NIR_RET='0' and c.NAI_RET='0' and c.SEX_RET='0' and c.SEJ_RET='0' and c.FHO_RET='0' and c.PMS_RET='0' and c.DAT_RET='0' and c.COH_NAI_RET='0' and c.COH_SEX_RET='0' 
and b.grg_ghm not like "90%"  
order by nir_ano_17; 
quit;

proc sort data=patient_chir_&per._&anneem;by eta_num rsa_num;run;

/* 2 - Suppression de l'hospit de chirurgie de l'�paule de r�f�rence de la s�lection des hospitalisations 2022*/
data selec_hosp_18m1;
merge selec_hosp_18m1 (in=a) patient_chir_&per._&anneem (in=b);
by eta_num rsa_num;
if a and not b;
keep ben_nir_psa date_hosp_prec ghm_prec;
run;

data selec_hosp_18m;
set selec_hosp_18m1 selec_hosp_18m2;
run;

/* 3 - On s�lectionne uniquement les s�jours 2021/2022 des patients ayant eu une chirurgie de l'�paule sur le S2 2022 (=chirurgie de r�f�rence) et on ajoute la date d'entr�e de ce s�jour de chirurgie*/
proc sort data=selec_hosp_18m;by ben_nir_psa;run;
proc sort data=patient_chir_&per._&anneem out=t1 (keep=ben_nir_psa date_entree rename=date_entree=date_chir);by ben_nir_psa;run;

data selec_hosp_18m;
merge selec_hosp_18m (in=a) t1 (in=b);
by ben_nir_psa;
if a and b;
run;

/* 4 - Parmi les s�jours pr�c�dents, s�lection des patients hospitalis�s en CMD 08, 21, 26 dans les 18 mois pr�c�dent l'entr�e du s�jour de chirugie de l'�paule de r�f�rence (540 jours)*/
data selec_hosp_18mois;
set selec_hosp_18m;
if date_hosp_prec>=date_chir then delete;/*suppression des hospitalisations survenues apr�s la chirurgie de l'�paule*/
delai=datepart(date_chir)-datepart(date_hosp_prec);
if delai<=540 and substr(ghm_prec,1,2) in (&cmd)  then output;
drop ghm_prec;
run;

proc sort data=selec_hosp_18mois (keep=ben_nir_psa) nodupkey;by ben_nir_psa;run;

/* 5 - On exclut de notre table de travail les patients ayant �t� hospitalis�s en CMD 08, 21 ou 26 dans les 18 mois pr�c�dent la chirurgie de l'�paule + cr�ation de la table de travail finale dans sasdata1*/
proc sort data=patient_chir_&per._&anneem;by ben_nir_psa;run;

data sasdata1.TENR_patient_chir_&per._&anneem ;
merge patient_chir_&per._&anneem (in=a) selec_hosp_18mois (in=b);
by ben_nir_psa;
if a and not b;
drop eta_num rsa_num ben_nir_psa;
run;

/*******************************************************************************************************************************
			4 - SELECTION DE TOUS LES IDENTIFIANTS PATIENT POSSIBLES POUR UN MEME PATIENT
*******************************************************************************************************************************/

/* Recherche de tous les couples ben_nir_psa||ben_rng_gem possibles dans IR_BEN_R pour un m�me BEN_IDT_ANO + cr�ation de la table de l'ensemble des identifiants patient possibles dans sasdata1*/
proc sql;
drop table orauser.id_patient;
create table orauser.id_patient as select distinct ben_idt_ano from sasdata1.TENR_patient_chir_S2_2022;
quit;

proc sql ;
create table sasdata1.TENR_id_patient_&per._&anneem as 
select distinct a.ben_idt_ano, a.ben_nir_psa, a.ben_rng_gem, a.ben_idt_top
FROM ORAVUE.IR_BEN_R a
inner join orauser.id_patient b on (a.ben_idt_ano = b.ben_idt_ano) 
order by ben_idt_ano;
quit;

/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.extract_id;
  drop table orauser.id_patient;
quit;


