/**************************************************************************************************************************************************************
							DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D�UNE ACROMIOPLASTIE ISOLEE
	 

CREATION DES TABLEAUX DESCRIPTIFS DE RESTITUTION DES RESULTATS:
	1 - DESCRIPTION DES PATIENTS ET DES ACTES DE CHIRURGIE DE L'EPAULE DE REFERENCE
	2 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI GLOBAL SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE
	3 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI PAR TRIMESTRE SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE
	4 - CALCUL D'INDICATEURS COMPLEMENTAIRES
	5 - PRESCRIPTEURS CORTICOIDES ET KINESITHERAPIE
	6 - MISE EN FORME DES DONNEES POUR REPRESENTATIONS GRAPHIQUES SOUS R

****************************************************************************************************************************************************************/

/***************************************************************D�finition des macrovariables**********************************************************************/
%let per=S2;/*p�riode �tude*/
%let anneem=2022;/*ann�e �tude*/

/*date de traitement*/
data _null_;
call symput("datetrt",left(put("&sysdate"d,ddmmyy10.)));
run;

proc sql noprint;
/*nb patients*/
select count(distinct ben_idt_ano) into:nb_patient from sasdata1.TENR_patient_chir_&per._&anneem;
/*nb corticoides injectables*/
select sum(prs_act_qte) into:nb_corticoide from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide";
/*nb actes kin�sith�rapie*/
select sum(prs_act_qte) into:nb_kine from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine";
/*nb patients avec corticoides injectables*/
select count(distinct ben_idt_ano) into:nb_pat_corticoide from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide";
/*nb patients avec actes kin�sith�rapie*/
select count(distinct ben_idt_ano) into:nb_pat_kine from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine";
quit;

/*Macros pour calcul Q1 et Q3 variables de d�lai et variable nb d'actes*/

/*D�lais*/
%macro q1_q3_delai_detail (num=,name=,condition=);
Proc MEANS Data=presta_detail_pre_op noprint ;
var &name._delai_pre_op;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

%macro q1_q3_delai_type (num=,name=,condition=);
Proc MEANS Data=type_presta_pre_op noprint ;
var &name._delai_pre_op;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

%macro q1_q3_delai_gpe (num=,name=,condition=);
Proc MEANS Data=gpe_presta_pre_op noprint ;
var &name._delai_pre_op;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

/*Nb actes*/
%macro q1_q3_acte_detail (num=,name=,condition=);
Proc MEANS Data=presta_detail_pre_op noprint ;
var prs_act_qte;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

%macro q1_q3_acte_type (num=,name=,condition=);
Proc MEANS Data=type_presta_pre_op noprint ;
var prs_act_qte;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

%macro q1_q3_acte_gpe (num=,name=,condition=);
Proc MEANS Data=gpe_presta_pre_op noprint ;
var prs_act_qte;
output out=t&num._ (drop=_type_ _freq_) q1=q1&name q3=q3&name;
&condition;
run;

data t&num;
merge t&num t&num._;
run;
%mend;

/***************************************************************D�finition des formats**********************************************************************/
proc format;

/*1 - DESCRIPTION DES PATIENTS ET DES ACTES DE CHIRURGIE DE L'EPAULE DE REFERENCE*/
value $desc_pat
'1'="Nombre de patients"
'2'="Age moyen"
'3'="Age m�dian"
'4'="Q1 Age"
'5'="Q3 Age"
'6'="% Plus de 65 ans"
'7'="% Hommes"
'8'="Nb femmes"
;

value $ccamfmt
'MEMA006'="MEMA006 Acromioplastie sans proth�se, par abord direct"
'MEMA017'="MEMA017 Acromioplastie sans proth�se avec arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par abord direct"
'MEMC003'="MEMC003 Acromioplastie sans proth�se, par arthroscopie"
'MEMC005'="MEMC005 Acromioplastie sans proth�se avec arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie"
'MEMA011'="MEMA011 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthrotomie"
'MEMC001'="MEMC001 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie"
'MEMC001-MEMC003'="MEMC001 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie - MEMC003 Acromioplastie sans proth�se, par arthroscopie"
'MEMA006-MEMA011'="MEMA006 Acromioplastie sans proth�se, par abord direct - MEMA011 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthrotomie"
'MEMA011-MEMC003'="MEMA011 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthrotomie - MEMC003 Acromioplastie sans proth�se, par arthroscopie"
'MEMC001-MEMC005'="MEMC001 Arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie - MEMC005 Acromioplastie sans proth�se avec arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie"
'MEMC003-MEMC005'="MEMC003 Acromioplastie sans proth�se, par arthroscopie - MEMC005 Acromioplastie sans proth�se avec arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par arthroscopie"
'MEMA006-MEMA017'="MEMA006 Acromioplastie sans proth�se, par abord direct - MEMA017 Acromioplastie sans proth�se avec arthroplastie acromioclaviculaire par r�section de l'extr�mit� lat�rale de la clavicule, par abord direct"
;

/*2 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE*/
value $suivi0_
'0'="Dans les 18 mois avant la chirurgie de l'�paule"
;

value $suivi1_
'0'="Corticoide injectable"
'1'="Infiltration sans guidage (sauf patients avec cortico�de injectable)"
'2'="Infiltration sous guidage (sauf patients avec cortico�de injectable)"
'3'="Total des infiltrations"
'4'="Acte de kin�sith�rapie de r��ducation"
'5'="Total des soins conservateurs"
'6'="Aucun soin conservateur"
;

value $suivi2_
'1'="Radiographie"
'2'="Echographie"
'3'="Total des actes d'imagerie de d�marche diagnostique"
'4'="Aucun acte d'imagerie de d�marche diagnostique"
;

value $suivi3_
'1'="IRM"
'2'="Arthro-IRM"
'3'="Arthro-scanner"
'4'="Total des actes d'imagerie avanc�e"
'5'="Aucun acte d'imagerie avanc�e"
;

value $suivi4_
'1'="Scanner"
;

value $suivi5_
'1'="Consultation de rhumatologie"
'2'="Consultation d'orthop�die"
'3'="Consultation de MPR"
'4'="Total consultations sp�cialis�es"
'5'="Aucune consultation sp�cialis�e"
'6'="Consultation de MG"
'7'="Total consultations"
'8'="Aucune consultation"
;

value $trimfmt
'T1'="1er trimestre avt chirurgie"
'T2'="2e trimestre avt chirurgie"
'T3'="3e trimestre avt chirurgie"
'T4'="4e trimestre avt chirurgie"
'T5'="5e trimestre avt chirurgie"
'T6'="6e trimestre avt chirurgie"
;

value $indic1_
'1'="Patients avec kin�sith�rapie et infiltration"
'2'="Patients avec infiltration sans kin�sith�rapie"
'3'="Patients avec radiographie et �chographie"
'4'="Patients avec �chographie sans radiographie"
'5'="Patients avec radiogaphie avant �chographie (*)"
'6'="Patients avec radiographie avant infiltration (**)"
'7'="Patients avec radiographie en premi�re intention (radiographie sans autre imagerie ou avant autre imagerie (IRM/arhroscanner/arthroIRM, �chographie, scanner))"
'8'="Patients avec �chographie sans IRM/arthro-IRM/arthro-scanner"
'9'="Patients avec �chographie sans IRM/arthro-IRM/arthro-scanner, radiographie, kin�sith�rapie, infiltration"
'10'="Patients avec scanner sans IRM/arthro-IRM/arthro-scanner, radiographie, �chographie"
'11'="Patients avec scanner sans IRM/arthro-IRM/arthro-scanner, �chographie"
'12'="Patients sans imagerie (IRM/arthro-IRM/arthro-scanner, radiographie, �chographie, scanner)"
'13'="Patients vus par un orthop�diste une seule fois"
'14'="Patients avec seulement consultations orthop�diste sans consultation rhumatologue ou MPR"
'15'="Actes CCAM d'infiltration sans guidage"
'16'="Actes CCAM d'infiltration sous guidage"
'17'="Total actes CCAM d'infiltration (sans ou sous guidage)"
'18'="Patients avec radiographie en premi�re intention parmi les patients ayant commenc� les soins conservateurs moins d'un an avant la chirurgie"
;

value $typefmt
'0'="Kin�sith�rapie"
'1'="Corticoides injectables"
;

value $indic2_
'1'="MG"
'2'="Rhumatologue"
'3'="Orthop�diste"
'4'="MPR"
'5'="Radiologue"
'6'="Autres sp�cialit�s m�dicales"
'7'="Sp�cialit� non renseign�e"
;

run;


/**********************************************************Param�trage fichier de sortie******************************************************************************************************/
Footnote "A0"X;
ods excel file="./TENR_res_description.xlsx" ;
ods excel options(start_at="B2" sheet_interval='none' embedded_titles="yes" embedded_footnotes='yes' sheet_name="Tableaux");
ods text="DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D�UNE ACROMIOPLASTIE ISOLEE";
ods text="Chirurgie de l'�paule r�alis�e sur le 2nd semestre 2022 - suivi pr�-op�ratoire de 18 mois avant la chirurgie - patients >= 40 ans";
ods text=" ";
ods text="Date de traitement: &datetrt";
ods text=" ";
ods text=" ";
/*************************************************************************************************************************************************************************************************/

/*********************************************************************************************************
			1 - DESCRIPTION DES PATIENTS ET DES ACTES DE CHIRURGIE DE L'EPAULE DE REFERENCE
***********************************************************************************************************/

/* 1 - DESCRIPTION DES PATIENTS*/
ods text=" ";
ods text="1 - DESCRIPTION DES PATIENTS";
ods text=" ";

proc sql noprint;
create table t1 as select '1' as indic,count(distinct ben_idt_ano) as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
create table t2 as select '2' as indic,round(mean(age),0.1) as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
create table t3 as select '3' as indic,round(median(age),0.1) as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
create table t5 as select '6' as indic,round((sum(age>65)*100)/&nb_patient,0.1) as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
create table t6 as select '7' as indic,round((sum(sexe='1')*100)/&nb_patient,0.1) as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
create table t7 as select '8' as indic,sum(sexe='2') as valeur from sasdata1.TENR_patient_chir_&per._&anneem;
quit;

/*Calcul Q1, Q3 Age*/
Proc MEANS Data=sasdata1.TENR_patient_chir_&per._&anneem noprint;
var age;
output out=t4_ (drop=_type_ _freq_) q1=q1 q3=q3;
run;

proc transpose data=t4_ out=t4;run;

data t4;
set t4;
if _name_="q1" then indic="4";
if _name_="q3" then indic="5";
drop  _name_;
rename col1=valeur;
run;

data res;
set t1 t2 t3 t4 t5 t6 t7;
run;

proc report data=res nowd  split='*'; 
columns  indic valeur;
define indic/format=$desc_pat. "Indicateurs" order=data;
define valeur/ "Valeur";
run;

/* 2 - DESCRIPTION DES ACTES DE CHIRURGIE DE L'EPAULE*/
ods text=" ";
ods text="2 - DESCRIPTION DES ACTES DE CHIRURGIE DE L'EPAULE";
ods text=" ";

proc sql noprint;
create table res as select actes_ccam,count(distinct ben_idt_ano) as nb_patient,sum(nb_actes) as nb_actes from sasdata1.TENR_patient_chir_&per._&anneem group by actes_ccam order by nb_patient desc;
quit;

proc report data=res nowd  split='*'; 
columns  actes_ccam nb_patient nb_actes;
define actes_ccam/format=$ccamfmt. "Actes ou association d'actes CCAM";
define nb_patient/ "Nb patients";
define nb_actes/"Nb actes";
RBREAK AFTER /SUMMARIZE;
run;


/**************************************************************************************************************************************
			2 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI GLOBAL SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE
***************************************************************************************************************************************/

/*Cr�ation des tables n�cessaires � l'analyse:
- gpe_presta_pre_op => contient par patient et par grand groupe de prestation: le nombre de prestations, la date et le d�lai de la derni�re prestation avant la chirurgie de l'�paule et la date et le d�lai de la premi�re prestation avant la chirurgie de l'�paule 
- presta_detail_pre_op => contient par patient et par prestation d�taill�e: le nombre de prestations, la date et le d�lai de la derni�re prestation avant la chirurgie de l'�paule et la date et le d�lai de la premi�re prestation avant la chirurgie de l'�paule */

proc sql;
/*gpe presta*/
create table gpe_presta_pre_op as select distinct ben_idt_ano,gpe_prestation,min(delai_pre_op) as last_delai_pre_op,max(exe_soi_dtd) as last_date format=DATETIME20.,max(delai_pre_op) as first_delai_pre_op,min(exe_soi_dtd) as first_date format=DATETIME20. ,sum(prs_act_qte) as prs_act_qte
from sasdata1.TENR_parcours_pre_op_&per._&anneem group by ben_idt_ano,gpe_prestation;
/*type presta*/
create table type_presta_pre_op as select distinct ben_idt_ano,type_presta,min(delai_pre_op) as last_delai_pre_op,max(exe_soi_dtd) as last_date format=DATETIME20.,max(delai_pre_op) as first_delai_pre_op,min(exe_soi_dtd) as first_date format=DATETIME20. ,sum(prs_act_qte) as prs_act_qte
from sasdata1.TENR_parcours_pre_op_&per._&anneem group by ben_idt_ano,type_presta;
/*presta d�taill�e*/
create table presta_detail_pre_op as select distinct ben_idt_ano,presta_detail,min(delai_pre_op) as last_delai_pre_op,max(exe_soi_dtd) as last_date format=DATETIME20.,max(delai_pre_op) as first_delai_pre_op,min(exe_soi_dtd) as first_date format=DATETIME20. ,sum(prs_act_qte) as prs_act_qte
from sasdata1.TENR_parcours_pre_op_&per._&anneem group by ben_idt_ano,presta_detail;
quit;

/************************************************************************************/
ods text=" ";
ods text= "3 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI GLOBAL SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE";


/* 1 - SOINS CONSERVATEURS SUR LES 18 MOIS AVANT LA CHIRURGIE (infiltration, kin�sith�rapie)*/
ods text=" ";
ods text= "Soins conservateurs r�alis�s sur les 18 mois avant la chirurgie (infiltration, kin�sith�rapie)";

proc sql noprint;
/*Corticoides*/
create table t0 as select '0' as suivi, '0' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="corticoide";
/*Infiltration sans guidage*/
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="infiltration_sans_guidage";
/*Infiltration sous guidage*/
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="infiltration_sous_guidage";
/*Total infiltrations*/
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from type_presta_pre_op where type_presta ="Infiltration";
/*kin�*/
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="kine";
/*Total soins conservateurs*/
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(min(prs_act_qte),0.1) as min_c,round(max(prs_act_qte),0.1) as max_c,round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from gpe_presta_pre_op where gpe_prestation="soins_conservateurs";
quit;

/*Calcul Q1 Q3 d�lais*/
%q1_q3_delai_detail (num=0,name=first,condition=where PRESTA_DETAIL="corticoide");%q1_q3_delai_detail (num=0,name=last,condition=where PRESTA_DETAIL="corticoide");
%q1_q3_delai_detail (num=1,name=first,condition=where PRESTA_DETAIL="infiltration_sans_guidage");%q1_q3_delai_detail (num=1,name=last,condition=where PRESTA_DETAIL="infiltration_sans_guidage");
%q1_q3_delai_detail (num=2,name=first,condition=where PRESTA_DETAIL="infiltration_sous_guidage");%q1_q3_delai_detail  (num=2,name=last,condition=where PRESTA_DETAIL="infiltration_sous_guidage");
%q1_q3_delai_type (num=3,name=first,condition=where type_presta= "Infiltration");%q1_q3_delai_type  (num=3,name=last,condition=where type_presta= "Infiltration");
%q1_q3_delai_detail (num=4,name=first,condition=where PRESTA_DETAIL="kine");%q1_q3_delai_detail  (num=4,name=last,condition=where PRESTA_DETAIL="kine");
%q1_q3_delai_gpe (num=5,name=first,condition=where gpe_prestation="soins_conservateurs");%q1_q3_delai_gpe (num=5,name=last,condition=where gpe_prestation="soins_conservateurs");

/*Calcul Q1 Q3 nb actes*/
%q1_q3_acte_detail (num=0,name=nbacte,condition=where PRESTA_DETAIL="corticoide");
%q1_q3_acte_detail (num=1,name=nbacte,condition=where PRESTA_DETAIL="infiltration_sans_guidage");
%q1_q3_acte_detail (num=2,name=nbacte,condition=where PRESTA_DETAIL="infiltration_sous_guidage");
%q1_q3_acte_type (num=3,name=nbacte,condition=where type_presta= "Infiltration");
%q1_q3_acte_detail (num=4,name=nbacte,condition=where PRESTA_DETAIL="kine");
%q1_q3_acte_gpe (num=5,name=nbacte,condition=where gpe_prestation="soins_conservateurs");

/*Aucun soin conservateur*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;
proc sort data=gpe_presta_pre_op out=s0 (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="soins_conservateurs";run;

data t6;
merge patient (in=a) s0 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t6;
quit;

data res;
set t0 t1 t2 t3 t4 t5 t6;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct nb_acte moy_c med_c min_c q1nbacte q3nbacte max_c moy_firstd med_firstd q1first q3first moy_lastd med_lastd q1last q3last) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi1_. "Soins conservateurs" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define min_c/ "Nb minimum par patient";
define q1nbacte / "Q1 Nb actes";
define q3nbacte / "Q3 Nb actes";
define max_c/ "Nb maximum par patient";
define moy_firstd/ "D�lai moyen en jr entre 1er acte et chirurgie";
define med_firstd/ "D�lai m�dian en jr entre 1er acte et chirurgie";
define q1first/"Q1 D�lai";
define q3first/"Q3 D�lai";
define moy_lastd/ "D�lai moyen en jr entre dernier acte et chirurgie";
define med_lastd/ "D�lai m�dian en jr entre dernier acte et chirurgie";
define q1last/"Q1 D�lai";
define q3last/"Q3 D�lai";
;
run;


/* 2 - ACTES D'IMAGERIE DE DEMARCHE DIAGNOSTIQUE SUR LES 18 MOIS AVANT LA CHIRURGIE (radiographie, �chographie)*/
ods text=" ";
ods text="Actes d'imagerie de d�marche diagnostique r�alis�s sur les 18 mois avant la chirurgie (radiographie, �chographie)";

proc sql noprint;
/*Radiographie*/
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="radio";
/*Echographie*/
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="echo";
/*Total actes d�marche diagnostique*/
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from gpe_presta_pre_op where gpe_prestation="imagerie_demarche_diagnostique";
quit;

/*Calcul Q1 Q3 d�lais*/
%q1_q3_delai_detail (num=1,name=first,condition=where PRESTA_DETAIL="radio");%q1_q3_delai_detail (num=1,name=last,condition=where PRESTA_DETAIL="radio");
%q1_q3_delai_detail (num=2,name=first,condition=where PRESTA_DETAIL="echo");%q1_q3_delai_detail  (num=2,name=last,condition=where PRESTA_DETAIL="echo");
%q1_q3_delai_gpe (num=3,name=first,condition=where gpe_prestation="imagerie_demarche_diagnostique");%q1_q3_delai_gpe (num=3,name=last,condition=where gpe_prestation="imagerie_demarche_diagnostique");

/*Aucun acte d�marche diagnostique*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;
proc sort data=gpe_presta_pre_op out=s0 (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="imagerie_demarche_diagnostique";run;

data t4;
merge patient (in=a) s0 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t4;
quit;

data res;
set t1 t2 t3 t4;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct nb_acte moy_c med_c moy_firstd med_firstd q1first q3first moy_lastd med_lastd q1last q3last) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi2_. "Actes imagerie d�marche diagnostique" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_firstd/ "D�lai moyen en jr entre 1er acte et chirurgie";
define med_firstd/ "D�lai m�dian en jr entre 1er acte et chirurgie";
define q1first/"Q1 D�lai";
define q3first/"Q3 D�lai";
define moy_lastd/ "D�lai moyen en jr entre dernier acte et chirurgie";
define med_lastd/ "D�lai m�dian en jr entre dernier acte et chirurgie";
define q1last/"Q1 D�lai";
define q3last/"Q3 D�lai";
;
run;


/* 3 - ACTES D'IMAGERIE AVANCEE SUR LES 18 MOIS AVANT LA CHIRURGIE (IRM, arthro-IRM, arthro-scanner)*/
ods text=" ";
ods text="Actes d'imagerie avanc�e r�alis�s sur les 18 mois avant la chirurgie (IRM, arthro-IRM, arthro-scanner)";

proc sql noprint;
/*IRM*/
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="IRM";
/*Artho-IRM*/
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="arthro_irm";
/*Arthro-scanner*/
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="arthro_scanner";
/*Total actes imagerie avanc�e*/
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from gpe_presta_pre_op where gpe_prestation="imagerie_avancee";
quit;

/*Calcul Q1 Q3 d�lais*/
%q1_q3_delai_detail (num=1,name=first,condition=where PRESTA_DETAIL="IRM");%q1_q3_delai_detail (num=1,name=last,condition=where PRESTA_DETAIL="IRM");
%q1_q3_delai_detail (num=2,name=first,condition=where PRESTA_DETAIL="arthro_irm");%q1_q3_delai_detail  (num=2,name=last,condition=where PRESTA_DETAIL="arthro_irm");
%q1_q3_delai_detail (num=3,name=first,condition=where PRESTA_DETAIL="arthro_scanner");%q1_q3_delai_detail  (num=3,name=last,condition=where PRESTA_DETAIL="arthro_scanner");
%q1_q3_delai_gpe (num=4,name=first,condition=where gpe_prestation="imagerie_avancee");%q1_q3_delai_gpe (num=4,name=last,condition=where gpe_prestation="imagerie_avancee");

/*Aucun acte imagerie avanc�e*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;
proc sort data=gpe_presta_pre_op out=s0 (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="imagerie_avancee";run;

data t5;
merge patient (in=a) s0 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t5;
quit;

data res;
set t1 t2 t3 t4 t5;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct nb_acte moy_c med_c moy_firstd med_firstd q1first q3first moy_lastd med_lastd q1last q3last) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi3_. "Actes imagerie avanc�e" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_firstd/ "D�lai moyen en jr entre 1er acte et chirurgie";
define med_firstd/ "D�lai m�dian en jr entre 1er acte et chirurgie";
define q1first/"Q1 D�lai";
define q3first/"Q3 D�lai";
define moy_lastd/ "D�lai moyen en jr entre dernier acte et chirurgie";
define med_lastd/ "D�lai m�dian en jr entre dernier acte et chirurgie";
define q1last/"Q1 D�lai";
define q3last/"Q3 D�lai";
;
run;


/* 4 - SCANNER SUR LES 18 MOIS AVANT LA CHIRURGIE*/
ods text=" ";
ods text="Scanners r�alis�s sur les 18 mois avant la chirurgie";

proc sql noprint;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="scanner";
quit;

/*Calcul Q1 Q3 d�lais*/
%q1_q3_delai_detail (num=1,name=first,condition=where PRESTA_DETAIL="scanner");%q1_q3_delai_detail (num=1,name=last,condition=where PRESTA_DETAIL="scanner");

data res;
set t1;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct nb_acte moy_c med_c moy_firstd med_firstd q1first q3first moy_lastd med_lastd q1last q3last) ;
define suivi/across "A0"X format=$suivi4_. order=data;
define indic/ group format=$suivi4_. "Scanner" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_firstd/ "D�lai moyen en jr entre 1�re consult et chirurgie";
define med_firstd/ "D�lai m�dian en jr entre 1�re consult et chirurgie";
define q1first/"Q1 D�lai";
define q3first/"Q3 D�lai";
define moy_lastd/ "D�lai moyen en jr entre derni�re consult et chirurgie";
define med_lastd/ "D�lai m�dian en jr entre derni�re consult et chirurgie";
define q1last/"Q1 D�lai";
define q3last/"Q3 D�lai";
;
run;


/* 5 - CONSULTATIONS SUR LES 18 MOIS AVANT LA CHIRURGIE (MG, ortho, rhumato, MPR)*/
ods text=" ";
ods text="Consultations r�alis�es sur les 18 mois avant la chirurgie (ortho, rhumato, MPR, MG)";

/*Cr�ation table sp�cifique agr�g�e pour calcul quartiles et moyenne consultations sp�cialis�es*/
data presta_detail_pre_op_;
set presta_detail_pre_op;
if PRESTA_DETAIL in ("C_MPR","C_ortho","C_rhumato") then presta="consultations sp�cialis�es";
else presta=presta_detail;
run;

proc sql;
create table presta_detail_pre_op_ as select distinct ben_idt_ano,presta,min(last_delai_pre_op) as last_delai_pre_op,max(last_date) as last_date format=DATETIME20.,max(first_delai_pre_op) as first_delai_pre_op,min(first_date) as first_date format=DATETIME20. ,sum(prs_act_qte) as prs_act_qte
from presta_detail_pre_op_ group by ben_idt_ano,presta;
quit;

proc sql noprint;
/*Consultation rhumato*/
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="C_rhumato";
/*Consultation ortho*/
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="C_ortho";
/*Consultation MPR*/
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="C_MPR";
/*Total consultations sp�cialis�es*/
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op_ where presta ="consultations sp�cialis�es";
/*Consultation MG*/
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from presta_detail_pre_op where PRESTA_DETAIL="C_MG";
/*Total consultations*/
create table t7 as select '0' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte,round(mean(prs_act_qte),0.1) as moy_c,round(median(prs_act_qte),0.1) as med_c,
round(mean(first_delai_pre_op),0.1) as moy_firstd,round(median(first_delai_pre_op),0.1) as med_firstd,round(mean(last_delai_pre_op),0.1) as moy_lastd,round(median(last_delai_pre_op),0.1) as med_lastd
from gpe_presta_pre_op where gpe_prestation="consultations";
quit;

/*Calcul Q1 Q3 d�lais*/
%q1_q3_delai_detail (num=1,name=first,condition=where PRESTA_DETAIL="C_rhumato");%q1_q3_delai_detail (num=1,name=last,condition=where PRESTA_DETAIL="C_rhumato");
%q1_q3_delai_detail (num=2,name=first,condition=where PRESTA_DETAIL="C_ortho");%q1_q3_delai_detail  (num=2,name=last,condition=where PRESTA_DETAIL="C_ortho");
%q1_q3_delai_detail (num=3,name=first,condition=where PRESTA_DETAIL="C_MPR");%q1_q3_delai_detail  (num=3,name=last,condition=where PRESTA_DETAIL="C_MPR");
%q1_q3_delai_detail (num=6,name=first,condition=where PRESTA_DETAIL="C_MG");%q1_q3_delai_detail  (num=6,name=last,condition=where PRESTA_DETAIL="C_MG");
%q1_q3_delai_gpe (num=7,name=first,condition=where gpe_prestation="consultations");%q1_q3_delai_gpe (num=7,name=last,condition=where gpe_prestation="consultations");


Proc MEANS Data=presta_detail_pre_op_ noprint ;
var first_delai_pre_op;
output out=t4_ (drop=_type_ _freq_) q1=q1first q3=q3first;
where presta="consultations sp�cialis�es";
run;

Proc MEANS Data=presta_detail_pre_op_ noprint ;
var last_delai_pre_op;
output out=t4__ (drop=_type_ _freq_) q1=q1last q3=q3last;
where presta="consultations sp�cialis�es";
run;

data t4;
merge t4 t4_ t4__;
run;

/*Aucune consultation sp�cialis�e*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;
proc sort data=presta_detail_pre_op out=s0 (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where PRESTA_DETAIL in ("C_MPR","C_ortho","C_rhumato");run;

data t5;
merge patient (in=a) s0 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t5;
quit;


/*Aucune consultation*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;
proc sort data=gpe_presta_pre_op out=s0 (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="consultations";run;

data t8;
merge patient (in=a) s0 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t8 as select '0' as suivi, '8' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t8;
quit;

data res;
set t1 t2 t3 t4 t5 t6 t7 t8;
run;

proc report data=res nowd  split='*'; 
columns  indic suivi, (nb pct nb_acte moy_c med_c moy_firstd med_firstd q1first q3first moy_lastd med_lastd q1last q3last) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$suivi5_. "Consultations" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes";
define moy_c/ "Nb moyen par patient";
define med_c/ "Nb m�dian par patient";
define moy_firstd/ "D�lai moyen en jr entre 1�re consult et chirurgie";
define med_firstd/ "D�lai m�dian en jr entre 1�re consult et chirurgie";
define q1first/"Q1 D�lai";
define q3first/"Q3 D�lai";
define moy_lastd/ "D�lai moyen en jr entre derni�re consult et chirurgie";
define med_lastd/ "D�lai m�dian en jr entre derni�re consult et chirurgie";
define q1last/"Q1 D�lai";
define q3last/"Q3 D�lai";
;
run;


/*********************************************************************************************************************************************
			3 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI PAR TRIMESTRE SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE
*************************************************************************************************************************************************/
/*On affecte � chaque prestation son trimestre de suivi avant la chirurgie de l'�paule*/
data presta_trimestre;
set sasdata1.TENR_parcours_pre_op_&per._&anneem;
if delai_pre_op<=90 then trimestre='T1';
if delai_pre_op>90 and delai_pre_op<=180 then trimestre='T2';
if delai_pre_op>180 and delai_pre_op<=270 then trimestre='T3';
if delai_pre_op>270 and delai_pre_op<=360 then trimestre='T4';
if delai_pre_op>360 and delai_pre_op<=450 then trimestre='T5';
if delai_pre_op>450 and delai_pre_op<=540 then trimestre='T6';
run;

/************************************************************************************/
ods text=" ";
ods text="4 - SUIVI PRE OPERATOIRE DE LA CHIRURGIE DE L'EPAULE - SUIVI PAR TRIMESTRE SUR LES 18 MOIS ANTERIEURS A LA CHIRURIGIE";

/* 1 - SOINS CONSERVATEURS PAR TRIMESTRE AVANT LA CHIRURGIE (infiltration, kin�sith�rapie)*/
ods text=" ";
ods text="Soins conservateurs par trimestre avant la chirurgie (infiltration, kin�sith�rapie)";

proc sql noprint;
/*Infiltration sans guidage*/
create table t0 as select distinct trimestre, '0' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="corticoide" group by trimestre;
/*Infiltration sans guidage*/
create table t1 as select distinct trimestre, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="infiltration_sans_guidage" group by trimestre;
/*Infiltration sous guidage*/
create table t2 as select distinct trimestre, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="infiltration_sous_guidage" group by trimestre;
/*Total infiltrations*/
create table t3 as select distinct trimestre, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where type_presta= "Infiltration" group by trimestre;
/*kin�*/
create table t4 as select distinct trimestre, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="kine" group by trimestre;
/*Total soins conservateurs*/
create table t5 as select distinct trimestre, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where gpe_prestation="soins_conservateurs" group by trimestre;
quit;

/*Aucun soin conservateur*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;

%macro aucun (num=);
proc sort data=presta_trimestre out=s&num (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="soins_conservateurs" and trimestre="T&num";run;
data s&num;
merge patient (in=a) s&num (in=b);
by ben_idt_ano;
if a and not b;
trimestre="T&num";
run;
%mend;
%aucun (num=1);%aucun (num=2);%aucun (num=3);%aucun (num=4);%aucun (num=5);%aucun (num=6);

data t6;
set s1 s2 s3 s4 s5 s6;
run;

proc sql;
create table t6 as select distinct trimestre, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t6 group by trimestre;
quit;

data res;
set t0 t1 t2 t3 t4 t5 t6;
run;

proc report data=res nowd  split='*'; 
columns  indic trimestre, (nb pct nb_acte) ;
define trimestre/across "A0"X format=$trimfmt. order=data;
define indic/ group format=$suivi1_. "Soins conservateurs" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/ "Nb actes";
run;


/* 2 - ACTES D'IMAGERIE DE DEMARCHE DIAGNOSTIQUE PAR TRIMESTRE AVANT LA CHIRURGIE  (radiographie, �chographie)*/
ods text=" ";
ods text="Actes d'imagerie de d�marche diagnostique par trimestre avant la chirurgie (radiographie, �chographie)";

proc sql noprint;
/*Radiographie*/
create table t1 as select distinct trimestre, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="radio" group by trimestre;
/*Echographie*/
create table t2 as select distinct trimestre, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="echo" group by trimestre;
/*Total actes d�marche diagnostique*/
create table t3 as select distinct trimestre, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where gpe_prestation="imagerie_demarche_diagnostique" group by trimestre;
quit;

/*Aucun acte d�marche diagnostique*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;

%macro aucun (num=);
proc sort data=presta_trimestre out=s&num (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="imagerie_demarche_diagnostique" and trimestre="T&num";run;
data s&num;
merge patient (in=a) s&num (in=b);
by ben_idt_ano;
if a and not b;
trimestre="T&num";
run;
%mend;
%aucun (num=1);%aucun (num=2);%aucun (num=3);%aucun (num=4);%aucun (num=5);%aucun (num=6);

data t4;
set s1 s2 s3 s4 s5 s6;
run;

proc sql;
create table t4 as select distinct trimestre, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t4 group by trimestre;
quit;

data res;
set t1 t2 t3 t4;
run;

proc report data=res nowd  split='*'; 
columns  indic trimestre, (nb pct nb_acte) ;
define trimestre/across "A0"X format=$trimfmt. order=data;
define indic/ group format=$suivi2_. "Actes imagerie d�marche diagnostique" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/ "Nb actes";
run;


/* 3 - ACTES D'IMAGERIE AVANCEE  PAR TRIMESTRE AVANT LA CHIRURGIE (IRM, arthro-IRM, arthro-scanner)*/
ods text=" ";
ods text="Actes d'imagerie avanc�e r�alis�s par trimestre avant la chirurgie (IRM, arthro-IRM, arthro-scanner)";

proc sql noprint;
/*IRM*/
create table t1 as select distinct trimestre, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="IRM" group by trimestre;
/*Artho-IRM*/
create table t2 as select distinct trimestre, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="arthro_irm" group by trimestre;
/*Arthro-scanner*/
create table t3 as select distinct trimestre, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="arthro_scanner" group by trimestre;
/*Total actes imagerie avanc�e*/
create table t4 as select distinct trimestre, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where gpe_prestation="imagerie_avancee" group by trimestre;
quit;

/*Aucun acte imagerie avanc�e*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;

%macro aucun (num=);
proc sort data=presta_trimestre out=s&num (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="imagerie_avancee" and trimestre="T&num";run;
data s&num;
merge patient (in=a) s&num (in=b);
by ben_idt_ano;
if a and not b;
trimestre="T&num";
run;
%mend;
%aucun (num=1);%aucun (num=2);%aucun (num=3);%aucun (num=4);%aucun (num=5);%aucun (num=6);

data t5;
set s1 s2 s3 s4 s5 s6;
run;

proc sql;
create table t5 as select distinct trimestre, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t5 group by trimestre;
quit;

data res;
set t1 t2 t3 t4 t5;
run;

proc report data=res nowd  split='*'; 
columns  indic trimestre, (nb pct nb_acte) ;
define trimestre/across "A0"X format=$trimfmt. order=data;
define indic/ group format=$suivi3_. "Actes imagerie avanc�e" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/ "Nb actes";
run;


/* 4 - SCANNER PAR TRIMESTRE AVANT LA CHIRURGIE*/
ods text=" ";
ods text="Scanners r�alis�s par trimestre avant la chirurgie";

proc sql noprint;
create table res as select distinct trimestre, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="scanner" group by trimestre; 
quit;

proc report data=res nowd  split='*'; 
columns  indic trimestre, (nb pct nb_acte) ;
define trimestre/across "A0"X format=$trimfmt. order=data;
define indic/ group format=$suivi4_. "Scanner" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/ "Nb actes";
run;


/* 5 - CONSULTATIONS PAR TRIMESTRE AVANT LA CHIRURGIE  (ortho, rhumato, MPR)*/
ods text=" ";
ods text="Consultations r�alis�es par trimestre avant la chirurgie (ortho, rhumato, MPR, MG)";

proc sql noprint;
/*Consultation rhumato*/
create table t1 as select distinct trimestre, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="C_rhumato" group by trimestre; 
/*Consultation ortho*/
create table t2 as select distinct trimestre, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="C_ortho" group by trimestre;
/*Consultation MPR*/
create table t3 as select distinct trimestre, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="C_MPR" group by trimestre;
/*Total consultations sp�cialis�es*/
create table t4 as select distinct trimestre, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL in ("C_MPR","C_ortho","C_rhumato") group by trimestre;
/*Consultation MG*/
create table t6 as select distinct trimestre, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where PRESTA_DETAIL="C_MG" group by trimestre;
/*Total consultations*/
create table t7 as select distinct trimestre, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(prs_act_qte) as nb_acte from presta_trimestre where gpe_prestation="consultations" group by trimestre;
quit;


/*Aucune consultation sp�cialis�e*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;

%macro aucun (num=);
proc sort data=presta_trimestre out=s&num (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where PRESTA_DETAIL in ("C_MPR","C_ortho","C_rhumato") and trimestre="T&num";run;
data s&num;
merge patient (in=a) s&num (in=b);
by ben_idt_ano;
if a and not b;
trimestre="T&num";
run;
%mend;
%aucun (num=1);%aucun (num=2);%aucun (num=3);%aucun (num=4);%aucun (num=5);%aucun (num=6);

data t5;
set s1 s2 s3 s4 s5 s6;
run;

proc sql;
create table t5 as select distinct trimestre, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t5 group by trimestre;
quit;


/*Aucune consultation*/
proc sort data=sasdata1.TENR_patient_chir_&per._&anneem out=patient (keep=ben_idt_ano) nodupkey;by ben_idt_ano;run;

%macro aucun (num=);
proc sort data=presta_trimestre out=s&num (keep=ben_idt_ano) nodupkey;by ben_idt_ano;where gpe_prestation="consultations" and trimestre="T&num";run;
data s&num;
merge patient (in=a) s&num (in=b);
by ben_idt_ano;
if a and not b;
trimestre="T&num";
run;
%mend;
%aucun (num=1);%aucun (num=2);%aucun (num=3);%aucun (num=4);%aucun (num=5);%aucun (num=6);

data t8;
set s1 s2 s3 s4 s5 s6;
run;

proc sql;
create table t8 as select distinct trimestre, '8' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t8 group by trimestre;
quit;

data res;
set t1 t2 t3 t4 t5 t6 t7 t8;
run;

proc report data=res nowd  split='*'; 
columns  indic trimestre, (nb pct nb_acte) ;
define trimestre/across "A0"X format=$trimfmt. order=data;
define indic/ group format=$suivi5_. "Consultations" order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/ "Nb actes";
run;


/*********************************************************************************************************************************************
			4 - CALCUL D'INDICATEURS COMPLEMENTAIRES
*************************************************************************************************************************************************/
ods text=" ";
ods text= "5 - CALCUL D'INDICATEURS COMPLEMENTAIRES";
ods text=" ";

proc sql noprint;
create table t0_1 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("infiltration_sans_guidage","infiltration_sous_guidage","corticoide");
create table t0_2 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("kine");
create table t0_3 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("IRM","arthro-scanner","arthro-irm");
create table t0_4 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("echo");
create table t0_5 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("radio");
create table t0_6 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("scanner");
create table t0_7 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("C_ortho");
create table t0_8 as select distinct ben_idt_ano from presta_detail_pre_op where PRESTA_DETAIL in ("C_rhumato", "C_MPR");
select count(distinct ben_idt_ano) into:nb_infiltration from presta_detail_pre_op where PRESTA_DETAIL in ("infiltration_sans_guidage","infiltration_sous_guidage","corticoide");
select count(distinct ben_idt_ano) into:nb_echo from presta_detail_pre_op where PRESTA_DETAIL in ("echo");
quit;

/*Patients avec kin� et infiltration*/
data t1;
merge t0_1 (in=a) t0_2 (in=b);
by ben_idt_ano;
if a and b;
run;

proc sql;
create table t1 as select '0' as suivi, '1' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t1;
quit;

/*Patients avec infiltration sans kin�sith�rapie*/
data t2;
merge t0_1 (in=a) t0_2 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t2 as select '0' as suivi, '2' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t2;
quit;

/*Patients avec radiographie et �chographie*/
data t3;
merge t0_4 (in=a) t0_5 (in=b);
by ben_idt_ano;
if a and b;
run;

proc sql;
create table t3 as select '0' as suivi, '3' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t3;
quit;

/*Patients avec �chographie sans radiographie*/
data t4;
merge t0_4 (in=a) t0_5 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t4 as select '0' as suivi, '4' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t4;
quit;

/*Patients avec radiographie avant �chographie*/
data t5;
set sasdata1.tenr_parcours_pre_op_&per._&anneem;
if PRESTA_DETAIL='radio' then parcours='1';
if PRESTA_DETAIL in ('echo') then parcours='2';
if parcours='' then delete;
run;

proc sort data=t5;by ben_idt_ano parcours exe_soi_dtd;run;

data t5;
set t5;
by  ben_idt_ano parcours exe_soi_dtd;
if first.parcours then output;
run;

proc sort data=t5;by ben_idt_ano exe_soi_dtd parcours;run;

data t5;
set t5;
format parcours_type $20.;
by ben_idt_ano exe_soi_dtd parcours;
retain parcours_type;
if first.ben_idt_ano then parcours_type=parcours;
else parcours_type=catx('-',parcours_type,parcours);
if last.ben_idt_ano and parcours_type='1-2' then output;
run;

proc sql;
create table t5 as select '0' as suivi, '5' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_echo,0.1) as pct from t5;
quit;

/*Patients avec radiographie avant infiltration*/
data t6;
set sasdata1.tenr_parcours_pre_op_&per._&anneem;
if PRESTA_DETAIL='radio' then parcours='1';
if PRESTA_DETAIL in ('infiltration_sans_guidage','infiltration_sous_guidage','corticoide') then parcours='2';
if parcours='' then delete;
run;

proc sort data=t6;by ben_idt_ano parcours exe_soi_dtd;run;

data t6;
set t6;
by  ben_idt_ano parcours exe_soi_dtd;
if first.parcours then output;
run;

proc sort data=t6;by ben_idt_ano exe_soi_dtd parcours;run;

data t6;
set t6;
format parcours_type $20.;
by ben_idt_ano exe_soi_dtd parcours;
retain parcours_type;
if first.ben_idt_ano then parcours_type=parcours;
else parcours_type=catx('-',parcours_type,parcours);
if last.ben_idt_ano and parcours_type='1-2' then output;
run;

proc sql;
create table t6 as select '0' as suivi, '6' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_infiltration,0.1) as pct from t6;
quit;

/*Radiographie en 1�re intention: Patients avec radiographie sans autre imagerie ou avec radiographie avant autre imagerie (IRM/arhroscanner/arthroIRM, �chographie, scanner)*/
data t7;
set sasdata1.tenr_parcours_pre_op_&per._&anneem;
if PRESTA_DETAIL='radio' then parcours='1';
if PRESTA_DETAIL in ('IRM','arthro_scanner','arthro_irm','scanner','echo') then parcours='2';
if parcours='' then delete;
run;

proc sort data=t7;by ben_idt_ano parcours exe_soi_dtd;run;

data t7;
set t7;
by  ben_idt_ano parcours exe_soi_dtd;
if first.parcours then output;
run;

proc sort data=t7;by ben_idt_ano exe_soi_dtd parcours;run;

data t7;
set t7;
format parcours_type $20.;
by ben_idt_ano exe_soi_dtd parcours;
retain parcours_type;
if first.ben_idt_ano then parcours_type=parcours;
else parcours_type=catx('-',parcours_type,parcours);
if last.ben_idt_ano and parcours_type in ('1','1-2') then output;
run;

proc sql;
create table t7 as select '0' as suivi, '7' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t7;
quit;

/*Patients sans IRM mais avec �chographie*/
data t8;
merge t0_4 (in=a) t0_3 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t8 as select '0' as suivi, '8' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t8;
quit;

/*Patients sans IRM, radiographie, kin�sith�rapie ni infiltration mais avec �chographie*/
data t9;
merge t0_1 (in=a) t0_2 (in=b) t0_3 (in=c) t0_4 (in=d) t0_5 (in=e);
by ben_idt_ano;
if d and not a and not b and not c and not e;
run;

proc sql;
create table t9 as select '0' as suivi, '9' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t9;
quit;

/*Patients sans IRM, �chographie ni radio mais avec scanner*/
data t10;
merge t0_3 (in=a) t0_4 (in=b) t0_5 (in=c) t0_6 (in=d);
by ben_idt_ano;
if d and not a and not b and not c;
run;

proc sql;
create table t10 as select '0' as suivi, '10' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t10;
quit;

/*Patients sans IRM ni �chographie, mais avec scanner*/
data t11;
merge t0_3 (in=a) t0_4 (in=b) t0_6 (in=d);
by ben_idt_ano;
if d and not a and not b;
run;

proc sql;
create table t11 as select '0' as suivi, '11' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t11;
quit;

/*Patients sans imagerie (IRM/arthro-IRM/arthro-scanner, radiographie, �chographie, scanner)*/
proc sql noprint;
select count(distinct ben_idt_ano) into:nb_imagerie from presta_detail_pre_op where PRESTA_DETAIL in ("IRM","arthro-scanner","arthro-irm","echo","radio","scanner");
quit;

data t12;
suivi='0';
indic='12';
nb=&nb_patient-&nb_imagerie;
pct=round((nb*100)/&nb_patient,0.1);
run;

/*Patients vus par un orthop�diste une seule fois*/
proc sql;
create table t13 as select distinct ben_idt_ano,sum(prs_act_qte) as nb_c from presta_detail_pre_op where presta_detail="C_ortho" group by ben_idt_ano having (calculated nb_c=1);
create table t13 as select '0' as suivi, '13' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t13;
quit;

/*Patients avec seulement consultations d�ortho sans consultation rhumato ou MPR*/
data t14;
merge t0_7 (in=a) t0_8 (in=b);
by ben_idt_ano;
if a and not b;
run;

proc sql;
create table t14 as select '0' as suivi, '14' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct from t14;
quit;

/*Infiltrations sans guidage*/
proc sql;
create table t15 as select '0' as suivi, '15' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(qte_sans_guidage) as nb_acte from sasdata1.TENR_parcours_pre_op_&per._&anneem
where sans_guidage='sans_guidage';
quit;

/*Infiltration sous guidage*/
proc sql;
create table t16 as select '0' as suivi, '16' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(qte_sous_guidage) as nb_acte from sasdata1.TENR_parcours_pre_op_&per._&anneem
where sous_guidage='sous_guidage';
quit;

/*Infiltrations sans guidage ou sous guidage*/
proc sql;
create table t17 as select '0' as suivi, '17' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient,0.1) as pct,sum(qte_sans_guidage+qte_sous_guidage) as nb_acte from sasdata1.TENR_parcours_pre_op_&per._&anneem
where sous_guidage='sous_guidage' or sans_guidage='sans_guidage';
quit;

/*Radiographie en 1�re intention sur la sous-population de patient qui n'a pas de traitement conservateur au d�but de la p�riode d'�tude (entre 12 et 18 mois avt la chirurgie) */

/*On ne s�lectionne que les parcours pr�-op des patients ayant commenc� les soins conservateurs moins d'un an avant la chirurgie*/
data selec1;
set sasdata1.TENR_PARCOURS_PRE_OP_S2_2022;
if delai_pre_op>360 and GPE_PRESTATION="soins_conservateurs" then output;
keep ben_idt_ano;
run;

proc sort data=selec1 nodupkey;by ben_idt_ano;run;
proc sort data=sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 out=s1; by ben_idt_ano;run;

data parcours_sous_pop;
merge selec1 (in=a) s1 (in=b);
by ben_idt_ano;
if b and not a then output;
run;

/*Nb patient dans sous population*/
proc sort data=sasdata1.TENR_PATIENT_CHIR_S2_2022 out=s2;by ben_idt_ano;run;

data sous_pop;
merge selec1 (in=a) s2 (in=b);
by ben_idt_ano;
if b and not a then output;
run;

proc sql noprint;
select count(distinct ben_idt_ano) into:nb_patient_sous_pop from sous_pop ;
quit;

data t18;
set parcours_sous_pop;
if PRESTA_DETAIL='radio' then parcours='1';
if PRESTA_DETAIL in ('IRM','arthro_scanner','arthro_irm','scanner','echo') then parcours='2';
if parcours='' then delete;
run;

proc sort data=t18;by ben_idt_ano parcours exe_soi_dtd;run;

data t18;
set t18;
by  ben_idt_ano parcours exe_soi_dtd;
if first.parcours then output;
run;

proc sort data=t18;by ben_idt_ano exe_soi_dtd parcours;run;

data t18;
set t18;
format parcours_type $20.;
by ben_idt_ano exe_soi_dtd parcours;
retain parcours_type;
if first.ben_idt_ano then parcours_type=parcours;
else parcours_type=catx('-',parcours_type,parcours);
if last.ben_idt_ano and parcours_type in ('1','1-2') then output;
run;

proc sql;
create table t18 as select '0' as suivi, '18' as indic,count(distinct ben_idt_ano) as nb,round((count(distinct ben_idt_ano)*100)/&nb_patient_sous_pop,0.1) as pct from t18;
quit;

data res;
format indic $2.;
set t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18;
run;

proc report data=res nowd ; 
columns  indic suivi, (nb pct nb_acte) ;
define suivi/across "A0"X format=$suivi0_. order=data;
define indic/ group format=$indic1_. "A0"X order=data;
define nb/ "Nb patients" ;
define pct/ "% patients";
define nb_acte/"Nb actes (***)";
Footnote1 "(*) % sur le total des patients ayant eu une �chographie";
Footnote2 "(**) % sur le total des patients ayant eu une infiltration";
Footnote3 "(***) Uniquement pour les actes CCAM d'infiltrations";
run;


/*********************************************************************************************************************************************
			5 - PRESCRIPTEURS CORTICOIDES ET KINESITHERAPIE
*************************************************************************************************************************************************/
ods text=" ";
ods text= "6 - PRESCRIPTEURS CORTICOIDES ET KINESITHERAPIE";
ods text=" ";


proc sql;
create table t1 as select '0' as type,'1' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (1,22,23);
create table t2 as select '0' as type,'2' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (14);
create table t3 as select '0' as type,'3' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (41);
create table t4 as select '0' as type,'4' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (31);
create table t5 as select '0' as type,'5' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (6);
create table t6 as select '0' as type,'6' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp not in (1,22,23,6,14,41,31,.,99,0);
create table t7 as select '0' as type,'7' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_kine,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_kine,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="kine" and spe_psp in (.,99,0);
create table t8 as select '1' as type,'1' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (1,22,23);
create table t9 as select '1' as type,'2' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (14);
create table t10 as select '1' as type,'3' as indic, count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct 
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (41);
create table t11 as select  '1' as type,'4' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (31);
create table t12 as select '1' as type,'5' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (6);
create table t13 as select '1' as type,'6' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp not in (1,22,23,6,14,41,31,.,99,0);
create table t14 as select '1' as type,'7' as indic,count(distinct ben_idt_ano) as nb_pat, round((count(distinct ben_idt_ano)*100)/&nb_pat_corticoide,0.1) as pct_pat,sum(prs_act_qte) as nb,round((sum(prs_act_qte)*100)/&nb_corticoide,0.1) as pct
from sasdata1.TENR_PARCOURS_PRE_OP_S2_2022 where presta_detail="corticoide" and spe_psp in (.,99,0);
quit;


data res;
set t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14;
run;

proc report data=res nowd ; 
columns  indic type, (nb_pat pct_pat nb pct) ;
define type/across "A0"X format=$typefmt. order=data;
define indic/ group format=$indic2_. "Prescripteur" order=data;
define nb_pat/ "Nb patients" ;
define pct_pat/ "% patients (*)";
define nb/"Nb actes";
define pct/"% actes (**)";
Footnote1 "(*) % sur total des patients ayant eu de la kin� ou des corticoides injectables";
Footnote2 "(**) % sur total des actes de kin� ou corticoides injectables";
run;



ods excel close;

/*********************************************************************************************************************************************
			6 - MISE EN FORME DES DONNEES POUR REPRESENTATIONS GRAPHIQUES SOUS R
*************************************************************************************************************************************************/

/* 
------------------------------------------------------
Mise en forme des donn�es pour:
 - diagramme en barre % patients par type d'acte
 - boxplot des d�lais pr�op�ratoires entre la chirurgie et les actes (premiers et derniers actes) 
 - histogrammes nb actes
 - diagrammes en barre prescripteur soins conservateurs (kin� et corticoides)
Ecriture des tables SAS sous sasdata1 pour utilisation sous R (lecture des tables SAS sous R avec package haven)
 -----------------------------------------------------
 */
 
/*Pour boxplot avec d�lai pr�-op�ratoire en mois*/
data type_presta_pre_op;
set sasdata1.TENR_parcours_pre_op_&per._&anneem;
delai_pre_op=round(delai_pre_op/30,0.1);
run;

proc sql;
create table type_presta_pre_op as select distinct ben_idt_ano,type_presta,min(delai_pre_op) as last_delai_pre_op,max(exe_soi_dtd) as last_date format=DATETIME20.,max(delai_pre_op) as first_delai_pre_op,min(exe_soi_dtd) as first_date format=DATETIME20. ,sum(prs_act_qte) as prs_act_qte
from type_presta_pre_op group by ben_idt_ano,type_presta;
quit;

data sasdata1.type_presta_pre_op;
set type_presta_pre_op;
if type_presta='Kin�sith�rapie' then ordre=8;
if type_presta='Infiltration' then ordre=7;
if type_presta='Radiographie' then ordre=6;
if type_presta='Echographie' then ordre=5;
if type_presta='IRM/Arthroscanner' then ordre=4;
if type_presta='Scanner' then ordre=3;
if type_presta='C MPR / Rhumatologue' then ordre=2;
if type_presta='C Orthop�diste' then ordre=1;
if type_presta='C MG' then delete;
drop last_date first_date;
run;

proc sort data=sasdata1.type_presta_pre_op;by ordre;run;


/*Pour diagrammes en barre prescripteurs des soins conservateurs*/
data sasdata1.prescripteur_soins_conservateurs;
set res;
format acte $15. prescripteur $15.;
if type='0' then acte="kin�";
if type='1' then acte="corticoides";
if indic='1' then do;prescripteur="MG";ordre_kine=1;ordre_corticoide=1;end;
if indic='2' then do;prescripteur="Rhumato";ordre_kine=3;ordre_corticoide=3;end;
if indic='3' then do;prescripteur="Ortho";ordre_kine=2;ordre_corticoide=4;end;
if indic='4' then do;prescripteur="MPR";ordre_kine=4;ordre_corticoide=5;end;
if indic='5' then do;prescripteur="Radio";ordre_kine=5;ordre_corticoide=2;end;
if indic='6' then do;prescripteur="Autre";ordre_kine=6;ordre_corticoide=6;end;
if indic='7' then do;prescripteur="NA";ordre_kine=7;ordre_corticoide=7;end;
drop type indic;
run;

