/**************************************************************************************************************************************************************
							DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D’UNE ACROMIOPLASTIE ISOLEE 
	 

RESTITUTION DES PARCOURS: TABLEAUX + MISE EN FORME DES TABLES POUR PRODUCTION DE GRAPHIQUES SOUS R
	1 - CREATION DE LA TABLE "PARCOURS" POUR LES ANALYSES
	2 - FREQUENCE DES EVENEMENTS DU PARCOURS 
	3 - RECONSTITUTION DU PARCOURS DU PATIENT PAR ORDRE CHRONOLOGIQUE
	4 - MISE EN FORME DES DONNEES POUR REPRESENTATIONS GRAPHIQUES SOUS R

****************************************************************************************************************************************************************/

/***************************************************************Définition des macrovariables**********************************************************************/

/*Période*/
%let per=S2;/*période étude*/
%let anneem=2022;/*année étude*/

/*Graphes*/
%let nb_graphe=65; /*Nb de "network" graphes de parcours à afficher (65 = la totalité)*/
%let nb_graphe2=16; /*Nb de "network" graphes d'association d'événements à afficher (16 = la totalité*/


/*date de traitement*/
data _null_;
call symput("datetrt",left(put("&sysdate"d,ddmmyy10.)));
run;

/***************************************************************Définition des formats**********************************************************************/
proc format;
value $parcoursfmt
'1-2-3-4-5'="Radio - Kiné - Infiltration - IRM/Arthroscanner - Chirurgie"
'1-2-4-3-5'="Radio - Kiné - IRM/Arthroscanner - Infiltration - Chirurgie"
'1-3-2-4-5'="Radio - Infiltration - Kiné - IRM/Arthroscanner - Chirurgie"
'1-3-4-2-5'="Radio - Infiltration - IRM/Arthroscanner - Kiné - Chirurgie"
'1-4-2-3-5'="Radio - IRM/Arthroscanner - Kiné - Infiltration - Chirurgie"
'1-4-3-2-5'="Radio - IRM/Arthroscanner - Infiltration - Kiné - Chirurgie"
'2-1-3-4-5'="Kiné - Radio - Infiltration - IRM/Arthroscanner - Chirurgie"
'2-1-4-3-5'="Kiné - Radio - IRM/Arthroscanner - Infiltration - Chirurgie"
'2-3-1-4-5'="Kiné - Infiltration - Radio - IRM/Arthroscanner - Chirurgie"
'2-3-4-1-5'="Kiné - Infiltration - IRM/Arthroscanner - Radio - Chirurgie"
'2-4-1-3-5'="Kiné - IRM/Arthroscanner - Radio - Infiltration - Chirurgie"
'2-4-3-1-5'="Kiné - IRM/Arthroscanner - Infiltration - Radio - Chirurgie"
'3-1-2-4-5'="Infiltration - Radio - Kiné - IRM/Arthroscanner - Chirurgie"
'3-1-4-2-5'="Infiltration - Radio - IRM/Arthroscanner - Kiné - Chirurgie"
'3-2-1-4-5'="Infiltration - Kiné - Radio - IRM/Arthroscanner - Chirurgie"
'3-2-4-1-5'="Infiltration - Kiné - IRM/Arthroscanner - Radio - Chirurgie"
'3-4-1-2-5'="Infiltration - IRM/Arthroscanner - Radio - Kiné - Chirurgie"
'3-4-2-1-5'="Infiltration - IRM/Arthroscanner - Kiné - Radio - Chirurgie"
'4-1-2-3-5'="IRM/Arthroscanner - Radio - Kiné - Infiltration - Chirurgie"
'4-1-3-2-5'="IRM/Arthroscanner - Radio - Infiltration - Kiné - Chirurgie"
'4-2-1-3-5'="IRM/Arthroscanner - Kiné - Radio - Infiltration - Chirurgie"
'4-2-3-1-5'="IRM/Arthroscanner - Kiné - Infiltration - Radio - Chirurgie"
'4-3-1-2-5'="IRM/Arthroscanner - Infiltration - Radio - Kiné - Chirurgie"
'4-3-2-1-5'="IRM/Arthroscanner - Infiltration - Kiné - Radio - Chirurgie"
'1-2-3-5'="Radio - Kiné - Infiltration - Chirurgie"
'1-2-4-5'="Radio - Kiné - IRM/Arthroscanner - Chirurgie"
'1-3-2-5'="Radio - Infiltration - Kiné - Chirurgie"
'1-3-4-5'="Radio - Infiltration - IRM/Arthroscanner - Chirurgie"
'1-4-2-5'="Radio - IRM/Arthroscanner - Kiné - Chirurgie"
'1-4-3-5'="Radio - IRM/Arthroscanner - Infiltration - Chirurgie"
'2-1-3-5'="Kiné - Radio - Infiltration - Chirurgie"
'2-1-4-5'="Kiné - Radio - IRM/Arthroscanner - Chirurgie"
'2-3-1-5'="Kiné - Infiltration - Radio - Chirurgie"
'2-3-4-5'="Kiné - Infiltration - IRM/Arthroscanner - Chirurgie"
'2-4-1-5'="Kiné - IRM/Arthroscanner - Radio - Chirurgie"
'2-4-3-5'="Kiné - IRM/Arthroscanner - Infiltration - Chirurgie"
'3-1-2-5'="Infiltration - Radio - Kiné - Chirurgie"
'3-1-4-5'="Infiltration - Radio - IRM/Arthroscanner - Chirurgie"
'3-2-1-5'="Infiltration - Kiné - Radio - Chirurgie"
'3-2-4-5'="Infiltration - Kiné - IRM/Arthroscanner - Chirurgie"
'3-4-1-5'="Infiltration - IRM/Arthroscanner - Radio - Chirurgie"
'3-4-2-5'="Infiltration - IRM/Arthroscanner - Kiné - Chirurgie"
'4-1-2-5'="IRM/Arthroscanner - Radio - Kiné - Chirurgie"
'4-1-3-5'="IRM/Arthroscanner - Radio - Infiltration - Chirurgie"
'4-2-1-5'="IRM/Arthroscanner - Kiné - Radio - Chirurgie"
'4-2-3-5'="IRM/Arthroscanner - Kiné - Infiltration - Chirurgie"
'4-3-1-5'="IRM/Arthroscanner - Infiltration - Radio - Chirurgie"
'4-3-2-5'="IRM/Arthroscanner - Infiltration - Kiné - Chirurgie"
'1-2-5'="Radio - Kiné - Chirurgie"
'1-3-5'="Radio - Infiltration - Chirurgie"
'1-4-5'="Radio - IRM/Arthroscanner - Chirurgie"
'2-1-5'="Kiné - Radio - Chirurgie"
'2-3-5'="Kiné - Infiltration - Chirurgie"
'2-4-5'="Kiné - IRM/Arthroscanner - Chirurgie"
'3-1-5'="Infiltration - Radio - Chirurgie"
'3-2-5'="Infiltration - Kiné - Chirurgie"
'3-4-5'="Infiltration - IRM/Arthroscanner - Chirurgie"
'4-1-5'="IRM/Arthroscanner - Radio - Chirurgie"
'4-2-5'="IRM/Arthroscanner - Kiné - Chirurgie"
'4-3-5'="IRM/Arthroscanner - Infiltration - Chirurgie"
'1-5'="Radio - Chirurgie"
'2-5'="Kiné - Chirurgie"
'3-5'="Infiltration - Chirurgie"
'4-5'="IRM/Arthroscanner - Chirurgie"
'5'="Chirurgie"
;

value $evfmt
'1'="Chirurgie seulement"
'2'="1 événement pré-opératoire + chirurgie"
'3'="2 événements pré-opératoires + chirurgie"
'4'="3 événements pré-opératoires + chirurgie"
'5'="4 événements pré-opératoires + chirurgie"
;
run;


/**************************************************************************************************************************************
			1 - CREATION DE LA TABLE "PARCOURS" POUR LES ANALYSES
***************************************************************************************************************************************/

/**************************************************************************************************************************************
Méthode
---------------------------------------------------
On considère 5 événements: radio, kiné, infiltration, IRM/Artho-IRM/Arthro-Scanner + la chirurgie de l'épaule (dans le cas où le patient n'aurait eu que la chirurgie)
On classe les événements par ordre chronologique de première survenue (un événement ne compte qu'une fois, lors de sa 1ère réalisation)
Si plusieurs événements ont lieu le même jour, on les classe selon l'ordre du parcours "idéal" (radio, kiné, infiltration, IRM/arthro-IRM/Arthro-sacanner, chirurgie), pour ne pas comptabiliser à tort de mauvaises pratiques

****************************************************************************************************************************************/

/*On crée les événements à suivre*/
data parcours;
set sasdata1.tenr_parcours_pre_op_&per._&anneem;
if PRESTA_DETAIL='radio' then parcours='1';
if PRESTA_DETAIL='kine' then parcours='2';
if PRESTA_DETAIL in ('infiltration_sans_guidage','infiltration_sous_guidage','corticoide') then parcours='3';
if PRESTA_DETAIL in ('IRM','arthro_irm','arthro_scanner') then parcours='4';
if parcours='' then delete;
run;

/*On onserve uniquement la 1ère occurence de chaque événement*/
proc sort data=parcours;by ben_idt_ano parcours exe_soi_dtd;run;

data parcours;
set parcours;
by  ben_idt_ano parcours exe_soi_dtd;
if first.parcours then output;
run;

/*Ajout d'une ligne avec la date de la chirurgie pour chaque patient de l'étude*/
proc sort data=sasdata1.TENR_PATIENT_CHIR_&per._&anneem out=chir (keep=ben_idt_ano date_entree);by ben_idt_ano;run;

data chir;
set chir;
rename date_entree=exe_soi_dtd;
parcours='5';
run;

data parcours;
set parcours chir;
keep ben_idt_ano parcours exe_soi_dtd;
run;

proc sort data=parcours;by ben_idt_ano exe_soi_dtd parcours;run;

/**********************************************************Paramétrage fichier de sortie******************************************************************************************************/
ods excel file="./TENR_res_parcours.xlsx" ;
ods excel options(start_at="B2" sheet_interval='none' embedded_titles="yes" sheet_name="Tableaux");
ods text="DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D’UNE ACROMIOPLASTIE ISOLEE";
ods text="Chirurgie de l'épaule réalisée sur le 2nd semestre 2022 - suivi pré-opératoire de 18 mois avant la chirurgie - patients >= 40 ans";
ods text=" ";
ods text="Date de traitement: &datetrt";
ods text=" ";
ods text=" ";
/*************************************************************************************************************************************************************************************************/

/**************************************************************************************************************************************
			2 - FREQUENCE DES EVENEMENTS DU PARCOURS
***************************************************************************************************************************************/
data evenement;
set parcours;
suivi=1;
drop exe_soi_dtd;
run;

proc transpose data=evenement out=evenement1 (drop=_name_) prefix=evenement_;by ben_idt_ano;id parcours;var suivi;run;

data evenement1;
set evenement1;
if evenement_1=. then evenement_1=0;
if evenement_2=. then evenement_2=0;
if evenement_3=. then evenement_3=0;
if evenement_4=. then evenement_4=0;
if evenement_5=. then evenement_5=0;
nb_evenement=sum(evenement_1,evenement_2,evenement_3,evenement_4, evenement_5);
run;

proc freq data=evenement1 noprint;
table nb_evenement/out=evenement1;
run;

data evenement1;
set evenement1;
percent=round(percent,0.1);
event=compress(put(nb_evenement,$1.));
run;

ods text="";
ods text="Evénements pré-opératoires sur les 18 mois précédent la chirurgie";
ods text="Evénements pré-opératoires comptabilisés une seule fois lors de leur première réalisation";
ods text="Evénements observés: kinésithérapie, infiltration, radiographie, IRM/arthro-IRM/arthro-scanner";
proc report data=evenement1 nowd  split='*'; 
columns event count percent;
define event/ "A0"X format=$evfmt. order=data;
define count/ "Nb patients";
define percent/"% patients";
run;

data evenement;
set parcours;
suivi='1';
drop exe_soi_dtd;
run;

proc transpose data=evenement out=evenement2 (drop=_name_) prefix=evenement_;by ben_idt_ano;id parcours;var suivi;run;

data evenement2;
retain ben_idt_ano evenement_1 evenement_2 evenement_3 evenement_4 evenement_5;
set evenement2;
if evenement_2='1' then evenement_2='2';
if evenement_3='1' then evenement_3='3';
if evenement_4='1' then evenement_4='4';
if evenement_5='1' then evenement_5='5';
conc_ev=catx('-',evenement_1,evenement_2,evenement_3,evenement_4,evenement_5);
run;

proc freq data=evenement2 order=freq noprint;
table conc_ev/out=evenement2_;
run;

data evenement2_;
set evenement2_;
percent=round(percent,0.1);
run;

ods text="";
ods text="Description des événements pré-opératoires sur les 18 mois précédent la chirurgie";
ods text="Evénements pré-opératoires non classés chronologiquement - comptabilisés lors de leur première réalisation";
ods text="Evénements observés: kinésithérapie, infiltration, radiographie, IRM/arthro-IRM/arthro-scanner";
proc report data=evenement2_ nowd  split='*'; 
columns conc_ev count percent;
define conc_ev/ "A0"X format=$parcoursfmt. order=data;
define count/ "Nb patients";
define percent/"% patients";
run;


/*********************************************************************************************************
			3 - RECONSTITUTION DU PARCOURS DU PATIENT PAR ORDRE CHRONOLOGIQUE
***********************************************************************************************************/

/*On garde une ligne par patient en concaténant les événements de son parcours par ordre chronologique*/
proc sort data=parcours;by ben_idt_ano exe_soi_dtd parcours;run;

data res_parcours;
set parcours;
format parcours_type $20.;
by ben_idt_ano exe_soi_dtd parcours;
retain parcours_type;
if first.ben_idt_ano then parcours_type=parcours;
else parcours_type=catx('-',parcours_type,parcours);
if last.ben_idt_ano then output;
run;

proc freq data=res_parcours order=freq noprint;
table parcours_type/out=res_parcours_;
run;

data res_parcours_;
set res_parcours_;
percent=round(percent,0.1);
run;

ods text="";
ods text="Parcours pré-opératoire des patients avant la chirurgie de l'épaule (suivi pré-opératoire de 18 mois)";
ods text="Classement des événements par ordre chronologique de première réalisation";
ods text="Evénements observés: kinésithérapie, infiltration, radiographie, IRM/arthro-IRM/arthro-scanner";
proc report data=res_parcours_ nowd  split='*'; 
columns  parcours_type count percent;
define parcours_type/ "Parcours" format=$parcoursfmt. order=data;
define count/ "Nb patients";
define percent/"% patients";
run;

ods excel close;

/*********************************************************************************************************************************************
			4 - MISE EN FORME DES DONNEES POUR REPRESENTATIONS GRAPHIQUES SOUS R
*************************************************************************************************************************************************/

/* 1 - Mise en forme des données pour création du 1er graphe "network" sous R => Affichage des parcours dans l'ordre chronologique*/

/* Création des variables provenance ("from") et destination ("to")*/
proc sort data=parcours out=to;by ben_idt_ano descending exe_soi_dtd descending parcours;run;

data to;
set to;
retain cpt2 0;
by ben_idt_ano descending exe_soi_dtd descending parcours;
to_=lag(parcours);
if first.ben_idt_ano then do;to_=.;cpt2=cpt2+1;end;
run;

proc sort data=to;by ben_idt_ano exe_soi_dtd parcours;run;
proc sort data=parcours;by ben_idt_ano exe_soi_dtd parcours;run;

data network;
merge parcours to;
by ben_idt_ano exe_soi_dtd parcours;
rename parcours=from_;
*if to_=. then delete;/*Si seulement chirurgie, on supprime le parcours*/
cpt=_n_;
drop exe_soi_dtd;
run;

/* Ajout de la fréquence et du pourcentage de chaque type de parcours*/
proc sort data=res_parcours out=t1(keep=ben_idt_ano parcours_type);by ben_idt_ano;run;

data network;
merge network(in=a) t1;
by ben_idt_ano;
if a;
run;

/*Pour pouvoir afficher le parcours "Chirurgie" uniquement*/
data network;
set network;
if parcours_type ne '5' and to_=. then delete;
if parcours_type='5' then do;from_='6';to_='5';end;
run;

proc sort data=network;by parcours_type;run;
proc sort data=res_parcours_;by parcours_type;run;

proc sql;
create table selec as select distinct parcours_type,min(cpt2) as min from network
group by parcours_type;
quit;

data network;
merge network (in=a) res_parcours_ selec;
by parcours_type;
if a;
run;

/* On agrège les données par type de parcours (suppression du ben_idt_ano) */
proc sort data=network;by descending count cpt cpt2;run;

data network;
set network;
if cpt2=min;
drop min ben_idt_ano cpt ;
run;

/* Classement des parcours par fréquence décroissante pour création d'un compteur (cpt)*/
proc sort data=network;by descending count cpt2;run;

/* Table mise en forme des différents parcours possibles => tbl_links*/
data tbl_links;
set network;
format from $20. to $20.;
by descending count cpt2;
retain cpt 0;
if first.cpt2 then cpt=cpt+1;
if from_=1 then from="Radio";
if from_=2 then from="Kiné";
if from_=3 then from="Infiltration";
if from_=4 then from="IRM/Arthroscanner";
if from_=5 then from="Chirurgie";
if from_=6 then from=" ";
if to_=1 then to="Radio";
if to_=2 then to="Kiné";
if to_=3 then to="Infiltration";
if to_=4 then to="IRM/Arthroscanner";
if to_=5 then to="Chirurgie";
drop from_ to_ ;
rename count=nb
percent=pct;
run;

/*Création table des paramètres (nom des événements et couleur des noeuds) => tbl_nodes*/
data tbl_nodes1;
set tbl_links;
keep  cpt from;
run;

data tbl_nodes2;
set tbl_links;
from="Chirurgie";
keep  cpt from;
run;

proc sort data=tbl_nodes2 nodupkey;by cpt;run;

data tbl_nodes;
set tbl_nodes1 tbl_nodes2;
format name $15. color $15.;
if from="Kiné" then color="#E34007";
if from="Radio" then color="#03a032";
if from="IRM/Arthroscanner" then color="#03a0a0";
if from="Infiltration" then color="#fec863";
if from="Chirurgie" then color="grey";
if from=" " then color="white";
rename from=name;
run;

proc sort data=tbl_nodes;by cpt;run;

/*Ecriture des tables SAS des parcours les plus fréquents sous sasdata1 pour utilisation sous R (lecture des tables SAS sous R avec package haven)*/
%macro tt;
%do i=1 %to &nb_graphe;
	data sasdata1.tbl_links&i;
	set tbl_links;
	where cpt=&i;
	drop cpt cpt2 parcours_type;
	run;

    data sasdata1.tbl_nodes&i;
	set tbl_nodes;
	where cpt=&i;
	drop cpt;
	run;
	%end;
%mend;
%tt;


/* 2 - Mise en forme des données pour création du 2è graphe "network" sous R => Affichage des associations d'événements possibles sans ordre chronologique*/

/* Création des variables provenance ("from") et destination ("to")*/
proc sort data=parcours out=to;by ben_idt_ano descending parcours;run;

data to;
set to;
retain cpt2 0;
by ben_idt_ano descending parcours;
to_=lag(parcours);
if first.ben_idt_ano then do;to_=.;cpt2=cpt2+1;end;
run;

proc sort data=to;by ben_idt_ano parcours;run;
proc sort data=parcours;by ben_idt_ano  parcours;run;

data network;
merge parcours to;
by ben_idt_ano  parcours;
rename parcours=from_;
*if to_=. then delete;/*Si seulement chirurgie, on supprime*/
cpt=_n_;
drop exe_soi_dtd;
run;

/* Ajout de la fréquence et du pourcentage de chaque association d'événements*/
proc sort data=evenement2 out=t1(keep=ben_idt_ano conc_ev);by ben_idt_ano;run;

data network;
merge network(in=a) t1;
by ben_idt_ano;
if a;
run;

/*Pour pouvoir afficher le parcours "Chirurgie" seul*/
data network;
set network;
if conc_ev ne '5' and to_=. then delete;
if conc_ev='5' then do;from_='6';to_='5';end;
run;

proc sort data=network;by conc_ev;run;
proc sort data=evenement2_;by conc_ev;run;

proc sql;
create table selec as select distinct conc_ev,min(cpt2) as min from network
group by conc_ev;
quit;

data network;
merge network (in=a) evenement2_ selec;
by conc_ev;
if a;
run;

/* On agrège les données par association d'événements (suppression du ben_idt_ano) */
proc sort data=network;by descending count cpt cpt2;run;

data network;
set network;
if cpt2=min;
drop min ben_idt_ano cpt ;
run;

/* Classement des associations d'événements par fréquence décroissante pour création d'un compteur (cpt)*/
proc sort data=network;by descending count cpt2;run;

/* Table mise en forme des différentes associations d'événements possibles => tbl_links*/
data tbl_links;
set network;
format from $20. to $20.;
by descending count cpt2;
retain cpt 0;
if first.cpt2 then cpt=cpt+1;
if from_=1 then from="Radio";
if from_=2 then from="Kiné";
if from_=3 then from="Infiltration";
if from_=4 then from="IRM/Arthroscanner";
if from_=5 then from="Chirurgie";
if from_=6 then from=" ";
if to_=1 then to="Radio";
if to_=2 then to="Kiné";
if to_=3 then to="Infiltration";
if to_=4 then to="IRM/Arthroscanner";
if to_=5 then to="Chirurgie";
drop from_ to_ ;
rename count=nb
percent=pct;
run;

/*Création table des paramètres (nom des événements et couleur des noeuds) => tbl_nodes*/
data tbl_nodes1;
set tbl_links;
keep cpt from;
run;

data tbl_nodes2;
set tbl_links;
from="Chirurgie";
keep cpt from;
run;

proc sort data=tbl_nodes2 nodupkey;by cpt;run;

data tbl_nodes;
set tbl_nodes1 tbl_nodes2;
format name $15. color $15.;
if from="Kiné" then color="#E34007";
if from="Radio" then color="#03a032";
if from="IRM/Arthroscanner" then color="#03a0a0";
if from="Infiltration" then color="#fec863";
if from="Chirurgie" then color="grey";
if from=" " then color="white";
rename from=name;
run;

proc sort data=tbl_nodes;by cpt;run;

/*Ecriture des tables SAS sous sasdata1 des différentes associations d'événements possibles (sans ordre chronologique) pour utilisation sous R (lecture des tables SAS sous R avec package haven)*/
%macro tt;
%do i=1 %to &nb_graphe2;
	data sasdata1.tbl_links_&i;
	set tbl_links;
	where cpt=&i;
	drop cpt cpt2 conc_ev;
	run;

    data sasdata1.tbl_nodes_&i;
	set tbl_nodes;
	where cpt=&i;
	drop cpt;
	run;
	%end;
%mend;
%tt;

/* 3 - Mise en forme des données pour création d'un diagramme en barre des associations d'événements possibles (sans ordre chronologique)*/

data evenement;
set parcours;
suivi='1';
drop exe_soi_dtd;
run;

proc transpose data=evenement out=evenement2 (drop=_name_) prefix=evenement_;by ben_idt_ano;id parcours;var suivi;run;

data evenement2;
set evenement2;
format asso_ev $100.;
if evenement_1='1' then evenement_1_='Radio';
if evenement_2='1' then evenement_2_='Kiné';
if evenement_3='1' then evenement_3_='Infiltration';
if evenement_4='1' then evenement_4_='IRM/Arthroscanner';
if evenement_5='1' then evenement_5_='Chirurgie';
asso_ev=catx('-',evenement_1_,evenement_2_,evenement_3_,evenement_4_,evenement_5_);
run;

proc freq data=evenement2 order=freq noprint;
table asso_ev/out=evenement2;
run;

/*Ecriture de la table SAS sous sasdata1 pour utilisation sous R (lecture de la table SAS sous R avec package haven)*/
data sasdata1.evenement_pre_op;
set evenement2;
percent=round(percent,0.1);
run;

