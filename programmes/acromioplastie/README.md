# Description des parcours de soins préopératoires des adultes de 40 ans et plus opérés d’une acromioplastie isolée

## Description
La Haute Autorité de Santé a publié, en septembre 2023, une recommandation sur la « conduite diagnostique devant une épaule douloureuse non traumatique de l’adulte et prise en charge des tendinopathies de la coiffe des rotateurs ». Conjointement, afin de contextualiser et d’appuyer cette recommandation, une étude sur le SNDS a été réalisée pour décrire le parcours de soins préopératoires des adultes de 40 ans et plus opérés d’une acromioplastie isolée sur le 2nd semestre 2022.

Ce projet a été mené par la Mission Data, en collaboration avec le Service des Bonnes Pratiques. Il a démarré en février 2023 et a été finalisé en juillet 2023. Les résultats ont été publiés en septembre 2023.

Ci-dessous les principaux résultats tirés de la fiche état des pratiques publiée sur le site de la HAS:

![Alt text](acromioplastie.PNG)

## Liens utiles

### Vers les documents de la HAS
- [Présentation de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/version_synthetique_du_rapport.pdf)
- [Rapport de l'étude](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/etat_des_pratiques.pdf)
- [Fiche état des pratiques](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/tendinopathie_non_rompue_de_l_epaule_infographie.pdf)
- [Recommandation de bonnes pratiques](https://www.has-sante.fr/jcms/p_3459565/fr/conduite-diagnostique-devant-une-epaule-douloureuse-non-traumatique-de-l-adulte-et-prise-en-charge-des-tendinopathies-de-la-coiffe-des-rotateurs), dont lien vers l'ensemble des documents


### Vers les programmes
L'ensemble des programmes de traitement se trouve [ici](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie).
- [01_TENR_extraction_population.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/01_TENR_extraction_population.sas)
- [02_TENR_parcours_soins_pre_op.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/02_TENR_parcours_soins_pre_op.sas)
- [03_1_TENR_restitution_description_tableaux.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/03_1_TENR_restitution_description_tableaux.sas)
- [03_2_TENR_restitution_description_graphes.R](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/03_2_TENR_restitution_description_graphes.R)
- [04_1_TENR_restitution_parcours_tableaux.sas](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/04_1_TENR_restitution_parcours_tableaux.sas)
- [04_2_TENR_restitution_parcours_graphes.R](https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/04_2_TENR_restitution_parcours_graphes.R)

## Contexte technique

### Données et logiciels utilisés
Les données utilisées (DCIR et PMSI MCO) sont issues du SNDS. L’accès aux données du SNDS s’est fait via le portail de l’assurance maladie, au titre de l’accès permanent dont dispose la HAS. 

L'extraction de données a été réalisée avec le logiciel SAS. Les analyses ont été réalisées avec les logiciels SAS et R.

 
## Contacts
Pour toute question sur la mise en place de cette étude et les programmes associés, n'hésitez pas à contacter la mission data: data@has-sante.fr

## Détails techniques

### Flux de données
Le flux de données ci-dessous accompagne les programmes mis à disposition sur le dépôt.

Cliquez sur les éléments du graphique pour accéder directement aux programmes ou à la description des tables

> 📝 **Notes :**
> - Le nom des programmes débute par leur numéro d'ordre de lancement, suivi du préfixe "TENR_"
> - Le nom des tables débute par le préfixe "TENR_" et se termine par la période de l'étude (ici "S2_2022")

```mermaid

   flowchart TB

    Data0(SNDS PORTAIL CNAM) --> Prog1[01_TENR_EXTRACTION_POPULATION.SAS]

    subgraph "SELECTION POPULATION"
        Prog1[01_TENR_EXTRACTION_POPULATION.SAS] --> Tab1(TENR_PATIENT_CHIR_S2_2022.SAS7BDAT)
        Prog1[01_TENR_EXTRACTION_POPULATION.SAS] --> Tab2(TENR_ID_PATIENT_S2_2022.SAS7BDAT)
    end

    Tab1(TENR_PATIENT_CHIR_S2_2022.SAS7BDAT) --> Data1(POPULATION D'ETUDE)
    Tab2(TENR_ID_PATIENT_S2_2022.SAS7BDAT) --> Data1(POPULATION D'ETUDE)
    Data1(POPULATION D'ETUDE) --> Prog2[02_TENR_PARCOURS_SOINS_PRE_OP.SAS]

    subgraph "TRAITEMENT DE DONNEES"
        Prog2[02_TENR_PARCOURS_SOINS_PRE_OP.SAS] --> Tab3(TENR_PARCOURS_PRE_OP_S2_2022.SAS7BDAT)
    end

    Tab3(TENR_PARCOURS_PRE_OP_S2_2022.SAS7BDAT) --> Data2(DONNEES A ANALYSER)
    Data2(DONNEES A ANALYSER) --> Prog3[03_1_TENR_RESTITUTION_DESCRIPTION_TABLEAUX.SAS]
    Data2(DONNEES A ANALYSER) --> Prog5[04_1_TENR_RESTITUTION_PARCOURS_TABLEAUX.SAS]

    subgraph "ANALYSE STATISTIQUE"
        Prog3[03_1_TENR_RESTITUTION_DESCRIPTION_TABLEAUX.SAS]
        Prog3[03_1_TENR_RESTITUTION_DESCRIPTION_TABLEAUX.SAS] --> Prog4[03_2_TENR_RESTITUTION_DESCRIPTION.R]
        Prog4[03_2_TENR_RESTITUTION_DESCRIPTION_GRAPHES.R]
        Prog5[04_1_TENR_RESTITUTION_PARCOURS_TABLEAUX.SAS] --> Prog6[04_2_TENR_RESTITUTION_PARCOURS_GRAPHES.R]
        Prog6[04_2_TENR_RESTITUTION_PARCOURS_GRAPHES.R]
    end

    Prog3[03_1_TENR_RESTITUTION_DESCRIPTION_TABLEAUX.SAS] --> Data3(RESULTATS)
    Prog4[03_2_TENR_RESTITUTION_DESCRIPTION_GRAPHES.R] --> Data3(RESULTATS)
    Prog5[04_1_TENR_RESTITUTION_PARCOURS_TABLEAUX.SAS] --> Data3(RESULTATS)
    Prog6[04_2_TENR_RESTITUTION_PARCOURS_GRAPHES.R] --> Data3(RESULTATS)

    click Tab1 " ./index.html#tenr-patient-chir-periode"
    click Tab2 "./index.html#tenr-id-patient-periode"
    click Tab3 "./index.html#tenr-parcours-pre-op-periode"


    click Prog1 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/01_TENR_extraction_population.sas"
    click Prog2 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/02_TENR_parcours_soins_pre_op.sas"
    click Prog3 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/03_1_TENR_restitution_description_tableaux.sas"
    click Prog4 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/03_2_TENR_restitution_description_graphes.R"
    click Prog5 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/04_1_TENR_restitution_parcours_tableaux.sas"
    click Prog6 "https://gitlab.has-sante.fr/has-sante/public/programmes-snds/-/blob/main/programmes/acromioplastie/04_2_TENR_restitution_parcours_graphes.R"

    style Tab1 fill:#ffe9a6,stroke:#f66
    style Tab2 fill:#ffe9a6,stroke:#f66
    style Tab3 fill:#ffe9a6,stroke:#f66
    style Data0 fill:#E5A099, stroke:#CC5D52
    style Data1 fill:#E5A099, stroke:#CC5D52
    style Data2 fill:#E5A099, stroke:#CC5D52
    style Data3 fill:#E5A099, stroke:#CC5D52
```
### Description des tables principales

#### TENR_PATIENT_CHIR_PERIODE
Table contenant les actes ou association d'actes de chirurgie de l'épaule (pour tendinoapthie non rompue de la coiffe des rotateurs) et les caractéristiques des patients inclus dans l'étude 

| Variable    | Libelle                                                                                                                                     |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| BEN_IDT_ANO | Identifiant du patient                                                                                                                      |
| SEXE        | Sexe du patient                                                                                                                             |
| AGE         | Age du patient                                                                                                                              |
| DPT_PATIENT | Département du patient                                                                                                                      |
| DATE_ENTREE | Date d'entrée du séjour de chirurgie de l'épaule                                                                                            |
| DATE_SORTIE | Date de sortie du séjour de chirurgie de l'épaule                                                                                           |
| ACTES_CCAM  | Actes ou association d'actes CCAM de chirurgie de l'épaule (pour tendinopathie non rompue de la coiffe des rotateurs) - 1 ligne par patient |
| NB_ACTES    | Nombre d'actes CCAM                                                                                                                         |
| DPT_EXEC    | Département de l'établissement exécutant                                                                                                    |
| FINESS_EXEC | Finess de l'établissement exécutant                                                                                                         |

#### TENR_ID_PATIENT_PERIODE
Table contenant l'ensemble des couples possibles d'identifiants patients (BEN_NIR_PSA||BEN_RNG_GEM) pour les patients inclus dans l'étude

| Variable    | Libelle                                                                 |
| ----------- | ----------------------------------------------------------------------- |
| BEN_IDT_ANO | Identifiant unique du patient                                           |
| BEN_IDT_TOP | Top identifiant patient: 1 si ben_idt_ano égal au NIR patient / 0 sinon |
| BEN_NIR_PSA | Identifiant patient (n'est pas forcément unique pour un patient)        |
| BEN_RNG_GEM | Rang gémellaire du patient                                              |

#### TENR_PARCOURS_PRE_OP_PERIODE
Table contenant les consultations, séances de kiné, corticoides injectables et les actes d'intérêt dans les 18 mois précédent la chirurgie de l'épaule de référence pour les patients inclus dans l'étude

| Variable         | Libelle                                                                                                                                                                                          |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| BEN_IDT_ANO      | Identifiant du patient                                                                                                                                                                           |
| TYPE             | Provenance des données: DCIR / PMSI_ACE                                                                                                                                                          |
| CODE             | Code CCAM de l'acte ou code de la consultation (en norme PS5) ou code CIP du corticoide ou code des soins de kinésithérapie (en norme PS5)                                                       |
| SANS_GUIDAGE     | Egal à "sans_guidage" si patient a eu un acte CCAM d'infiltration sans guidage (n'est renseigné qu'une seule fois par patient si plusieurs prestations); vide sinon                              |
| SOUS_GUIDAGE     | Egal à "sous_guidage" si patient a eu un acte CCAM d'infiltration sous guidage (n'est renseigné qu'une seule fois par patient si plusieurs prestations); vide sinon                              |
| EXE_SOI_DTD      | Date de la prestation ou de la délivrance pour corticoides                                                                                                                                       |
| GPE_PRESTATION   | Grand groupe de prestations: soins_conservateurs / consultations / imagerie_demarche_diagnostique / imagerie_avancee / scanner                                                                   |
| TYPE_PRESTA      | Type de prestation: Kinésithérapie / Infiltration / Radiographie / Echographie / "IRM/Arthroscanner" / C MG / "C MPR / Rhumatologue" / C Orthopédiste / Scanner                                  |
| PRESTA_DETAIL    | Prestation détaillée: C_MG / C_MPR / C_ortho / C_rhumato / kine / IRM / scanner / radio /echo / arthro_irm / arthro_scanner / infiltration_sans_guidage / infiltration_sous_guidage / corticoide |
| DELAI_PRE_OP     | Délai en jour entre la prestation (ou la délivrance pour les corticoides) et la chirurgie de l'épaule de référence                                                                               |
| PRS_ACT_QTE      | Nombre d'actes / consultations / séances de kiné / délivrances de corticoides                                                                                                                    |
| QTE_SANS_GUIDAGE | Egal au nombre d'actes CCAM d'infiltration sans guidage (n'est renseigné qu'une seule fois par patient si plusieurs prestations); à 0 sinon                                                      |
| QTE_SOUS_GUIDAGE | Egal au nombre d'actes CCAM d'infiltration sous guidage (n'est renseigné qu'une seule fois par patient si plusieurs prestations); à 0 sinon                                                      |
| DPT_EXEC         | Département de l'exécutant  (Dpt de l'établissement ou du professionnel de santé ou de la pharmacie pour corticoides)                                                                            |
| FINESS_EXEC      | Finess de l'établissement exécutant ou de la pharmacie pour corticoides (peut être non renseigné pour le DCIR)                                                                                   |
| SPE_PSP          | Spécialité du prescripteur de corticoides ou de kinésithérapie       