/**************************************************************************************************************************************************************
	 						DESCRIPTION DES PARCOURS DE SOINS PREOPERATOIRES DES ADULTES DE 40 ANS ET PLUS OPERES D�UNE ACROMIOPLASTIE ISOLEE

PARCOURS DE SOINS PRE-OPERATOIRE:
SELECTION DES ACTES D'IMAGERIE ET D'INFILTRATION, DES CONSULTATIONS / TELECONSULTATIONS DE SPECIALISTES, DES SOINS DE KINESITHERAPIE, ET DES CORTICOIDES INJECTABLES DANS LES 18 MOIS AVANT LA CHIRURGIE DE L'EPAULE
EN LIBERAL ET EN CONSULTATION EXTERNE (ACE)
	
	1 - EXTRACTION DCIR DES CONSULTATIONS, SEANCES KINE, CORTICOIDES ET DES ACTES D'INTERET DANS LES 18 MOIS PRECEDENT LA CHIRURGIE DE LA COIFFE DES ROTATEURS
	2 - EXTRACTION PMSI MCO ACE DES CONSULTATIONS ET DES ACTES D'INTERET DANS LES 18 MOIS PRECEDENT LA CHIRURGIE DE LA COIFFE DES ROTATEURS
	3 - CARACTERISATION DES TYPES DE CONSULTATIONS ET D'ACTES
		
	Table produite par le programme:
		- TABLE sasdata1.TENR_parcours_pre_op_S2_2022
	
**************************************************************************************************************************************************************/


/***************************************************************D�fintion des macrovariables**********************************************************************/

/*P�riode d'�tude*/
%let per=S2;
%let anneem=2022;
%let anneep=22;
%let anneep1=21;
%let date_debflux=202102;
%let date_finflux=202307;
%let date_debm=202101;
%let date_finm=202212;

/*Delai de suivi en jour avant la chirurgie de l'�paule de r�f�rence*/
%let delai=540;

/*Actes � prendre en compte DCIR et PMSI*/
%let radio='MAQK001','MAQK002','MAQK003','MZQK003';
%let irm='MZQN001','MZQJ001';
%let scanner='MZQH002','MZQK002';
%let arthro='MEQH001';/*Pour s�lection arthro IRM*/
%let arthro_scanner='MZQH001';
%let echo='PBQM001','PBQM002','PBQM003','PBQM004','PCQM001';
%let infiltration='MZLB001','MZLH001','	MZLH002';

/*Prestations DCIR*/
%let consult=1098,1099,1101,1102,1103,1105,1109,1110,1111,1112,1113,1114,1115,1117,1118,1140,1168;
%let tele_consult=1096,1157,1164,1191,1192;
%let kine=3121,3122,3125;
%let cip13=3400932005093,3400932057771,3400930109670,3400930236758,3400930515587,3400933697860,3400930240656,3400930218044,3400930217986,3400956307579,3400930218808;
%let cip13c='3400932005093','3400932057771','3400930109670','3400930236758','3400930515587','3400933697860','3400930240656','3400930218044','3400930217986','3400956307579','3400930218808'; 
%let cip7=3200509,3205777,3010967,3023675,3051558,3369786,3024065,3021804,3021798,5630757,3021880;

/*Prestations PMSI*/
%let consultp='C','C   N','C   F','CS','CS  N','CS  F','G','G   F','G   N','GS','GS  N','GS  F','CA','CA  N','CA  F','CSC','CSC N','CSC F','CDE','CDE F','CDE N','CNP','CNP F','CNP N',
'APU','APU N','APU F','APY','APY N','APY F','APC','APC N ','APC F','CCX','CCX N','CCX F','CCP','CCP N','CCP F';
%let tele_consultp='TTE','TTE N','TTE F','TCP','TCP N','TCP F','TLC','TLC F','TLC N','TC','TC  N','TC  F','TCG','TCG F','TCG N';
%let kinep='AMS','AMS N','AMS F','AMK','AMK N','AMK F','AMC','AMC F','AMC N';

%let fin_geo ='130780521', '130783236', '130783293', '130784234', '130804297','600100101','750041543', '750100018', '750100042', '750100075', '750100083', '750100091', '750100109', '750100125', '750100166','750100208', '750100216', 
'750100232', '750100273', '750100299' , '750801441', '750803447', '750803454', '910100015','910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037','930100045', '940100027', 
'940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137','690784152', '690784178', '690787478', '830100558' ;

/********************************************************************************************************************************************************************/
/********************************************************************************************************************************************************************/

/*Pour chaque couple possible ben_nir_psa||ben_rng_gem, calcul de la date maximale � prendre en compte avant la chirurgie pour le suivi pr�-op�ratoire
Pour rappel, le suivi pr�-op�ratoire est de 18 mois => 540 jours*/
proc sql;
create table id_patient_suivi as select distinct a.ben_nir_psa, a.ben_rng_gem,a.ben_idt_ano, b.date_entree 
from sasdata1.TENR_id_patient_&per._&anneem  a
inner join sasdata1.TENR_patient_chir_&per._&anneem  b on (a.ben_idt_ano=b.ben_idt_ano);
quit;

data id_patient_suivi;
set id_patient_suivi;
format date_limite DATETIME20.;
date_limite=date_entree-(&delai*24*3600);
run;

proc sql;
  drop table orauser.id_patient_suivi;
  CREATE TABLE orauser.id_patient_suivi AS SELECT * from id_patient_suivi;
QUIT;

/*******************************************************************************************************************************
			1 - EXTRACTION DCIR DES CONSULTATIONS, SEANCES KINE, CORTICOIDES ET DES ACTES D'INTERET DANS LES 18 MOIS PRECEDENT LA CHIRURGIE DE LA COIFFE DES ROTATEURS
*******************************************************************************************************************************/


/**************************************************************************
Filtres DCIR
---------------------------------------------------------------------------
Exclusion des "m�decins fictifs"
Exclusion des consultations / actes / corticoides le jour de la chirurgie de l'�paule de r�f�rence
Exclusion de l'activit� des ES Publics (ACE) => r�cup�r�e dans le PMSI 
Exclusion des prestations r�alis�es lors d'hospitalisations (ES ex-OQN) => r�cup�r�es dans le PMSI
Exclusion des lignes de consultations / actes / corticoides avec quantit� <=0
*************************************************************************/

/* 1 - Extraction des prestations d'int�r�t de chaque patient dans les 18 mois pr�c�dent la chirurgie de l'�paule de r�f�rence*/

%macro extract(date);

/*S�lection dpt et commune de chaque PS par mois d'extraction*/
PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM
FROM oravue.DA_PRA_R
WHERE DTE_ANN_TRT = "&an1" AND dte_moi_fin = "&mois1" ;
QUIT;

/*Extraction des consultations et soins de kin�sith�rapie*/
proc sql;
%connectora;

CREATE TABLE consult_dcir_pre_&date AS SELECT * from connection to oracle 
(select b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.psp_spe_cod,b.PSE_SPE_COD, b.PRS_NAT_REF, b.PRS_ACT_CFT,b.EXE_SOI_DTD,a.date_entree, a.ben_idt_ano,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num,
sum(b.PRS_ACT_QTE) as PRS_ACT_QTE
      FROM ER_PRS_F b 
      inner join id_patient_suivi a on (a.BEN_NIR_PSA = b.BEN_NIR_PSA AND a.BEN_RNG_GEM = b.BEN_RNG_GEM )
      left join ER_ETE_F c on 		  
      	b.FLX_DIS_DTD = c.FLX_DIS_DTD and b.FLX_TRT_DTD = c.FLX_TRT_DTD and b.FLX_EMT_TYP = c.FLX_EMT_TYP and b.FLX_EMT_NUM = c.FLX_EMT_NUM and b.FLX_EMT_ORD = c.FLX_EMT_ORD and b.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = c.DCT_ORD_NUM and b.PRS_ORD_NUM = c.PRS_ORD_NUM and b.REM_TYP_AFF = c.REM_TYP_AFF
      WHERE b.flx_dis_dtd=to_date(&date.01,'YYYYMMDD')
      AND a.date_entree > b.EXE_SOI_DTD 
      AND a.DATE_LIMITE <= b.EXE_SOI_DTD 
      AND b.CPL_MAJ_TOP < 2 
      AND b.DPN_QLF NOT IN (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      AND PSE_STJ_COD not in (99) /*exclusion des m�decins fictifs*/
      AND b.BSE_PRS_NAT IN (&consult, &tele_consult, &kine) /*codes consultations et t�l�consultations*/
GROUP BY b.BEN_NIR_PSA, b.BEN_RNG_GEM, b.PFS_EXE_NUM, b.psp_spe_cod,b.PSE_SPE_COD, b.PRS_NAT_REF, b.PRS_ACT_CFT,b.EXE_SOI_DTD,a.date_entree, a.ben_idt_ano,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num);
disconnect from oracle;
quit;

proc sql;
create table consult_dcir_pre_&date as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from consult_dcir_pre_&date a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;

/*Extraction des actes (imagerie et infiltration)*/
proc sql;
%connectora;

create table acte_dcir_pre_&date as SELECT * from connection to oracle 
(select b.ben_nir_psa, b.ben_rng_gem, b.PFS_EXE_NUM,b.pse_spe_cod,b.EXE_SOI_DTD,b.prs_nat_ref,d.cam_prs_ide as code_ccam,a.date_entree,a.ben_idt_ano,
c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num,
(SUM(CASE WHEN b.CPL_MAJ_TOP <2 THEN b.PRS_ACT_QTE ELSE 0 END)) as prs_act_qte
from er_prs_f b 
  inner join id_patient_suivi a on (a.BEN_NIR_PSA = b.BEN_NIR_PSA AND a.BEN_RNG_GEM = b.BEN_RNG_GEM )
  inner join er_cam_f d on
	d.FLX_DIS_DTD = b.FLX_DIS_DTD and d.FLX_TRT_DTD = b.FLX_TRT_DTD and d.FLX_EMT_TYP = b.FLX_EMT_TYP and d.FLX_EMT_NUM = b.FLX_EMT_NUM and d.FLX_EMT_ORD = b.FLX_EMT_ORD and d.ORG_CLE_NUM = b.ORG_CLE_NUM and
  	d.DCT_ORD_NUM = b.DCT_ORD_NUM and d.PRS_ORD_NUM = b.PRS_ORD_NUM and d.REM_TYP_AFF = b.REM_TYP_AFF
  left join ER_ETE_F c on 		  
	b.FLX_DIS_DTD = c.FLX_DIS_DTD and b.FLX_TRT_DTD = c.FLX_TRT_DTD and b.FLX_EMT_TYP = c.FLX_EMT_TYP and b.FLX_EMT_NUM = c.FLX_EMT_NUM and b.FLX_EMT_ORD = c.FLX_EMT_ORD and b.ORG_CLE_NUM = c.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = c.DCT_ORD_NUM and b.PRS_ORD_NUM = c.PRS_ORD_NUM and b.REM_TYP_AFF = c.REM_TYP_AFF
  WHERE b.flx_dis_dtd=to_date(&date.01,'YYYYMMDD')
      AND a.date_entree > b.EXE_SOI_DTD 
      AND a.DATE_LIMITE <= b.EXE_SOI_DTD 
      AND b.PSE_STJ_COD not in (99) /*exclusion des m�decins fictifs*/
      and b.DPN_QLF not in (71) /*exclusion de l'activit� des h�pitaux publics transmis pour information => activit� r�cup�r�e dans le PMSI */
      and d.cam_prs_ide in (&radio,&irm,&scanner,&arthro_scanner,&echo,&infiltration,&arthro) and d.CAM_ACT_COD = '1' /*s�lection actes, activt�=1 */
group by b.ben_nir_psa, b.ben_rng_gem, b.PFS_EXE_NUM,b.pse_spe_cod,b.EXE_SOI_DTD,b.prs_nat_ref,d.cam_prs_ide,a.date_entree,a.ben_idt_ano,c.ETE_IND_TAA, c.ETE_CAT_COD, c.PRS_PPU_SEC, c.ETB_EXE_FIN, c.mdt_cod,c.ete_ghs_num);
disconnect from oracle;
quit;

proc sql;
create table acte_dcir_pre_&date as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM
from acte_dcir_pre_&date a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;
%mend;

%macro lance;
%do i=&date_debflux %to &date_finflux;
        %let an1=%sysfunc(substr(&i,1,4));
	%let mois1=%eval(%sysfunc(substr(&i,5,2))-1);
	%if %length(&mois1)=1 %then %do;
			%let mois1=%sysfunc(cats(0,&mois1));
	%end;
    	%if &i=202113 %then %do;
        	%let i=202201;
    	%end;
    	%if &i=202201 %then %do;
	            %let an1=2021;
	            %let mois1=12;
    	%end;
        %if &i=202213 %then %do;
               %let i=202301;
    	%end;
    	%if &i=202301 %then %do;
		     %let an1=2022;
		     %let mois1=12;
    	%end;
%extract(&i);
%end;
%mend;
%lance;

data consult_dcir;
set consult_dcir_pre:;
run;

data acte_dcir;
set acte_dcir_pre:;
run;

/*Extraction des corticoides injectables (disponibles dans DCIR uniquement)*/
%MACRO CREA_TABLE ( FLX_DIS_DTD ) ;
%PUT ### Mois de flux ( &FLX_DIS_DTD ) D�but %SYSFUNC( datetime(),datetime. ) ;

proc sql;
create table med_dcir_ as select b.ben_nir_psa, b.ben_rng_gem,b.PFS_EXE_NUM,b.psp_spe_cod,b.EXE_SOI_DTD,b.prs_nat_ref,d.pha_prs_c13,d.pha_prs_ide,d.pha_act_qsn
from oravue.er_prs_f b 
   inner join oravue.er_pha_f d on
	b.FLX_DIS_DTD = d.FLX_DIS_DTD and b.FLX_TRT_DTD = d.FLX_TRT_DTD and b.FLX_EMT_TYP = d.FLX_EMT_TYP and b.FLX_EMT_NUM = d.FLX_EMT_NUM and b.FLX_EMT_ORD = d.FLX_EMT_ORD and b.ORG_CLE_NUM = d.ORG_CLE_NUM and
  	b.DCT_ORD_NUM = d.DCT_ORD_NUM and b.PRS_ORD_NUM = d.PRS_ORD_NUM and b.REM_TYP_AFF = d.REM_TYP_AFF
  WHERE b.flx_dis_dtd="&flx_dis_dtd:0:0:0"dt
  and b.exe_soi_dtd between ("&date_exe_deb:0:0:0"dt) and ("&date_exe_fin:0:0:0"dt)
      and b.DPN_QLF not in (71)
      and (d.pha_prs_c13 in (&cip13,&cip7) or d.pha_prs_ide in (&cip13,&cip7)); /*Codage CIP 13 normalement pour pha_prs_c13 et cip7 pour pha_prs_ide*/
quit;

PROC SQL;
CREATE TABLE PS AS SELECT pfs_pfs_num,PFS_COD_POS,PFS_EXC_COM,input(DTE_MOI_FIN,2.) as dte_fin,PFS_FIN_NUM
FROM ORAVUE.DA_PRA_R
WHERE DTE_ANN_TRT = "&an" AND (calculated DTE_FIN) = &mois1 ;
QUIT;

proc sql;
create table med_dcir_ as select a.*, b.PFS_COD_POS,b.PFS_EXC_COM,b.PFS_FIN_NUM
from med_dcir_ a
left join ps b
on (a.pfs_exe_num=b.pfs_pfs_num);
quit;

%PUT ### CREA_TABLE ( &FLX_DIS_DTD ) Fin %SYSFUNC( datetime(),datetime. ) ;
%MEND CREA_TABLE ;

/***  Macro Compilation des tables mensuelles (proc append)****/
%MACRO COMPILATION() ;
%IF %SYSFUNC(exist(med_dcir)) eq 0 %THEN %DO ;
proc datasets nolist memtype=data;
change med_dcir_=med_dcir;
quit;
%END ;
%ELSE %DO;
proc append base=med_dcir data=med_dcir_ FORCE;run;
proc delete data=med_dcir_;run;
%END;
%MEND COMPILATION ;

/*** Ex�cution pour chaque mois de flux***/
%MACRO TAB_PRS( Date_DEB= ,Date_FIN= ) ;

/*DEFINITION DES PARAMETRES*/
%LET AN_DEB = %SUBSTR( &DATE_DEB,1,4 ) ;
%LET mois_deb = %SUBSTR( &DATE_DEB,5,2 ) ;
%LET AN_FIN = %SUBSTR( &DATE_FIN,1,4 ) ;
%LET mois_fin = %SUBSTR( &DATE_FIN,5,2 ) ;

/*dates de soin en format SAS*/
%LET EXE_DEB = %SYSFUNC( mdy( &mois_deb,1,&AN_DEB )) ;
%LET EXE_FIN0 = %SYSFUNC( mdy( &mois_fin,1,&an_fin )) ; 
%LET EXE_FIN = %EVAL( %SYSFUNC( intnx( month,&EXE_FIN0, 1 ))-1 ) ; /*pour aller jusqu'au dernier jour du mois de la date de fin donn�e*/

/*dates de soin en format date9*/
%LET DATE_EXE_DEB = %SYSFUNC( putn( &EXE_DEB,date9. )) ;
%LET DATE_EXE_FIN = %SYSFUNC( putn( &EXE_FIN,date9. )) ;
 
 /*suppression de la table compil�e des mois extraits si ce n'est pas la 1�re ex�cution*/
%IF %SYSFUNC(exist(med_dcir)) %THEN %DO;
 	PROC DELETE DATA=med_dcir;
	RUN ;
%END;

/*BOUCLE MENSUELLE DE SELECTION DES PRESTATIONS*/
%LET iterDeb = 1; 
%LET iterFin = %SYSFUNC( intck( month,&EXE_DEB,&EXE_FIN )) ;/*Nombre de mois entre le d�but et la fin des soins */

/*BOUCLE SUR LES MOIS*/
	%DO mois=&iterDeb %TO &iterFin + 7 ;/*flux sur p�riode + 6 mois*/
	%LET FLX_DIS_DTD = %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois ), date9. ) ;
	%LET an = %SYSFUNC( year( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1 )))) ;
	%LET mois1 = %SYSFUNC( month( %SYSFUNC( intnx( MONTH,&EXE_DEB,&mois-1)))) ;/*s�lection mois dans DA_PRA_R*/
				 
	/*affichage dans le journal*/
	%PUT "Valeurs des param�tres" ;
	%PUT DATE_EXE_DEB = &DATE_EXE_DEB ;
	%PUT DATE_EXE_FIN = &DATE_EXE_FIN ;
	%PUT an = &an ;
	%put mois= &mois1;
	%PUT FLX_DIS_DTD = &FLX_DIS_DTD ;

	%CREA_TABLE(&FLX_DIS_DTD) ;
	%COMPILATION() ;			
%END ;

%MEND TAB_PRS;

/*lancement de la macro*/
OPTIONS MPRINT ;
%TAB_PRS(Date_DEB = &date_debm,Date_FIN = &date_finm) ;
 
/*S�lection des patients de l'�tude avec corticoides d�livr�s dans les 18 mois pr�c�dent la chirurgie*/
proc sort data=med_dcir;by ben_nir_psa ben_rng_gem;run;
proc sort data=id_patient_suivi;by ben_nir_psa ben_rng_gem;run;
 
data med_dcir;
merge med_dcir (in=a) id_patient_suivi (in=b);
by ben_nir_psa ben_rng_gem;
if a and b then output;
drop ben_nir_psa ben_rng_gem;
run;
 
data med_dcir;
set med_dcir;
if date_entree > EXE_SOI_DTD AND DATE_LIMITE <= EXE_SOI_DTD then output;
drop date_limite;
run;

/* 2 - Nettoyage table consultations / soins kin� */

/*exclusion de l'activit� des �tablissements publics ex-DG (ACE) et des consultations lors d'hospitalisations (ES ex-OQN) */

proc sql;
create table consult_dcir as select * 
from consult_dcir
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data consult_dcir;
set consult_dcir;
if prs_ppu_sec=1 or mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
run;

proc sql;
create table consult_dcir as select * 
from consult_dcir
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;


/*suppression des lignes si qte <= 0 */
proc sql;
create table consult_dcir as select distinct ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, psp_spe_cod as spe_psp,PRS_NAT_REF,prs_act_cft,EXE_SOI_DTD,date_entree, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM,sum(PRS_ACT_QTE) as PRS_ACT_QTE 
from consult_dcir
group by ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, psp_spe_cod,PRS_NAT_REF, prs_act_cft,EXE_SOI_DTD, date_entree, ETB_EXE_FIN,PFS_COD_POS,PFS_EXC_COM
having (calculated prs_act_qte)>0;
quit;


/* 3 - Nettoyage table actes */

/* exclusion de l'activit� des �tablissements publics ex-DG (ACE) et des actes lors d'hospitalisations (ES ex-OQN) */
proc sql;
create table acte_dcir as select * 
from acte_dcir
where NOT(ETE_IND_TAA= 1 OR (ETE_IND_TAA= 0 AND PRS_PPU_SEC=1 AND ETE_CAT_COD in (101,355,131,106,122,365,128,129) AND MDT_COD in (0,3,7,10))) OR (ETE_IND_TAA is null AND PRS_PPU_SEC is null AND ETE_CAT_COD is null AND MDT_COD is null) ;
quit;

data acte_dcir;
set acte_dcir;
if prs_ppu_sec=1 or mdt_cod in (3,4,5,6,11,12,13,15,17,20,22,24,37,38,39) then delete;
run;

proc sql;
create table acte_dcir as select * 
from acte_dcir
where (ETE_IND_TAA not in (1,2) or ete_ind_taa is null) and (ETE_GHS_NUM=0 or ete_ghs_num is null);
quit;


/*suppression des lignes d'actes si qte <= 0 */
proc sql;
create table acte_dcir as select distinct ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD,EXE_SOI_DTD,date_entree, ETB_EXE_FIN, code_ccam,PFS_COD_POS,PFS_EXC_COM,sum(PRS_ACT_QTE) as PRS_ACT_QTE 
from acte_dcir
group by ben_idt_ano,PFS_EXE_NUM, PSE_SPE_COD, EXE_SOI_DTD, date_entree, ETB_EXE_FIN, code_ccam,PFS_COD_POS,PFS_EXC_COM
having (calculated prs_act_qte)>0;
quit;

/* 4 - Nettoyage table corticoides injectables */

/*suppression des lignes si qte <= 0*/
proc sql;
create table med_dcir as select distinct ben_idt_ano,pfs_exe_num,psp_spe_cod as spe_psp,PRS_NAT_REF,EXE_SOI_DTD,date_entree,PFS_FIN_NUM as etb_exe_fin, PFS_COD_POS,PFS_EXC_COM,pha_prs_c13, sum(pha_act_qsn) as pha_act_qsn
from med_dcir
group by ben_idt_ano,pfs_exe_num,psp_spe_cod,PRS_NAT_REF,EXE_SOI_DTD,date_entree,PFS_FIN_NUM,PFS_COD_POS,PFS_EXC_COM,pha_prs_c13
having (calculated pha_act_qsn)>0;
quit;


/*******************************************************************************************************************************
			2 - EXTRACTION PMSI MCO ACE DES CONSULTATIONS ET DES ACTES D'INTERET DANS LES 18 MOIS PRECEDENT LA CHIRURGIE DE LA COIFFE DES ROTATEURS
*******************************************************************************************************************************/
/**************************************************************************
Filtres PMSI
---------------------------------------------------------------------------
Exclusion des finess geographiques APHP/APHM/HCL (donn�es en doublon sinon)
Exclusion des patients avec des codes retour incorrects (<>0)
Donn�es ACE MCO ex-DGF 2021 et 2022
Exclusion des consultations / actes le jour de la chirurgie de l'�paule de r�f�rence
*************************************************************************/

/* 1 - MCO ACE consultations / soins de kin�sith�rapie*/

proc sql; 
/*ann�e n*/
create table consult_pmsi_ace_mco_&anneep as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.date_entree,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD, c.ACT_COE as PRS_ACT_CFT,sum(ACT_NBR) as prs_act_qte
from  oravue.t_mco&anneep.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp,&kinep) /*actes de consultations et t�l�consultations*/
and b.date_entree > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.date_entree, b.ben_idt_ano,c.EXE_SPE, c.ACT_COD, c.ACT_COE;
/*ann�e n-1*/
proc sql; 
create table consult_pmsi_ace_mco_&anneep1 as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.date_entree,b.ben_idt_ano,input(c.exe_spe,2.) as pse_spe_cod, c.ACT_COD,c.ACT_COE as PRS_ACT_CFT,sum(ACT_NBR) as prs_act_qte
from  oravue.t_mco&anneep1.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep1.fcstc as c on a.eta_num=c.eta_num and c.seq_num=a.seq_num
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and ACT_COD IN (&tele_consultp,&consultp,&kinep) /*actes de consultations et t�l�consultations*/
and b.date_entree > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD
group by a.nir_ano_17,c.eta_num_geo ,c.exe_soi_dtd, b.date_entree,b.ben_idt_ano,c.EXE_SPE, c.ACT_COD, c.ACT_COE ;
quit;

data consult_pmsi_ace_mco;
set consult_pmsi_ace_mco_&anneep consult_pmsi_ace_mco_&anneep1;
run;

proc sql; 
create table consult_pmsi_ace_mco as select distinct ben_idt_ano,finess_exec,exe_soi_dtd, date_entree, pse_spe_cod, ACT_COD, PRS_ACT_CFT,sum(prs_act_qte) as prs_act_qte 
from  consult_pmsi_ace_mco
group by ben_idt_ano,finess_exec,exe_soi_dtd, date_entree,pse_spe_cod, ACT_COD, PRS_ACT_CFT;
quit;

/* 2 - MCO ACE actes CCAM*/

proc sql; 
/*ann�e n*/
create table acte_pmsi_ace_mco_&anneep as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd, b.date_entree,b.ben_idt_ano,c.ccam_cod as code_ccam
from  oravue.t_mco&anneep.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep.fmstc as c on (a.eta_num=c.eta_num and c.seq_num=a.seq_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and c.ccam_cod in (&radio,&irm,&scanner,&arthro_scanner,&echo,&infiltration,&arthro) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.date_entree > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD;
/*ann�e n-1*/
create table acte_pmsi_ace_mco_&anneep1 as select distinct 
a.nir_ano_17 as ben_nir_psa,c.eta_num_geo as finess_exec,c.exe_soi_dtd,b.date_entree, b.ben_idt_ano,c.ccam_cod as code_ccam
from  oravue.t_mco&anneep1.cstc as a 
		inner join orauser.id_patient_suivi as b on (a.nir_ano_17=b.ben_nir_psa)
		inner join oravue.t_mco&anneep1.fmstc as c on (a.eta_num=c.eta_num and c.seq_num=a.seq_num)
where a.eta_num not in (&fin_geo) /*exclusion des finess geographiques APHP/APHM/HCL*/
and a.nir_ret="0" and a.nai_ret="0" and a.sex_ret="0" and a.ias_ret="0" and a.ent_dat_ret="0" /* On ne garde que les patients avec des codes retours corrects */
and c.ccam_cod in (&radio,&irm,&scanner,&arthro_scanner,&echo,&infiltration,&arthro) and c.acv_act='1' /*S�lection actes, activit� =1 */
and b.date_entree > c.EXE_SOI_DTD AND b.DATE_LIMITE <= c.EXE_SOI_DTD;
quit; 

data acte_pmsi_ace_mco;
set acte_pmsi_ace_mco_&anneep acte_pmsi_ace_mco_&anneep1;
run;

proc sql; 
create table acte_pmsi_ace_mco as select distinct ben_idt_ano,finess_exec,exe_soi_dtd,date_entree, code_ccam,count(*) as prs_act_qte 
from acte_pmsi_ace_mco
group by ben_idt_ano,finess_exec,exe_soi_dtd, date_entree, code_ccam;
quit;


/*******************************************************************************************************************************
			3 - CARACTERISATION DES TYPES DE CONSULTATIONS ET D'ACTES
*******************************************************************************************************************************/

/* 1 - Harmonisation DCIR / PMSI sur finess => on met les finess DCIR sur 9 caract�res; si finess pas retrouv� dans la table datasante, on laisse le finess sur 8 caract�res*/
proc sql;
create table consult_dcir as select a.*, b.finess as finessgeo_exec
from consult_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);

create table acte_dcir as select a.*, b.finess as finessgeo_exec
from acte_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);

create table med_dcir as select a.*, b.finess as finessgeo_exec
from med_dcir a
left join rfcommun.datasante_t_finess b on (b.finess8=a.etb_exe_fin);
quit;

/* 2 - Concat�nation des tables DCIR (hors corticoides) et ACE PMSI et cr�ation de variables pour l'analyse */
data parcours_pre_op;
set consult_dcir (in=a) acte_dcir (in=b) consult_pmsi_ace_mco (in=c) acte_pmsi_ace_mco (in=d);

/*Cr�ation variable base de provenance des donn�es (PMSI / DCIR)*/
if (c or d) then type='PMSI_ACE';
if (a or b) then type='DCIR';

/*Cr�ation variable finess ex�cutant pour donn�es du DCIR*/
if type='DCIR' then do;
finess_exec=finessgeo_exec;
if etb_exe_fin ne '' and finessgeo_exec='' then finess_exec=etb_exe_fin;
end;

/*Cr�ation variable dpt ex�cutant: Dpt �tablissement si finess ex�cutant rempli (et pas en erreur)/ Dpt du PS sinon*/
format dpt_exec $3.;
if finess_exec ne '' then do;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);
else dpt_exec=substr(finess_exec,1,2);
end;
if substr(finess_exec,1,4) ='2001' then dpt_exec='2A';
if substr(finess_exec,1,4) ='2002' then dpt_exec='2B';
if finess_exec=' ' or substr(finess_exec,1,2)='99' then do;
if substr(pfs_cod_pos,1,3)  in ('201','200') then dpt_exec='2A'; 
else if substr(pfs_cod_pos,1,3) in ('202','206') then dpt_exec='2B'; 
else if substr(pfs_cod_pos,1,3) = '980' then dpt_exec=substr(pfs_cod_pos,1,3);
else if substr(pfs_cod_pos,1,2) in ('97','98') then dpt_exec=SUBSTR(PFS_COD_POS,1,2) || substr(PFS_EXC_COM,1,1);
else dpt_exec=substr(pfs_cod_pos,1,2);
end;
if dpt_exec in ('981','') then dpt_exec='99';

/*Cr�ation variable code prestation en norme PS5 pour les donn�es PMSI*/
format code $50.;
if type='PMSI_ACE' then do;
if act_cod in ('C','C   N','C   F') then prs_nat_ref=1111;
if act_cod in ('CS','CS  N','CS  F') then prs_nat_ref=1112;
if act_cod in ('G','G   F','G   N') then prs_nat_ref=1110;
if act_cod in ('GS','GS  N','GS  F') then prs_nat_ref=1109;
if act_cod in ('CA','CA  N','CA  F') then prs_nat_ref=1115;
if act_cod in ('CSC','CSC N','CSC F') then prs_nat_ref=1114;
if act_cod in ('CDE','CDE F','CDE N') then prs_nat_ref=1140;
if act_cod in ('CNP','CNP F','CNP N') then prs_nat_ref=1113;
if act_cod in ('APU','APU N','APU F') then prs_nat_ref=1101;
if act_cod in ('APY','APY N','APY F') then prs_nat_ref=1102;
if act_cod in ('APC','APC N ','APC F') then prs_nat_ref=1103;
if act_cod in ('CCX','CCX N','CCX F') then prs_nat_ref=1105;
if act_cod in ('TTE','TTE N','TTE F') then prs_nat_ref=1096;
if act_cod in ('TCP','TCP N','TCP F') then prs_nat_ref=1157;
if act_cod in ('TLC','TLC F','TLC N') then prs_nat_ref=1164;
if act_cod in ('CCP','CCP F','CCP N') then prs_nat_ref=1168;
if act_cod in ('TC','TC  N','TC  F') then prs_nat_ref=1191;
if act_cod in ('TCG','TCG F','TCG N') then prs_nat_ref=1192;
if act_cod in ('AMS','AMS N','AMS F') then prs_nat_ref=3125;
if act_cod in ('AMK','AMK N','AMK F') then prs_nat_ref=3122;
if act_cod in ('AMC','AMC N','AMC F') then prs_nat_ref=3121;
end;
if (b or d) then code=code_ccam;
if (a or c) then code=compress(put(prs_nat_ref,8.));

/*Correction pse_spe_cod si mqt et prs_nat_ref = consult de MG*/
if pse_spe_cod in (.,99,0) and prs_nat_ref in (1111,1110,1192) then pse_spe_cod=1;

if finess_exec ne '' then pfs_exe_num='';

if prs_nat_ref in (&tele_consult, &consult) and pse_spe_cod not in (1,22,23,14,31,41) then delete; /*Suppression des consultations non r�alis�es par MG, ortho, rhumato, MPR*/
if prs_nat_ref in (&kine) and PRS_ACT_CFT not in (7.5,9.5) then delete;/*Suppression des soins de kin�sith�rapie qui ne sont pas des actes de r��ducation*/

drop act_cod finessgeo_exec etb_exe_fin PFS_COD_POS PFS_EXC_COM;
run;

proc sql;
create table parcours_pre_op as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,prs_nat_ref,pse_spe_cod,spe_psp,pfs_exe_num,dpt_exec,sum(prs_act_qte) as prs_act_qte
from parcours_pre_op
group by ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,prs_nat_ref,pse_spe_cod,spe_psp,pfs_exe_num,dpt_exec;
quit;


/* 3 - Cr�ation d'un indicateur pour rep�rer les arthro IRM: association MEQH001 + MZQN001 => actes r�alis�s sur m�me patient, m�me jour et m�me Finess ou PS*/

/*Cr�ation d'une table contenant les arthro-IRM*/
proc sql;
create table selec_acte as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,dpt_exec,pfs_exe_num,sum(prs_act_qte) as prs_act_qte
from parcours_pre_op
where code in ('MEQH001','MZQN001')
group by ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,dpt_exec,pfs_exe_num;
quit;

data selec_acte;
set selec_acte;
arthro=0;irm=0;
if code='MEQH001' then arthro=prs_act_qte;
if code='MZQN001' then irm=prs_act_qte;
run;

proc sql;
create table selec_acte as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,finess_exec,dpt_exec,pfs_exe_num,sum(arthro) as arthro,sum(irm) as irm
from selec_acte
group by ben_idt_ano,exe_soi_dtd,date_entree,type,finess_exec,dpt_exec,pfs_exe_num;
quit;

data selec_acte;
set selec_acte;
if arthro>=1 and irm>=1 then do; prs_act_qte_=min(arthro,irm);code="MEQH001-MZQN001";output;end;
drop arthro irm;
run;

/*Suppression de la table des codes MEQH001 et MZQN001 qui repr�sentent des arthro IRM et ajout dans la table des lignes sp�cifiques d'arthro-IRM*/
proc sort data=parcours_pre_op;by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;run;

proc sort data=selec_acte out=selec_acte_ (keep=ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num prs_act_qte_);by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;run;

data parcours_pre_op;
merge parcours_pre_op (in=a) selec_acte_ (in=b);
by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;
if b and code in ('MEQH001','MZQN001') then do;top_arthro_irm=1;nb=prs_act_qte-prs_act_qte_;end;
run;

data parcours_pre_op;
set parcours_pre_op;
if top_arthro_irm=1 then do;
if nb=0 then delete;
else prs_act_qte=nb;
end;
drop nb top_arthro_irm prs_act_qte_;
run;

data parcours_pre_op;
set  selec_acte (rename=prs_act_qte_=prs_act_qte) parcours_pre_op;
if code in (&arthro) then delete; /*Suppression des arthrographies qui ne sont pas des arthro-IRM*/
run;

/* 4 - Cr�ation d'un indicateur pour rep�rer les infiltrations sous �chographie: association d'un code d'�chographie (PBQM001,PBQM002,PBQM003,PBQM004,PCQM001) + MZLB001=> actes r�alis�s sur m�me patient, m�me jour et m�me Finess ou PS*/

/*Cr�ation d'une table contenant les infiltrations sous echo*/
proc sql;
create table selec_acte as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,dpt_exec,pfs_exe_num,sum(prs_act_qte) as prs_act_qte
from parcours_pre_op
where code in ('PBQM001','PBQM002','PBQM003','PBQM004','PCQM001','MZLB001')
group by ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,dpt_exec,pfs_exe_num;
quit;

data selec_acte;
set selec_acte;
pbqm001=0;pbqm002=0;pbqm003=0;pbqm004=0;pcqm001=0;infiltration=0;
if code='PBQM001' then pbqm001=prs_act_qte;
if code='PBQM002' then pbqm002=prs_act_qte;
if code='PBQM003' then pbqm003=prs_act_qte;
if code='PBQM004' then pbqm004=prs_act_qte;
if code='PCQM001' then pcqm001=prs_act_qte;
if code='MZLB001' then infiltration=prs_act_qte;
run;

proc sql;
create table selec_acte as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,finess_exec,dpt_exec,pfs_exe_num,sum(pbqm001) as pbqm001,sum(pbqm002) as pbqm002,sum(pbqm003) as pbqm003,sum(pbqm004) as pbqm004,sum(pcqm001) as pcqm001,sum(infiltration) as infiltration
from selec_acte
group by ben_idt_ano,exe_soi_dtd,date_entree,type,finess_exec,dpt_exec,pfs_exe_num;
quit;

data selec_acte;
set selec_acte;
if pbqm001>=1 and infiltration>=1 then do; prs_act_qte_=min(pbqm001,infiltration);code="PBQM001-MZLB001";infiltration=infiltration-1;top_pbqm001=1;end;
if pbqm002>=1 and infiltration>=1 then do; prs_act_qte_=min(pbqm002,infiltration);code="PBQM002-MZLB001";infiltration=infiltration-1;top_pbqm002=1;end;
if pbqm003>=1 and infiltration>=1 then do; prs_act_qte_=min(pbqm003,infiltration);code="PBQM003-MZLB001";infiltration=infiltration-1;top_pbqm003=1;end;
if pbqm004>=1 and infiltration>=1 then do; prs_act_qte_=min(pbqm004,infiltration);code="PBQM004-MZLB001";infiltration=infiltration-1;top_pbqm004=1;end;
if pcqm001>=1 and infiltration>=1 then do; prs_act_qte_=min(pcqm001,infiltration);code="PCQM001-MZLB001";top_pcqm001=1;end;
if (top_pbqm001=1 or top_pbqm002=1 or top_pbqm003=1 or top_pbqm004=1 or top_pcqm001=1) then output;
drop pbqm001 pbqm002 pbqm003 pbqm004 pcqm001 infiltration;
run;

/*Suppression de la table des codes PBQM001, PBQM002, PBQM003, PBQM004, PCQM001, MZLB001 qui repr�sentent des infiltrations sous �cho et ajout dans la table des lignes sp�cifiques d'infiltration sous �cho*/
proc sort data=parcours_pre_op;by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;run;

proc sort data=selec_acte out=selec_acte_ (keep=ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num prs_act_qte_ top_pbqm001 top_pbqm002 top_pbqm003 top_pbqm004 top_pcqm001);by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;run;

data parcours_pre_op;
merge parcours_pre_op (in=a) selec_acte_ (in=b);
by ben_idt_ano exe_soi_dtd finess_exec pfs_exe_num;
if b and code in ('PBQM001','MZLB001') and top_pbqm001=1 then do;top_infiltration_echo=1;nb=prs_act_qte-prs_act_qte_;end;
if b and code in ('PBQM002','MZLB001') and top_pbqm002=1 then do;top_infiltration_echo=1;nb=prs_act_qte-prs_act_qte_;end;
if b and code in ('PBQM003','MZLB001') and top_pbqm003=1 then do;top_infiltration_echo=1;nb=prs_act_qte-prs_act_qte_;end;
if b and code in ('PBQM004','MZLB001') and top_pbqm004=1 then do;top_infiltration_echo=1;nb=prs_act_qte-prs_act_qte_;end;
if b and code in ('PCQM001','MZLB001') and top_pcqm001=1 then do;top_infiltration_echo=1;nb=prs_act_qte-prs_act_qte_;end;
drop top_pbqm001 top_pbqm002 top_pbqm003 top_pbqm004 top_pcqm001;
run;

data parcours_pre_op;
set parcours_pre_op;
if top_infiltration_echo=1 then do;
if nb=0 then delete;
else prs_act_qte=nb;
end;
drop nb top_infiltration_echo prs_act_qte_ ;
run;

data parcours_pre_op;
set  selec_acte (rename=prs_act_qte_=prs_act_qte drop=top_pbqm001 top_pbqm002 top_pbqm003 top_pbqm004 top_pcqm001) parcours_pre_op;
run;

/*Cr�ation d'une table pour pouvoir compter les patients avec actes CCAM d'infiltration avec et sans guidage apr�s l'ajout des corticoides � l'�tape d'apr�s (car priorisation des corticoides sur les actes d'infiltration)*/
data sous_guidage (keep=ben_idt_ano sous_guidage qte_sous_guidage where=(sous_guidage ne '')) sans_guidage (keep=ben_idt_ano sans_guidage qte_sans_guidage where=(sans_guidage ne ''));
set parcours_pre_op;
qte_sous_guidage=0;qte_sans_guidage=0;
if code in ('MZLB001') then do;sans_guidage='sans_guidage';qte_sans_guidage=prs_act_qte;output sans_guidage;end;
else if code in (&infiltration) or code in ('PBQM001-MZLB001') or code in ('PBQM002-MZLB001') or code in ('PBQM003-MZLB001') or code in ('PBQM004-MZLB001') or code in ('PCQM001-MZLB001') then do;sous_guidage='sous_guidage';qte_sous_guidage=prs_act_qte;output sous_guidage;end;
run;

proc sql;
create table sous_guidage as select distinct ben_idt_ano,sous_guidage,sum(qte_sous_guidage) as qte_sous_guidage from sous_guidage group by ben_idt_ano,sous_guidage;
create table sans_guidage as select distinct ben_idt_ano,sans_guidage,sum(qte_sans_guidage) as qte_sans_guidage from sans_guidage group by ben_idt_ano,sans_guidage;
quit;

data selec_act_infilt;
merge sans_guidage sous_guidage;
by ben_idt_ano;
run;

/* 5 - Int�gration des corticoides injectables pour compter les infiltrations: choix de prioriser les corticoides aux actes d'infiltration: On ne compte les actes d'infiltration que si aucun corticoide n'a �t� d�livr� au patient dans les 18 mois avant la chirurgie*/

/* Cr�ation de variables et harmonisation avec les variables pr�c�demment cr��es dans la table de travail*/
data med_dcir;
set med_dcir;
type='DCIR';
cip=compress(put(pha_prs_c13,13.));
/*Cr�ation variable finess ex�cutant */
finess_exec=finessgeo_exec;
if etb_exe_fin ne '' and finessgeo_exec='' then finess_exec=etb_exe_fin;
/*Cr�ation variable dpt ex�cutant*/
format dpt_exec $3.;
if finess_exec ne '' then do;
if substr(finess_exec,1,2) in ('97','98') then dpt_exec=substr(finess_exec,1,2)||substr(finess_exec,4,1);
else dpt_exec=substr(finess_exec,1,2);
end;
if substr(finess_exec,1,4) ='2001' then dpt_exec='2A';
if substr(finess_exec,1,4) ='2002' then dpt_exec='2B';
if finess_exec=' ' or substr(finess_exec,1,2)='99' then do;
if substr(pfs_cod_pos,1,3)  in ('201','200') then dpt_exec='2A'; 
else if substr(pfs_cod_pos,1,3) in ('202','206') then dpt_exec='2B'; 
else if substr(pfs_cod_pos,1,3) = '980' then dpt_exec=substr(pfs_cod_pos,1,3);
else if substr(pfs_cod_pos,1,2) in ('97','98') then dpt_exec=SUBSTR(PFS_COD_POS,1,2) || substr(PFS_EXC_COM,1,1);
else dpt_exec=substr(pfs_cod_pos,1,2);
end;
if dpt_exec in ('981','') then dpt_exec='99';
drop finessgeo_exec etb_exe_fin PFS_COD_POS PFS_EXC_COM pfs_exe_num pha_prs_c13 pha_act_qsn  prs_nat_ref;
run;

/*Cr�ation variable nombre de d�livrance (1 d�livrance de corticoide = d�livrance pour un m�me patient � une m�me date) => On garde une ligne par patient et date de d�livrance: Si plusieurs codes CIP pour un patient � une m�me date: on agr�ge les codes CIP*/
proc sort data=med_dcir nodupkey;by ben_idt_ano exe_soi_dtd cip;run;

data med_dcir;
set med_dcir;
format code $50.;
by ben_idt_ano exe_soi_dtd cip;
retain code ;
if first.exe_soi_dtd then code=cip;
else code=catx('-',code,cip) ;
prs_act_qte=1;
if last.exe_soi_dtd then output;
drop cip;
run;

/* On supprime les patients avec actes d'infiltrations de la table de travail s'ils ont �galement des corticoides injectables => Priorisation des corticoides aux actes d'infiltration*/
proc sql;
create table med as select distinct ben_idt_ano from med_dcir;
quit;

proc sort data=parcours_pre_op;by ben_idt_ano;run;

data parcours_pre_op;
merge parcours_pre_op (in=a) med (in=b);
by ben_idt_ano;
if b and (code in (&infiltration) or code in ('PBQM001-MZLB001') or code in ('PBQM002-MZLB001') or code in ('PBQM003-MZLB001') or code in ('PBQM004-MZLB001') or code in ('PCQM001-MZLB001')) then delete;
else if a then output;
run;

/*On ajoute les corticoides injectables*/
data parcours_pre_op;
set med_dcir parcours_pre_op ;
format code $50.;
run;

proc sql;
create table parcours_pre_op as select distinct ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,prs_nat_ref,pse_spe_cod,SPE_PSP,dpt_exec,sum(prs_act_qte) as prs_act_qte
from parcours_pre_op
group by ben_idt_ano,exe_soi_dtd,date_entree,type,code,finess_exec,prs_nat_ref,pse_spe_cod,SPE_PSP,dpt_exec;
quit;


/*On cr�e des variables suppl�mentaires dans la table pour pouvoir compter les actes CCAM d'infiltration avec et sans guidage*/
proc sort data=parcours_pre_op;by ben_idt_ano exe_soi_dtd code;run;

data parcours_pre_op;
merge parcours_pre_op (in=b) selec_act_infilt (in=a);
by ben_idt_ano;
if b and not a then do;qte_sous_guidage=0;qte_sans_guidage=0;sans_guidage='';sous_guidage='';end;
if a and not first.ben_idt_ano then do;
sans_guidage='';
sous_guidage='';
qte_sous_guidage=0;
qte_sans_guidage=0;
end;
run;

/* 6 - Cr�ation variables groupe de prestation, type de prestation et d�tail prestation*/
data parcours_pre_op;
set parcours_pre_op;

format presta_detail $30. gpe_prestation $50. type_presta $50.;

if prs_nat_ref in (&kine) then do;gpe_prestation='soins_conservateurs';type_presta='Kin�sith�rapie';presta_detail="kine";end;

if code in ('MZLB001') then do;gpe_prestation='soins_conservateurs';type_presta='Infiltration';presta_detail='infiltration_sans_guidage';end;
else if code in (&infiltration) or code in ('PBQM001-MZLB001') or code in ('PBQM002-MZLB001') or code in ('PBQM003-MZLB001') or code in ('PBQM004-MZLB001') or code in ('PCQM001-MZLB001') then do;gpe_prestation='soins_conservateurs';type_presta='Infiltration';presta_detail='infiltration_sous_guidage';end;
if substr(code,1,13) in (&cip13c) then do;gpe_prestation='soins_conservateurs';type_presta='Infiltration';presta_detail='corticoide';end;

if code in (&radio) then do;gpe_prestation='imagerie_demarche_diagnostique';type_presta='Radiographie';presta_detail='radio';end;
if code in (&echo) then do;gpe_prestation='imagerie_demarche_diagnostique';type_presta='Echographie';presta_detail='echo';end;
if code in (&irm) then do;gpe_prestation='imagerie_avancee';type_presta='IRM/Arthroscanner';presta_detail='IRM';end;
if code in ('MEQH001-MZQN001') then do;gpe_prestation='imagerie_avancee';type_presta='IRM/Arthroscanner';presta_detail='arthro_irm';end;
if code in (&arthro_scanner) then do;gpe_prestation='imagerie_avancee';type_presta='IRM/Arthroscanner';presta_detail='arthro_scanner';end;
if code in (&scanner) then do;gpe_prestation='scanner';type_presta='Scanner';presta_detail='scanner';end;

if prs_nat_ref in (&tele_consult, &consult) then do;
if pse_spe_cod in (14) then do;gpe_prestation='consultations';type_presta='C MPR / Rhumatologue';presta_detail='C_rhumato';end;
else if pse_spe_cod in (41) then do;gpe_prestation='consultations';type_presta='C Orthop�diste';presta_detail='C_ortho';end;
else if pse_spe_cod in (31) then do;gpe_prestation='consultations';type_presta='C MPR / Rhumatologue';presta_detail='C_MPR';end;
else if pse_spe_cod in (1,22,23) then do;gpe_prestation='consultations';type_presta='C MG';presta_detail='C_MG';end;
end;

if qte_sans_guidage=. then qte_sans_guidage=0;
if qte_sous_guidage=. then qte_sous_guidage=0;

if presta_detail not in ("kine","corticoide") then spe_psp=.;/*On ne garde l'info sur le prescripteur que pour les  corticoides et la kin�, pas fiable sinon*/

drop prs_nat_ref pse_spe_cod;
run;

proc sql;
create table parcours_pre_op as select distinct BEN_IDT_ANO,TYPE,gpe_prestation,type_presta,PRESTA_DETAIL,CODE,EXE_SOI_DTD,date_entree,DPT_EXEC,FINESS_EXEC,sans_guidage,sous_guidage,SPE_PSP,sum(PRS_ACT_QTE) as prs_act_qte,sum(qte_sans_guidage) as qte_sans_guidage,
sum(qte_sous_guidage) as qte_sous_guidage from parcours_pre_op
group by BEN_IDT_ANO,TYPE,gpe_prestation,type_presta,PRESTA_DETAIL,CODE,EXE_SOI_DTD,date_entree,DPT_EXEC,FINESS_EXEC,sans_guidage,sous_guidage,SPE_PSP;
quit;

/* 6 - Cr�ation variable d�lai entre la chirurgie de l'�paule et la consult ou l'acte + cr�ation de la table de travail finale dans sasdata1 */
data sasdata1.TENR_parcours_pre_op_&per._&anneem;
set parcours_pre_op;
delai_pre_op=datepart(date_entree)-datepart(exe_soi_dtd);
drop date_entree;
run;

proc sort data=sasdata1.TENR_parcours_pre_op_&per._&anneem;by ben_idt_ano exe_soi_dtd code;run;

/******************************************SUPPRESSION DES TABLES CREEES DANS ORAUSER**********************************************/
proc sql;
  drop table orauser.id_patient_suivi;
quit;

